add file udf2.py;
add file udf.py;

drop table kpmg.kevin_sum_event;
create table kpmg.kevin_sum_event
as
select attuid,substr(date,6,2),date,product,total_task,normalized_volume,set
from
(
select TRANSFORM(attuid, to_date(completed),l.product,count(*)
,sum(
case
when l.event='DD'  and l.product='eaAVPN' and l.Ethernet_Switched_Ind='TRUE' and w.PON_Newest_In_Region='TRUE' and l.OCO1_JEP!='I63' Then 0.1
when l.event='DD'  and l.product='eaAVPN' and l.Ethernet_Switched_Ind='TRUE' and w.PON_Newest_In_Region='TRUE' and l.OCO1_JEP='I63' Then 0
when l.event='DD'  and l.product='eaAVPN' and l.Ethernet_Switched_Ind='TRUE' and w.PON_Newest_In_Region='FALSE' and l.OCO1_JEP!='I63' Then 0.2
when l.event='DD'  and l.product='eaAVPN' and l.Ethernet_Switched_Ind='TRUE' and w.PON_Newest_In_Region='FALSE' and l.OCO1_JEP='I63' Then 0.1
when l.event='DD'  and l.product='eaAVPN' and l.Ethernet_Switched_Ind='FALSE' and w.PON_Newest_In_Region='TRUE' and l.OCO1_JEP!='I63' Then 0.2
when l.event='DD'  and l.product='eaAVPN' and l.Ethernet_Switched_Ind='FALSE' and w.PON_Newest_In_Region='TRUE' and l.OCO1_JEP='I63' Then 0.1
when l.event='DD'  and l.product='eaAVPN' and l.Ethernet_Switched_Ind='FALSE' and w.PON_Newest_In_Region='FALSE' and l.OCO1_JEP!='I63' Then 0.2
when l.event='DD'  and l.product='eaAVPN' and l.Ethernet_Switched_Ind='FALSE' and w.PON_Newest_In_Region='FALSE' and l.OCO1_JEP='I63' Then 0.1
when l.event='DD'  and  l.product='EPLS' and l.OCO1_JEP!='I63' Then 0.8
when l.event='DD'  and l.product='EPLS' and l.OCO1_JEP='I63' Then 0.4
else weight
END
)
,collect_list(concat(w.event,'-', w.Ethernet_Switched_Ind,'-',w.Pon_Newest_In_Region))) USING 'python udf.py' as (attuid string,date string, product string, total_task int,normalized_volume double,set map<string,int>)
from kpmg_ws.kevin_raw l
inner join kpmg_ws.SME_Weights w
on l.event=w.event and l.product=w.product and l.Ethernet_Switched_Ind=w.Ethernet_Switched_Ind  and l.Pon_Newest_In_Region_Ind=w.Pon_Newest_In_Region
group by attuid, to_date(completed),l.product
)x;


drop table kpmg.kevin_sum;
create table kpmg.kevin_sum
as
select attuid,substr(date,6,2),date,total_task,normalized_volume,set
from
(
select TRANSFORM(attuid, to_date(completed),count(*)
,sum(
case 
when l.event='DD'  and l.product='eaAVPN' and l.Ethernet_Switched_Ind='TRUE' and w.PON_Newest_In_Region='TRUE' and l.OCO1_JEP!='I63' Then 0.1
when l.event='DD'  and l.product='eaAVPN' and l.Ethernet_Switched_Ind='TRUE' and w.PON_Newest_In_Region='TRUE' and l.OCO1_JEP='I63' Then 0
when l.event='DD'  and l.product='eaAVPN' and l.Ethernet_Switched_Ind='TRUE' and w.PON_Newest_In_Region='FALSE' and l.OCO1_JEP!='I63' Then 0.2
when l.event='DD'  and l.product='eaAVPN' and l.Ethernet_Switched_Ind='TRUE' and w.PON_Newest_In_Region='FALSE' and l.OCO1_JEP='I63' Then 0.1
when l.event='DD'  and l.product='eaAVPN' and l.Ethernet_Switched_Ind='FALSE' and w.PON_Newest_In_Region='TRUE' and l.OCO1_JEP!='I63' Then 0.2
when l.event='DD'  and l.product='eaAVPN' and l.Ethernet_Switched_Ind='FALSE' and w.PON_Newest_In_Region='TRUE' and l.OCO1_JEP='I63' Then 0.1
when l.event='DD'  and l.product='eaAVPN' and l.Ethernet_Switched_Ind='FALSE' and w.PON_Newest_In_Region='FALSE' and l.OCO1_JEP!='I63' Then 0.2
when l.event='DD'  and l.product='eaAVPN' and l.Ethernet_Switched_Ind='FALSE' and w.PON_Newest_In_Region='FALSE' and l.OCO1_JEP='I63' Then 0.1
when l.event='DD'  and  l.product='EPLS' and l.OCO1_JEP!='I63' Then 0.8
when l.event='DD'  and l.product='EPLS' and l.OCO1_JEP='I63' Then 0.4
else weight
END
)
,collect_list(concat(w.event,'-', w.Ethernet_Switched_Ind,'-',w.Pon_Newest_In_Region))) USING 'python udf2.py' as (attuid string,date string,total_task int,normalized_volume double,set map<string,int>)
from kpmg_ws.kevin_raw l
inner join kpmg_ws.SME_Weights w
on l.event=w.event and l.product=w.product and l.Ethernet_Switched_Ind=w.Ethernet_Switched_Ind  and l.Pon_Newest_In_Region_Ind=w.Pon_Newest_In_Region
group by attuid, to_date(completed)
)x;
