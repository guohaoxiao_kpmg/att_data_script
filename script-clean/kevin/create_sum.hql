use kpmg_ws;
drop table kevin_raw;
create table kevin_raw
(
ATTUID String,
Type	String,	
USO	String,	
Event	String,	
Completed	String,	
Circuit_ID	String,	
USO_Circuit_ID	String,	
Main_Order_Type	String,	
Product	String,	
Project_Text	String,	
Speed	String,	
Ethernet_Switched_Ind	String,	
Single_Control_Plane_Ind	String,	
Pon_Newest_In_Region_Ind	String,	
SpreadSheet String
)
ROW format delimited fields terminated by "|"
