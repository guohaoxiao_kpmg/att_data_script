use kpmg_ws;
drop table lssd_completed;
drop table hssd_completed;
drop table mis_completed;
drop table bvoip_completed;

drop table lssd_issued;
drop table hssd_issued;
drop table mis_issued;
drop table bvoip_issued;


drop table mis_uso_uid_lookup;
create table mis_uso_uid_lookup
as

select uso,project_id,Order_Creator,ORDER_MANAGER,w1.attuid as order_creator_attuid,w2.attuid as order_manager_attuid
from mis_uso_lookup l
left join my_webphone w1
on upper(Order_Creator)=upper(concat(w1.last_name,", ",w1.first_name))
left join my_webphone w2
on upper(Order_manager)=upper(concat(w2.last_name,", ",w2.first_name))

;

create table lssd_completed 
as
select x.*,if(USO_Originator_HRID is not null,USO_Originator_HRID,c.uid) as uid from 
(
select * from lssd_completed_ytd
union all
select * from lssd_completed_2015
)x
left join lookup_lssd_completed c
 on x.ncon=c.ncon and row=1;


create table hssd_completed
as
select x.*,if(USO_Originator_HRID is not null,USO_Originator_HRID,c.uid) as uid  from
(
select * from hssd_completed_ytd
union all
select * from hssd_completed_2015
)x
left join lookup_hssd_completed c
 on x.ncon=c.ncon and row=1;



create table lssd_issued
as
select x.*,if(USO_Originator_HRID is not null,USO_Originator_HRID,c.uid) as uid  from
(
select * from lssd_issued_ytd
union all
select * from lssd_issued_2015
)x
left join lookup_lssd_issued c
 on x.ncon=c.ncon and row=1;


create table hssd_issued
as
select x.*,if(USO_Originator_HRID is not null,USO_Originator_HRID,c.uid) as uid  from
(
select * from hssd_issued_ytd
union all
select * from hssd_issued_2015
)x
left join lookup_hssd_issued c
 on x.ncon=c.ncon and row=1;

create table mis_completed
as
select b.*,if(l.OM_ATTUID is null,if(u.order_creator_attuid is null,u1.order_creator_attuid,u.order_creator_attuid),l.OM_ATTUID ) as OM_ATTUID,if(l.IPManager_ATTUID is null,if(u.order_manager_attuid is null,u1.order_manager_attuid ,u.order_manager_attuid ),l.IPManager_ATTUID ) as IPManager_ATTUID
from
(
select * from mis_completed_ytd
union all
select * from mis_completed_2015
)b
left join mis_lookup l
on l.USRP_Order_ID=b.USRP_Order_ID
left join  mis_uso_uid_lookup u
on trim(upper(b.uso_number))=trim(upper(u.uso)) 

left join  mis_uso_uid_lookup u1
on  trim(upper(b.USRP_Order_ID))=trim(upper(u1.project_id))

;


select count(*),count(u.uso),count(u1.project_id)

from
(
select * from mis_completed_ytd
union all
select * from mis_completed_2015
)b
left join mis_lookup l
on l.USRP_Order_ID=b.USRP_Order_ID
left join  mis_uso_uid_lookup u
on trim(upper(b.uso_number))=trim(upper(u.uso))

left join  mis_uso_uid_lookup u1
on  trim(upper(b.USRP_Order_ID))=trim(upper(u1.project_id))
;



create table mis_issued
as
select b.*,if(l.OM_ATTUID is null,if(u.order_creator_attuid is null,u1.order_creator_attuid,u.order_creator_attuid),l.OM_ATTUID ) as OM_ATTUID,if(l.IPManager_ATTUID is null,if(u.order_manager_attuid is null,u1.order_manager_attuid ,u.order_manager_attuid ),l.IPManager_ATTUID ) as IPManager_ATTUID 
 from
(
select * from mis_issued_ytd
union all
select * from mis_issued_2015
)b
left join mis_lookup l
on l.USRP_Order_ID=b.USRP_Order_ID
left join  mis_uso_uid_lookup u
on trim(upper(b.uso_number))=trim(upper(u.uso)) 
left join  mis_uso_uid_lookup u1
on  trim(upper(b.USRP_Order_ID))=trim(upper(u1.project_id))


;



create table bvoip_completed
as
select b.*,l.OM_ATTUID,l.IPManager_ATTUID from
(
select * from bvoip_completed_ytd
union all
select * from bvoip_completed_2015
)b
left join bvoip_lookup l
on l.USRP_Order_ID=b.USRP_Order_ID
;

create table bvoip_issued
as
select b.*,l.OM_ATTUID,l.IPManager_ATTUID from
(
select * from bvoip_issued_ytd
union all
select * from bvoip_issued_2015
)b
left join bvoip_lookup l
on l.USRP_Order_ID=b.USRP_Order_ID
;




