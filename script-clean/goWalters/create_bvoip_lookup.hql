use kpmg_ws;
drop table bvoip_lookup;
create table bvoip_lookup(
USRP_Order_ID string,
Complete_Date string,
type string,
speed string,
ACT string,
AVPN_Ind string,
Transport_Type string,
Product_Tracker string,
IPManager_ATTUID string,
OM_ATTUID string)
ROW format delimited fields terminated by "|";

