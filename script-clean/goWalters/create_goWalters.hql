use kpmg_ws;
drop table gowalters_duplicated;
create table gowalters_duplicated as 
select uso as OrderIdentifier,
main_order_type as ServiceType,
null as order_issued_date,
DD_Comp as Order_Completion_Date,
if(product in ('Private Line','ATM','Frame_Relay','Private_Line','IP'),'Legacy',product) as product,
product as dmsproduct,
uid as order_creator,
uid as order_manager,
speed_grouping as speed_grouping,
"lssd_completed" as tag
from 
lssd_completed

union all

select uso as OrderIdentifier,
main_order_type as ServiceType,
aadate  as order_issued_date,
null as Order_Completion_Date,
if(product in ('Private Line','ATM','Frame_Relay','Private_Line','IP'),'Legacy',product) as product,
product as dmsproduct,
uid as order_creator,
uid as order_manager,

speed_grouping as speed_grouping,
"lssd_issued" as tag
from
lssd_issued


union all

select uso as OrderIdentifier,
main_order_type as ServiceType,
null as order_issued_date,
DD_Comp as Order_Completion_Date,
if(product in ('Private Line','ATM','Frame_Relay','Private_Line','IP'),'Legacy',product) as product,
product as dmsproduct,
uid as order_creator,
uid as order_manager,

speed_grouping as speed_grouping,
"hssd_completed" as tag
from
hssd_completed

union all

select uso as OrderIdentifier,
main_order_type as ServiceType,
aadate as order_issued_date,
null as Order_Completion_Date,
if(product in ('Private Line','ATM','Frame_Relay','Private_Line','IP'),'Legacy',product) as product,
product as dmsproduct,
uid as order_creator,
uid as order_manager,

speed_grouping as speed_grouping,
"hssd_issued" as tag
from
hssd_issued

union all

select usrp_order_id as OrderIdentifier,
if(act='N','New',if(act='C','Change',if(act='D','Disco','Other'))) as ServiceType,
null as order_issued_date,
issued_date as Order_Completion_Date,
if(usrp_product_name in ('Private Line','ATM','Frame_Relay','Private_Line','IP'),'Legacy',if(upper(speed_grouping)='ETH' or upper(speed_grouping)='ETHERNET','MIS-ETH','MIS-TDM')) as product,
usrp_product_name as dmsproduct,
om_attuid as order_creator,
ipmanager_attuid as order_manager,
speed_grouping as speed_grouping,
"mis_completed" as tag
from
mis_completed

union all

select usrp_order_id as OrderIdentifier,
if(act='N','New',if(act='C','Change',if(act='D','Disco','Other'))) as ServiceType,
issued_date as order_issued_date,
null as Order_Completion_Date,
if(usrp_product_name in ('Private Line','ATM','Frame_Relay','Private_Line','IP'),'Legacy',if(upper(speed_grouping)='ETH' or upper(speed_grouping)='ETHERNET','MIS-ETH','MIS-TDM')) as product,
usrp_product_name as dmsproduct,
om_attuid as order_creator,
ipmanager_attuid as order_manager,

speed_grouping as speed_grouping,
"mis_issued" as tag
from
mis_issued

union all

select usrp_order_id as OrderIdentifier,
if(act='N','New',if(act='C','Change',if(act='D','Disco','Other'))) as ServiceType,
null as order_issued_date,
complete_date as Order_Completion_Date,
if(product_name in ('Private Line','ATM','Frame_Relay','Private_Line','IP'),'Legacy',if(upper(speed)='ETH' or upper(speed)='ETHERNET','BVOIP-ETH','BVOIP-TDM')) as product,
product_name as dmsproduct,
om_attuid as order_creator,
ipmanager_attuid as order_manager,

speed as speed_grouping,
"bvoip_completed" as tag
from
bvoip_completed

union all

select usrp_order_id as OrderIdentifier,
if(act='N','New',if(act='C','Change',if(act='D','Disco','Other'))) as ServiceType,
issued_date as order_issued_date,
null as Order_Completion_Date,
if(product_name in ('Private Line','ATM','Frame_Relay','Private_Line','IP'),'Legacy',if(upper(speed)='ETH' or upper(speed)='ETHERNET','BVOIP-ETH','BVOIP-TDM')) as product,
product_name as dmsproduct,
om_attuid as order_creator,
ipmanager_attuid as order_manager,

speed as speed_grouping,
"bvoip_issued" as tag
from
bvoip_issued
;

use kpmg_ws;
drop table gowalters;
create table kpmg_ws.gowalters
as
select OrderIdentifier,ServiceType,order_issued_date,Order_Completion_Date,product,dmsproduct,order_creator,order_manager,speed_grouping,tag from
(
select * , row_number() over( partition by OrderIdentifier,order_issued_date,Order_Completion_Date,dmsproduct,tag,speed_grouping order by product) as row
from kpmg_ws.gowalters_duplicated
)x
where row =1;


