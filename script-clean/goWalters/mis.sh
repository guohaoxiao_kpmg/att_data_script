
hive -e "
use kpmg_ws
;drop table  mis_Issued_ytd;
 create external table MIS_Issued_ytd(
usrp_order_id   String,
speed_grouping  String,
usrp_service_type       String,
customer_name   String,
uso_number      String,
circuit_id      String,
usrp_circuit_speed      String,
issued_date     String,
usrp_product_name String,
usrp_order_type String,
act String
)
;

use kpmg_ws
;drop table  MIS_Completed_ytd;
 create external table MIS_Completed_ytd(
usrp_order_id   String,
speed_grouping  String,
customer_name   String,
uso_number      String,
circuit_id      String,
usrp_circuit_speed      String,
issued_date     String,
usrp_product_name String,
usrp_order_type String,
act String
)
;

"


hadoop dfs -rmr /sandbox/sandbox31/kpmg_ws/mis_*_ytd

sqoop  import \
-libjars "/usr/hdp/2.2.8.0-3150/sqoop/lib/jtds-1.3.1.jar"                                                                        \
--driver          "net.sourceforge.jtds.jdbc.Driver"                                                                                          \
--connect         "jdbc:jtds:sqlserver://MOSTLS1ABSPMD06.itservices.sbc.com;instance=PD_MANDMX01;databaseName=views_user;domain=itservices"   \
--username        jl7392                                                                                                                      \
--password        '@tlanta1050'                                                                                                               \
--as-textfile \
--target-dir '/sandbox/sandbox31/kpmg_ws/mis_completed_ytd' \
-m 64 \
--split-by usrp_order_id \
--query 'select
usrp_order_id,speed_grouping,customer_name,uso_number,circuit_id,usrp_circuit_speed,issued_date,usrp_product_name,usrp_order_type,act
from  views_user..vw_Report_MIS_Details_OrdersCompleted_Base_Tab_YTD  where $CONDITIONS' \
--null-string '\\N' \
--null-non-string '\\N' \
--fields-terminated-by '\001' \
--hive-drop-import-delims ;



sqoop  import \
-libjars "/usr/hdp/2.2.8.0-3150/sqoop/lib/jtds-1.3.1.jar"                                                                        \
--driver          "net.sourceforge.jtds.jdbc.Driver"                                                                                          \
--connect         "jdbc:jtds:sqlserver://MOSTLS1ABSPMD06.itservices.sbc.com;instance=PD_MANDMX01;databaseName=views_user;domain=itservices"   \
--username        jl7392                                                                                                                      \
--password        '@tlanta1050'                                                                                                               \
--as-textfile \
--target-dir '/sandbox/sandbox31/kpmg_ws/mis_issued_ytd' \
-m 64 \
--split-by usrp_order_id \
--query 'select
usrp_order_id,speed_grouping,usrp_service_type,customer_name,uso_number,circuit_id,usrp_circuit_speed,issued_date,usrp_product_name,usrp_order_type,act
from  views_user..vw_Report_MIS_Details_OrdersIssued_Base_Tab_YTD  where $CONDITIONS' \
--null-string '\\N' \
--null-non-string '\\N' \
--fields-terminated-by '\001' \
--hive-drop-import-delims ;


