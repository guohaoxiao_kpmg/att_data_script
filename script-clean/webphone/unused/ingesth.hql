use kpmg_ws;

drop table kpmg_ws.webphone;
CREATE EXTERNAL TABLE `kpmg_ws.webphone`(
  `country_code`            string,
  `dept_id`                 string,
  `dept_name`               string,
  `business_group_id`       string,
  `business_group_name`     string,
  `business_subgroup`       string,
  `business_subgroup_name`  string,
  `business_unit`           string,
  `business_unit_name`      string,
  `vp_org`                  string,
  `vp_org_name`             string,
  `cel`                     string,
  `city`                    string,
  `country_desc`            string,
  `email`                   string,
  `fax_tel_number`          string,
  `phone_extension`         string,
  `first_name`              string,
  `handle`                  string,
  `hrid`                    string,
  `job_title_code`          string,
  `jt_name`                 string,
  `last_name`               string,
  `location_code`           string,
  `middle_name`             string,
  `costcenter`              string,
  `pager`                   string,
  `pager_pin`               string,
  `preferred_name`          string,
  `prefix_name`             string,
  `room_number`             string,
  `state`                   string,
  `status`                  string,
  `address`                 string,
  `name_suffix`             string,
  `telephone_number`        string,
  `level`                   string,
  `title_name`              string,
  `zip`                     string,
  `supervisor_hrid`         string,
  `a1_sbscuid_attuid`       string,
  `cogs`                    string,
  `silo`                    string,
  `direct_total_reports`    string,
  `bellsouth_uid`           string,
  `cingular_cuid`           string,
  `financial_loc_code`      string,
  `email_alias`             string,
  `payroll_id`              string,
  `consulting_company`      string,
  `management_level_indicator` string,
  `occupancy_indicator`     string,
  `primary_gtr`             string,
  `secondary_gtr`           string,
  `cpid_record_indicator`   string,
  `geographic_location_code` string,
  `architechtural_object_id` string,
  `company_code`             string,
  `company_description`      string,
  `consultant_flag`          string,
  `preferred_mailing_address` string
)
ROW FORMAT DELIMITED
  FIELDS TERMINATED BY '|'
STORED AS INPUTFORMAT
  'org.apache.hadoop.mapred.TextInputFormat'
OUTPUTFORMAT
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  'hdfs://ylpd267.kmdc.att.com:8020/sandbox/sandbox31/stg/webphone'
;


drop table temp_webphone;
create table temp_webphone
as
select UPPER(handle) AS ATTUID
      ,CONCAT(UPPER(FIRST_NAME),' ',UPPER(MIDDLE_NAME),' ',UPPER(LAST_NAME))   AS NAME
      ,UPPER(FIRST_NAME)                                                   AS FIRST_NAME
      ,UPPER(MIDDLE_NAME)                                                  AS MIDDLE_NAME
      ,UPPER(LAST_NAME)                                                    AS LAST_NAME
      ,UPPER(DEPT_NAME)                                                    AS DEPARTMENT
      ,UPPER(CITY)                                                         AS CITY
      ,UPPER(STATE)                                                        AS STATE
      ,UPPER(COUNTRY_CODE)                                                 AS COUNTRY_CODE
      ,UPPER(COUNTRY_DESC)                                                 AS COUNTRY_DESC
      ,UPPER(JOB_TITLE_CODE)                                               AS JOB_TITLE_CODE
      ,UPPER(JT_NAME)                                                      AS JOB_TITLE_NAME
      ,UPPER(LEVEL)                                                        AS LEVEL
      ,UPPER(STATUS)                                                       AS STATUS
      ,UPPER(SUPERVISOR_HRID)                                              AS SUPERVISOR_ATTUID
      ,UPPER(CONSULTING_COMPANY)                                           AS CONSULTING_COMPANY
      ,UPPER(CONSULTANT_FLAG)                                              AS CONSULTANT_FLAG
      ,UPPER(COMPANY_CODE)                                                 AS COMPANY_CODE
      ,UPPER(COMPANY_DESCRIPTION)                                          AS COMPANY_DESCRIPTION
FROM KPMG_ws.webphone
;
     
DROP TABLE if exists idesk_org1;

CREATE TABLE idesk_org1
AS
SELECT A.*
      ,B.ATTUID              AS L1_REPORT_TO_ATTUID
      ,B.NAME                AS L1_REPORT_TO_NAME
      ,B.LEVEL               AS L1_REPORT_TO_LEVEL
      ,B.JOB_TITLE_CODE      AS L1_REPORT_TO_JOB_TITLE
      ,B.SUPERVISOR_ATTUID   AS L2_REPORT_TO_ATTUID
FROM temp_webphone      A
  LEFT OUTER JOIN
     temp_webphone      B
   ON A.SUPERVISOR_ATTUID = B.ATTUID
;


DROP TABLE if exists idesk_org2;

CREATE TABLE idesk_org2
AS
SELECT A.*
      ,B.NAME                AS L2_REPORT_TO_NAME
      ,B.LEVEL               AS L2_REPORT_TO_LEVEL
      ,B.JOB_TITLE_CODE      AS L2_REPORT_TO_JOB_TITLE
      ,B.SUPERVISOR_ATTUID   AS L3_REPORT_TO_ATTUID
FROM idesk_org1       A
  LEFT OUTER JOIN
     temp_webphone      B
   ON A.L2_REPORT_TO_ATTUID = B.ATTUID
;

DROP TABLE if exists idesk_org3;

CREATE TABLE idesk_org3
AS
SELECT A.*
      ,B.NAME                AS L3_REPORT_TO_NAME
      ,B.LEVEL               AS L3_REPORT_TO_LEVEL
      ,B.JOB_TITLE_CODE      AS L3_REPORT_TO_JOB_TITLE
      ,B.SUPERVISOR_ATTUID   AS L4_REPORT_TO_ATTUID
FROM idesk_org2       A
  LEFT OUTER JOIN
     temp_webphone      B
   ON A.L3_REPORT_TO_ATTUID = B.ATTUID
;


DROP TABLE if exists idesk_org4;

CREATE TABLE idesk_org4
AS
SELECT A.*
      ,B.NAME                AS L4_REPORT_TO_NAME
      ,B.LEVEL               AS L4_REPORT_TO_LEVEL
      ,B.JOB_TITLE_CODE      AS L4_REPORT_TO_JOB_TITLE
      ,B.SUPERVISOR_ATTUID   AS L5_REPORT_TO_ATTUID
FROM idesk_org3       A
  LEFT OUTER JOIN
     temp_webphone      B
   ON A.L4_REPORT_TO_ATTUID = B.ATTUID
;

DROP TABLE if exists idesk_org5;

CREATE TABLE idesk_org5
AS
SELECT A.*
      ,B.NAME                AS L5_REPORT_TO_NAME
      ,B.LEVEL               AS L5_REPORT_TO_LEVEL
      ,B.JOB_TITLE_CODE      AS L5_REPORT_TO_JOB_TITLE
      ,B.SUPERVISOR_ATTUID   AS L6_REPORT_TO_ATTUID
FROM idesk_org4       A
  LEFT OUTER JOIN
     temp_webphone      B
   ON A.L5_REPORT_TO_ATTUID = B.ATTUID
;

DROP TABLE if exists idesk_org6;

CREATE TABLE idesk_org6
AS
SELECT A.*
      ,B.NAME                AS L6_REPORT_TO_NAME
      ,B.LEVEL               AS L6_REPORT_TO_LEVEL
      ,B.JOB_TITLE_CODE      AS L6_REPORT_TO_JOB_TITLE
      ,B.SUPERVISOR_ATTUID   AS L7_REPORT_TO_ATTUID
FROM idesk_org5       A
  LEFT OUTER JOIN
     temp_webphone      B
   ON A.L6_REPORT_TO_ATTUID = B.ATTUID
;

DROP TABLE if exists idesk_org7;

CREATE TABLE idesk_org7
AS
SELECT A.*
      ,B.NAME                AS L7_REPORT_TO_NAME
      ,B.LEVEL               AS L7_REPORT_TO_LEVEL
      ,B.JOB_TITLE_CODE      AS L7_REPORT_TO_JOB_TITLE
      ,B.SUPERVISOR_ATTUID   AS L8_REPORT_TO_ATTUID
FROM idesk_org6       A
  LEFT OUTER JOIN
     temp_webphone      B
   ON A.L7_REPORT_TO_ATTUID = B.ATTUID
;

DROP TABLE if exists idesk_org8;

CREATE TABLE idesk_org8
AS
SELECT A.*
      ,B.NAME                AS L8_REPORT_TO_NAME
      ,B.LEVEL               AS L8_REPORT_TO_LEVEL
      ,B.JOB_TITLE_CODE      AS L8_REPORT_TO_JOB_TITLE
      ,B.SUPERVISOR_ATTUID   AS L9_REPORT_TO_ATTUID
FROM idesk_org7       A
  LEFT OUTER JOIN
     temp_webphone      B
   ON A.L8_REPORT_TO_ATTUID = B.ATTUID
;

-- CHECK IF WE HAVE REACH THE END: RS2982 as supervisor or no more supervisor
SELECT L9_REPORT_TO_ATTUID, COUNT(*)
FROM idesk_org8
group by L9_REPORT_TO_ATTUID
ORDER BY L9_REPORT_TO_ATTUID
;
-- NOT YET

DROP TABLE if exists idesk_org9;

CREATE TABLE idesk_org9
AS
SELECT A.*
      ,B.NAME                AS L9_REPORT_TO_NAME
      ,B.LEVEL               AS L9_REPORT_TO_LEVEL
      ,B.JOB_TITLE_CODE      AS L9_REPORT_TO_JOB_TITLE
      ,B.SUPERVISOR_ATTUID   AS L10_REPORT_TO_ATTUID
FROM idesk_org8       A
  LEFT OUTER JOIN
     temp_webphone      B
   ON A.L9_REPORT_TO_ATTUID = B.ATTUID
;

SELECT L10_REPORT_TO_ATTUID, COUNT(*)
FROM idesk_org9
group by L10_REPORT_TO_ATTUID
ORDER BY L10_REPORT_TO_ATTUID
;

-- Not Yet




DROP TABLE if exists idesk_org10;

CREATE TABLE idesk_org10
AS
SELECT A.*
      ,B.NAME                AS L10_REPORT_TO_NAME
      ,B.LEVEL               AS L10_REPORT_TO_LEVEL
      ,B.JOB_TITLE_CODE      AS L10_REPORT_TO_JOB_TITLE
      ,B.SUPERVISOR_ATTUID   AS L11_REPORT_TO_ATTUID
FROM idesk_org9       A
  LEFT OUTER JOIN
     temp_webphone      B
   ON A.L10_REPORT_TO_ATTUID = B.ATTUID
;

SELECT L11_REPORT_TO_ATTUID, COUNT(*)
FROM idesk_org10
group by L11_REPORT_TO_ATTUID
ORDER BY L11_REPORT_TO_ATTUID
;

-- Not Yet


DROP TABLE if exists idesk_org11;

CREATE TABLE idesk_org11
AS
SELECT A.*
      ,B.NAME                AS L11_REPORT_TO_NAME
      ,B.LEVEL               AS L11_REPORT_TO_LEVEL
      ,B.JOB_TITLE_CODE      AS L11_REPORT_TO_JOB_TITLE
      ,B.SUPERVISOR_ATTUID   AS L12_REPORT_TO_ATTUID
FROM idesk_org10       A
  LEFT OUTER JOIN
     temp_webphone      B
   ON A.L11_REPORT_TO_ATTUID = B.ATTUID
;

SELECT L12_REPORT_TO_ATTUID, COUNT(*)
FROM idesk_org11
group by L12_REPORT_TO_ATTUID
ORDER BY L12_REPORT_TO_ATTUID
;

DROP TABLE if exists idesk_org12;

CREATE TABLE idesk_org12
AS
SELECT A.*
      ,B.NAME                AS L12_REPORT_TO_NAME
      ,B.LEVEL               AS L12_REPORT_TO_LEVEL
      ,B.JOB_TITLE_CODE      AS L12_REPORT_TO_JOB_TITLE
      ,B.SUPERVISOR_ATTUID   AS L13_REPORT_TO_ATTUID
FROM idesk_org11       A
  LEFT OUTER JOIN
     temp_webphone      B
   ON A.L12_REPORT_TO_ATTUID = B.ATTUID
;

SELECT L13_REPORT_TO_ATTUID, COUNT(*)
FROM idesk_org12
group by L13_REPORT_TO_ATTUID
ORDER BY L13_REPORT_TO_ATTUID
;


DROP TABLE if exists idesk_org13;

CREATE TABLE idesk_org13
AS
SELECT A.*
      ,B.NAME                AS L13_REPORT_TO_NAME
      ,B.LEVEL               AS L13_REPORT_TO_LEVEL
      ,B.JOB_TITLE_CODE      AS L13_REPORT_TO_JOB_TITLE
      ,B.SUPERVISOR_ATTUID   AS L14_REPORT_TO_ATTUID
FROM idesk_org12       A
  LEFT OUTER JOIN
     temp_webphone      B
   ON A.L13_REPORT_TO_ATTUID = B.ATTUID
;

SELECT L14_REPORT_TO_ATTUID, COUNT(*)
FROM idesk_org13
group by L14_REPORT_TO_ATTUID
ORDER BY L14_REPORT_TO_ATTUID
;


DROP TABLE if exists idesk_org14;

CREATE TABLE idesk_org14
AS
SELECT A.*
      ,B.NAME                AS L14_REPORT_TO_NAME
      ,B.LEVEL               AS L14_REPORT_TO_LEVEL
      ,B.JOB_TITLE_CODE      AS L14_REPORT_TO_JOB_TITLE
      ,B.SUPERVISOR_ATTUID   AS L15_REPORT_TO_ATTUID
FROM idesk_org13       A
  LEFT OUTER JOIN
     temp_webphone      B
   ON A.L14_REPORT_TO_ATTUID = B.ATTUID
;

SELECT L15_REPORT_TO_ATTUID, COUNT(*)
FROM idesk_org14
group by L15_REPORT_TO_ATTUID
ORDER BY L15_REPORT_TO_ATTUID
;




-- in OS
-- hadoop dfs -chmod -R 775 /sandbox/sandbox31
 
------ GCS is lead by xw2829  Williams, XAVIER D -- EVP-GLOBAL CUSTOMER SERVICE
------ Check the showing in the org-chart

SELECT COUNT(*)   TOTAL_COUNT
      ,SUM(CASE WHEN L13_REPORT_TO_ATTUID = 'XW2829' THEN 1 ELSE 0 END)    L13_COUNT
      ,SUM(CASE WHEN L12_REPORT_TO_ATTUID = 'XW2829' THEN 1 ELSE 0 END)    L12_COUNT
      ,SUM(CASE WHEN L11_REPORT_TO_ATTUID = 'XW2829' THEN 1 ELSE 0 END)    L11_COUNT
      ,SUM(CASE WHEN L10_REPORT_TO_ATTUID = 'XW2829' THEN 1 ELSE 0 END)    L10_COUNT
      ,SUM(CASE WHEN L9_REPORT_TO_ATTUID  = 'XW2829' THEN 1 ELSE 0 END)    L9_COUNT
      ,SUM(CASE WHEN L8_REPORT_TO_ATTUID  = 'XW2829' THEN 1 ELSE 0 END)    L8_COUNT
      ,SUM(CASE WHEN L7_REPORT_TO_ATTUID  = 'XW2829' THEN 1 ELSE 0 END)    L7_COUNT
      ,SUM(CASE WHEN L6_REPORT_TO_ATTUID  = 'XW2829' THEN 1 ELSE 0 END)    L6_COUNT
      ,SUM(CASE WHEN L5_REPORT_TO_ATTUID  = 'XW2829' THEN 1 ELSE 0 END)    L5_COUNT
      ,SUM(CASE WHEN L4_REPORT_TO_ATTUID  = 'XW2829' THEN 1 ELSE 0 END)    L4_COUNT
      ,SUM(CASE WHEN L3_REPORT_TO_ATTUID  = 'XW2829' THEN 1 ELSE 0 END)    L3_COUNT
      ,SUM(CASE WHEN L2_REPORT_TO_ATTUID  = 'XW2829' THEN 1 ELSE 0 END)    L2_COUNT
      ,SUM(CASE WHEN L1_REPORT_TO_ATTUID  = 'XW2829' THEN 1 ELSE 0 END)    L1_COUNT
FROM idesk_org14
WHERE L13_REPORT_TO_ATTUID = 'XW2829'
   OR L12_REPORT_TO_ATTUID = 'XW2829'
   OR L11_REPORT_TO_ATTUID = 'XW2829'
   OR L10_REPORT_TO_ATTUID = 'XW2829'
   OR L9_REPORT_TO_ATTUID = 'XW2829'
   OR L8_REPORT_TO_ATTUID = 'XW2829'
   OR L7_REPORT_TO_ATTUID = 'XW2829'
   OR L6_REPORT_TO_ATTUID = 'XW2829'
   OR L5_REPORT_TO_ATTUID = 'XW2829'
   OR L4_REPORT_TO_ATTUID = 'XW2829'
   OR L3_REPORT_TO_ATTUID = 'XW2829'
   OR L2_REPORT_TO_ATTUID = 'XW2829'
   OR L1_REPORT_TO_ATTUID = 'XW2829'
;
-- 27663   0       0       0       0       147     688     8634    10760   5880    1268    236     43      7


------ CREATE USER_ORG in KPMG;
USE KPMG;

DROP TABLE IF EXISTS USER_ORG;

CREATE TABLE USER_ORG
AS
SELECT ATTUID
      ,NAME
      ,DEPARTMENT
      ,CITY
      ,STATE
      ,COUNTRY_CODE AS COUNTRY
      ,LEVEL
      ,CASE WHEN L13_REPORT_TO_ATTUID = 'XW2829' THEN L12_REPORT_TO_NAME
            WHEN L12_REPORT_TO_ATTUID = 'XW2829' THEN L11_REPORT_TO_NAME
            WHEN L11_REPORT_TO_ATTUID = 'XW2829' THEN L10_REPORT_TO_NAME
            WHEN L10_REPORT_TO_ATTUID = 'XW2829' THEN L9_REPORT_TO_NAME
            WHEN L9_REPORT_TO_ATTUID  = 'XW2829' THEN L8_REPORT_TO_NAME
            WHEN L8_REPORT_TO_ATTUID  = 'XW2829' THEN L7_REPORT_TO_NAME
            WHEN L7_REPORT_TO_ATTUID  = 'XW2829' THEN L6_REPORT_TO_NAME
            WHEN L6_REPORT_TO_ATTUID  = 'XW2829' THEN L5_REPORT_TO_NAME
            WHEN L5_REPORT_TO_ATTUID  = 'XW2829' THEN L4_REPORT_TO_NAME
            WHEN L4_REPORT_TO_ATTUID  = 'XW2829' THEN L3_REPORT_TO_NAME
            WHEN L3_REPORT_TO_ATTUID  = 'XW2829' THEN L2_REPORT_TO_NAME
            WHEN L2_REPORT_TO_ATTUID  = 'XW2829' THEN L1_REPORT_TO_NAME
            ELSE ''
       END        AS SVP_NAME
      ,CASE WHEN L13_REPORT_TO_ATTUID = 'XW2829' THEN L11_REPORT_TO_NAME
            WHEN L12_REPORT_TO_ATTUID = 'XW2829' THEN L10_REPORT_TO_NAME
            WHEN L11_REPORT_TO_ATTUID = 'XW2829' THEN L9_REPORT_TO_NAME 
            WHEN L10_REPORT_TO_ATTUID = 'XW2829' THEN L8_REPORT_TO_NAME 
            WHEN L9_REPORT_TO_ATTUID  = 'XW2829' THEN L7_REPORT_TO_NAME 
            WHEN L8_REPORT_TO_ATTUID  = 'XW2829' THEN L6_REPORT_TO_NAME 
            WHEN L7_REPORT_TO_ATTUID  = 'XW2829' THEN L5_REPORT_TO_NAME 
            WHEN L6_REPORT_TO_ATTUID  = 'XW2829' THEN L4_REPORT_TO_NAME 
            WHEN L5_REPORT_TO_ATTUID  = 'XW2829' THEN L3_REPORT_TO_NAME 
            WHEN L4_REPORT_TO_ATTUID  = 'XW2829' THEN L2_REPORT_TO_NAME 
            WHEN L3_REPORT_TO_ATTUID  = 'XW2829' THEN L1_REPORT_TO_NAME 
            ELSE ''
       END        AS VP_NAME
      ,CASE WHEN L13_REPORT_TO_ATTUID = 'XW2829' THEN L10_REPORT_TO_NAME
            WHEN L12_REPORT_TO_ATTUID = 'XW2829' THEN L9_REPORT_TO_NAME 
            WHEN L11_REPORT_TO_ATTUID = 'XW2829' THEN L8_REPORT_TO_NAME 
            WHEN L10_REPORT_TO_ATTUID = 'XW2829' THEN L7_REPORT_TO_NAME 
            WHEN L9_REPORT_TO_ATTUID  = 'XW2829' THEN L6_REPORT_TO_NAME 
            WHEN L8_REPORT_TO_ATTUID  = 'XW2829' THEN L5_REPORT_TO_NAME 
            WHEN L7_REPORT_TO_ATTUID  = 'XW2829' THEN L4_REPORT_TO_NAME 
            WHEN L6_REPORT_TO_ATTUID  = 'XW2829' THEN L3_REPORT_TO_NAME 
            WHEN L5_REPORT_TO_ATTUID  = 'XW2829' THEN L2_REPORT_TO_NAME 
            WHEN L4_REPORT_TO_ATTUID  = 'XW2829' THEN L1_REPORT_TO_NAME 
            ELSE ''
       END        AS AVP_NAME
      ,CASE WHEN L13_REPORT_TO_ATTUID = 'XW2829' THEN L9_REPORT_TO_NAME
            WHEN L12_REPORT_TO_ATTUID = 'XW2829' THEN L8_REPORT_TO_NAME
            WHEN L11_REPORT_TO_ATTUID = 'XW2829' THEN L7_REPORT_TO_NAME
            WHEN L10_REPORT_TO_ATTUID = 'XW2829' THEN L6_REPORT_TO_NAME
            WHEN L9_REPORT_TO_ATTUID  = 'XW2829' THEN L5_REPORT_TO_NAME
            WHEN L8_REPORT_TO_ATTUID  = 'XW2829' THEN L4_REPORT_TO_NAME
            WHEN L7_REPORT_TO_ATTUID  = 'XW2829' THEN L3_REPORT_TO_NAME
            WHEN L6_REPORT_TO_ATTUID  = 'XW2829' THEN L2_REPORT_TO_NAME
            WHEN L5_REPORT_TO_ATTUID  = 'XW2829' THEN L1_REPORT_TO_NAME
            ELSE ''
       END        AS DIRECTOR_NAME       
FROM kpmg_ws.idesk_org14
WHERE L13_REPORT_TO_ATTUID = 'XW2829'
   OR L12_REPORT_TO_ATTUID = 'XW2829'
   OR L11_REPORT_TO_ATTUID = 'XW2829'
   OR L10_REPORT_TO_ATTUID = 'XW2829'
   OR L9_REPORT_TO_ATTUID = 'XW2829'
   OR L8_REPORT_TO_ATTUID = 'XW2829'
   OR L7_REPORT_TO_ATTUID = 'XW2829'
   OR L6_REPORT_TO_ATTUID = 'XW2829'
   OR L5_REPORT_TO_ATTUID = 'XW2829'
   OR L4_REPORT_TO_ATTUID = 'XW2829'
   OR L3_REPORT_TO_ATTUID = 'XW2829'
   OR L2_REPORT_TO_ATTUID = 'XW2829'
   OR L1_REPORT_TO_ATTUID = 'XW2829'
;            

ANALYZE USER_ORG COMPUTE STATISTICS;
