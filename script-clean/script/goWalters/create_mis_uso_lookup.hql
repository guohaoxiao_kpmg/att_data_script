use kpmg_ws;
drop table mis_uso_lookup;
create table mis_uso_lookup(

Customer String,
SVC_LOC	String,
Service_Type String,
Area_Manager String,
Order_Creator String,
ORDER_MANAGER String,
USO String,
Order_Status String,
ECRM_SR_ID String,
Project_ID String
)
ROW format delimited fields terminated by "|";

