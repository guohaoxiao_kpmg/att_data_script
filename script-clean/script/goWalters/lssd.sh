
hive -e "
use kpmg_ws
;drop table  LSSD_Completed_YTD;
 create external table LSSD_Completed_YTD(
USO     String,
ATT_Telco_USO_Ind       String,
C_Project_Number        String,
Center  String,
Circuit_ID      String,
Completed_From_Group    String,
Constr_Cust_Ind String,
Constr_LEC_Ind  String,
CR_IOM_GCSM     String,
Customer_Strata_Group   String,
Customer_Name   String,
Expedite_Ind    String,
FTB_Ind String,
Main_Order_Type String,
Managed_Order_Ind       String,
Market  String,
Market_Segment  String,
MCN     String,
NCON    String,
OCO     String,
OCO_Zone        String,
Order_Type      String,
Ordering_Partner        String,
PON_APRN_Newest String,
Product String,
Project_Text    String,
Related_Circuit String,
SAART_Segment   String,
Sales_Code      String,
Speed   String,
Speed_Grouping  String,
SR_NO   String,
Supp_Code1      String,
Supp_Code2      String,
Supp_Code3      String,
Supp_Code4      String,
Supp_Code5      String,
Supp_NCON_Ind   String,
Supp_Number     String,
AADate  String,
App     String,
Billing_Start_Date      String,
CPDD_Comp       String,
CRDD    String,
SID_Comp        String,
RID_Due String,
RID_ECD String,
RID_Comp        String,
RID_Jep String,
WOT_Due String,
WOT_Comp        String,
WOT_Jep String,
CTA_Due String,
CTA_ECD String,
CTA_Comp        String,
CTA_Jep String,
DD_Due  String,
DD_Due_Frozen   String,
DD_ECD  String,
DD_Comp String,
DD_Jep  String,
DD_Jep_Frozen   String,
CNR_Ind String,
OUDD    String,
EIRD_Due        String,
EIRD_Comp       String,
EIRD_Jep        String,
OCO2_Jep        String,
OCO2_Jep_Frozen String,
CT_CPDD_SID     String,
CT_SID_RID      String,
CT_RID_WOT      String,
CT_RID_CTA      String,
CT_WOT_CTA      String,
CT_CTA_DD       String,
CT_CPDD_CTA     String,
CT_CPDD_DD      String,
CT_CPDD_EIRD    String,
CT_CPDD_BSD     String,
CT_Omit_Ind     String,
CT_Omit_Text    String,
USO_Originator_HRID     String)
;

use kpmg_ws
;drop table  LSSD_Cancelled_YTD;
 create external table LSSD_Cancelled_YTD(
USO     String,
C_Project_Number        String,
Center  String,
Circuit_ID      String,
Constr_Cust_Ind String,
Constr_LEC_Ind  String,
CR_IOM_GCSM     String,
Customer_Strata_Group   String,
Customer_Name   String,
Expedite_Ind    String,
FTB_Ind String,
Main_Order_Type String,
Managed_Order_Ind       String,
Market  String,
Market_Segment  String,
MCN     String,
NCON    String,
OCO     String,
OCO_Zone        String,
Order_Type      String,
Ordering_Partner        String,
PON_APRN_Newest String,
Product String,
Project_Text    String,
Related_Circuit String,
SAART_Segment   String,
Sales_Code      String,
Speed   String,
Speed_Grouping  String,
SR_NO   String,
Supp_Code1      String,
AADate  String,
App     String,
Cancel_Date     String,
CPDD_Comp       String,
CRDD    String,
DD_Due  String,
DD_Jep  String,
DD_Jep_Frozen   String,
CNR_Ind String,
USO_Originator_HRID     String)
;

use kpmg_ws
;drop table  LSSD_Issued_YTD;
 create external table LSSD_Issued_YTD(
USO     String,
ATT_Telco_USO_Ind       String,
C_Project_Number        String,
Center  String,
Circuit_ID      String,
Constr_Cust_Ind String,
Constr_LEC_Ind  String,
CR_IOM_GCSM     String,
Customer_Strata_Group   String,
Customer_Name   String,
Expedite_Ind    String,
FTB_Ind String,
Main_Order_Type String,
Managed_Order_Ind       String,
Market  String,
Market_Segment  String,
MCN     String,
NCON    String,
OCO     String,
OCO_Zone        String,
Order_Type      String,
Ordering_Partner        String,
PON_APRN_Newest String,
Product String,
Project_Text    String,
Related_Circuit String,
SAART_Segment   String,
Sales_Code      String,
Speed   String,
Speed_Grouping  String,
SR_NO   String,
Supp_Number     String,
AADate  String,
App     String,
CPDD_Comp       String,
CRDD    String,
DD_Due  String,
DD_Jep  String,
DD_Jep_Frozen   String,
CNR_Ind String,
USO_Originator_HRID     String)
;

use kpmg_ws
;drop table  LSSD_WIP_YTD;
 create external table LSSD_WIP_YTD(
USO     String,
ATT_Telco_USO_Ind       String,
C_Project_Number        String,
Center  String,
Circuit_ID      String,
Constr_Cust_Ind String,
Constr_LEC_Ind  String,
CR_IOM_GCSM     String,
Customer_Strata_Group   String,
Customer_Name   String,
Days_Past_Due   String,
Expedite_Ind    String,
FTB_Ind String,
Main_Order_Type String,
Managed_Order_Ind       String,
Market  String,
Market_Segment  String,
MCN     String,
NCON    String,
OCO     String,
OCO_Zone        String,
Order_Aging     String,
Order_Type      String,
Ordering_Partner        String,
PON_APRN_Newest String,
Product String,
Project_Text    String,
Related_Circuit String,
SAART_Segment   String,
Sales_Code      String,
Speed   String,
Speed_Grouping  String,
SR_NO   String,
AADate  String,
App     String,
Billing_Start_Date      String,
CPDD_Comp       String,
CRDD    String,
SID_Comp        String,
RID_Due String,
RID_ECD String,
RID_Comp        String,
RID_Jep String,
WOT_Due String,
WOT_Comp        String,
WOT_Jep String,
CTA_Due String,
CTA_ECD String,
CTA_Comp        String,
CTA_Jep String,
DD_Due  String,
DD_Due_Frozen   String,
DD_ECD  String,
DD_Jep  String,
DD_Jep_Frozen   String,
CNR_Ind String,
OUDD    String,
EIRD_Due        String,
EIRD_Comp       String,
EIRD_Jep        String,
OCO2_Jep        String,
OCO2_Jep_Frozen String,
CT_CPDD_SID     String,
CT_SID_RID      String,
CT_RID_WOT      String,
CT_RID_CTA      String,
CT_WOT_CTA      String,
CT_CPDD_CTA     String,
USO_Originator_HRID     String)
;

use kpmg_ws
;drop table  LSSD_Due_YTD;
 create external table LSSD_Due_YTD(
USO     String,
ATT_Telco_USO_Ind       String,
C_Project_Number        String,
Center  String,
Circuit_ID      String,
Constr_Cust_Ind String,
Constr_LEC_Ind  String,
CR_IOM_GCSM     String,
Customer_Strata_Group   String,
Customer_Name   String,
DAB_Ind String,
Expedite_Ind    String,
FTB_Ind String,
Main_Order_Type String,
Managed_Order_Ind       String,
Market  String,
Market_Segment  String,
MCN     String,
NCON    String,
OCO     String,
OCO_Zone        String,
Order_Type      String,
Ordering_Partner        String,
PON_APRN_Newest String,
Product String,
Project_Text    String,
Related_Circuit String,
SAART_Segment   String,
Sales_Code      String,
Speed   String,
Speed_Grouping  String,
SR_NO   String,
AADate  String,
App     String,
CPDD_Comp       String,
CRDD    String,
SID_Comp        String,
RID_Due String,
RID_ECD String,
RID_Comp        String,
RID_Jep String,
WOT_Due String,
WOT_Comp        String,
WOT_Jep String,
CTA_Due String,
CTA_ECD String,
CTA_Comp        String,
CTA_Jep String,
DD_Due  String,
DD_Due_Frozen   String,
DD_ECD  String,
DD_Comp String,
DD_Jep  String,
DD_Jep_Frozen   String,
CNR_Ind String,
OTStatus        String,
OTStatus_DA     String,
OUDD    String,
EIRD_Due        String,
EIRD_Comp       String,
EIRD_Jep        String,
Billing_Start_Date      String,
OCO2_Jep        String,
OCO2_Jep_Frozen String,
CT_CPDD_SID     String,
CT_SID_RID      String,
CT_RID_WOT      String,
CT_RID_CTA      String,
CT_WOT_CTA      String,
CT_CTA_DD       String,
CT_CPDD_CTA     String,
CT_CPDD_DD      String)
;

"


hadoop dfs -rmr /sandbox/sandbox31/kpmg_ws/lssd_*_ytd

sqoop  import \
-libjars "/usr/hdp/2.2.8.0-3150/sqoop/lib/jtds-1.3.1.jar"                                                                        \
--driver          "net.sourceforge.jtds.jdbc.Driver"                                                                                          \
--connect         "jdbc:jtds:sqlserver://MOSTLS1ABSPMD06.itservices.sbc.com;instance=PD_MANDMX01;databaseName=views_user;domain=itservices"   \
--username        jl7392                                                                                                                      \
--password        '@tlanta1050'                                                                                                               \
--as-textfile \
--target-dir '/sandbox/sandbox31/kpmg_ws/lssd_completed_ytd' \
-m 64 \
--split-by uso \
--query 'select USO,ATT_Telco_USO_Ind,C_Project_Number,Center,Circuit_ID,Completed_From_Group,Constr_Cust_Ind,Constr_LEC_Ind,CR_IOM_GCSM,Customer_Strata_Group,Customer_Name,Expedite_Ind,FTB_Ind,Main_Order_Type,Managed_Order_Ind,Market,Market_Segment,MCN,NCON,OCO,OCO_Zone,Order_Type,Ordering_Partner,PON_APRN_Newest,Product,Project_Text,Related_Circuit,SAART_Segment,Sales_Code,Speed,Speed_Grouping,SR_NO,Supp_Code1,Supp_Code2,Supp_Code3,Supp_Code4,Supp_Code5,Supp_NCON_Ind,Supp_Number,AADate,App,Billing_Start_Date,CPDD_Comp,CRDD,SID_Comp,RID_Due,RID_ECD,RID_Comp,RID_Jep,WOT_Due,WOT_Comp,WOT_Jep,CTA_Due,CTA_ECD,CTA_Comp,CTA_Jep,DD_Due,DD_Due_Frozen,DD_ECD,DD_Comp,DD_Jep,DD_Jep_Frozen,CNR_Ind,OUDD,EIRD_Due,EIRD_Comp,EIRD_Jep,OCO2_Jep,OCO2_Jep_Frozen,CT_CPDD_SID,CT_SID_RID,CT_RID_WOT,CT_RID_CTA,CT_WOT_CTA,CT_CTA_DD,CT_CPDD_CTA,CT_CPDD_DD,CT_CPDD_EIRD,CT_CPDD_BSD,CT_Omit_Ind,CT_Omit_Text,USO_Originator_HRID
 from  views_user..vw_IDAT_LSSD_Linedata_OrdersCompleted_YTD  where $CONDITIONS' \
--null-string '\\N' \
--null-non-string '\\N' \
--fields-terminated-by '\001' \
--hive-drop-import-delims ;



sqoop  import \
-libjars "/usr/hdp/2.2.8.0-3150/sqoop/lib/jtds-1.3.1.jar"                                                                        \
--driver          "net.sourceforge.jtds.jdbc.Driver"                                                                                          \
--connect         "jdbc:jtds:sqlserver://MOSTLS1ABSPMD06.itservices.sbc.com;instance=PD_MANDMX01;databaseName=views_user;domain=itservices"   \
--username        jl7392                                                                                                                      \
--password        '@tlanta1050'                                                                                                               \
--as-textfile \
--target-dir '/sandbox/sandbox31/kpmg_ws/lssd_issued_ytd' \
-m 64 \
--split-by uso \
--query 'select
USO,ATT_Telco_USO_Ind,C_Project_Number,Center,Circuit_ID,Constr_Cust_Ind,Constr_LEC_Ind,CR_IOM_GCSM,Customer_Strata_Group,Customer_Name,Expedite_Ind,FTB_Ind,Main_Order_Type,Managed_Order_Ind,Market,Market_Segment,MCN,NCON,OCO,OCO_Zone,Order_Type,Ordering_Partner,PON_APRN_Newest,Product,Project_Text,Related_Circuit,SAART_Segment,Sales_Code,Speed,Speed_Grouping,SR_NO,Supp_Number,AADate,App,CPDD_Comp,CRDD,DD_Due,DD_Jep,DD_Jep_Frozen,CNR_Ind,USO_Originator_HRID
from  views_user..vw_IDAT_LSSD_Linedata_OrdersIssued_YTD  where $CONDITIONS' \
--null-string '\\N' \
--null-non-string '\\N' \
--fields-terminated-by '\001' \
--hive-drop-import-delims ;


sqoop  import \
-libjars "/usr/hdp/2.2.8.0-3150/sqoop/lib/jtds-1.3.1.jar"                                                                        \
--driver          "net.sourceforge.jtds.jdbc.Driver"                                                                                          \
--connect         "jdbc:jtds:sqlserver://MOSTLS1ABSPMD06.itservices.sbc.com;instance=PD_MANDMX01;databaseName=views_user;domain=itservices"   \
--username        jl7392                                                                                                                      \
--password        '@tlanta1050'                                                                                                               \
--as-textfile \
--target-dir '/sandbox/sandbox31/kpmg_ws/lssd_cancelled_ytd' \
-m 64 \
--split-by uso \
--query 'select
USO,C_Project_Number,Center,Circuit_ID,Constr_Cust_Ind,Constr_LEC_Ind,CR_IOM_GCSM,Customer_Strata_Group,Customer_Name,Expedite_Ind,FTB_Ind,Main_Order_Type,Managed_Order_Ind,Market,Market_Segment,MCN,NCON,OCO,OCO_Zone,Order_Type,Ordering_Partner,PON_APRN_Newest,Product,Project_Text,Related_Circuit,SAART_Segment,Sales_Code,Speed,Speed_Grouping,SR_NO,Supp_Code1,AADate,App,Cancel_Date,CPDD_Comp,CRDD,DD_Due,DD_Jep,DD_Jep_Frozen,CNR_Ind,USO_Originator_HRID
from  views_user..vw_IDAT_LSSD_Linedata_Orderscancelled_YTD  where $CONDITIONS' \
--null-string '\\N' \
--null-non-string '\\N' \
--fields-terminated-by '\001' \
--hive-drop-import-delims ;


sqoop  import \
-libjars "/usr/hdp/2.2.8.0-3150/sqoop/lib/jtds-1.3.1.jar"                                                                        \
--driver          "net.sourceforge.jtds.jdbc.Driver"                                                                                          \
--connect         "jdbc:jtds:sqlserver://MOSTLS1ABSPMD06.itservices.sbc.com;instance=PD_MANDMX01;databaseName=views_user;domain=itservices"   \
--username        jl7392                                                                                                                      \
--password        '@tlanta1050'                                                                                                               \
--as-textfile \
--target-dir '/sandbox/sandbox31/kpmg_ws/lssd_wip_ytd' \
-m 64 \
--split-by uso \
--query 'select
USO,ATT_Telco_USO_Ind,C_Project_Number,Center,Circuit_ID,Constr_Cust_Ind,Constr_LEC_Ind,CR_IOM_GCSM,Customer_Strata_Group,Customer_Name,Days_Past_Due,Expedite_Ind,FTB_Ind,Main_Order_Type,Managed_Order_Ind,Market,Market_Segment,MCN,NCON,OCO,OCO_Zone,Order_Aging,Order_Type,Ordering_Partner,PON_APRN_Newest,Product,Project_Text,Related_Circuit,SAART_Segment,Sales_Code,Speed,Speed_Grouping,SR_NO,AADate,App,Billing_Start_Date,CPDD_Comp,CRDD,SID_Comp,RID_Due,RID_ECD,RID_Comp,RID_Jep,WOT_Due,WOT_Comp,WOT_Jep,CTA_Due,CTA_ECD,CTA_Comp,CTA_Jep,DD_Due,DD_Due_Frozen,DD_ECD,DD_Jep,DD_Jep_Frozen,CNR_Ind,OUDD,EIRD_Due,EIRD_Comp,EIRD_Jep,OCO2_Jep,OCO2_Jep_Frozen,CT_CPDD_SID,CT_SID_RID,CT_RID_WOT,CT_RID_CTA,CT_WOT_CTA,CT_CPDD_CTA,USO_Originator_HRID
from  views_user..vw_IDAT_LSSD_Linedata_Orderswip_YTD  where $CONDITIONS' \
--null-string '\\N' \
--null-non-string '\\N' \
--fields-terminated-by '\001' \
--hive-drop-import-delims ;

sqoop  import \
-libjars "/usr/hdp/2.2.8.0-3150/sqoop/lib/jtds-1.3.1.jar"                                                                        \
--driver          "net.sourceforge.jtds.jdbc.Driver"                                                                                          \
--connect         "jdbc:jtds:sqlserver://MOSTLS1ABSPMD06.itservices.sbc.com;instance=PD_MANDMX01;databaseName=views_user;domain=itservices"   \
--username        jl7392                                                                                                                      \
--password        '@tlanta1050'                                                                                                               \
--as-textfile \
--target-dir '/sandbox/sandbox31/kpmg_ws/lssd_due_ytd' \
-m 64 \
--split-by uso \
--query 'select
USO,ATT_Telco_USO_Ind,C_Project_Number,Center,Circuit_ID,Constr_Cust_Ind,Constr_LEC_Ind,CR_IOM_GCSM,Customer_Strata_Group,Customer_Name,DAB_Ind,Expedite_Ind,FTB_Ind,Main_Order_Type,Managed_Order_Ind,Market,Market_Segment,MCN,NCON,OCO,OCO_Zone,Order_Type,Ordering_Partner,PON_APRN_Newest,Product,Project_Text,Related_Circuit,SAART_Segment,Sales_Code,Speed,Speed_Grouping,SR_NO,AADate,App,CPDD_Comp,CRDD,SID_Comp,RID_Due,RID_ECD,RID_Comp,RID_Jep,WOT_Due,WOT_Comp,WOT_Jep,CTA_Due,CTA_ECD,CTA_Comp,CTA_Jep,DD_Due,DD_Due_Frozen,DD_ECD,DD_Comp,DD_Jep,DD_Jep_Frozen,CNR_Ind,OTStatus,OTStatus_DA,OUDD,EIRD_Due,EIRD_Comp,EIRD_Jep,Billing_Start_Date,OCO2_Jep,OCO2_Jep_Frozen,CT_CPDD_SID,CT_SID_RID,CT_RID_WOT,CT_RID_CTA,CT_WOT_CTA,CT_CTA_DD,CT_CPDD_CTA,CT_CPDD_DD
from  views_user..vw_IDAT_LSSD_Linedata_Ordersdue_YTD  where $CONDITIONS' \
--null-string '\\N' \
--null-non-string '\\N' \
--fields-terminated-by '\001' \
--hive-drop-import-delims ;

