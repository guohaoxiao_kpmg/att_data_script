
| bvoip_id             | current_system | csm_id               | crd_id      | srq_id      | ord_id      | sod_id      | usrp_order_id        | svc_order_id | svc_request_id | usrp_activity_id | uso_number       | order_category | uso_unique_id        | mis_id               | mis_crd_id  | mis_srq_id  | mis_ord_id           | mis_sod_id           | mis_usrp_order_id    | mis_uso_number | act | status               | version | type                 | subtype              | order_type           | ord_order_type       | customer_name        | circuit_id           | speed | access_type          | port_speed           | toi                  | fasttrack_type | bundle_type          | product_name         | product_config       | mcn        | svid       | suborder_sequence | sub_product | sitetype_id | ipld_ind | iplocal_ind | ipcentrex_ind | iptf_ind | avoics_ind | mis_usrp_specialorder_type | dms_status_reason    | project_number       | concur_calls | concur_calls_prev | concur_calls_chng    | requestoption_string | requestoption_desc_string | crd_due_date_srce    | wip_notes            | gcsc_reject_reason   | gcsc_reject_count | workcenter           | team_leader          | coe_om_fullname      | business_group       | market_segment       | dnae_email           | saart_segment        | created_date        | sod_entered_date    | issued_date         | efms_crdd_date      | crd_due_date        | dab_due_date         | sod_due_date        | orig_sched_tt_date  | curr_sched_tt_date  | complete_date       | cancel_date         | gcsm_cpdd_date      | bvoip_acdd_date     | att_ready_due_date  | att_ready_date      | att_ready_date_bvoip | att_ready_date_mis  | crdd_date           | supp_date           | billing_date        | usrp_sched_date     | usrp_actual_date    | giom_sched_date     | giom_actual_date    | ord_rtr_sched_date  | ord_rtr_actual_date | lsr_sched_date      | lsr_actual_date     | transfoc_sched_date | transfoc_actual_date | transdd_sched_date  | transdd_actual_date | attready_sched_date | attready_actual_date | ttu_sched_sched_date | ttu_sched_actual_date | ttu_sched_date      | ttu_actual_date     | fqdd_date            | fqdd_snapshot_date   | acdd_date           | pi_75days_date      | cnr_start_date      | conduct_pretest_actual_date | milestone_616_comp_date | cpe_staging_ctr_comp_date | cust_neg_delivery_date | asap_proj_init_date | gcsc_accept_date    | billing_dd_date      | gcsc_orig_start_date | gcsc_reject_date     | gcsc_lastreject_fix_date | gcsc_actual_start_date | avpn_ind | asap_ind | att_ready_da_sts | att_ready_otstatus | att_ready_wip        | att_ready_wip_aging | backlog_ind | backlog_aging | bib_ind | billable_cnr_ind | billing_ind | brrf_reported_ind | brrf_reconciled_ind | cancel_ind | cnr_ind | completed_from_group | crd_otstatus         | cust_impact_flag     | frontlog_ind | frontlog_aging | late_cancel_ind | nip_ete_ind | nonbillable_cnr_ind | ord_bvoip_ind | sod_otstatus         | ttu_da_sts | validorder_ind | wip_ind | wip_ind_attnotready | wip_ind_attready | wip_aging   | wip                  | fq_met_ind | porting_ind | coordinated_add_disc_ind | cta_comp_attready_pending_ind | sbs_ind | ipflex_avpn_omit_ind | change_to_existing_svc_ind | transport_type       | enhanced_feature_ind | ct_cr_orig_sched_tt | ct_cr_usrp  | ct_cr_giom  | ct_cr_ord_rtr | ct_cr_lsr   | ct_cr_transfoc | ct_cr_transdd | ct_cr_attready | ct_cr_ttu_sched | ct_cr_ttu   | ct_cr_billing | ct_issued_attready | ct_issued_ttu | ct_cr_ttu_no_sbs | ct_cr_ttu_sbs_only | ct_release_ttu_sbs_only | sd_aging    | ct_cr_to_gcsc_accept | ct_issued_to_gcsc_accept | ct_ttu_to_gcsc_accept | frequency | logdate              |







use kpmg_ws
;drop table  BVOIP_Metrics_Scorecard_Details_MPR_Orders_Issued;
 create external table BVOIP_Metrics_Scorecard_Details_MPR_Orders_Issued(
usrp_order_id   String,
avpn_ind        String,
customer_name   String,
type    String,
speed   String,
issued_date     String,
uso_unique_id   String,
uso_number      String,
oco1_jeop       String)
ROW format delimited fields terminated by "|";

use kpmg_ws
;drop table  BVOIP_Metrics_Scorecard_Details_MPR_Orders_Completed;
 create external table BVOIP_Metrics_Scorecard_Details_MPR_Orders_Completed(
usrp_order_id   String,
avpn_ind        String,
customer_name   String,
type    String,
speed   String,
complete_date   String,
uso_unique_id   String,
uso_number      String,
oco1_jeop       String)
ROW format delimited fields terminated by "|";

