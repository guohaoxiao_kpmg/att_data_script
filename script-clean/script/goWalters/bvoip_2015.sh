
hive -e "
use kpmg_ws
;drop table  BVOIP_Issued_2015;
 create external table BVOIP_Issued_2015(
usrp_order_id   String,
avpn_ind        String,
customer_name   String,
type    String,
speed   String,
issued_date     String,
uso_unique_id   String,
uso_number      String,
ord_order_type String,
product_name String,
act String

);

use kpmg_ws
;drop table  BVOIP_Completed_2015;
 create external table BVOIP_Completed_2015(
usrp_order_id   String,
avpn_ind        String,
customer_name   String,
type    String,
speed   String,
complete_date   String,
uso_unique_id   String,
uso_number      String,
ord_order_type String,
product_name String,
act String
);


"


hadoop dfs -rmr /sandbox/sandbox31/kpmg_ws/bvoip_*_2015

sqoop  import \
-libjars "/usr/hdp/2.2.8.0-3150/sqoop/lib/jtds-1.3.1.jar"                                                                        \
--driver          "net.sourceforge.jtds.jdbc.Driver"                                                                                          \
--connect         "jdbc:jtds:sqlserver://MOSTLS1ABSPMD06.itservices.sbc.com;instance=PD_MANDMX01;databaseName=views_user;domain=itservices"   \
--username        jl7392                                                                                                                      \
--password        '@tlanta1050'                                                                                                               \
--as-textfile \
--target-dir '/sandbox/sandbox31/kpmg_ws/bvoip_completed_2015' \
-m 64 \
--split-by usrp_order_id \
--query 'select 
usrp_order_id,avpn_ind,customer_name,type,speed,complete_date,uso_unique_id,uso_number,ord_order_type,product_name,act
from  views_user..vw_Report_BVOIP_Details_OrdersCompleted_Base_Tab_YPR_2015  where $CONDITIONS' \
--null-string '\\N' \
--null-non-string '\\N' \
--fields-terminated-by '\001' \
--hive-drop-import-delims ;



sqoop  import \
-libjars "/usr/hdp/2.2.8.0-3150/sqoop/lib/jtds-1.3.1.jar"                                                                        \
--driver          "net.sourceforge.jtds.jdbc.Driver"                                                                                          \
--connect         "jdbc:jtds:sqlserver://MOSTLS1ABSPMD06.itservices.sbc.com;instance=PD_MANDMX01;databaseName=views_user;domain=itservices"   \
--username        jl7392                                                                                                                      \
--password        '@tlanta1050'                                                                                                               \
--as-textfile \
--target-dir '/sandbox/sandbox31/kpmg_ws/bvoip_issued_2015' \
-m 64 \
--split-by usrp_order_id \
--query 'select
usrp_order_id,avpn_ind,customer_name,type,speed,issued_date,uso_unique_id,uso_number,ord_order_type,product_name,act
from  views_user..vw_Report_BVOIP_Details_OrdersIssued_Base_Tab_YPR_2015  where $CONDITIONS' \
--null-string '\\N' \
--null-non-string '\\N' \
--fields-terminated-by '\001' \
--hive-drop-import-delims ;


