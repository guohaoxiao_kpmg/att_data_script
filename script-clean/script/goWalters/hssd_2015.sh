
hive -e "
use kpmg_ws
;drop table  HSSD_Completed_2015;
 create external table HSSD_Completed_2015(
USO     String,
AADate  String,
Main_Order_Type String,
Baseline_Ind    String,
CDDDDate        String,
CDDD_Extended_Ind       String,
Center  String,
Circuit_ID      String,
CNR_Ind String,
Constr_Cust_Ind String,
Constr_LEC_Ind  String,
CPDD_Comp       String,
Cust_Order_Ind  String,
Customer_Strata_Group   String,
Customer_Name   String,
Expedite_Ind    String,
Market  String,
Market_Segment  String,
MCN     String,
NCON    String,
OCO     String,
OCO_Zone        String,
DD_Comp String,
DD_Due  String,
DD_Jep  String,
DD_Jep_Frozen   String,
Ordering_Partner        String,
Product String,
Project_Text    String,
Related_Circuit String,
Special_Cust_Ind        String,
Speed   String,
Speed_Grouping  String,
CR_IOM_GCSM     String,
Ethernet_Standalone_Ind String,
Ethernet_Switched_Ind   String,
Kaizen_Ind      String,
Stream_Process_Ind      String,
Single_Control_Plane_Ind        String,
FTB_Ind String,
PON_APRN_Newest String,
PON_Newest      String,
PON_Newest_CKL_Location String,
PON_Newest_EAD_ECD      String,
PON_Newest_EAD_Jep      String,
PON_Newest_EAD_Jep_Posted       String,
PON_Newest_FOC_Complete String,
PON_Newest_FOC_Due      String,
PON_Newest_FOC_Jep      String,
PON_Newest_ICSC String,
PON_Newest_In_Region_Ind        String,
PON_Newest_LEC  String,
PON_Newest_LEC_Company  String,
SID_Comp        String,
SID_Jep String,
RID_Due String,
RID_Comp        String,
RID_Jep String,
WOT_Due String,
WOT_Comp        String,
WOT_Jep String,
CTA_Due String,
CTA_Comp        String,
CTA_Jep String,
DD_Due_Frozen   String,
Billing_Start_Date      String,
OUDD    String,
OCO2_Jep        String,
OCO2_Jep_Frozen String,
Billing_DD      String,
FQ_Due  String,
FQ_Comp String,
FQ_Jep  String,
FQ_Jep_Post     String,
MGDD_Due        String,
MGDD_Comp       String,
CT_CPDD_SID     String,
CT_SID_RID      String,
CT_RID_WOT      String,
CT_RID_CTA      String,
CT_WOT_CTA      String,
CT_CTA_DD       String,
CT_CPDD_CTA     String,
CT_CPDD_DD      String,
CT_CPDD_BSD     String,
CT_Omit_Ind     String,
CT_Omit_Text    String,
Completed_From_Group    String,
Supp_Number     String,
Supp_Code2      String,
Supp_Code3      String,
Supp_Code4      String,
Supp_Code5      String,
Supp_NCON_Ind   String,
DAB_Ind String,
C_Project_Number        String,
SAART_Segment   String,
Site_Bundle_Type        String,
Site_BundleID   String,
Primary_Bundle_Desc     String,
Secondary_Bundle_Desc   String,
ICore_ID        String,
USO_Originator_HRID     String,
SVID    String,
OCO1_Jep        String);

use kpmg_ws
;drop table  HSSD_Cancelled_2015;
 create external table HSSD_Cancelled_2015(
USO     String,
AADate  String,
Main_Order_Type String,
Baseline_Ind    String,
CDDDDate        String,
CDDD_Extended_Ind       String,
Center  String,
Circuit_ID      String,
CNR_Ind String,
Constr_Cust_Ind String,
Constr_LEC_Ind  String,
CPDD_Comp       String,
Cust_Order_Ind  String,
Customer_Strata_Group   String,
Customer_Name   String,
Expedite_Ind    String,
Market  String,
Market_Segment  String,
MCN     String,
NCON    String,
OCO     String,
OCO_Zone        String,
Cancel_Date     String,
DD_Due  String,
DD_Jep  String,
DD_Jep_Frozen   String,
Ordering_Partner        String,
Product String,
Project_Text    String,
Related_Circuit String,
Special_Cust_Ind        String,
Speed   String,
Speed_Grouping  String,
CR_IOM_GCSM     String,
Ethernet_Standalone_Ind String,
Ethernet_Switched_Ind   String,
Kaizen_Ind      String,
Stream_Process_Ind      String,
Single_Control_Plane_Ind        String,
FTB_Ind String,
PON_APRN_Newest String,
PON_Newest      String,
PON_Newest_EAD_Jep_Posted       String,
PON_Newest_In_Region_Ind        String,
PON_Newest_LEC  String,
PON_Newest_LEC_Company  String,
C_Project_Number        String,
SAART_Segment   String,
SID_Jep String,
Billing_DD      String,
FQ_Due  String,
FQ_Comp String,
FQ_Jep  String,
FQ_Jep_Post     String,
MGDD_Due        String,
MGDD_Comp       String,
Site_Bundle_Type        String,
Site_BundleID   String,
Primary_Bundle_Desc     String,
Secondary_Bundle_Desc   String,
ICore_ID        String,
USO_Originator_HRID     String,
SVID    String)
;

use kpmg_ws
;drop table  HSSD_WIP_2015;
 create external table HSSD_WIP_2015(
USO     String,
AADate  String,
Main_Order_Type String,
Baseline_Ind    String,
CDDDDate        String,
CDDD_Extended_Ind       String,
Center  String,
Circuit_ID      String,
CNR_Ind String,
Constr_Cust_Ind String,
Constr_LEC_Ind  String,
CPDD_Comp       String,
Cust_Order_Ind  String,
Customer_Strata_Group   String,
Customer_Name   String,
Expedite_Ind    String,
Market  String,
Market_Segment  String,
MCN     String,
NCON    String,
OCO     String,
OCO_Zone        String,
Billing_Start_Date      String,
DD_Due  String,
DD_Jep  String,
DD_Jep_Frozen   String,
Ordering_Partner        String,
Product String,
Project_Text    String,
Related_Circuit String,
Special_Cust_Ind        String,
Speed   String,
Speed_Grouping  String,
CR_IOM_GCSM     String,
Ethernet_Standalone_Ind String,
Ethernet_Switched_Ind   String,
Kaizen_Ind      String,
Stream_Process_Ind      String,
Single_Control_Plane_Ind        String,
FTB_Ind String,
PON_APRN_Newest String,
PON_Newest      String,
PON_Newest_CKL_Location String,
PON_Newest_EAD_ECD      String,
PON_Newest_EAD_Jep      String,
PON_Newest_EAD_Jep_Posted       String,
PON_Newest_FOC_Complete String,
PON_Newest_FOC_Due      String,
PON_Newest_FOC_Jep      String,
PON_Newest_ICSC String,
PON_Newest_In_Region_Ind        String,
PON_Newest_LEC  String,
PON_Newest_LEC_Company  String,
SID_Comp        String,
SID_Jep String,
RID_Due String,
RID_Comp        String,
RID_Jep String,
WOT_Due String,
WOT_Comp        String,
WOT_Jep String,
CTA_Due String,
CTA_Comp        String,
CTA_Jep String,
DD_Due_Frozen   String,
OUDD    String,
OCO2_Jep        String,
OCO2_Jep_Frozen String,
Billing_DD      String,
FQ_Due  String,
FQ_Comp String,
FQ_Jep  String,
FQ_Jep_Post     String,
MGDD_Due        String,
MGDD_Comp       String,
CT_CPDD_SID     String,
CT_SID_RID      String,
CT_RID_WOT      String,
CT_RID_CTA      String,
CT_WOT_CTA      String,
CT_CTA_DD       String,
CT_CPDD_CTA     String,
CT_CPDD_DD      String,
Order_Aging     String,
Days_Past_Due   String,
Backlog_Type    String,
DAB_Ind String,
C_Project_Number        String,
SAART_Segment   String,
Site_Bundle_Type        String,
Site_BundleID   String,
Primary_Bundle_Desc     String,
Secondary_Bundle_Desc   String,
ICore_ID        String,
USO_Originator_HRID     String,
SVID    String);

use kpmg_ws
;drop table  HSSD_Issued_2015;
 create external table HSSD_Issued_2015(
USO     String,
AADate  String,
Main_Order_Type String,
Baseline_Ind    String,
CDDDDate        String,
CDDD_Extended_Ind       String,
Center  String,
Circuit_ID      String,
CNR_Ind String,
Constr_Cust_Ind String,
Constr_LEC_Ind  String,
CPDD_Comp       String,
Cust_Order_Ind  String,
Customer_Strata_Group   String,
Customer_Name   String,
Expedite_Ind    String,
Market  String,
Market_Segment  String,
MCN     String,
NCON    String,
OCO     String,
OCO_Zone        String,
Supp_Number     String,
DD_Due  String,
DD_Jep  String,
DD_Jep_Frozen   String,
Ordering_Partner        String,
Product String,
Project_Text    String,
Related_Circuit String,
Special_Cust_Ind        String,
Speed   String,
Speed_Grouping  String,
CR_IOM_GCSM     String,
Ethernet_Standalone_Ind String,
Ethernet_Switched_Ind   String,
Kaizen_Ind      String,
Stream_Process_Ind      String,
Single_Control_Plane_Ind        String,
FTB_Ind String,
PON_APRN_Newest String,
PON_Newest      String,
PON_Newest_CKL_Location String,
PON_Newest_EAD_ECD      String,
PON_Newest_EAD_Jep      String,
PON_Newest_EAD_Jep_Posted       String,
PON_Newest_FOC_Complete String,
PON_Newest_FOC_Due      String,
PON_Newest_FOC_Jep      String,
PON_Newest_ICSC String,
PON_Newest_In_Region_Ind        String,
PON_Newest_LEC  String,
PON_Newest_LEC_Company  String,
C_Project_Number        String,
SAART_Segment   String,
SID_Jep String,
Billing_DD      String,
FQ_Due  String,
FQ_Comp String,
FQ_Jep  String,
FQ_Jep_Post     String,
MGDD_Due        String,
MGDD_Comp       String,
Site_Bundle_Type        String,
Site_BundleID   String,
Primary_Bundle_Desc     String,
Secondary_Bundle_Desc   String,
ICore_ID        String,
USO_Originator_HRID     String,
SVID    String,
OCO1_Jep        String);
"


hadoop dfs -rmr /sandbox/sandbox31/kpmg_ws/hssd_*_2015

sqoop  import \
-libjars "/usr/hdp/2.2.8.0-3150/sqoop/lib/jtds-1.3.1.jar"                                                                        \
--driver          "net.sourceforge.jtds.jdbc.Driver"                                                                                          \
--connect         "jdbc:jtds:sqlserver://MOSTLS1ABSPMD06.itservices.sbc.com;instance=PD_MANDMX01;databaseName=views_user;domain=itservices"   \
--username        jl7392                                                                                                                      \
--password        '@tlanta1050'                                                                                                               \
--as-textfile \
--target-dir '/sandbox/sandbox31/kpmg_ws/hssd_completed_2015' \
-m 64 \
--split-by uso \
--query 'select USO,AADate,Main_Order_Type,Baseline_Ind,CDDDDate,CDDD_Extended_Ind,Center,Circuit_ID,CNR_Ind,Constr_Cust_Ind,Constr_LEC_Ind,CPDD_Comp,Cust_Order_Ind,Customer_Strata_Group,Customer_Name,Expedite_Ind,Market,Market_Segment,MCN,NCON,OCO,OCO_Zone,DD_Comp,DD_Due,DD_Jep,DD_Jep_Frozen,Ordering_Partner,Product,Project_Text,Related_Circuit,Special_Cust_Ind,Speed,Speed_Grouping,CR_IOM_GCSM,Ethernet_Standalone_Ind,Ethernet_Switched_Ind,Kaizen_Ind,Stream_Process_Ind,Single_Control_Plane_Ind,FTB_Ind,PON_APRN_Newest,PON_Newest,PON_Newest_CKL_Location,PON_Newest_EAD_ECD,PON_Newest_EAD_Jep,PON_Newest_EAD_Jep_Posted,PON_Newest_FOC_Complete,PON_Newest_FOC_Due,PON_Newest_FOC_Jep,PON_Newest_ICSC,PON_Newest_In_Region_Ind,PON_Newest_LEC,PON_Newest_LEC_Company,SID_Comp,SID_Jep,RID_Due,RID_Comp,RID_Jep,WOT_Due,WOT_Comp,WOT_Jep,CTA_Due,CTA_Comp,CTA_Jep,DD_Due_Frozen,Billing_Start_Date,OUDD,OCO2_Jep,OCO2_Jep_Frozen,Billing_DD,FQ_Due,FQ_Comp,FQ_Jep,FQ_Jep_Post,MGDD_Due,MGDD_Comp,CT_CPDD_SID,CT_SID_RID,CT_RID_WOT,CT_RID_CTA,CT_WOT_CTA,CT_CTA_DD,CT_CPDD_CTA,CT_CPDD_DD,CT_CPDD_BSD,CT_Omit_Ind,CT_Omit_Text,Completed_From_Group,Supp_Number,Supp_Code2,Supp_Code3,Supp_Code4,Supp_Code5,Supp_NCON_Ind,DAB_Ind,C_Project_Number,SAART_Segment,Site_Bundle_Type,Site_BundleID,Primary_Bundle_Desc,Secondary_Bundle_Desc,ICore_ID,USO_Originator_HRID,SVID,OCO1_Jep
 from  views_user..vw_IDAT_HSSD_Linedata_OrdersCompleted_YTD_EOY_2015  where $CONDITIONS' \
--null-string '\\N' \
--null-non-string '\\N' \
--fields-terminated-by '\001' \
--hive-drop-import-delims ;



sqoop  import \
-libjars "/usr/hdp/2.2.8.0-3150/sqoop/lib/jtds-1.3.1.jar"                                                                        \
--driver          "net.sourceforge.jtds.jdbc.Driver"                                                                                          \
--connect         "jdbc:jtds:sqlserver://MOSTLS1ABSPMD06.itservices.sbc.com;instance=PD_MANDMX01;databaseName=views_user;domain=itservices"   \
--username        jl7392                                                                                                                      \
--password        '@tlanta1050'                                                                                                               \
--as-textfile \
--target-dir '/sandbox/sandbox31/kpmg_ws/hssd_issued_2015' \
-m 64 \
--split-by uso \
--query 'select USO,AADate,Main_Order_Type,Baseline_Ind,CDDDDate,CDDD_Extended_Ind,Center,Circuit_ID,CNR_Ind,Constr_Cust_Ind,Constr_LEC_Ind,CPDD_Comp,Cust_Order_Ind,Customer_Strata_Group,Customer_Name,Expedite_Ind,Market,Market_Segment,MCN,NCON,OCO,OCO_Zone,Supp_Number,DD_Due,DD_Jep,DD_Jep_Frozen,Ordering_Partner,Product,Project_Text,Related_Circuit,Special_Cust_Ind,Speed,Speed_Grouping,CR_IOM_GCSM,Ethernet_Standalone_Ind,Ethernet_Switched_Ind,Kaizen_Ind,Stream_Process_Ind,Single_Control_Plane_Ind,FTB_Ind,PON_APRN_Newest,PON_Newest,PON_Newest_CKL_Location,PON_Newest_EAD_ECD,PON_Newest_EAD_Jep,PON_Newest_EAD_Jep_Posted,PON_Newest_FOC_Complete,PON_Newest_FOC_Due,PON_Newest_FOC_Jep,PON_Newest_ICSC,PON_Newest_In_Region_Ind,PON_Newest_LEC,PON_Newest_LEC_Company,C_Project_Number,SAART_Segment,SID_Jep,Billing_DD,FQ_Due,FQ_Comp,FQ_Jep,FQ_Jep_Post,MGDD_Due,MGDD_Comp,Site_Bundle_Type,Site_BundleID,Primary_Bundle_Desc,Secondary_Bundle_Desc,ICore_ID,USO_Originator_HRID,SVID,OCO1_Jep
 from  views_user..vw_IDAT_HSSD_Linedata_OrdersIssued_YTD_EOY_2015  where $CONDITIONS' \
--null-string '\\N' \
--null-non-string '\\N' \
--fields-terminated-by '\001' \
--hive-drop-import-delims ;


sqoop  import \
-libjars "/usr/hdp/2.2.8.0-3150/sqoop/lib/jtds-1.3.1.jar"                                                                        \
--driver          "net.sourceforge.jtds.jdbc.Driver"                                                                                          \
--connect         "jdbc:jtds:sqlserver://MOSTLS1ABSPMD06.itservices.sbc.com;instance=PD_MANDMX01;databaseName=views_user;domain=itservices"   \
--username        jl7392                                                                                                                      \
--password        '@tlanta1050'                                                                                                               \
--as-textfile \
--target-dir '/sandbox/sandbox31/kpmg_ws/hssd_cancelled_2015' \
-m 64 \
--split-by uso \
--query 'select USO,AADate,Main_Order_Type,Baseline_Ind,CDDDDate,CDDD_Extended_Ind,Center,Circuit_ID,CNR_Ind,Constr_Cust_Ind,Constr_LEC_Ind,CPDD_Comp,Cust_Order_Ind,Customer_Strata_Group,Customer_Name,Expedite_Ind,Market,Market_Segment,MCN,NCON,OCO,OCO_Zone,Cancel_Date,DD_Due,DD_Jep,DD_Jep_Frozen,Ordering_Partner,Product,Project_Text,Related_Circuit,Special_Cust_Ind,Speed,Speed_Grouping,CR_IOM_GCSM,Ethernet_Standalone_Ind,Ethernet_Switched_Ind,Kaizen_Ind,Stream_Process_Ind,Single_Control_Plane_Ind,FTB_Ind,PON_APRN_Newest,PON_Newest,PON_Newest_EAD_Jep_Posted,PON_Newest_In_Region_Ind,PON_Newest_LEC,PON_Newest_LEC_Company,C_Project_Number,SAART_Segment,SID_Jep,Billing_DD,FQ_Due,FQ_Comp,FQ_Jep,FQ_Jep_Post,MGDD_Due,MGDD_Comp,Site_Bundle_Type,Site_BundleID,Primary_Bundle_Desc,Secondary_Bundle_Desc,ICore_ID,USO_Originator_HRID,SVID
 from  views_user..vw_IDAT_HSSD_Linedata_Orderscancelled_YTD_EOY_2015  where $CONDITIONS' \
--null-string '\\N' \
--null-non-string '\\N' \
--fields-terminated-by '\001' \
--hive-drop-import-delims ;


sqoop  import \
-libjars "/usr/hdp/2.2.8.0-3150/sqoop/lib/jtds-1.3.1.jar"                                                                        \
--driver          "net.sourceforge.jtds.jdbc.Driver"                                                                                          \
--connect         "jdbc:jtds:sqlserver://MOSTLS1ABSPMD06.itservices.sbc.com;instance=PD_MANDMX01;databaseName=views_user;domain=itservices"   \
--username        jl7392                                                                                                                      \
--password        '@tlanta1050'                                                                                                               \
--as-textfile \
--target-dir '/sandbox/sandbox31/kpmg_ws/hssd_wip_2015' \
-m 64 \
--split-by uso \
--query 'select USO,AADate,Main_Order_Type,Baseline_Ind,CDDDDate,CDDD_Extended_Ind,Center,Circuit_ID,CNR_Ind,Constr_Cust_Ind,Constr_LEC_Ind,CPDD_Comp,Cust_Order_Ind,Customer_Strata_Group,Customer_Name,Expedite_Ind,Market,Market_Segment,MCN,NCON,OCO,OCO_Zone,Billing_Start_Date,DD_Due,DD_Jep,DD_Jep_Frozen,Ordering_Partner,Product,Project_Text,Related_Circuit,Special_Cust_Ind,Speed,Speed_Grouping,CR_IOM_GCSM,Ethernet_Standalone_Ind,Ethernet_Switched_Ind,Kaizen_Ind,Stream_Process_Ind,Single_Control_Plane_Ind,FTB_Ind,PON_APRN_Newest,PON_Newest,PON_Newest_CKL_Location,PON_Newest_EAD_ECD,PON_Newest_EAD_Jep,PON_Newest_EAD_Jep_Posted,PON_Newest_FOC_Complete,PON_Newest_FOC_Due,PON_Newest_FOC_Jep,PON_Newest_ICSC,PON_Newest_In_Region_Ind,PON_Newest_LEC,PON_Newest_LEC_Company,SID_Comp,SID_Jep,RID_Due,RID_Comp,RID_Jep,WOT_Due,WOT_Comp,WOT_Jep,CTA_Due,CTA_Comp,CTA_Jep,DD_Due_Frozen,OUDD,OCO2_Jep,OCO2_Jep_Frozen,Billing_DD,FQ_Due,FQ_Comp,FQ_Jep,FQ_Jep_Post,MGDD_Due,MGDD_Comp,CT_CPDD_SID,CT_SID_RID,CT_RID_WOT,CT_RID_CTA,CT_WOT_CTA,CT_CTA_DD,CT_CPDD_CTA,CT_CPDD_DD,Order_Aging,Days_Past_Due,Backlog_Type,DAB_Ind,C_Project_Number,SAART_Segment,Site_Bundle_Type,Site_BundleID,Primary_Bundle_Desc,Secondary_Bundle_Desc,ICore_ID,USO_Originator_HRID,SVID
 from  views_user..vw_IDAT_HSSD_Linedata_Orderswip_YTD_EOY_2015  where $CONDITIONS' \
--null-string '\\N' \
--null-non-string '\\N' \
--fields-terminated-by '\001' \
--hive-drop-import-delims ;



