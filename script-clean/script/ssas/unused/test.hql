
drop table kpmg_ws.organization
;

add file encode.py;

create table kpmg_ws.organization
as select  TRANSFORM(*) using 'python encode.py' as
number ,
attuid                  ,
name                    ,
first_name              ,
last_name               ,
department              ,
city                    ,
state                   ,
country                 ,
level                   ,
job_title_code          ,
job_title_name          ,
costcenter              ,
financial_loc_code      ,
svp_attuid              ,
svp_level               ,
svp_name                ,
svp_job_title           ,
vp_attuid               ,
vp_level                ,
vp_name                 ,
vp_job_title            ,
avp_attuid              ,
avp_level               ,
avp_name                ,
avp_job_title           ,
director_attuid         ,
director_level          ,
director_name           ,
director_job_title      ,
area_manager_attuid     ,
area_manager_level      ,
area_manager_name       ,
area_manager_job_title  ,
manager_attuid          ,
manager_level           ,
manager_name            ,
manager_job_title       ,
non_manager_supervisor_attuid   ,
non_manager_supervisor_level    ,
non_manager_supervisor_name     ,
non_manager_supervisor_job_title        ,
manager_count_report_area_manager      ,
non_manager_count_report_area_manager  ,
other_count_report_area_manager   from kpmg.organization;

