echo "LinkNumber1+"|kinit

hive -e "
drop table kpmg_ws.USAGEANALYSIS;
drop table KPMG_ws.BENEFITS;
drop table KPMG_ws.TOPAPPS;
drop table kpmg_ws.organization;
use kpmg_ws;
create table USAGEANALYSIS 
as select row_number() over () as row_number,* from kpmg.USAGEANALYSIS;

create table BENEFITS
as select row_number() over () as row_number,* from kpmg.BENEFITS;

add file encode.py;

create table organization
as select  TRANSFORM(*) using 'python encode.py' as
number ,
attuid                  ,
name                    ,
first_name              ,
last_name               ,
department              ,
city                    ,
state                   ,
country                 ,
level                   ,
job_title_code          ,
job_title_name          ,
costcenter              ,
financial_loc_code      ,
svp_attuid              ,
svp_level               ,
svp_name                ,
svp_job_title           ,
vp_attuid               ,
vp_level                ,
vp_name                 ,
vp_job_title            ,
avp_attuid              ,
avp_level               ,
avp_name                ,
avp_job_title           ,
director_attuid         ,
director_level          ,
director_name           ,
director_job_title      ,
area_manager_attuid     ,
area_manager_level      ,
area_manager_name       ,
area_manager_job_title  ,
manager_attuid          ,
manager_level           ,
manager_name            ,
manager_job_title       ,
non_manager_supervisor_attuid   ,
non_manager_supervisor_level    ,
non_manager_supervisor_name     ,
non_manager_supervisor_job_title        ,
manager_count_report_area_manager      ,
non_manager_count_report_area_manager  ,
other_count_report_area_manager   from kpmg.organization;


use kpmg_ws;
create table TOPAPPS
as select * from kpmg.TOPAPPS;

"


#  10,787,246

# BENEFITS_WEEKLY_SUMMARY has been created and inserted
# here is the sqoop script

sqoop eval -libjars "/usr/hdp/2.2.8.0-3150/sqoop/lib/sqljdbc4.jar" \
--driver "com.microsoft.sqlserver.jdbc.SQLServerDriver" \
--connect "jdbc:sqlserver://Clddev0sql01683.itservices.sbc.com\DD_ECCRIX01" \
--username iDESK-REPORTUSER \
--password 'du3!Tax3s' \
--query "Truncate table SDA_Stage.dbo.Benefits"

sqoop export -libjars "/usr/hdp/2.2.8.0-3150/sqoop/lib/sqljdbc4.jar" -m 64 \
--driver "com.microsoft.sqlserver.jdbc.SQLServerDriver" \
--connect "jdbc:sqlserver://Clddev0sql01683.itservices.sbc.com\DD_ECCRIX01" \
--username iDESK-REPORTUSER \
--password 'du3!Tax3s' \
--table "SDA_Stage.dbo.Benefits" \
--export-dir "hdfs://COREITKGMTNC20PROD01/sandbox/sandbox31/kpmg_ws/benefits" \
--fields-terminated-by "\\001"                                                \
--lines-terminated-by "\\n"                                                   \
--input-null-string '\\N'                                                           \
--input-null-non-string '\\N'                                                 \
--batch

# ------ idesk_daily_user_profile

sqoop eval -libjars "/usr/hdp/2.2.8.0-3150/sqoop/lib/sqljdbc4.jar" \
--driver "com.microsoft.sqlserver.jdbc.SQLServerDriver" \
--connect "jdbc:sqlserver://Clddev0sql01683.itservices.sbc.com\DD_ECCRIX01" \
--username iDESK-REPORTUSER \
--password 'du3!Tax3s' \
--query "Truncate table SDA_Stage.dbo.UsageAnalysis"


sqoop export -libjars "/usr/hdp/2.2.8.0-3150/sqoop/lib/sqljdbc4.jar" -m 64 \
--driver "com.microsoft.sqlserver.jdbc.SQLServerDriver" \
--connect "jdbc:sqlserver://Clddev0sql01683.itservices.sbc.com\DD_ECCRIX01" \
--username iDESK-REPORTUSER \
--password 'du3!Tax3s' \
--table "SDA_Stage.dbo.UsageAnalysis" \
--export-dir "hdfs://COREITKGMTNC20PROD01/sandbox/sandbox31/kpmg_ws/usageanalysis" \
--fields-terminated-by "\\001"                                                \
--lines-terminated-by "\\n"                                                   \
--input-null-string '\\N'                                                           \
--input-null-non-string '\\N'                                                       \
--batch

# 2,112,730


sqoop eval -libjars "/usr/hdp/2.2.8.0-3150/sqoop/lib/sqljdbc4.jar" \
--driver "com.microsoft.sqlserver.jdbc.SQLServerDriver" \
--connect "jdbc:sqlserver://Clddev0sql01683.itservices.sbc.com\DD_ECCRIX01" \
--username iDESK-REPORTUSER \
--password 'du3!Tax3s' \
--query "Truncate table SDA_Stage.dbo.elink_sum"



sqoop export -libjars "/usr/hdp/2.2.8.0-3150/sqoop/lib/sqljdbc4.jar" -m 64 \
--driver "com.microsoft.sqlserver.jdbc.SQLServerDriver" \
--connect "jdbc:sqlserver://Clddev0sql01683.itservices.sbc.com\DD_ECCRIX01" \
--username iDESK-REPORTUSER \
--password 'du3!Tax3s' \
--table "SDA_Stage.dbo.elink_sum" \
--export-dir "hdfs://COREITKGMTNC20PROD01/sandbox/sandbox31/kpmg/elink_sum" \
--fields-terminated-by "\\001"                                                \
--lines-terminated-by "\\n"                                                   \
--input-null-string '\\N'                                                           \
--input-null-non-string '\\N'                                                       \
--batch

sqoop eval -libjars "/usr/hdp/2.2.8.0-3150/sqoop/lib/sqljdbc4.jar" \
--driver "com.microsoft.sqlserver.jdbc.SQLServerDriver" \
--connect "jdbc:sqlserver://Clddev0sql01683.itservices.sbc.com\DD_ECCRIX01" \
--username iDESK-REPORTUSER \
--password 'du3!Tax3s' \
--query "Truncate table SDA_Stage.dbo.Organization"

 
 
sqoop export -libjars "/usr/hdp/2.2.8.0-3150/sqoop/lib/sqljdbc4.jar" -m 1 \
--driver "com.microsoft.sqlserver.jdbc.SQLServerDriver" \
--connect "jdbc:sqlserver://Clddev0sql01683.itservices.sbc.com\DD_ECCRIX01" \
--username iDESK-REPORTUSER \
--password 'du3!Tax3s' \
--table "SDA_Stage.dbo.organization" \
--export-dir "hdfs://COREITKGMTNC20PROD01/sandbox/sandbox31/kpmg_ws/organization" \
--fields-terminated-by "\\001"                                                \
--lines-terminated-by "\\n"                                                   \
--input-null-string '\\N'                                                           \
--input-null-non-string '\\N'                                                       \
--batch
 

sqoop eval -libjars "/usr/hdp/2.2.8.0-3150/sqoop/lib/sqljdbc4.jar" \
--driver "com.microsoft.sqlserver.jdbc.SQLServerDriver" \
--connect "jdbc:sqlserver://Clddev0sql01683.itservices.sbc.com\DD_ECCRIX01" \
--username iDESK-REPORTUSER \
--password 'du3!Tax3s' \
--query "Truncate table SDA_Stage.dbo.SecurityTable"

 
sqoop export -libjars "/usr/hdp/2.2.8.0-3150/sqoop/lib/sqljdbc4.jar" -m 1 \
--driver "com.microsoft.sqlserver.jdbc.SQLServerDriver" \
--connect "jdbc:sqlserver://Clddev0sql01683.itservices.sbc.com\DD_ECCRIX01" \
--username iDESK-REPORTUSER \
--password 'du3!Tax3s' \
--table "SDA_Stage.dbo.SecurityTable" \
--export-dir "hdfs://COREITKGMTNC20PROD01/sandbox/sandbox31/kpmg/securitytable" \
--fields-terminated-by "\\001"                                                \
--lines-terminated-by "\\n"                                                   \
--input-null-string '\\N'                                                           \
--input-null-non-string '\\N'                                                       \
--batch


sqoop eval -libjars "/usr/hdp/2.2.8.0-3150/sqoop/lib/sqljdbc4.jar" \
--driver "com.microsoft.sqlserver.jdbc.SQLServerDriver" \
--connect "jdbc:sqlserver://Clddev0sql01683.itservices.sbc.com\DD_ECCRIX01" \
--username iDESK-REPORTUSER \
--password 'du3!Tax3s' \
--query "Truncate table SDA_Stage.dbo.TOPAPPS"

sqoop export -libjars "/usr/hdp/2.2.8.0-3150/sqoop/lib/sqljdbc4.jar" -m 64 \
--driver "com.microsoft.sqlserver.jdbc.SQLServerDriver" \
--connect "jdbc:sqlserver://Clddev0sql01683.itservices.sbc.com\DD_ECCRIX01" \
--username iDESK-REPORTUSER \
--password 'du3!Tax3s' \
--table "SDA_Stage.dbo.TOPAPPS" \
--export-dir "hdfs://COREITKGMTNC20PROD01/sandbox/sandbox31/kpmg_ws/topapps" \
--fields-terminated-by "\\001"                                                \
--lines-terminated-by "\\n"                                                   \
--input-null-string '\\N'                                                           \
--input-null-non-string '\\N'                                                       \
--batch




