drop table kpmg_ws.workdays;
CREATE TABLE kpmg_ws.workdays
(
date string,
workday_count int
)
ROW format delimited fields terminated by "|"
location '/sandbox/sandbox31/kpmg_ws/workdays'
;

