use kpmg;
drop table kpmg.base ;

create table base as
select * from
(
select u.*
from 
kpmg.Organization u
inner join 
(
select attuid from kpmg.idesk_log_detail
group by attuid
) list
on (u.attuid=upper(list.attuid))
inner join kpmg_ws.whitelist list2
on (upper(u.attuid)=upper(list2.attuid))
)x cross join kpmg_ws.date_drive;


