import sys
import datetime as dt
import math
import datetime
def previous_quarter(ref):
    if ref.month < 4:
        return datetime.date(ref.year - 1, 12, 31)
    elif ref.month < 7:
        return datetime.date(ref.year, 3, 31)
    elif ref.month < 10:
        return datetime.date(ref.year, 6, 30)
    return datetime.date(ref.year, 9, 30)




start_date = dt.datetime(int(sys.argv[1][0:4]),int(sys.argv[1][4:6]),int(sys.argv[1][6:8]))
end_date = dt.datetime(int(sys.argv[2][0:4]),int(sys.argv[2][4:6]),int(sys.argv[2][6:8]))

total_days = (end_date - start_date).days + 1 #inclusive 5 days

for day_number in range(total_days):
    d = (start_date + dt.timedelta(days = day_number)).date()
    d_plus = (start_date + dt.timedelta(days = day_number+1)).date()
    #print "date|month|year|quater|weekNum|dayofweek|dayofMonth|dayofQ|dayofY|nameOfWeekDay"
    if str(d.year)==str(d_plus.year):
        print d.strftime('%Y%m%d')+"|"+ str(d.month)+'|'+str(d.year)+'|'+str(int(math.ceil(float(d.month) / 3)))+"|"+d_plus.strftime("%W")+"|"+str(d_plus.weekday())+"|"+str(d.day)+"|"+str((d-previous_quarter(d)).days)+"|"+d.strftime("%j")+"|"+d.strftime("%A")
    else:
        print d.strftime('%Y%m%d')+"|"+ str(d.month)+'|'+str(d.year)+'|'+str(int(math.ceil(float(d.month) / 3)))+"|"+d.strftime("%W")+"|"+str(d_plus.weekday())+"|"+str(d.day)+"|"+str((d-previous_quarter(d)).days)+"|"+d.strftime("%j")+"|"+d.strftime("%A")
