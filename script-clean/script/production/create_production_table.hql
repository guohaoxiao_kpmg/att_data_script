drop table kpmg_ws.production;
create table kpmg_ws.production
as
select base.attuid as user_id,regexp_replace(base.name,",","") as name,regexp_replace(base.job_title_name,",","-") as job_title_name, base.date as work_date,productive_time_hours,idle_lock_time_hours,exception_time_hours,i.overtime,if(absence_time is null,0,absence_time) as  absence_time,if(BREAK_TIME_HOURS_NO_LUNCH is null,0,BREAK_TIME_HOURS_NO_LUNCH) as break_time,if(MAX_BENEFIT_NO_LUNCH_CORRECTED is null,0,MAX_BENEFIT_NO_LUNCH_CORRECTED) as LEAKED_TIME,(8+i.overtime- if(absence_time is null,0,absence_time)) as incurred_time,  productive_time_hours+if(BREAK_TIME_HOURS_NO_LUNCH is null,0,BREAK_TIME_HOURS_NO_LUNCH) as EFFECTIVE_time, 33 as effective_rate,(unix_timestamp(end_time)-unix_timestamp(start_time))/3600 as work_time,if(o.total_task is null,0,o.total_task) as iol_total_task ,if(normalized_volume is null,0,normalized_volume) as iol_volume,  (productive_time_hours+if(BREAK_TIME_HOURS_NO_LUNCH is null,0,BREAK_TIME_HOURS_NO_LUNCH))/(if(normalized_volume is null,0,normalized_volume)+if(b.volume is null,0,b.volume)+if(s.volume is null,0,s.volume)) as actual_time_per_task,33* (productive_time_hours+if(BREAK_TIME_HOURS_NO_LUNCH is null,0,BREAK_TIME_HOURS_NO_LUNCH))/(if(normalized_volume is null,0,normalized_volume)+if(b.volume is null,0,b.volume)+if(s.volume is null,0,s.volume)) as actual_cost_per_task, (8+i.overtime-if(absence_time is null,0,absence_time))/(if(normalized_volume is null,0,normalized_volume)+if(b.volume is null,0,b.volume)+if(s.volume is null,0,s.volume)) as 	incurred_time_per_task, 33* (8+i.overtime-if(absence_time is null,0,absence_time))/(if(normalized_volume is null,0,normalized_volume)+if(b.volume is null,0,b.volume)+if(s.volume is null,0,s.volume))  as incurred_cost_per_task,if(b.volume is null,0,b.volume) as bert_volume, if(s.volume is null,0,s.volume) as global_ordering_volume
from  kpmg.base base
left join
Kpmg_ws.idesk_start_end_time time
on  upper(base.attuid)=time.attuid and base.date=time.date_id
left join
(
select attuid,date,if(productive_time_hours is null,0,productive_time_hours) as productive_time_hours, if(idle_lock_time_hours is null,0,idle_lock_time_hours) as idle_lock_time_hours, if(overtime is null,0,overtime) as overtime,if(exception_time_hours is null,0,exception_time_hours) as exception_time_hours
from
kpmg.usageanalysis )i 
on upper(base.attuid)=upper(i.attuid) and trim(base.date)=trim(regexp_replace(to_date( from_unixtime(unix_timestamp(i.date ,"mm/dd/yyyy"),'yyyy-mm-dd')),"-",""))
left join 
(
select attuid,calendar_day,if(absence_time is null,0,absence_time) as absence_time
from
kpmg.elink_sum )e
on upper(base.attuid)=e.attuid and base.date=regexp_replace(e.calendar_day,"-","")
left join kpmg.iol_sum o
on upper(base.attuid)=o.attuid and base.date=regexp_replace(o.date,"-","")
left join 
(
select uid,date,sum(NORMALIZED_VOLUME) as volume
from
kpmg_ws.BERT_PROD_SUMMARY
group by uid,date) b
on upper(base.attuid)=upper(trim(b.uid)) and trim(base.date)=trim(regexp_replace(b.date,"-",""))
left join 
(select uid,date,sum(total_nomalization) as volume from kpmg_ws.Global_Ordering_Prod_sum group by uid, date ) s
on upper(base.attuid)=upper(trim(s.uid)) and trim(base.date)=trim(regexp_replace(s.date,"-",""))
left join
(
select attuid,date,BREAK_TIME_HOURS_NO_LUNCH,MAX_BENEFIT_NO_LUNCH_CORRECTED from kpmg.benefits_daily_summary) benefit
on base.attuid=benefit.attuid and base.date=benefit.date
--where b.uid is not null or s.uid is not null or o.attuid is not null
;


drop table kpmg_ws.final_production;

create table kpmg_ws.final_production
as 
select 
user_id,
if(name is null,"",name) as name ,
if(job_title_name is null,"",job_title_name) as job_title_name         ,
if(work_date is null,"",work_date) as work_date              ,
if(productive_time_hours is null,"",productive_time_hours) as productive_time_hours ,
if(idle_lock_time_hours is null,"",idle_lock_time_hours) as idle_lock_time_hours   ,
if(exception_time_hours  is null,"",exception_time_hours) as exception_time_hours ,
if(overtime is null,"",overtime) as overtime               ,
if(absence_time is null,"",absence_time) as absence_time          ,
if(break_time   is null ,"", break_time) as break_time          ,
if(leaked_time is null,"",leaked_time) as leaked_time            ,
if(incurred_time is null,"",incurred_time) as incurred_time          ,
if(effective_time is null,"",effective_time) as effective_time         ,
if(effective_rate  is null,"",effective_rate) as effective_rate       ,
if(work_time   is null,"",work_time) as work_time           ,
if(iol_total_task is null,"",iol_total_task) as iol_total_task     ,
if(iol_volume  is null,"",iol_volume) as iol_volume           ,
if(actual_time_per_task is null,"",actual_time_per_task) as actual_time_per_task   ,
if(actual_cost_per_task is null,"",actual_cost_per_task) as actual_cost_per_task   ,
if(incurred_time_per_task is null,"",incurred_time_per_task) as incurred_time_per_task ,
if(incurred_cost_per_task is null,"",incurred_cost_per_task) as incurred_cost_per_task,
if(bert_volume is null,"",bert_volume)  as bert_volume ,
if(global_ordering_volume is null,"",global_ordering_volume) as global_ordering_volume
from kpmg_ws.production;


