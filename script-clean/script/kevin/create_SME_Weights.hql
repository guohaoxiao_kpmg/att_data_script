use kpmg_ws;
drop table SME_Weights;
create table SME_Weights
(
Event string, 
Product string, 
Ethernet_Switched_Ind string, 
PON_Newest_In_Region string, 
Order string, 
Fallout_Type string, 
Weight double
)
ROW format delimited fields terminated by "|"

