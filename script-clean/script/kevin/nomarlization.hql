set hive.cli.print.header=true;

drop table kpmg_ws.temp_kevin_raw ;
create table kpmg_ws.temp_kevin_raw as
select attuid,uso,circuit_id,if(event is null,"null",event) as event,completed,type,main_order_type,product,project_text,speed,if(ethernet_switched_ind is null,"null",ethernet_switched_ind) as ethernet_switched_ind, if(single_control_plane_ind is null,"null",single_control_plane_ind) as single_control_plane_ind, if(pon_newest_in_region_ind is null,"null",pon_newest_in_region_ind) as pon_newest_in_region_ind,spreadsheet
from  kpmg_ws.kevin_raw;


drop table kpmg_ws.temp_freq;
drop table kpmg_ws.temp_density;
drop table kpmg_ws.kevin_weight;
create table kpmg_ws.temp_freq as
select type,event,Ethernet_Switched_Ind,Single_Control_Plane_Ind,Pon_Newest_In_Region_Ind,sum(if(week='1',count,0)) as mon,sum(if(week='2',count,0)) as Tue,sum(if(week='3',count,0)) as Wed,sum(if(week='4',count,0)) as Thu,sum(if(week='5',count,0)) as Fri, sum(count) as sum, sum(count)/5 as avg,rank() over( partition by type order by sum(count) desc) as rank
from 
(
select type,  from_unixtime(unix_timestamp(to_date(completed),'yyyy-MM-dd'),'u') as week,event,Ethernet_Switched_Ind,Single_Control_Plane_Ind,Pon_Newest_In_Region_Ind,count(*) as count
from kpmg_ws.temp_kevin_raw
where completed is not null and event is not null and from_unixtime(unix_timestamp(to_date(completed),'yyyy-MM-dd'),'u')!='6' and from_unixtime(unix_timestamp(to_date(completed),'yyyy-MM-dd'),'u')!='7'
group by from_unixtime(unix_timestamp(to_date(completed),'yyyy-MM-dd'),'u'),event,type,Ethernet_Switched_Ind,Single_Control_Plane_Ind,Pon_Newest_In_Region_Ind) x
group by event,type,Ethernet_Switched_Ind,Single_Control_Plane_Ind,Pon_Newest_In_Region_Ind
;

create table kpmg_ws.temp_density as
select type,event,Ethernet_Switched_Ind,Single_Control_Plane_Ind,Pon_Newest_In_Region_Ind,sum(if(week='1',count,0)) as mon,sum(if(week='2',count,0)) as Tue,sum(if(week='3',count,0)) as Wed,sum(if(week='4',count,0)) as Thu,sum(if(week='5',count,0)) as Fri,
 sum(count) as sum,sum(count)/5 as avg,rank() over( partition by type order by sum(count) desc) as rank
from
(
select type, from_unixtime(unix_timestamp(to_date(completed),'yyyy-MM-dd'),'u') as week,event,Ethernet_Switched_Ind,Single_Control_Plane_Ind,Pon_Newest_In_Region_Ind,count(distinct attuid) as count
from kpmg_ws.temp_kevin_raw
where completed is not null and event is not null and from_unixtime(unix_timestamp(to_date(completed),'yyyy-MM-dd'),'u')!='6' and from_unixtime(unix_timestamp(to_date(completed),'yyyy-MM-dd'),'u')!='7'
group by from_unixtime(unix_timestamp(to_date(completed),'yyyy-MM-dd'),'u'),event,type,Ethernet_Switched_Ind,Single_Control_Plane_Ind,Pon_Newest_In_Region_Ind) x
group by event,type,Ethernet_Switched_Ind,Single_Control_Plane_Ind,Pon_Newest_In_Region_Ind
;

select * from kpmg_ws.temp_freq;
select * from kpmg_ws.temp_density;

create table kpmg_ws.kevin_weight
as
select f.type as type,f.event as event,f.Ethernet_Switched_Ind,f.Single_Control_Plane_Ind,f.Pon_Newest_In_Region_Ind,f.avg as  freq_avg,d.avg as den_avg,f.avg/d.avg as avg,f.rank as frank,d.rank as drank,f.rank+d.rank as total,(max+1)/(f.avg/d.avg+1) as mul  from
kpmg_ws.temp_freq f
--full outer 
inner join kpmg_ws.temp_density d
on f.event=d.event and f.type=d.type and f.Ethernet_Switched_Ind=d.Ethernet_Switched_Ind and f.Single_Control_Plane_Ind=d.Single_Control_Plane_Ind and f.Pon_Newest_In_Region_Ind = d.Pon_Newest_In_Region_Ind,
(
select type,max(avg) as max
from
(
select f.type as type,f.event as event,f.avg as  freq_avg,d.avg as den_avg,f.avg/d.avg as avg,f.rank as frank,d.rank as drank,f.rank+d.rank as total from
kpmg_ws.temp_freq f
--full outer 
inner join kpmg_ws.temp_density d
on  f.event=d.event and f.type=d.type and f.Ethernet_Switched_Ind=d.Ethernet_Switched_Ind and f.Single_Control_Plane_Ind=d.Single_Control_Plane_Ind and f.Pon_Newest_In_Region_Ind = d.Pon_Newest_In_Region_Ind)x
group by type
)y
where y.type=f.type
;


select * from kpmg_ws.kevin_weight;
