

#!/usr/bin/env python

import sys
import string
import hashlib
import re

while True:
  line = sys.stdin.readline()
  if not line:
    break
  map={}
  fields=line.split("\t")
  attuid=fields[0]
  date=fields[1]
  totalTask=fields[2]
  nomarlizedVolume=fields[3]
  elements = fields[4].strip("[").strip("]\n").split(",")
  for element in elements:
      key=element.replace('"','')
      if map.has_key(key):
          map[key]= map[key]+1
      else:
          map[key]=1
  list=[]
  for key in map:
      list.append(key+"\003"+str(map[key]))
  print attuid+"\t"+date+"\t"+totalTask+"\t"+nomarlizedVolume+"\t"+"\002".join(list)


