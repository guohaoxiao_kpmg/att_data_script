
use kpmg_ws;
drop table idat_agg;

create table idat_agg as
select * 
from(
select uso,Circuit_ID,Main_Order_Type,Product,Project_Text,Speed,Ethernet_Switched_Ind,Single_Control_Plane_Ind,Pon_Newest_In_Region_Ind,"cancelled" as SpreadSheet, "" as  OCO1_JEP 
from idat_hssd_scorecard_ethernet_linedata_mpr_uso_cancelled
union all
select uso,Circuit_ID,Main_Order_Type,Product,Project_Text,Speed,Ethernet_Switched_Ind,  Single_Control_Plane_Ind,Pon_Newest_In_Region_Ind,"completed" as SpreadSheet, OCO1_JEP 
from idat_hssd_scorecard_ethernet_linedata_mpr_uso_completed_dedup
union all
select uso,Circuit_ID,Main_Order_Type,Product,Project_Text,Speed,Ethernet_Switched_Ind,  Single_Control_Plane_Ind,Pon_Newest_In_Region_Ind,"issued" as SpreadSheet , OCO1_JEP 
from idat_hssd_scorecard_ethernet_linedata_mpr_uso_issued_dedup
union all
select uso,Circuit_ID,Main_Order_Type,Product,Project_Text,Speed,Ethernet_Switched_Ind,  Single_Control_Plane_Ind,Pon_Newest_In_Region_Ind,"wip" as SpreadSheet , "" as OCO1_JEP 
from idat_hssd_scorecard_ethernet_linedata_mpr_uso_wip
)x
;
use kpmg_ws;

drop table kevin_raw;
create table kevin_raw as 
select k.*,"data" as type,
if(a.Main_Order_Type is not null,a.Main_Order_Type,d.Main_Order_Type),if(a.Product is not null,a.Product,d.Product), if(a.Project_Text is not null,a.Project_Text,d.Project_Text) ,if( a.Speed is not null,a.Speed,d.Speed) ,if( a.Ethernet_Switched_Ind is not null, a.Ethernet_Switched_Ind, d.Ethernet_Switched_Ind ),  if(a.Single_Control_Plane_Ind is not null, a.Single_Control_Plane_Ind,d.Single_Control_Plane_Ind),if(a.Pon_Newest_In_Region_Ind is not null, a.Pon_Newest_In_Region_Ind,d.Pon_Newest_In_Region_Ind),if(a.SpreadSheet  is not null,a.SpreadSheet,"completed"), if(a.OCO1_JEP is not null, a.OCO1_JEP, d.OCO1_JEP)  from 
kevin_new k
left join idat_agg a
on regexp_replace(regexp_replace(concat(a.uso,'-',a.circuit_id)," ",""),"LL","") =   regexp_replace(regexp_replace(k.USO_Circuit_ID," ",""),"\t","")
left join 
kpmg_ws.idat_hssd_scorecard_ethernet_linedata_mpr_uso_completed_dedup d
on k.uso= regexp_replace(d.uso,'L','') and month(k.completed)= month(date_add('1899-12-30',cast(d.DD_Comp as int))) and  year(k.completed)= year(date_add('1899-12-30',cast(d.DD_Comp as int)))
;


select sum(count1),sum(count3),sum(count2),sum(count4),count(*)
from
(
select
if(a.Main_Order_Type is not null,1,0) as count1,if(a.Main_Order_Type is null,0,if(d.Main_Order_Type is not null,1,0)) as count2 , if(d.Main_Order_Type is not null,1,0) as count3,if(a.Main_Order_Type is not null,1,if(d.Main_Order_Type is not null,1,0)) as count4 from
kevin_new k
left join idat_agg a
on regexp_replace(regexp_replace(concat(a.uso,'-',a.circuit_id)," ",""),"LL","") =   regexp_replace(regexp_replace(k.USO_Circuit_ID," ",""),"\t","")
left join
kpmg_ws.idat_hssd_scorecard_ethernet_linedata_mpr_uso_completed_dedup d
on k.uso= regexp_replace(d.uso,'L','') and month(k.completed)= month(date_add('1899-12-30',cast(d.DD_Comp as int))) and  year(k.completed)= year(date_add('1899-12-30',cast(d.DD_Comp as int))) 
)x;

select
k.*
kevin_new k
left join idat_agg a
on regexp_replace(regexp_replace(concat(a.uso,'-',a.circuit_id)," ",""),"LL","") =   regexp_replace(regexp_replace(k.USO_Circuit_ID," ",""),"\t","")
left join
kpmg_ws.idat_hssd_scorecard_ethernet_linedata_mpr_uso_completed_dedup d
on k.uso= regexp_replace(d.uso,'L','') and month(k.completed)= month(date_add('1899-12-30',cast(d.DD_Comp as int))) and  year(k.completed)= year(date_add('1899-12-30',cast(d.DD_Comp as int)))
where a.Main_Order_Type is null and d.Main_Order_Type is null;


