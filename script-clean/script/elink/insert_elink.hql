set hive.exec.dynamic.partition.mode=nonstrict;
set hive.exec.dynamic.partition=true;
set hive.exec.max.dynamic.partitions.pernode=300;
set hive.exec.max.dynamic.partitions =10000000;
set hive.optimize.sort.dynamic.partition=true;
--set hive.execution.engine=tez;
set hive.execution.engine=tez;

set fs.file.impl.disable.cache=false;
set fs.hdfs.impl.disable.cache=false;
use kpmg_ws;
drop table kpmg_ws.temp_elink;
create table kpmg_ws.temp_elink as 
select 
attuid,
calendar_day ,
calendar_month ,
actual_time ,
org_unit ,
time_type ,
time_type_descr
 from kpmg.elink_standard
where calendar_day < ${hiveconf:date}
union all
select * from kpmg_ws.elink;
;


use kpmg;
drop table  elink_standard;

CREATE TABLE kpmg.elink_standard
(
attuid string,
calendar_day string,
calendar_month string,
actual_time double,
org_unit string,
time_type string,
time_type_descr string
)
partitioned by (start_month   string)
ROW format delimited fields terminated by "|"
location '/sandbox/sandbox31/kpmg/standard/elink_standard'
;

insert into table kpmg.elink_standard
PARTITION(start_month)
select 
upper(attuid),
calendar_day,
calendar_month,
actual_time,
upper(org_unit),
upper(time_type),
upper(time_type_descr),
calendar_month as start_month
from kpmg_ws.temp_elink
group by
attuid,
calendar_day,
calendar_month,
actual_time,
org_unit,
time_type,
time_type_descr;

