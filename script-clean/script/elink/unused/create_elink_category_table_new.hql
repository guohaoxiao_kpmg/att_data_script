drop table kpmg_ws.elink_absent;
drop table kpmg_ws.elink_vacation;
CREATE TABLE kpmg_ws.elink_absent
(
description string
)
ROW format delimited fields terminated by "|"
location '/sandbox/sandbox31/kpmg_ws/elink_vacation'
;

CREATE TABLE kpmg_ws.elink_overtime
(
description string
)
ROW format delimited fields terminated by "|"
location '/sandbox/sandbox31/kpmg/elink_overtime'
;


