#! /bin/bash
# * ----------------------------------------------------------------------------------
# * Project:         ATT
# *   Console script
# *
# * Log:
# *
# *    2015/11/30    Guohao xiao    copy ELink data from local directory and load it 
# *                                 into a partitioned table.
# * ----------------------------------------------------------------------------------

hadoop dfs -mv /sandbox/sandbox31/kpmg_ws/elink/* /sandbox/sandbox31/backup/elink_backup/
hadoop dfs -rmr  /sandbox/sandbox31/kpmg_ws/elink/*
hadoop dfs -copyFromLocal /opt/data/share05/sandbox/sandbox31/Elink_feed/*.txt /sandbox/sandbox31/kpmg_ws/elink/

hive -f /home/gc096s/guohao/script/elink/insert_elink.hql



hadoop dfs -chmod -R 755 /sandbox/sandbox31/kpmg/standard/elink*

