use kpmg;

create table elink_sum as 
select if(attuid is not null,attuid,uid) as attuid,if(attuid is not null,calendar_day,date) as calendar_day,if(attuid is not null,total_time,8) as total_time,if(attuid is not null,overtime,0) as overtime, if(attuid is not null,absence_time,8) as absence_time, if(attuid is not null,note,"Holiday Hours Not Originally in eLink Data") as note
from ( select i.*,z.attuid as uid,regexp_replace(z.date,"-","") as date from elink_sum_ini i
full outer join(
select * from (select attuid from elink_sum_ini 
        group by attuid)x
        cross join kpmg_ws.holiday
        where date<'20160715'
)z 
on i.attuid=z.attuid and i.calendar_day=regexp_replace(z.date,"-","")
) p;
