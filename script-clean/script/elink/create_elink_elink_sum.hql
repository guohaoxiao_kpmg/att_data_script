use kpmg;
set hive.execution.engine=tez;

--drop table kpmg_ws.elink_sum_old;

--create table kpmg_ws.elink_sum_old as
--select * from elink_sum;

drop table elink_sum_ini;
drop table elink_sum;
--drop table kpmg_ws.elink_sum_old;


--create table kpmg_ws.elink_sum_old as
--select attuid,calendar_day,sum(actual_time) as total_time, sum(if(c.overtime ='YES',actual_time,0)) as overtime,sum(if(c.absent='YES',actual_time,0)) as absence_time
--from kpmg.elink_standard e 
--left join kpmg_ws.elink_classification c
--on upper(trim(e.time_type_descr)) = upper(trim(c.description))
--group by attuid, calendar_day;




create table elink_sum_ini as
select 
attuid,calendar_day,if(h.date1 is null,total_time,if(absence_time=0,total_time+8,total_time)) as total_time,overtime,if(h.date1 is null,absence_time,if(absence_time=0,8,absence_time)) as absence_time,if(h.date1 is not null and absence_time=0,"Holiday Hours Not Originally in eLink Data","") as note , regular_hour
from
(
select attuid,calendar_day,sum(total_time) as total_time,sum(overtime) as overtime,sum(absence_time) as absence_time,sum(regular_hour) as regular_hour
from
(
select attuid,calendar_day,sum(actual_time) as total_time,overtime,sum(absence) as absence_time,sum(regular_hour) as regular_hour
from
(
select attuid,calendar_day,actual_time, if(c.overtime ='YES',actual_time,0) as overtime,if(c.absent='YES',actual_time,0) as absence,if(time_type="REGA" or time_type="REGP",actual_time,0) as regular_hour
from kpmg.elink_standard e 
left join kpmg_ws.elink_classification c
on upper(trim(e.time_type_descr)) = upper(trim(c.description))
)x
group by attuid, calendar_day,overtime
)y
group by attuid,calendar_day
) z 
left join
(select date_format as date1 from kpmg_ws.holiday) h
on z.calendar_day=regexp_replace(h.date1,"-","");


create table elink_sum as
select
 if(attuid is not null,attuid,uid) as attuid,if(attuid is not null,calendar_day,date1) as calendar_day,if(attuid is not null,total_time,8) as total_time,if(attuid is not null,overtime,0) as overtime, if(attuid is not null,absence_time,8) as absence_time,  if(attuid is not null,note,"Holiday Hours Not Originally in eLink Data") as note,  if(attuid is not null,regular_hour,0) as regular_hour
from
(
select i.*,z.attuid as uid,regexp_replace(z.date_format,"-","") as date1 from elink_sum_ini i
full outer join
(        
 select * from
  (select attuid from elink_sum_ini
    group by attuid)x
 cross join kpmg_ws.holiday  y
 where y.date_format<current_date) z 
on i.attuid=z.attuid and i.calendar_day=regexp_replace(z.date_format,"-","")
) g;


--select o.overtime-e.overtime,count(*) as c from 
--elink_sum e
--inner join kpmg_ws.elink_sum_old o
--on e.attuid=o.attuid and e.calendar_day=o.calendar_day
--group by 
--o.overtime-e.overtime
--order by c;
