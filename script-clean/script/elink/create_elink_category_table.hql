drop table  kpmg_ws.elink_classification;

CREATE TABLE kpmg_ws.elink_classification
(
description string,
absent string,
overtime string
)
ROW format delimited fields terminated by ","
location '/sandbox/sandbox31/kpmg_ws/standard/elink_classification'
;

