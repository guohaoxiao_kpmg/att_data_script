use kpmg_ws;

SELECT A.level,count(*),sum(if(A.level=B.level,1,0)),sum(if(cast(B.level as int)-cast(A.level as int)>1,1,0))
FROM my_webphone      A
  LEFT OUTER JOIN
     my_webphone      B
   ON A.SUPERVISOR_ATTUID = B.ATTUID
group by A.level
;


SELECT count(*),sum(if(A.level=B.level,1,0)),sum(if(cast(B.level as int)-cast(A.level as int)>1,1,0))
FROM my_webphone      A
  LEFT OUTER JOIN
     my_webphone      B
   ON A.SUPERVISOR_ATTUID = B.ATTUID
;

