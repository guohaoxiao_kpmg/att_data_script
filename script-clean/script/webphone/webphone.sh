#! /bin/bash

#  * ----------------------------------------------------------------------------------------------
#  * Project:         ATT iDesk POC
#  *                    
#  * Functionality: 
#  *                  The shell script that pull the webphone extract.
#  *                  and publish the webphone and org_chart tables in KPMG Hive schema.
#  * Log:
#  *    2015/11/30    John Liao      Initial. Based on Guohao's original. 
#  *    2015/12/03    John Liao      Add job_title_code into user_org
#  *    2015/12/21    John Liao      Use LVEL to determine REPORTING HIERARCHY, ADD JOB_TITLE_NAME
#
#  * ----------------------------------------------------------------------------------------------
echo "LinkNumber1+"|kinit

# Default location/directory
myLocalStageDir="/opt/data/share05/sandbox/sandbox31/webphone/"
myHDFSStageDir="/sandbox/sandbox31/stg/webphone/"
#myHDFSBackupDir="/sandbox/sandbox31/backup/webphone_backup/"
myHDFSBackupDir="/sandbox/sandbox31/tmp/"
myWebPhoneSource="ftp://extract.webphone.att.com/pub/attextractpeople.txt"

# timestamp to use
myTimeStamp=`date '+%Y-%m-%d-%H-%M-%S'`

wget ${myWebPhoneSource} -O ${myLocalStageDir}WebPhone_${myTimeStamp}.txt

hadoop dfs -mv ${myHDFSStageDir}* ${myHDFSBackupDir}
hadoop dfs -copyFromLocal ${myLocalStageDir}WebPhone_${myTimeStamp}.txt ${myHDFSStageDir}

sh /home/gc096s/guohao/script/WihteList/ingest.sh

# Hive script to massage the data
hive -v -e "
set hive.mapred.mode=nonstrict;
use KPMG_WS;

drop table if exists webphone_orig;

CREATE EXTERNAL TABLE webphone_orig 
(
country_code                       STRING,        
dept_ID                            STRING,        
dept_name                          STRING,
business_group_id                  STRING,
business_group_name                STRING,
business_subgroup                  STRING,
business_subgroup_name             STRING,
business_unit                      STRING,
business_unit_name                 STRING,
vp_org                             STRING,
vp_org_name                        STRING,
cel                                STRING,
city                               STRING,
country_desc                       STRING,
email                              STRING,
fax_tel_number                     STRING,
phone_extension                    STRING,
first_name                         STRING,
handle                             STRING,
hrid                               STRING,
job_title_code                     STRING,
JOB_TITLE_NAME                     STRING,
last_name                          STRING,
location_code                      STRING,
middle_name                        STRING,
costcenter                         STRING,
pager                              STRING,
pager_pin                          STRING,
preferred_name                     STRING,
prefix_name                        STRING,
room_number                        STRING,
state                              STRING,
status                             STRING,
address                            STRING,
name_suffix                        STRING,
telephone_number                   STRING,
level                              STRING,
title_name                         STRING,
zip                                STRING,
supervisor_hrid                    STRING,
a1_sbscuid_attuid                  STRING,
cogs                               STRING,
silo                               STRING,
Direct_total_reports               STRING,
Bellsouth_uid                      STRING,
Cingular_cuid                      STRING,
Financial_loc_code                 STRING,
Email_alias                        STRING,
payroll_id                         STRING,
consulting_company                 STRING,
management_level_indicator         STRING,
occupancy_indicator                STRING,
primary_gtr                        STRING,
secondary_gtr                      STRING,
cpid_record_indicator              STRING,
geographic_location_code           STRING,
architechtural_object_id           STRING,
company_code                       STRING,
company_description                STRING,
consultant_flag                    STRING,
preferred_mailing_address          STRING
)
ROW FORMAT DELIMITED
  FIELDS TERMINATED BY '|'
STORED AS INPUTFORMAT
  'org.apache.hadoop.mapred.TextInputFormat'
OUTPUTFORMAT
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  '${myHDFSStageDir}'
;

-- standardize into KPMG
use KPMG;

drop table if exists web_phone;

create table web_phone
stored as orc
as
SELECT UPPER(country_code              )      AS country_code              
      ,UPPER(dept_ID                   )      AS dept_ID                   
      ,UPPER(dept_name                 )      AS dept_name                 
      ,UPPER(business_group_id         )      AS business_group_id         
      ,UPPER(business_group_name       )      AS business_group_name       
      ,UPPER(business_subgroup         )      AS business_subgroup         
      ,UPPER(business_subgroup_name    )      AS business_subgroup_name    
      ,UPPER(business_unit             )      AS business_unit             
      ,UPPER(business_unit_name        )      AS business_unit_name        
      ,UPPER(vp_org                    )      AS vp_org 
      ,upper(telephone_number)                as telephone_number                   
      ,UPPER(vp_org_name               )      AS vp_org_name               
      ,UPPER(city                      )      AS city                      
      ,UPPER(country_desc              )      AS country_desc              
      ,UPPER(email                     )      AS email                     
      ,UPPER(first_name                )      AS first_name                
      ,UPPER(handle                    )      AS ATTUID
      ,UPPER(job_title_code            )      AS job_title_code            
      ,UPPER(JOB_TITLE_NAME            )      AS JOB_TITLE_NAME                   
      ,UPPER(last_name                 )      AS last_name                 
      ,UPPER(location_code             )      AS location_code             
      ,UPPER(middle_name               )      AS middle_name               
      ,UPPER(room_number               )      AS room_number               
      ,UPPER(state                     )      AS state                     
      ,UPPER(status                    )      AS status                    
      ,UPPER(address                   )      AS address                   
      ,UPPER(level                     )      AS level                     
      ,UPPER(title_name                )      AS title_name                
      ,UPPER(zip                       )      AS zip                       
      ,UPPER(supervisor_hrid           )      AS supervisor_hrid           
      ,UPPER(consulting_company        )      AS consulting_company        
      ,UPPER(management_level_indicator)      AS management_level_indicator
      ,UPPER(occupancy_indicator       )      AS occupancy_indicator       
      ,UPPER(primary_gtr               )      AS primary_gtr               
      ,UPPER(secondary_gtr             )      AS secondary_gtr             
      ,UPPER(company_code              )      AS company_code              
      ,UPPER(company_description       )      AS company_description       
      ,UPPER(consultant_flag           )      AS consultant_flag           
      ,UPPER(costcenter)               as costcenter
      ,UPPER(Financial_loc_code)       as Financial_loc_code
from KPMG_WS.WEBPHONE_ORIG
;

analyze table web_phone compute statistics;


-- from webphone, create USER_ORG
-- Build temp tables in KPMG_WS
USE KPMG_WS;

drop table if exists my_webphone;

create table my_webphone
as
select ATTUID
      ,CONCAT(FIRST_NAME,' ',MIDDLE_NAME,' ',LAST_NAME)   AS NAME
      ,FIRST_NAME                                                   AS FIRST_NAME
      ,MIDDLE_NAME                                                  AS MIDDLE_NAME
      ,LAST_NAME                                                    AS LAST_NAME
      ,DEPT_NAME                                                    AS DEPARTMENT
      ,CITY                                                         AS CITY
      ,STATE                                                        AS STATE
      ,COUNTRY_CODE                                                 AS COUNTRY_CODE
      ,COUNTRY_DESC                                                 AS COUNTRY_DESC
      ,JOB_TITLE_CODE                                               AS JOB_TITLE_CODE
      ,JOB_TITLE_NAME                                               AS JOB_TITLE_NAME
      ,LEVEL                                                        AS LEVEL
      ,STATUS                                                       AS STATUS
      ,SUPERVISOR_HRID                                              AS SUPERVISOR_ATTUID
      ,CONSULTING_COMPANY                                           AS CONSULTING_COMPANY
      ,CONSULTANT_FLAG                                              AS CONSULTANT_FLAG
      ,COMPANY_CODE                                                 AS COMPANY_CODE
      ,COMPANY_DESCRIPTION                                          AS COMPANY_DESCRIPTION
      ,telephone_number                                             as telephone_number
      ,costcenter as  costcenter
      ,Financial_loc_code as Financial_loc_code
FROM KPMG.web_phone
;
      
      
DROP TABLE if exists idesk_org1;

CREATE TABLE idesk_org1
AS

SELECT A.*
      ,B.ATTUID              AS L1_REPORT_TO_ATTUID
      ,B.NAME                AS L1_REPORT_TO_NAME
      ,B.LEVEL               AS L1_REPORT_TO_LEVEL
      ,B.JOB_TITLE_CODE      AS L1_REPORT_TO_JOB_TITLE
      ,B.JOB_TITLE_NAME      AS L1_REPORT_TO_JOB_TITLE_NAME
      ,B.SUPERVISOR_ATTUID   AS L2_REPORT_TO_ATTUID
FROM my_webphone      A
  LEFT OUTER JOIN
     my_webphone      B
   ON A.SUPERVISOR_ATTUID = B.ATTUID
;


DROP TABLE if exists idesk_org2;

CREATE TABLE idesk_org2
AS
SELECT A.*
      ,B.NAME                AS L2_REPORT_TO_NAME
      ,B.LEVEL               AS L2_REPORT_TO_LEVEL
      ,B.JOB_TITLE_CODE      AS L2_REPORT_TO_JOB_TITLE
      ,B.JOB_TITLE_NAME      AS L2_REPORT_TO_JOB_TITLE_NAME
      ,B.SUPERVISOR_ATTUID   AS L3_REPORT_TO_ATTUID
FROM idesk_org1       A
  LEFT OUTER JOIN
     my_webphone      B
   ON A.L2_REPORT_TO_ATTUID = B.ATTUID
;

DROP TABLE if exists idesk_org3;

CREATE TABLE idesk_org3
AS
SELECT A.*
      ,B.NAME                AS L3_REPORT_TO_NAME
      ,B.LEVEL               AS L3_REPORT_TO_LEVEL
      ,B.JOB_TITLE_CODE      AS L3_REPORT_TO_JOB_TITLE
      ,B.JOB_TITLE_NAME      AS L3_REPORT_TO_JOB_TITLE_NAME
      ,B.SUPERVISOR_ATTUID   AS L4_REPORT_TO_ATTUID
FROM idesk_org2       A
  LEFT OUTER JOIN
     my_webphone      B
   ON A.L3_REPORT_TO_ATTUID = B.ATTUID
;


DROP TABLE if exists idesk_org4;

CREATE TABLE idesk_org4
AS
SELECT A.*
      ,B.NAME                AS L4_REPORT_TO_NAME
      ,B.LEVEL               AS L4_REPORT_TO_LEVEL
      ,B.JOB_TITLE_CODE      AS L4_REPORT_TO_JOB_TITLE
      ,B.JOB_TITLE_NAME             AS L4_REPORT_TO_JOB_TITLE_NAME
      ,B.SUPERVISOR_ATTUID   AS L5_REPORT_TO_ATTUID
FROM idesk_org3       A
  LEFT OUTER JOIN
     my_webphone      B
   ON A.L4_REPORT_TO_ATTUID = B.ATTUID
;

DROP TABLE if exists idesk_org5;

CREATE TABLE idesk_org5
AS
SELECT A.*
      ,B.NAME                AS L5_REPORT_TO_NAME
      ,B.LEVEL               AS L5_REPORT_TO_LEVEL
      ,B.JOB_TITLE_CODE      AS L5_REPORT_TO_JOB_TITLE
      ,B.JOB_TITLE_NAME      AS L5_REPORT_TO_JOB_TITLE_NAME
      ,B.SUPERVISOR_ATTUID   AS L6_REPORT_TO_ATTUID
FROM idesk_org4       A
  LEFT OUTER JOIN
     my_webphone      B
   ON A.L5_REPORT_TO_ATTUID = B.ATTUID
;

DROP TABLE if exists idesk_org6;

CREATE TABLE idesk_org6
AS
SELECT A.*
      ,B.NAME                AS L6_REPORT_TO_NAME
      ,B.LEVEL               AS L6_REPORT_TO_LEVEL
      ,B.JOB_TITLE_CODE      AS L6_REPORT_TO_JOB_TITLE
      ,B.JOB_TITLE_NAME      AS L6_REPORT_TO_JOB_TITLE_NAME
      ,B.SUPERVISOR_ATTUID   AS L7_REPORT_TO_ATTUID
FROM idesk_org5       A
  LEFT OUTER JOIN
     my_webphone      B
   ON A.L6_REPORT_TO_ATTUID = B.ATTUID
;

DROP TABLE if exists idesk_org7;

CREATE TABLE idesk_org7
AS
SELECT A.*
      ,B.NAME                AS L7_REPORT_TO_NAME
      ,B.LEVEL               AS L7_REPORT_TO_LEVEL
      ,B.JOB_TITLE_CODE      AS L7_REPORT_TO_JOB_TITLE
      ,B.JOB_TITLE_NAME      AS L7_REPORT_TO_JOB_TITLE_NAME
      ,B.SUPERVISOR_ATTUID   AS L8_REPORT_TO_ATTUID
FROM idesk_org6       A
  LEFT OUTER JOIN
     my_webphone      B
   ON A.L7_REPORT_TO_ATTUID = B.ATTUID
;

DROP TABLE if exists idesk_org8;

CREATE TABLE idesk_org8
AS
SELECT A.*
      ,B.NAME                AS L8_REPORT_TO_NAME
      ,B.LEVEL               AS L8_REPORT_TO_LEVEL
      ,B.JOB_TITLE_CODE      AS L8_REPORT_TO_JOB_TITLE
      ,B.JOB_TITLE_NAME      AS L8_REPORT_TO_JOB_TITLE_NAME
      ,B.SUPERVISOR_ATTUID   AS L9_REPORT_TO_ATTUID
FROM idesk_org7       A
  LEFT OUTER JOIN
     my_webphone      B
   ON A.L8_REPORT_TO_ATTUID = B.ATTUID
;

DROP TABLE if exists idesk_org9;

CREATE TABLE idesk_org9
AS
SELECT A.*
      ,B.NAME                AS L9_REPORT_TO_NAME
      ,B.LEVEL               AS L9_REPORT_TO_LEVEL
      ,B.JOB_TITLE_CODE      AS L9_REPORT_TO_JOB_TITLE
      ,B.JOB_TITLE_NAME      AS L9_REPORT_TO_JOB_TITLE_NAME
      ,B.SUPERVISOR_ATTUID   AS L10_REPORT_TO_ATTUID
FROM idesk_org8       A
  LEFT OUTER JOIN
     my_webphone      B
   ON A.L9_REPORT_TO_ATTUID = B.ATTUID
;


DROP TABLE if exists idesk_org10;

CREATE TABLE idesk_org10
AS
SELECT A.*
      ,B.NAME                AS L10_REPORT_TO_NAME
      ,B.LEVEL               AS L10_REPORT_TO_LEVEL
      ,B.JOB_TITLE_CODE      AS L10_REPORT_TO_JOB_TITLE
      ,B.JOB_TITLE_NAME      AS L10_REPORT_TO_JOB_TITLE_NAME
      ,B.SUPERVISOR_ATTUID   AS L11_REPORT_TO_ATTUID
FROM idesk_org9       A
  LEFT OUTER JOIN
     my_webphone      B
   ON A.L10_REPORT_TO_ATTUID = B.ATTUID
;


DROP TABLE if exists idesk_org11;

CREATE TABLE idesk_org11
AS
SELECT A.*
      ,B.NAME                AS L11_REPORT_TO_NAME
      ,B.LEVEL               AS L11_REPORT_TO_LEVEL
      ,B.JOB_TITLE_CODE      AS L11_REPORT_TO_JOB_TITLE
      ,B.JOB_TITLE_NAME      AS L11_REPORT_TO_JOB_TITLE_NAME
      ,B.SUPERVISOR_ATTUID   AS L12_REPORT_TO_ATTUID
FROM idesk_org10       A
  LEFT OUTER JOIN
     my_webphone      B
   ON A.L11_REPORT_TO_ATTUID = B.ATTUID
;

DROP TABLE if exists idesk_org12;

CREATE TABLE idesk_org12
AS
SELECT A.*
      ,B.NAME                AS L12_REPORT_TO_NAME
      ,B.LEVEL               AS L12_REPORT_TO_LEVEL
      ,B.JOB_TITLE_CODE      AS L12_REPORT_TO_JOB_TITLE
      ,B.JOB_TITLE_NAME      AS L12_REPORT_TO_JOB_TITLE_NAME
      ,B.SUPERVISOR_ATTUID   AS L13_REPORT_TO_ATTUID
FROM idesk_org11       A
  LEFT OUTER JOIN
     my_webphone      B
   ON A.L12_REPORT_TO_ATTUID = B.ATTUID
;

DROP TABLE if exists idesk_org13;

CREATE TABLE idesk_org13
AS
SELECT A.*
      ,B.NAME                AS L13_REPORT_TO_NAME
      ,B.LEVEL               AS L13_REPORT_TO_LEVEL
      ,B.JOB_TITLE_CODE      AS L13_REPORT_TO_JOB_TITLE
      ,B.JOB_TITLE_NAME      AS L13_REPORT_TO_JOB_TITLE_NAME
      ,B.SUPERVISOR_ATTUID   AS L14_REPORT_TO_ATTUID
FROM idesk_org12       A
  LEFT OUTER JOIN
     my_webphone      B
   ON A.L13_REPORT_TO_ATTUID = B.ATTUID
;

DROP TABLE if exists idesk_org14;

CREATE TABLE idesk_org14
AS
SELECT A.*
      ,B.NAME                AS L14_REPORT_TO_NAME
      ,B.LEVEL               AS L14_REPORT_TO_LEVEL
      ,B.JOB_TITLE_CODE      AS L14_REPORT_TO_JOB_TITLE
      ,B.JOB_TITLE_NAME      AS L14_REPORT_TO_JOB_TITLE_NAME
      ,B.SUPERVISOR_ATTUID   AS L15_REPORT_TO_ATTUID
FROM idesk_org13       A
  LEFT OUTER JOIN
     my_webphone      B
   ON A.L14_REPORT_TO_ATTUID = B.ATTUID
;


-- CREATE USER_ORG in KPMG
USE KPMG_ws;

DROP TABLE IF EXISTS Organization_before_whitelist;


CREATE TABLE Organization_before_whitelist
AS
SELECT *
FROM
(
SELECT ATTUID
      ,NAME
      ,first_name
      ,last_name
      ,DEPARTMENT
      ,CITY
      ,STATE
      ,COUNTRY_CODE AS COUNTRY
      ,LEVEL
      ,JOB_TITLE_CODE
      ,JOB_TITLE_NAME
      ,costcenter
      ,Financial_loc_code
      ,CASE WHEN L8_REPORT_TO_LEVEL  = '6' THEN L8_REPORT_TO_attuid
            WHEN L7_REPORT_TO_LEVEL  = '6' THEN L7_REPORT_TO_attuid
            WHEN L6_REPORT_TO_LEVEL  = '6' THEN L6_REPORT_TO_attuid
            WHEN L5_REPORT_TO_LEVEL  = '6' THEN L5_REPORT_TO_attuid
            WHEN L4_REPORT_TO_LEVEL  = '6' THEN L4_REPORT_TO_attuid
            WHEN L3_REPORT_TO_LEVEL  = '6' THEN L3_REPORT_TO_attuid
            WHEN L2_REPORT_TO_LEVEL  = '6' THEN L2_REPORT_TO_attuid
            WHEN L1_REPORT_TO_LEVEL  = '6' THEN L1_REPORT_TO_attuid
            ELSE ''
       END        AS SVP_attuid
       ,CASE WHEN L8_REPORT_TO_LEVEL  = '6' THEN L8_REPORT_TO_LEVEL
            WHEN L7_REPORT_TO_LEVEL  = '6' THEN L7_REPORT_TO_LEVEL
            WHEN L6_REPORT_TO_LEVEL  = '6' THEN L6_REPORT_TO_LEVEL
            WHEN L5_REPORT_TO_LEVEL  = '6' THEN L5_REPORT_TO_LEVEL
            WHEN L4_REPORT_TO_LEVEL  = '6' THEN L4_REPORT_TO_LEVEL
            WHEN L3_REPORT_TO_LEVEL  = '6' THEN L3_REPORT_TO_LEVEL
            WHEN L2_REPORT_TO_LEVEL  = '6' THEN L2_REPORT_TO_LEVEL
            WHEN L1_REPORT_TO_LEVEL  = '6' THEN L1_REPORT_TO_LEVEL
            ELSE ''
       END        AS SVP_LEVEL


      ,CASE WHEN L8_REPORT_TO_LEVEL  = '6' THEN L8_REPORT_TO_NAME
            WHEN L7_REPORT_TO_LEVEL  = '6' THEN L7_REPORT_TO_NAME
            WHEN L6_REPORT_TO_LEVEL  = '6' THEN L6_REPORT_TO_NAME
            WHEN L5_REPORT_TO_LEVEL  = '6' THEN L5_REPORT_TO_NAME
            WHEN L4_REPORT_TO_LEVEL  = '6' THEN L4_REPORT_TO_NAME
            WHEN L3_REPORT_TO_LEVEL  = '6' THEN L3_REPORT_TO_NAME
            WHEN L2_REPORT_TO_LEVEL  = '6' THEN L2_REPORT_TO_NAME
            WHEN L1_REPORT_TO_LEVEL  = '6' THEN L1_REPORT_TO_NAME
            ELSE ''
       END        AS SVP_NAME
      ,CASE WHEN L8_REPORT_TO_LEVEL  = '6' THEN L8_REPORT_TO_JOB_TITLE_NAME
            WHEN L7_REPORT_TO_LEVEL  = '6' THEN L7_REPORT_TO_JOB_TITLE_NAME
            WHEN L6_REPORT_TO_LEVEL  = '6' THEN L6_REPORT_TO_JOB_TITLE_NAME
            WHEN L5_REPORT_TO_LEVEL  = '6' THEN L5_REPORT_TO_JOB_TITLE_NAME
            WHEN L4_REPORT_TO_LEVEL  = '6' THEN L4_REPORT_TO_JOB_TITLE_NAME
            WHEN L3_REPORT_TO_LEVEL  = '6' THEN L3_REPORT_TO_JOB_TITLE_NAME
            WHEN L2_REPORT_TO_LEVEL  = '6' THEN L2_REPORT_TO_JOB_TITLE_NAME
            WHEN L1_REPORT_TO_LEVEL  = '6' THEN L1_REPORT_TO_JOB_TITLE_NAME
            ELSE ''
       END        AS SVP_JOB_TITLE
       ,CASE WHEN L8_REPORT_TO_LEVEL  = '5' THEN L8_REPORT_TO_attuid
            WHEN L7_REPORT_TO_LEVEL  = '5' THEN L7_REPORT_TO_attuid
            WHEN L6_REPORT_TO_LEVEL  = '5' THEN L6_REPORT_TO_attuid
            WHEN L5_REPORT_TO_LEVEL  = '5' THEN L5_REPORT_TO_attuid
            WHEN L4_REPORT_TO_LEVEL  = '5' THEN L4_REPORT_TO_attuid
            WHEN L3_REPORT_TO_LEVEL  = '5' THEN L3_REPORT_TO_attuid
            WHEN L2_REPORT_TO_LEVEL  = '5' THEN L2_REPORT_TO_attuid
            WHEN L1_REPORT_TO_LEVEL  = '5' THEN L1_REPORT_TO_attuid
            ELSE ''
       END        AS VP_attuid
       ,CASE WHEN L8_REPORT_TO_LEVEL  = '5' THEN L8_REPORT_TO_LEVEL
            WHEN L7_REPORT_TO_LEVEL  = '5' THEN L7_REPORT_TO_LEVEL
            WHEN L6_REPORT_TO_LEVEL  = '5' THEN L6_REPORT_TO_LEVEL
            WHEN L5_REPORT_TO_LEVEL  = '5' THEN L5_REPORT_TO_LEVEL
            WHEN L4_REPORT_TO_LEVEL  = '5' THEN L4_REPORT_TO_LEVEL
            WHEN L3_REPORT_TO_LEVEL  = '5' THEN L3_REPORT_TO_LEVEL
            WHEN L2_REPORT_TO_LEVEL  = '5' THEN L2_REPORT_TO_LEVEL
            WHEN L1_REPORT_TO_LEVEL  = '5' THEN L1_REPORT_TO_LEVEL
            ELSE ''
       END        AS VP_LEVEL


      ,CASE WHEN L8_REPORT_TO_LEVEL  = '5' THEN L8_REPORT_TO_NAME
            WHEN L7_REPORT_TO_LEVEL  = '5' THEN L7_REPORT_TO_NAME
            WHEN L6_REPORT_TO_LEVEL  = '5' THEN L6_REPORT_TO_NAME
            WHEN L5_REPORT_TO_LEVEL  = '5' THEN L5_REPORT_TO_NAME
            WHEN L4_REPORT_TO_LEVEL  = '5' THEN L4_REPORT_TO_NAME
            WHEN L3_REPORT_TO_LEVEL  = '5' THEN L3_REPORT_TO_NAME
            WHEN L2_REPORT_TO_LEVEL  = '5' THEN L2_REPORT_TO_NAME
            WHEN L1_REPORT_TO_LEVEL  = '5' THEN L1_REPORT_TO_NAME
            ELSE ''
       END        AS VP_NAME
      ,CASE WHEN L8_REPORT_TO_LEVEL  = '5' THEN L8_REPORT_TO_JOB_TITLE_NAME
            WHEN L7_REPORT_TO_LEVEL  = '5' THEN L7_REPORT_TO_JOB_TITLE_NAME
            WHEN L6_REPORT_TO_LEVEL  = '5' THEN L6_REPORT_TO_JOB_TITLE_NAME
            WHEN L5_REPORT_TO_LEVEL  = '5' THEN L5_REPORT_TO_JOB_TITLE_NAME
            WHEN L4_REPORT_TO_LEVEL  = '5' THEN L4_REPORT_TO_JOB_TITLE_NAME
            WHEN L3_REPORT_TO_LEVEL  = '5' THEN L3_REPORT_TO_JOB_TITLE_NAME
            WHEN L2_REPORT_TO_LEVEL  = '5' THEN L2_REPORT_TO_JOB_TITLE_NAME
            WHEN L1_REPORT_TO_LEVEL  = '5' THEN L1_REPORT_TO_JOB_TITLE_NAME
            ELSE ''
       END        AS VP_JOB_TITLE
      ,CASE WHEN L8_REPORT_TO_LEVEL  = '4' THEN L8_REPORT_TO_attuid
            WHEN L7_REPORT_TO_LEVEL  = '4' THEN L7_REPORT_TO_attuid
            WHEN L6_REPORT_TO_LEVEL  = '4' THEN L6_REPORT_TO_attuid
            WHEN L5_REPORT_TO_LEVEL  = '4' THEN L5_REPORT_TO_attuid
            WHEN L4_REPORT_TO_LEVEL  = '4' THEN L4_REPORT_TO_attuid
            WHEN L3_REPORT_TO_LEVEL  = '4' THEN L3_REPORT_TO_attuid
            WHEN L2_REPORT_TO_LEVEL  = '4' THEN L2_REPORT_TO_attuid
            WHEN L1_REPORT_TO_LEVEL  = '4' THEN L1_REPORT_TO_attuid
            ELSE ''
       END        AS AVP_attuid
      ,CASE WHEN L8_REPORT_TO_LEVEL  = '4' THEN L8_REPORT_TO_LEVEL
            WHEN L7_REPORT_TO_LEVEL  = '4' THEN L7_REPORT_TO_LEVEL
            WHEN L6_REPORT_TO_LEVEL  = '4' THEN L6_REPORT_TO_LEVEL
            WHEN L5_REPORT_TO_LEVEL  = '4' THEN L5_REPORT_TO_LEVEL
            WHEN L4_REPORT_TO_LEVEL  = '4' THEN L4_REPORT_TO_LEVEL
            WHEN L3_REPORT_TO_LEVEL  = '4' THEN L3_REPORT_TO_LEVEL
            WHEN L2_REPORT_TO_LEVEL  = '4' THEN L2_REPORT_TO_LEVEL
            WHEN L1_REPORT_TO_LEVEL  = '4' THEN L1_REPORT_TO_LEVEL
            ELSE ''
       END        AS AVP_LEVEL


      ,CASE WHEN L8_REPORT_TO_LEVEL  = '4' THEN L8_REPORT_TO_NAME
            WHEN L7_REPORT_TO_LEVEL  = '4' THEN L7_REPORT_TO_NAME
            WHEN L6_REPORT_TO_LEVEL  = '4' THEN L6_REPORT_TO_NAME
            WHEN L5_REPORT_TO_LEVEL  = '4' THEN L5_REPORT_TO_NAME
            WHEN L4_REPORT_TO_LEVEL  = '4' THEN L4_REPORT_TO_NAME
            WHEN L3_REPORT_TO_LEVEL  = '4' THEN L3_REPORT_TO_NAME
            WHEN L2_REPORT_TO_LEVEL  = '4' THEN L2_REPORT_TO_NAME
            WHEN L1_REPORT_TO_LEVEL  = '4' THEN L1_REPORT_TO_NAME
            ELSE ''
       END        AS AVP_NAME
      ,CASE WHEN L8_REPORT_TO_LEVEL  = '4' THEN L8_REPORT_TO_JOB_TITLE_NAME
            WHEN L7_REPORT_TO_LEVEL  = '4' THEN L7_REPORT_TO_JOB_TITLE_NAME
            WHEN L6_REPORT_TO_LEVEL  = '4' THEN L6_REPORT_TO_JOB_TITLE_NAME
            WHEN L5_REPORT_TO_LEVEL  = '4' THEN L5_REPORT_TO_JOB_TITLE_NAME
            WHEN L4_REPORT_TO_LEVEL  = '4' THEN L4_REPORT_TO_JOB_TITLE_NAME
            WHEN L3_REPORT_TO_LEVEL  = '4' THEN L3_REPORT_TO_JOB_TITLE_NAME
            WHEN L2_REPORT_TO_LEVEL  = '4' THEN L2_REPORT_TO_JOB_TITLE_NAME
            WHEN L1_REPORT_TO_LEVEL  = '4' THEN L1_REPORT_TO_JOB_TITLE_NAME
            ELSE ''
       END        AS AVP_JOB_TITLE
        ,CASE WHEN L8_REPORT_TO_LEVEL  = '3' THEN L8_REPORT_TO_attuid
            WHEN L7_REPORT_TO_LEVEL  = '3' THEN L7_REPORT_TO_attuid
            WHEN L6_REPORT_TO_LEVEL  = '3' THEN L6_REPORT_TO_attuid
            WHEN L5_REPORT_TO_LEVEL  = '3' THEN L5_REPORT_TO_attuid
            WHEN L4_REPORT_TO_LEVEL  = '3' THEN L4_REPORT_TO_attuid
            WHEN L3_REPORT_TO_LEVEL  = '3' THEN L3_REPORT_TO_attuid
            WHEN L2_REPORT_TO_LEVEL  = '3' THEN L2_REPORT_TO_attuid
            WHEN L1_REPORT_TO_LEVEL  = '3' THEN L1_REPORT_TO_attuid
            ELSE ''
       END        AS DIRECTOR_attuid
       ,CASE WHEN L8_REPORT_TO_LEVEL  = '3' THEN L8_REPORT_TO_LEVEL
            WHEN L7_REPORT_TO_LEVEL  = '3' THEN L7_REPORT_TO_LEVEL
            WHEN L6_REPORT_TO_LEVEL  = '3' THEN L6_REPORT_TO_LEVEL
            WHEN L5_REPORT_TO_LEVEL  = '3' THEN L5_REPORT_TO_LEVEL
            WHEN L4_REPORT_TO_LEVEL  = '3' THEN L4_REPORT_TO_LEVEL
            WHEN L3_REPORT_TO_LEVEL  = '3' THEN L3_REPORT_TO_LEVEL
            WHEN L2_REPORT_TO_LEVEL  = '3' THEN L2_REPORT_TO_LEVEL
            WHEN L1_REPORT_TO_LEVEL  = '3' THEN L1_REPORT_TO_LEVEL
            ELSE ''
       END        AS DIRECTOR_LEVEL


      ,CASE WHEN L8_REPORT_TO_LEVEL  = '3' THEN L8_REPORT_TO_NAME
            WHEN L7_REPORT_TO_LEVEL  = '3' THEN L7_REPORT_TO_NAME
            WHEN L6_REPORT_TO_LEVEL  = '3' THEN L6_REPORT_TO_NAME
            WHEN L5_REPORT_TO_LEVEL  = '3' THEN L5_REPORT_TO_NAME
            WHEN L4_REPORT_TO_LEVEL  = '3' THEN L4_REPORT_TO_NAME
            WHEN L3_REPORT_TO_LEVEL  = '3' THEN L3_REPORT_TO_NAME
            WHEN L2_REPORT_TO_LEVEL  = '3' THEN L2_REPORT_TO_NAME
            WHEN L1_REPORT_TO_LEVEL  = '3' THEN L1_REPORT_TO_NAME
            ELSE ''
       END        AS DIRECTOR_NAME
      ,CASE WHEN L8_REPORT_TO_LEVEL  = '3' THEN L8_REPORT_TO_JOB_TITLE_NAME
            WHEN L7_REPORT_TO_LEVEL  = '3' THEN L7_REPORT_TO_JOB_TITLE_NAME
            WHEN L6_REPORT_TO_LEVEL  = '3' THEN L6_REPORT_TO_JOB_TITLE_NAME
            WHEN L5_REPORT_TO_LEVEL  = '3' THEN L5_REPORT_TO_JOB_TITLE_NAME
            WHEN L4_REPORT_TO_LEVEL  = '3' THEN L4_REPORT_TO_JOB_TITLE_NAME
            WHEN L3_REPORT_TO_LEVEL  = '3' THEN L3_REPORT_TO_JOB_TITLE_NAME
            WHEN L2_REPORT_TO_LEVEL  = '3' THEN L2_REPORT_TO_JOB_TITLE_NAME
            WHEN L1_REPORT_TO_LEVEL  = '3' THEN L1_REPORT_TO_JOB_TITLE_NAME
            ELSE ''
       END        AS DIRECTOR_JOB_TITLE
      ,CASE WHEN L8_REPORT_TO_LEVEL  = '2' THEN L8_REPORT_TO_attuid
            WHEN L7_REPORT_TO_LEVEL  = '2' THEN L7_REPORT_TO_attuid
            WHEN L6_REPORT_TO_LEVEL  = '2' THEN L6_REPORT_TO_attuid
            WHEN L5_REPORT_TO_LEVEL  = '2' THEN L5_REPORT_TO_attuid
            WHEN L4_REPORT_TO_LEVEL  = '2' THEN L4_REPORT_TO_attuid
            WHEN L3_REPORT_TO_LEVEL  = '2' THEN L3_REPORT_TO_attuid
            WHEN L2_REPORT_TO_LEVEL  = '2' THEN L2_REPORT_TO_attuid
            WHEN L1_REPORT_TO_LEVEL  = '2' THEN L1_REPORT_TO_attuid
            ELSE ''
       END        AS AREA_MANAGER_attuid
       ,CASE WHEN L8_REPORT_TO_LEVEL  = '2' THEN L8_REPORT_TO_LEVEL
            WHEN L7_REPORT_TO_LEVEL  = '2' THEN L7_REPORT_TO_LEVEL
            WHEN L6_REPORT_TO_LEVEL  = '2' THEN L6_REPORT_TO_LEVEL
            WHEN L5_REPORT_TO_LEVEL  = '2' THEN L5_REPORT_TO_LEVEL
            WHEN L4_REPORT_TO_LEVEL  = '2' THEN L4_REPORT_TO_LEVEL
            WHEN L3_REPORT_TO_LEVEL  = '2' THEN L3_REPORT_TO_LEVEL
            WHEN L2_REPORT_TO_LEVEL  = '2' THEN L2_REPORT_TO_LEVEL
            WHEN L1_REPORT_TO_LEVEL  = '2' THEN L1_REPORT_TO_LEVEL
            ELSE ''
       END        AS AREA_MANAGER_LEVEL

       
      ,CASE WHEN L8_REPORT_TO_LEVEL  = '2' THEN L8_REPORT_TO_NAME
            WHEN L7_REPORT_TO_LEVEL  = '2' THEN L7_REPORT_TO_NAME
            WHEN L6_REPORT_TO_LEVEL  = '2' THEN L6_REPORT_TO_NAME
            WHEN L5_REPORT_TO_LEVEL  = '2' THEN L5_REPORT_TO_NAME
            WHEN L4_REPORT_TO_LEVEL  = '2' THEN L4_REPORT_TO_NAME
            WHEN L3_REPORT_TO_LEVEL  = '2' THEN L3_REPORT_TO_NAME
            WHEN L2_REPORT_TO_LEVEL  = '2' THEN L2_REPORT_TO_NAME
            WHEN L1_REPORT_TO_LEVEL  = '2' THEN L1_REPORT_TO_NAME
            ELSE ''
       END        AS AREA_MANAGER_NAME
      ,CASE WHEN L8_REPORT_TO_LEVEL  = '2' THEN L8_REPORT_TO_JOB_TITLE_NAME
            WHEN L7_REPORT_TO_LEVEL  = '2' THEN L7_REPORT_TO_JOB_TITLE_NAME
            WHEN L6_REPORT_TO_LEVEL  = '2' THEN L6_REPORT_TO_JOB_TITLE_NAME
            WHEN L5_REPORT_TO_LEVEL  = '2' THEN L5_REPORT_TO_JOB_TITLE_NAME
            WHEN L4_REPORT_TO_LEVEL  = '2' THEN L4_REPORT_TO_JOB_TITLE_NAME
            WHEN L3_REPORT_TO_LEVEL  = '2' THEN L3_REPORT_TO_JOB_TITLE_NAME
            WHEN L2_REPORT_TO_LEVEL  = '2' THEN L2_REPORT_TO_JOB_TITLE_NAME
            WHEN L1_REPORT_TO_LEVEL  = '2' THEN L1_REPORT_TO_JOB_TITLE_NAME
            ELSE ''
       END        AS AREA_MANAGER_JOB_TITLE
       ,CASE WHEN L8_REPORT_TO_LEVEL  = '1' THEN L8_REPORT_TO_attuid
            WHEN L7_REPORT_TO_LEVEL  = '1' THEN L7_REPORT_TO_attuid
            WHEN L6_REPORT_TO_LEVEL  = '1' THEN L6_REPORT_TO_attuid
            WHEN L5_REPORT_TO_LEVEL  = '1' THEN L5_REPORT_TO_attuid
            WHEN L4_REPORT_TO_LEVEL  = '1' THEN L4_REPORT_TO_attuid
            WHEN L3_REPORT_TO_LEVEL  = '1' THEN L3_REPORT_TO_attuid
            WHEN L2_REPORT_TO_LEVEL  = '1' THEN L2_REPORT_TO_attuid
            WHEN L1_REPORT_TO_LEVEL  = '1' THEN L1_REPORT_TO_attuid
            ELSE ''
       END        AS MANAGER_attuid
       ,CASE WHEN L8_REPORT_TO_LEVEL  = '1' THEN L8_REPORT_TO_LEVEL
            WHEN L7_REPORT_TO_LEVEL  = '1' THEN L7_REPORT_TO_LEVEL
            WHEN L6_REPORT_TO_LEVEL  = '1' THEN L6_REPORT_TO_LEVEL
            WHEN L5_REPORT_TO_LEVEL  = '1' THEN L5_REPORT_TO_LEVEL
            WHEN L4_REPORT_TO_LEVEL  = '1' THEN L4_REPORT_TO_LEVEL
            WHEN L3_REPORT_TO_LEVEL  = '1' THEN L3_REPORT_TO_LEVEL
            WHEN L2_REPORT_TO_LEVEL  = '1' THEN L2_REPORT_TO_LEVEL
            WHEN L1_REPORT_TO_LEVEL  = '1' THEN L1_REPORT_TO_LEVEL
            ELSE ''
       END        AS MANAGER_LEVEL



      ,CASE WHEN L8_REPORT_TO_LEVEL  = '1' THEN L8_REPORT_TO_NAME
            WHEN L7_REPORT_TO_LEVEL  = '1' THEN L7_REPORT_TO_NAME
            WHEN L6_REPORT_TO_LEVEL  = '1' THEN L6_REPORT_TO_NAME
            WHEN L5_REPORT_TO_LEVEL  = '1' THEN L5_REPORT_TO_NAME
            WHEN L4_REPORT_TO_LEVEL  = '1' THEN L4_REPORT_TO_NAME
            WHEN L3_REPORT_TO_LEVEL  = '1' THEN L3_REPORT_TO_NAME
            WHEN L2_REPORT_TO_LEVEL  = '1' THEN L2_REPORT_TO_NAME
            WHEN L1_REPORT_TO_LEVEL  = '1' THEN L1_REPORT_TO_NAME
            ELSE ''
       END        AS MANAGER_NAME
      ,CASE WHEN L8_REPORT_TO_LEVEL  = '1' THEN L8_REPORT_TO_JOB_TITLE_NAME
            WHEN L7_REPORT_TO_LEVEL  = '1' THEN L7_REPORT_TO_JOB_TITLE_NAME
            WHEN L6_REPORT_TO_LEVEL  = '1' THEN L6_REPORT_TO_JOB_TITLE_NAME
            WHEN L5_REPORT_TO_LEVEL  = '1' THEN L5_REPORT_TO_JOB_TITLE_NAME
            WHEN L4_REPORT_TO_LEVEL  = '1' THEN L4_REPORT_TO_JOB_TITLE_NAME
            WHEN L3_REPORT_TO_LEVEL  = '1' THEN L3_REPORT_TO_JOB_TITLE_NAME
            WHEN L2_REPORT_TO_LEVEL  = '1' THEN L2_REPORT_TO_JOB_TITLE_NAME
            WHEN L1_REPORT_TO_LEVEL  = '1' THEN L1_REPORT_TO_JOB_TITLE_NAME
            ELSE ''
       END        AS MANAGER_JOB_TITLE
      ,CASE WHEN L8_REPORT_TO_LEVEL  IN ('', '0') THEN L8_REPORT_TO_attuid
            WHEN L7_REPORT_TO_LEVEL  IN ('', '0') THEN L7_REPORT_TO_attuid
            WHEN L6_REPORT_TO_LEVEL  IN ('', '0') THEN L6_REPORT_TO_attuid
            WHEN L5_REPORT_TO_LEVEL  IN ('', '0') THEN L5_REPORT_TO_attuid
            WHEN L4_REPORT_TO_LEVEL  IN ('', '0') THEN L4_REPORT_TO_attuid
            WHEN L3_REPORT_TO_LEVEL  IN ('', '0') THEN L3_REPORT_TO_attuid
            WHEN L2_REPORT_TO_LEVEL  IN ('', '0') THEN L2_REPORT_TO_attuid
            WHEN L1_REPORT_TO_LEVEL  IN ('', '0') THEN L1_REPORT_TO_attuid
            ELSE ''
       END        AS NON_MANAGER_SUPERVISOR_attuid
 
       ,CASE WHEN L8_REPORT_TO_LEVEL IN ('', '0') THEN L8_REPORT_TO_LEVEL
            WHEN L7_REPORT_TO_LEVEL  IN ('', '0') THEN L7_REPORT_TO_LEVEL
            WHEN L6_REPORT_TO_LEVEL  IN ('', '0') THEN L6_REPORT_TO_LEVEL
            WHEN L5_REPORT_TO_LEVEL  IN ('', '0') THEN L5_REPORT_TO_LEVEL
            WHEN L4_REPORT_TO_LEVEL  IN ('', '0') THEN L4_REPORT_TO_LEVEL
            WHEN L3_REPORT_TO_LEVEL  IN ('', '0') THEN L3_REPORT_TO_LEVEL
            WHEN L2_REPORT_TO_LEVEL  IN ('', '0') THEN L2_REPORT_TO_LEVEL
            WHEN L1_REPORT_TO_LEVEL  IN ('', '0') THEN L1_REPORT_TO_LEVEL
            ELSE ''
       END        AS NON_MANAGER_SUPERVISOR_LEVEL


      ,CASE WHEN L8_REPORT_TO_LEVEL  IN ('', '0') THEN L8_REPORT_TO_NAME
            WHEN L7_REPORT_TO_LEVEL  IN ('', '0') THEN L7_REPORT_TO_NAME
            WHEN L6_REPORT_TO_LEVEL  IN ('', '0') THEN L6_REPORT_TO_NAME
            WHEN L5_REPORT_TO_LEVEL  IN ('', '0') THEN L5_REPORT_TO_NAME
            WHEN L4_REPORT_TO_LEVEL  IN ('', '0') THEN L4_REPORT_TO_NAME
            WHEN L3_REPORT_TO_LEVEL  IN ('', '0') THEN L3_REPORT_TO_NAME
            WHEN L2_REPORT_TO_LEVEL  IN ('', '0') THEN L2_REPORT_TO_NAME
            WHEN L1_REPORT_TO_LEVEL  IN ('', '0') THEN L1_REPORT_TO_NAME
            ELSE ''
       END        AS NON_MANAGER_SUPERVISOR_NAME
      ,CASE WHEN L8_REPORT_TO_LEVEL  IN ('', '0') THEN L8_REPORT_TO_JOB_TITLE_NAME
            WHEN L7_REPORT_TO_LEVEL  IN ('', '0') THEN L7_REPORT_TO_JOB_TITLE_NAME
            WHEN L6_REPORT_TO_LEVEL  IN ('', '0') THEN L6_REPORT_TO_JOB_TITLE_NAME
            WHEN L5_REPORT_TO_LEVEL  IN ('', '0') THEN L5_REPORT_TO_JOB_TITLE_NAME
            WHEN L4_REPORT_TO_LEVEL  IN ('', '0') THEN L4_REPORT_TO_JOB_TITLE_NAME
            WHEN L3_REPORT_TO_LEVEL  IN ('', '0') THEN L3_REPORT_TO_JOB_TITLE_NAME
            WHEN L2_REPORT_TO_LEVEL  IN ('', '0') THEN L2_REPORT_TO_JOB_TITLE_NAME
            WHEN L1_REPORT_TO_LEVEL  IN ('', '0') THEN L1_REPORT_TO_JOB_TITLE_NAME
            ELSE ''
       END        AS NON_MANAGER_SUPERVISOR_JOB_TITLE
              
FROM kpmg_ws.idesk_org14
WHERE L13_REPORT_TO_ATTUID in ('XW2829')
   OR L12_REPORT_TO_ATTUID in ('XW2829')
   OR L11_REPORT_TO_ATTUID in ('XW2829')
   OR L10_REPORT_TO_ATTUID in ('XW2829')
   OR L9_REPORT_TO_ATTUID in ('XW2829')
   OR L8_REPORT_TO_ATTUID in ('XW2829')
   OR L7_REPORT_TO_ATTUID in ('XW2829')
   OR L6_REPORT_TO_ATTUID in ('XW2829')
   OR L5_REPORT_TO_ATTUID in ('XW2829')
   OR L4_REPORT_TO_ATTUID in ('XW2829')
   OR L3_REPORT_TO_ATTUID in ('XW2829')
   OR L2_REPORT_TO_ATTUID in ('XW2829')
   OR L1_REPORT_TO_ATTUID in ('XW2829')
)                T
;              
-- GCS is led by XW2829(EVP XAVIER D WILLIAMS), COREY ANTHONY is the SVP of Service Delivery
-- currently, we only need COREY's org
-- 'CL8371','MP4686','NN1413','WM2643'

USE KPMG;



drop table organization;

create table organization 
as 
select o.*,x.manager_count_report_area_manager,x.non_manager_count_report_area_manager,x.other_count_report_area_manager from
kpmg_ws.organization_before_whitelist o
left join 
(
select o.attuid,sum(if(list.level='1',1,0)) as manager_count_report_area_manager,sum(if(list.level='0',1,0)) as non_manager_count_report_area_manager,sum(if(list.level not in ('0','1'),1,0)) as other_count_report_area_manager from
kpmg_ws.organization_before_whitelist o
left join kpmg_ws.my_webphone list
on (o.attuid=list.SUPERVISOR_ATTUID)
where o.level=2
group by o.attuid
) x
on o.attuid=upper(x.attuid)
;

"
hive -f ~/guohao/script/webphone/security.hql
sh ~/guohao/script/WihteList/ingest.sh
sh ~/guohao/script/base_table/runBash.sh
# back in bash again
hadoop dfs -chmod -R 775 /sandbox/sandbox31/stg
hadoop dfs -chmod -R 775 /sandbox/sandbox31/kpmg
hadoop dfs -chmod -R 775 /sandbox/sandbox31/kpmg_ws


output=$(hive -e "select max(formatted_date) as date1 from kpmg.base;")

MailList="guohaoxiao@kpmg.com,smuthersbaugh@kpmg.com"
messageStr=`printf '%s \n' ${output}`
echo -e ${messageStr}
echo -e ${messageStr} | mailx -s "max base date" ${MailList}
