use kpmg_ws;
drop table idat_mul;
create table idat_mul
(
product String,
type String,
order_creation    double,
order_management     double
)
ROW format delimited fields terminated by "|"

