add file /home/gc096s/guohao/script/idat_sum/udf.py;
select count(uid),count(*)
from
(
select  uso,circuit_id,uid
from
(
select x.uso,x.circuit_id,if(x.uid is not null and x.uid!="",x.uid,if((y.uid is not null and y.uid!=""),y.uid,u.attuid)) as uid
from
(
select TRANSFORM(concat(uso,'\t',circuit_id,'\t',if(USO_Originator_HRID is null,"",USO_Originator_HRID),"\t",ncon)) USING "python udf.py" as (uso,circuit_id,uid,first_name,last_name) from kpmg_ws.idat_hssd_scorecard_ethernet_linedata_mpr_uso_completed
)x
left join
(
select TRANSFORM(concat(uso,'\t',circuit_id,'\t',if(USO_Originator_HRID is null,"",USO_Originator_HRID),"\t",ncon)) USING "python udf.py" as (uso,circuit_id,uid,first_name,last_name) from kpmg_ws.idat_hssd_scorecard_ethernet_linedata_mpr_uso_completed
where USO_Originator_HRID is not null
)y
on y.first_name=x.first_name and y.last_name=x.last_name
left join  kpmg.user_org u
on u.first_name=x.first_name and u.last_name=x.last_name
)z
group by uso,circuit_id,uid
)l;
