use kpmg_ws;
add file /home/gc096s/guohao/script/idat_sum/udf.py;

--select count(distinct ncon) from  kpmg_ws.idat_hssd_scorecard_ethernet_linedata_mpr_uso_issued_dedup;





drop table temp_lookup_issued;
create table  temp_lookup_issued as
select ncon,USO_Originator_HRID as uid,"history" as name,row
from
(
select x.ncon,y.USO_Originator_HRID,row_number() over(partition by x.ncon order by y.aadate desc) as row
from
(
select * from kpmg_ws.idat_hssd_scorecard_ethernet_linedata_mpr_uso_issued_dedup
)x
inner join
(
select ncon,USO_Originator_HRID,aadate from kpmg_ws.idat_hssd_scorecard_ethernet_linedata_mpr_uso_issued_dedup
where USO_Originator_HRID is not null
group by  ncon,USO_Originator_HRID,aadate
)y
on x.ncon=y.ncon
)z

union all

select ncon,uid,"webphone" as name,row
from
(
select  x.ncon,u.handle as uid,row_number() over(partition by x.ncon order by u.handle) as row
from
(
select x.ncon,regexp_replace(split(x.ncon," ")[size(split(x.ncon," "))-1],"-","") as phone
from
(
select * from kpmg_ws.idat_hssd_scorecard_ethernet_linedata_mpr_uso_issued_dedup
)x
left join
(
select ncon,USO_Originator_HRID from kpmg_ws.idat_hssd_scorecard_ethernet_linedata_mpr_uso_issued_dedup
where USO_Originator_HRID is not null
group by ncon,USO_Originator_HRID
)y
on x.ncon=y.ncon
where y.ncon is null
group by x.ncon
)x
left join  webphone_orig u
on split(u.telephone_number," ")[size(split(u.telephone_number," "))-1]=x.phone 
where 
split(u.telephone_number," ")[size(split(u.telephone_number," "))-1] is not null and
instr(x.ncon,u.first_name)>0 and instr(x.ncon,u.last_name) >0 and instr(x.ncon,u.first_name) is not null and instr(x.ncon,u.last_name)  is not null
)result;








drop table temp_lookup_completed;
create table  temp_lookup_completed as
select ncon,USO_Originator_HRID as uid,"history" as name,row
from
(
select x.ncon,y.USO_Originator_HRID,row_number() over(partition by x.ncon order by y.aadate desc) as row
from
(
select * from kpmg_ws.idat_hssd_scorecard_ethernet_linedata_mpr_uso_completed_dedup
)x
inner join
(
select ncon,USO_Originator_HRID,aadate from kpmg_ws.idat_hssd_scorecard_ethernet_linedata_mpr_uso_completed_dedup
where USO_Originator_HRID is not null
group by  ncon,USO_Originator_HRID,aadate
)y
on x.ncon=y.ncon
)z

union all

select ncon,uid,"webphone" as name,row
from
(
select  x.ncon,u.handle as uid,row_number() over(partition by x.ncon order by u.handle) as row
from
(
select x.ncon,regexp_replace(split(x.ncon," ")[size(split(x.ncon," "))-1],"-","") as phone
from
(
select * from kpmg_ws.idat_hssd_scorecard_ethernet_linedata_mpr_uso_completed_dedup
)x
left join
(
select ncon,USO_Originator_HRID from kpmg_ws.idat_hssd_scorecard_ethernet_linedata_mpr_uso_completed_dedup
where USO_Originator_HRID is not null
group by ncon,USO_Originator_HRID
)y
on x.ncon=y.ncon
where y.ncon is null
group by x.ncon
)x
left join  webphone_orig u
on split(u.telephone_number," ")[size(split(u.telephone_number," "))-1]=x.phone
where
split(u.telephone_number," ")[size(split(u.telephone_number," "))-1] is not null and
instr(x.ncon,u.first_name)>0 and instr(x.ncon,u.last_name) >0 and instr(x.ncon,u.first_name) is not null and instr(x.ncon,u.last_name)  is not null
)result;





select count(*) from
kpmg_ws.idat_hssd_scorecard_ethernet_linedata_mpr_uso_issued_dedup;

select count(*) from
kpmg_ws.idat_hssd_scorecard_ethernet_linedata_mpr_uso_completed_dedup;



select count(c.row),sum(if(c.row=1,1,0))
from
kpmg_ws.idat_hssd_scorecard_ethernet_linedata_mpr_uso_issued_dedup d
left join 
temp_lookup_issued c
on d.ncon=c.ncon ;




select count(c.row),sum(if(c.row=1,1,0))
from
kpmg_ws.idat_hssd_scorecard_ethernet_linedata_mpr_uso_completed_dedup d
left join
temp_lookup_completed c
on d.ncon=c.ncon;


select "compleeted",name,count(*),count(distinct ncon)
from
temp_lookup_completed 
group by name;

select "issued",name,count(*),count(distinct ncon)
from
temp_lookup_issued
group by name;




dfsfsdf

select c,count(*)
from
(
select ncon,count(distinct USO_Originator_HRID) as c
from 
kpmg_ws.idat_hssd_scorecard_ethernet_linedata_mpr_uso_issued_dedup
group by ncon
)x
group by c;



select count(*),count(y.ncon)
from
(
select * from kpmg_ws.idat_hssd_scorecard_ethernet_linedata_mpr_uso_issued_dedup
)x
left join
(
select ncon,USO_Originator_HRID from kpmg_ws.idat_hssd_scorecard_ethernet_linedata_mpr_uso_issued_dedup
where USO_Originator_HRID is not null
group by ncon,USO_Originator_HRID
)y
on x.ncon=y.ncon;










select c,count(*)
from
(
select ncon,count(distinct USO_Originator_HRID) as c
from
kpmg_ws.idat_hssd_scorecard_ethernet_linedata_mpr_uso_completed_dedup
group by ncon
)x
group by c;



select count(*),count(y.ncon)
from
(
select * from kpmg_ws.idat_hssd_scorecard_ethernet_linedata_mpr_uso_completed_dedup
)x
left join
(
select ncon,USO_Originator_HRID from kpmg_ws.idat_hssd_scorecard_ethernet_linedata_mpr_uso_completed_dedup
where USO_Originator_HRID is not null
group by ncon,USO_Originator_HRID
)y
on x.ncon=y.ncon;












select count(*),count(y.last_name)
from
(
select TRANSFORM(concat(uso,'\t',circuit_id,'\t',if(USO_Originator_HRID is null,"",USO_Originator_HRID),"\t",ncon)) USING "python udf.py" as (uso,circuit_id,uid,first_name,last_name,phone) from kpmg_ws.idat_hssd_scorecard_ethernet_linedata_mpr_uso_issued_dedup
)x
left join
(
select TRANSFORM(concat(uso,'\t',circuit_id,'\t',if(USO_Originator_HRID is null,"",USO_Originator_HRID),"\t",ncon)) USING "python udf.py" as (uso,circuit_id,uid,first_name,last_name,phone) from kpmg_ws.idat_hssd_scorecard_ethernet_linedata_mpr_uso_issued_dedup
where USO_Originator_HRID is not null
)y
on x.first_name=y.first_name and x.last_name=y.last_name;




