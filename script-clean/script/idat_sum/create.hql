
use kpmg_ws
;drop table  IDAT_HSSD_Scorecard_Ethernet_Linedata_MPR_USO_Completed;
 create external table IDAT_HSSD_Scorecard_Ethernet_Linedata_MPR_USO_Completed(
USO	String,
AADate	String,
Main_Order_Type	String,
Baseline_Ind	String,
CDDDDate	String,
CDDD_Extended_Ind	String,
Center	String,
Circuit_ID	String,
CNR_Ind	String,
Constr_Cust_Ind	String,
Constr_LEC_Ind	String,
CPDD_Comp	String,
Cust_Order_Ind	String,
Customer_Strata_Group	String,
Customer_Name	String,
Expedite_Ind	String,
Market	String,
Market_Segment	String,
MCN	String,
NCON	String,
OCO	String,
OCO_Zone	String,
DD_Comp	String,
DD_Due	String,
DD_Jep	String,
DD_Jep_Frozen	String,
APP_Date	String,
Ordering_Partner	String,
Product	String,
Project_Text	String,
Related_Circuit	String,
Special_Cust_Ind	String,
Speed	String,
Speed_Grouping	String,
CR_IOM_GCSM	String,
Ethernet_Standalone_Ind	String,
Ethernet_Switched_Ind	String,
Kaizen_Ind	String,
Stream_Process_Ind	String,
Single_Control_Plane_Ind	String,
FTB_Ind	String,
PON_APRN_Newest	String,
PON_Newest	String,
PON_Newest_CKL_Location	String,
PON_Newest_EAD_ECD	String,
PON_Newest_EAD_Jep	String,
PON_Newest_EAD_Jep_Posted	String,
PON_Newest_FOC_Complete	String,
PON_Newest_FOC_Due	String,
PON_Newest_FOC_Jep	String,
PON_Newest_ICSC	String,
PON_Newest_In_Region_Ind	String,
PON_Newest_LEC	String,
PON_Newest_LEC_Company	String,
SID_Comp	String,
SID_Jep	String,
RID_Due	String,
RID_Comp	String,
RID_Jep	String,
WOT_Due	String,
WOT_Comp	String,
WOT_Jep	String,
CTA_Due	String,
CTA_Comp	String,
CTA_Jep	String,
DD_Due_Frozen	String,
Billing_Start_Date	String,
OUDD	String,
OCO_Due	String,
OCO_Jep	String,
OCO2_Jep	String,
OCO2_Jep_Frozen	String,
Billing_DD	String,
FQ_Due	String,
FQ_Comp	String,
FQ_Jep	String,
FQ_Jep_Post	String,
MGDD_Due	String,
MGDD_Comp	String,
CT_CPDD_SID	String,
CT_SID_RID	String,
CT_RID_WOT	String,
CT_RID_CTA	String,
CT_WOT_CTA	String,
CT_CTA_DD	String,
CT_CPDD_CTA	String,
CT_CPDD_DD	String,
CT_CPDD_BSD	String,
CT_Omit_Ind	String,
CT_Omit_Text	String,
Completed_From_Group	String,
Supp_Number	String,
Supp_Code	String,
Supp_Code2	String,
Supp_Code3	String,
Supp_Code4	String,
Supp_Code5	String,
Supp_NCON_Ind	String,
DAB_Ind	String,
C_Project_Number	String,
SAART_Segment	String,
Site_Bundle_Type	String,
Site_BundleID	String,
Primary_Bundle_Desc	String,
Secondary_Bundle_Desc	String,
ICore_ID	String,
USO_Originator_HRID	String,
SVID	String)
ROW format delimited fields terminated by "|";

use kpmg_ws
;drop table  IDAT_HSSD_Scorecard_Ethernet_Linedata_MPR_USO_Cancelled;
 create external table IDAT_HSSD_Scorecard_Ethernet_Linedata_MPR_USO_Cancelled(
USO	String,
AADate	String,
Main_Order_Type	String,
Baseline_Ind	String,
CDDDDate	String,
CDDD_Extended_Ind	String,
Center	String,
Circuit_ID	String,
CNR_Ind	String,
Constr_Cust_Ind	String,
Constr_LEC_Ind	String,
CPDD_Comp	String,
Cust_Order_Ind	String,
Customer_Strata_Group	String,
Customer_Name	String,
Expedite_Ind	String,
Market	String,
Market_Segment	String,
MCN	String,
NCON	String,
OCO	String,
OCO_Zone	String,
Cancel_Date	String,
DD_Due	String,
DD_Jep	String,
DD_Jep_Frozen	String,
APP_Date	String,
Ordering_Partner	String,
Product	String,
Project_Text	String,
Related_Circuit	String,
Special_Cust_Ind	String,
Speed	String,
Speed_Grouping	String,
CR_IOM_GCSM	String,
Ethernet_Standalone_Ind	String,
Ethernet_Switched_Ind	String,
Kaizen_Ind	String,
Stream_Process_Ind	String,
Single_Control_Plane_Ind	String,
FTB_Ind	String,
PON_APRN_Newest	String,
PON_Newest	String,
PON_Newest_EAD_Jep_Posted	String,
PON_Newest_In_Region_Ind	String,
PON_Newest_LEC	String,
PON_Newest_LEC_Company	String,
C_Project_Number	String,
SAART_Segment	String,
SID_Jep	String,
Billing_DD	String,
FQ_Due	String,
FQ_Comp	String,
FQ_Jep	String,
FQ_Jep_Post	String,
MGDD_Due	String,
MGDD_Comp	String,
Site_Bundle_Type	String,
Site_BundleID	String,
Primary_Bundle_Desc	String,
Secondary_Bundle_Desc	String,
Supp_Code	String,
ICore_ID	String,
USO_Originator_HRID	String,
SVID	String)
ROW format delimited fields terminated by "|";

use kpmg_ws
;drop table  IDAT_HSSD_Scorecard_Ethernet_Linedata_MPR_USO_WIP;
 create external table IDAT_HSSD_Scorecard_Ethernet_Linedata_MPR_USO_WIP(
USO	String,
AADate	String,
Main_Order_Type	String,
Baseline_Ind	String,
CDDDDate	String,
CDDD_Extended_Ind	String,
Center	String,
Circuit_ID	String,
CNR_Ind	String,
Constr_Cust_Ind	String,
Constr_LEC_Ind	String,
CPDD_Comp	String,
Cust_Order_Ind	String,
Customer_Strata_Group	String,
Customer_Name	String,
Expedite_Ind	String,
Market	String,
Market_Segment	String,
MCN	String,
NCON	String,
OCO	String,
OCO_Zone	String,
Billing_Start_Date	String,
DD_Due	String,
DD_Jep	String,
DD_Jep_Frozen	String,
APP_Date	String,
Ordering_Partner	String,
Product	String,
Project_Text	String,
Related_Circuit	String,
Special_Cust_Ind	String,
Speed	String,
Speed_Grouping	String,
CR_IOM_GCSM	String,
Ethernet_Standalone_Ind	String,
Ethernet_Switched_Ind	String,
Kaizen_Ind	String,
Stream_Process_Ind	String,
Single_Control_Plane_Ind	String,
FTB_Ind	String,
PON_APRN_Newest	String,
PON_Newest	String,
PON_Newest_CKL_Location	String,
PON_Newest_EAD_ECD	String,
PON_Newest_EAD_Jep	String,
PON_Newest_EAD_Jep_Posted	String,
PON_Newest_FOC_Complete	String,
PON_Newest_FOC_Due	String,
PON_Newest_FOC_Jep	String,
PON_Newest_ICSC	String,
PON_Newest_In_Region_Ind	String,
PON_Newest_LEC	String,
PON_Newest_LEC_Company	String,
SID_Comp	String,
SID_Jep	String,
RID_Due	String,
RID_Comp	String,
RID_Jep	String,
WOT_Due	String,
WOT_Comp	String,
WOT_Jep	String,
CTA_Due	String,
CTA_Comp	String,
CTA_Jep	String,
DD_Due_Frozen	String,
OUDD	String,
OCO_Due	String,
OCO_Jep	String,
OCO2_Jep	String,
OCO2_Jep_Frozen	String,
Billing_DD	String,
FQ_Due	String,
FQ_Comp	String,
FQ_Jep	String,
FQ_Jep_Post	String,
MGDD_Due	String,
MGDD_Comp	String,
CT_CPDD_SID	String,
CT_SID_RID	String,
CT_RID_WOT	String,
CT_RID_CTA	String,
CT_WOT_CTA	String,
CT_CTA_DD	String,
CT_CPDD_CTA	String,
CT_CPDD_DD	String,
Order_Aging	String,
Days_Past_Due	String,
Backlog_Type	String,
DAB_Ind	String,
C_Project_Number	String,
SAART_Segment	String,
Site_Bundle_Type	String,
Site_BundleID	String,
Primary_Bundle_Desc	String,
Secondary_Bundle_Desc	String,
ICore_ID	String,
USO_Originator_HRID	String,
SVID	String)
ROW format delimited fields terminated by "|";

use kpmg_ws
;drop table  IDAT_HSSD_Scorecard_Ethernet_Linedata_MPR_USO_Issued;
 create external table IDAT_HSSD_Scorecard_Ethernet_Linedata_MPR_USO_Issued(
USO	String,
AADate	String,
Main_Order_Type	String,
Baseline_Ind	String,
CDDDDate	String,
CDDD_Extended_Ind	String,
Center	String,
Circuit_ID	String,
CNR_Ind	String,
Constr_Cust_Ind	String,
Constr_LEC_Ind	String,
CPDD_Comp	String,
Cust_Order_Ind	String,
Customer_Strata_Group	String,
Customer_Name	String,
Expedite_Ind	String,
Market	String,
Market_Segment	String,
MCN	String,
NCON	String,
OCO	String,
OCO_Zone	String,
Supp_Number	String,
DD_Due	String,
DD_Jep	String,
DD_Jep_Frozen	String,
APP_Date	String,
Ordering_Partner	String,
Product	String,
Project_Text	String,
Related_Circuit	String,
Special_Cust_Ind	String,
Speed	String,
Speed_Grouping	String,
CR_IOM_GCSM	String,
Ethernet_Standalone_Ind	String,
Ethernet_Switched_Ind	String,
Kaizen_Ind	String,
Stream_Process_Ind	String,
Single_Control_Plane_Ind	String,
FTB_Ind	String,
PON_APRN_Newest	String,
PON_Newest	String,
PON_Newest_CKL_Location	String,
PON_Newest_EAD_ECD	String,
PON_Newest_EAD_Jep	String,
PON_Newest_EAD_Jep_Posted	String,
PON_Newest_FOC_Complete	String,
PON_Newest_FOC_Due	String,
PON_Newest_FOC_Jep	String,
PON_Newest_ICSC	String,
PON_Newest_In_Region_Ind	String,
PON_Newest_LEC	String,
PON_Newest_LEC_Company	String,
OCO_Due	String,
OCO_Jep	String,
C_Project_Number	String,
SAART_Segment	String,
SID_Jep	String,
Billing_DD	String,
FQ_Due	String,
FQ_Comp	String,
FQ_Jep	String,
FQ_Jep_Post	String,
MGDD_Due	String,
MGDD_Comp	String,
Site_Bundle_Type	String,
Site_BundleID	String,
Primary_Bundle_Desc	String,
Secondary_Bundle_Desc	String,
ICore_ID	String,
USO_Originator_HRID	String,
SVID	String)
ROW format delimited fields terminated by "|";
