use kpmg_ws;

drop table idat_lssd_scorecard_details_uso_mpr_uso_completed_dedup;
create table kpmg_ws.idat_lssd_scorecard_details_uso_mpr_uso_completed_dedup
as
select * from
(
select * , row_number() over( partition by USO,Circuit_ID,AADate,project_text order by ncon) as row
from kpmg_ws.idat_lssd_scorecard_details_uso_mpr_uso_completed
)x
where row =1;

drop table idat_lssd_scorecard_details_uso_mpr_uso_issued_dedup;
create table kpmg_ws.idat_lssd_scorecard_details_uso_mpr_uso_issued_dedup
as
select * from
(
select * , row_number() over( partition by USO,Circuit_ID,AADate,project_text order by ncon) as row
from kpmg_ws.idat_lssd_scorecard_details_uso_mpr_uso_issued
)x
where row =1;


drop table idat_hssd_scorecard_ethernet_linedata_mpr_uso_completed_dedup;
create table kpmg_ws.idat_hssd_scorecard_ethernet_linedata_mpr_uso_completed_dedup
as
select * from
(
select * , row_number() over( partition by USO,Circuit_ID,AADate,project_text order by ncon) as row
from kpmg_ws.idat_hssd_scorecard_ethernet_linedata_mpr_uso_completed
)x
where row =1;


drop table idat_hssd_scorecard_ethernet_linedata_mpr_uso_issued_dedup;
create table kpmg_ws.idat_hssd_scorecard_ethernet_linedata_mpr_uso_issued_dedup
as
select * 
from
(
select * , row_number() over( partition by USO,Circuit_ID,AADate,project_text order by ncon) as row
from kpmg_ws.idat_hssd_scorecard_ethernet_linedata_mpr_uso_issued
)x
where row =1;


