#!/usr/bin/env python

import sys
import string
def hasNumbers(inputString):
    return any(char.isdigit() for char in inputString)

while True:
  line = sys.stdin.readline()
  if not line:
    break
  if len(line.split("\t"))==3:
      uso= line.split("\t")[0]
      circuit_id=line.split("\t")[1]
      uid=""
      elements = line.split("\t")[2].replace("\t"," ").replace("."," ").replace(","," ").split(" ")
  else:
      uso= line.split("\t")[0]
      circuit_id=line.split("\t")[1]
      uid=line.split("\t")[2]
      elements = line.split("\t")[3].replace("\t"," ").replace("."," ").replace(","," ").split(" ")
#  print line
  if len(elements)==3:
      print uso+"\t"+circuit_id+'\t'+uid+"\t"+elements[0]+"\t" + elements[1]+"\t" + elements[-1]
  elif len(elements)>3:
      if hasNumbers(elements[2]):
          print  uso+"\t"+circuit_id+'\t'+uid+"\t"+elements[0]+"\t" + elements[1]+"\t" + elements[-1]
      elif  hasNumbers(elements[3]):
          print  uso+"\t"+circuit_id+'\t'+uid+"\t"+elements[0]+"\t"+elements[2]+"\t" + elements[-1]
