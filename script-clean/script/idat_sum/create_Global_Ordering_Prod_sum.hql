use kpmg_ws;

drop table Global_Ordering_Prod_sum;
create table Global_Ordering_Prod_sum as 
select uid,trim(date) as date,Main_Order_Type,product,sum(create) as order_create_count,sum(complete) as order_complete_count,sum(create*order_creation) as order_create_count_nomarlized,sum(complete*order_management) as order_complete_count_normalized, sum(create*order_creation)+sum(complete*order_management) as total_nomalization
from
(
select if(USO_Originator_HRID_issued is null,USO_Originator_HRID_completed,USO_Originator_HRID_issued ) as UID, if(Order_Creation_Date is null,order_completion_date,Order_Creation_Date
) as date,v.product,if(USO_Originator_HRID_issued is null,0,1) as create, if(USO_Originator_HRID_issued is null,1,0) as complete, m.order_creation,m.order_management,v.Main_Order_Type
from  Global_Ordering_Prod_Volume v
inner join idat_mul m
on upper(v.product)=upper(m.product) and upper(v.Main_Order_Type)=upper(m.type)
)x
where uid is not null and uid !='999999' and uid!=''
group by uid,date,product,Main_Order_Type ;


drop table Global_Ordering_Prod_sum_uid;
create table Global_Ordering_Prod_sum_uid as
select uid,date,sum(total_nomalization) as total_nomalization,sum(order_create_count) as order_create_count_total,sum(order_complete_count) as order_complete_count_total
from Global_Ordering_Prod_sum 
group by uid,date;

