echo "LinkNumber1+"|kinit
elink_status=$(hive -e "select * from kpmg_ws.elink_status")
idesk_status=$(hive -e "select * from kpmg_ws.idesk_status")

MailList="guohaoxiao@kpmg.com,smuthersbaugh@kpmg.com,tbaweja@kpmg.com"
if [ "$elink_status" = "ok" ] && [ "$idesk_status" = "ok" ]
then
  echo "ok"
   messageStr="start running classification"
   echo -e ${messageStr}
   echo -e ${messageStr} | mailx -s "automation status" ${MailList}


  python2.7 /opt/data/share05/sandbox/sandbox31/iDesk_Classification_Reports/iDeskClassification/main.py -st_pgm 0 -end_pgm 10
else
  echo "failed"
   messageStr="elink or idesk is not updated, cannot run classification"
   echo -e ${messageStr}
   echo -e ${messageStr} | mailx -s "automation status:" ${MailList}

fi

classification_status=$(hive -e "select * from kpmg_ws.auto_classification_flag")

if [ "$classification_status" = "1" ] 
then
  echo "classification ok"
   messageStr="classificaiton finished, start pushing tables to report server"
   echo -e ${messageStr}
   echo -e ${messageStr} | mailx -s "automation status:" ${MailList}
   sh /home/gc096s/guohao/script/ssas/sqoop.sh 2>&1 >log
else
   echo "classfication failed"
   messageStr="classificaiton failed"
   echo -e ${messageStr}
   echo -e ${messageStr} | mailx -s "automation status:" ${MailList}

fi

  messageStr="$(grep 'ERR' log)"
   echo -e ${messageStr}
   echo -e ${messageStr} | mailx -s "sqoop error logs:" ${MailList}


