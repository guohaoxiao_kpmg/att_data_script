import os
from StringIO import StringIO
from datetime import datetime
import commands
from SqoopUtil import SqoopUtil
from collections import *
class HiveUtil:
    def describe_hive(self,data_base=None, table_name=None, verbose=True):
        """
         send a describe query for a table and returns dataframe with col name, data type and comments
         :param data_base: database name
         :param table_name: table name
         :param verbose: print info
         :return: data frame
        """
        if data_base is None:
            raise Exception("Please provide database name as a parameter.\n")
        if table_name is None:
            raise Exception("Please provide table name as a parameter.\n")
        hive_query = """USE {0}; describe {1};""".format(data_base, table_name)
        cmd = """hive -S -e '%s'""" % hive_query
        try:
            result=os.popen(cmd).read()
            for line in result.split("\n"):
                print line.split("\t")
            return result
        except:
            raise Exception("Error. Code %s.\n" % "1")

    def query_read_hive(self,hql_query, data_base='KPMG', deliminator="\t", clean_columns=True, add_pyudfs=None,add_pickle=None):
        """
        query a hive table and return data as a dataframe
        :param hql_query: Hive query; string
        :param data_base: HIve database name; string
        :param deliminator: DEFAULTS to "\t", if you have different deliminator then change it
        :param clean_columns: columns names return with table name example mytable.mycol
                              if clean_columns =True if strips table name off and upper cases the name  MYCOL
        :param add_pyudfs: path to the UDF you want; list
        :param add_pickle: path to the Pickle File you want; string list
        :return:
        """
        if data_base is not None or len(data_base) > 0:
            hive_query = """USE %s;""" % data_base
        else:
            raise Exception("Please provide database name as a parameter.\n")
        if add_pyudfs is not None:
            for udf in add_pyudfs:
                hive_query += """DELETE FILE {0};ADD FILE {0};""".format(udf)
        if add_pickle is not None:
            for pickle in add_pickle:
                hive_query += """DELETE FILE {0};ADD FILE {0};""".format(pickle)
        hive_query += """set hive.cli.print.header=true;"""
        hive_query += " ".join(hql_query.splitlines())
        cmd = """hive -S -e '%s'""" % hive_query
        try:
            print os.popen(cmd).read()
            for line in os.popen(cmd).read().split("\n"):
                print line.split("\t")

        except:
            print cmd
            raise Exception("Error. Code %s.\n" % "1")

    def createTablefromSqoop(self,db,table,sqoopdb,sqoopTable):
        SQL2HIVE_TYPES = {"bigint": "bigint"
        , "float": "float"
        , "varchar": "string"
        , "int": "int"
        , "tinyint": "tinyint"
        , "smallint": "smallint"
        , "float":"float"
        , "decimal": "decimal"
        , "timestamp": "string"
        , "date": "string"
        , "char": "string"
        , "binary": "binary"
        , "bit":"smallint"
        , "nvarchar":"string"
        , "datetime":"string"
        , "ntext":'string'
        , "smalldatetime":"string"}
        result =self.sqoopClient.getTableDescription(sqoopdb,sqoopTable)
        column_types=[]
        
        for element in list(result):
            if "COLUMN_NAME" in element:
               continue
            colname=str(element.split("\t")[0]).replace("(","_").replace(")","_").replace("?","_").replace("/","_").replace("\\","_").replace(" ","_").replace("%","percent").replace("-","_").replace("FUNCTION","func")
            sqltype=element.split("\t")[1]
            column_types.append((colname, SQL2HIVE_TYPES[sqltype]))
        columns = ',\n  '.join("%s %s" % x for x in column_types)
          
        template_create = """hive -e \"USE %(db)s;drop table %(name)s; CREATE TABLE %(name)s (
              %(columns)s
             ) ;\"
           
             """
         
        create = template_create % {'db':db, 'name': table, 'columns': columns}
        print create
        try:
           result=os.popen(create).read()
        except:
           raise Exception("Error. Code %s.\n" % "1")



    def createTableInSqoop(self,db,table,sqoopTable):
        HIVE2PY_TYPES = {"bigint": "bigint"
        , "double": "float"
        , "string": "varchar(2000)"
        , "int": "int"
        , "tinyint": "tinyint"
        , "smallint": "smallint"
        , "float":"float"
        , "decimal": "decimal"
        , "timestamp": "text"
        , "date": "date"
        , "varchar":"varchar(2000)"
        , "char": "char"
        , "binary": "binary"
        , "boolean": "bit"}

        result=self.describe_hive(db,table)
        colDic=OrderedDict()
        for line in result.split('\n'):
            array=line.split('\t')
            if len(array)>=2:
                colDic[array[0].strip()]=HIVE2PY_TYPES[array[1].strip()]
             
        self.sqoopClient.createTable(sqoopTable,colDic)

    def createSqoopClient(self,libjars,driver,connect,username,password,mapper):
        self.sqoopClient=SqoopUtil()
        self.sqoopClient.configure(libjars,driver,connect,username,password,mapper)

    def dropSqoopTable(self,sqoopTable):
        self.sqoopClient.query("drop table "+sqoopTable)
    def exportToSqoop(self,sqoopTable,export_dir,fields_teminator,lines_teminator,null_string,null_non_string,mapper):
        self.sqoopClient.export(sqoopTable,export_dir,fields_teminator,lines_teminator,null_string,null_non_string,mapper)
    def importFromSqoop(self,target_dir,split_by,mapper,query,null_string,null_non_string,fields_teminator):
        self.sqoopClient.importToHive(target_dir,split_by,mapper,query,null_string,null_non_string,fields_teminator)


