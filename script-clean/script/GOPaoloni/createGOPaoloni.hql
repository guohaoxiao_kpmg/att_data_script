use kpmg_ws;
drop table kpmg_ws.GOPaoloni;
create table GOPaoloni
as
SELECT 

sr_accepted_date,
Number_of_Circuits,
sr_id,
Is_this_a_Managed_service,
Number_of_Sites,
Order_Type,
sr_Order_type_and_numbers,
task_css_template,
oppty_id,
sr_productList,
task_completed_actual_date,
task_name,
task_status,
task_last_updated_by,
task_Product_Group

FROM kpmg_ws.IW_SP1269 
WHERE lower(TASK_NAME) LIKE "%order request% submitted%";

