import os
from StringIO import StringIO
from datetime import datetime
import commands

HIVE2PY_TYPES = {"bigint": np.int64
    , "double": np.float64
    , "string": str
    , "int": np.int64
    , "tinyint": np.int16
    , "smallint": np.int32
    , "float": np.float64
    , "decimal": np.float64
    , "timestamp": datetime
    , "date": datetime
    , "varchar": str
    , "char": str
    , "binary": np.bool_
    , "boolean": np.bool_}

PY2HIVE_TYPES = {'DATE': 'DATE', 'DATETIME': 'TIMESTAMP', 'INT': 'BIGINT', 'FLOAT': 'DOUBLE',
                 'VARCHAR': 'STRING'}


def header(statement="Hello", symbol="-", print_len=75):
    print
    print("{0}".format(symbol) * print_len)
    if len(statement) < print_len - 4:
        pstatement = "{0} ".format(symbol) + statement + " " * (print_len - len(statement) - 3) + "{0}".format(symbol)
        print(pstatement)
    else:
        while len(statement) > print_len - 4:
            if len(statement[print_len - 4:]) < print_len - 4:
                pstatement = "{0} ".format(symbol) + statement[:print_len - 4] + " " * (
                print_len - len(statement[:print_len - 3])) + "{0}".format(symbol)
            else:
                pstatement = "{0} ".format(symbol) + statement[:print_len - 4] + " " * (
                print_len - len(statement[:print_len - 4]) - 3) + "{0}".format(symbol)
            statement = statement[print_len - 4:]
            print(pstatement)
        pstatement = "{0} ".format(symbol) + statement[:print_len - 4] + " " * (
        print_len - len(statement) - 3) + "{0}".format(symbol)
        print(pstatement)

    print("{0}".format(symbol) * print_len)
    print


def db_colname(pandas_colname):
    """
    convert pandas column name to a DBMS column name
    :param pandas_colname:  pandas dataframe column names
    :return: list of netezza accecpt names
    """
    colname = pandas_colname.replace(' ', '_').strip().upper()
    return colname


def describe_hive(data_base=None, table_name=None, verbose=True):
    """
    send a describe query for a table and returns dataframe with col name, data type and comments
    :param data_base: database name
    :param table_name: table name
    :param verbose: print info
    :return: data frame
    """
    if data_base is None:
        raise Exception("Please provide database name as a parameter.\n")
    if table_name is None:
        raise Exception("Please provide table name as a parameter.\n")
    hive_query = """set hive.cli.print.header=true;USE {0}; describe {1};""".format(data_base, table_name)
    cmd = """hive -S -e '%s'""" % hive_query
    try:
        #data = pd.concat(pd.read_table(StringIO(os.popen(cmd).read())
        #                               , na_values=['', r'\N'], sep="\t", low_memory=False, chunksize=750000))
        print StringIO(os.popen(cmd).read())
        print data          
        data.columns = [x.upper() for x in data.columns]
        for col in data.columns:
            data[col] = data[col].str.strip()
        data.reset_index(inplace=True, drop=True)
        return data
    except:
        raise Exception("Error. Code %s.\n" % "1")


def read_hive(data_base=None, table_name=None, limit=None, deliminator="\t"):
    """
    return all data from a hive table as data frame
    :param data_base: HIve database name; string
    :param table_name: table name
    :param limit: number of records to return
    :param deliminator: DEFAULTS to "\t", if you have different deliminator then change it
    :return:
    """
    if data_base is None:
        raise Exception("Please provide database name as a parameter.\n")
    if table_name is None:
        raise Exception("Please provide database name as a parameter.\n")
    hive_query = """USE %s;""" % data_base
    hive_query += """select * from {0}""".format(table_name)
    if limit is not None:
        hive_query += """ limit {0}""".format(limit)
    hive_query += """;"""
    hive_query += " ".join(hive_query.splitlines())
    cmd = """hive -S -e '%s'""" % hive_query
    # get data info
    data_info = describe_hive(data_base=data_base, table_name=table_name)
    col_names = [x.upper().strip() for x in data_info['COL_NAME'].tolist()]
    input_col_names = [table_name.lower() + "." + c for c in col_names]
    data_types = ["".join(x.split("(")[0]) for x in data_info['DATA_TYPE'].tolist()]
    data_types = dict(zip(input_col_names, [HIVE2PY_TYPES[d] for d in data_types]))
    try:
        data = pd.concat(pd.read_table(StringIO(os.popen(cmd).read())
                                       , dtype=data_types
                                       , na_values=['NULL', r'\N', 'NA', 'NaN', " ", ""], sep=deliminator,
                                       low_memory=False, chunksize=750000))
        data.rename(columns=dict(zip(data.columns.tolist(), col_names)), inplace=True)
        data.reset_index(inplace=True, drop=True)
        return data
    except:
        raise Exception("Error. Code %s.\n" % "1")


def query_read_hive(hql_query, data_base='KPMG', deliminator="\t", clean_columns=True, add_pyudfs=None,
                    add_pickle=None):
    """
    query a hive table and return data as a dataframe
    :param hql_query: Hive query; string
    :param data_base: HIve database name; string
    :param deliminator: DEFAULTS to "\t", if you have different deliminator then change it
    :param clean_columns: columns names return with table name example mytable.mycol
                          if clean_columns =True if strips table name off and upper cases the name  MYCOL
    :param add_pyudfs: path to the UDF you want; list
    :param add_pickle: path to the Pickle File you want; string list
    :return:
    """
    if data_base is not None or len(data_base) > 0:
        hive_query = """USE %s;""" % data_base
    else:
        raise Exception("Please provide database name as a parameter.\n")
    if add_pyudfs is not None:
        for udf in add_pyudfs:
            hive_query += """DELETE FILE {0};ADD FILE {0};""".format(udf)
    if add_pickle is not None:
        for pickle in add_pickle:
            hive_query += """DELETE FILE {0};ADD FILE {0};""".format(pickle)
    hive_query += """set hive.cli.print.header=true;"""
    hive_query += " ".join(hql_query.splitlines())
    cmd = """hive -S -e '%s'""" % hive_query
    try:
        data = pd.concat(pd.read_table(StringIO(os.popen(cmd).read())
                                       , na_values=['', r'\N'], sep=deliminator, low_memory=False, chunksize=750000))
        if clean_columns:
            data.columns = [x if len("".join(x.upper().split(".")[1:])) == 0 else "".join(x.upper().split(".")[1:]) for
                            x in data.columns]
        data.reset_index(inplace=True, drop=True)
        return data
    except:
        print cmd
        raise Exception("Error. Code %s.\n" % "1")


def send_query(hql_query, type="f"):
    """
    sends a hive query and return nothing
    type="F" means using a hive sql file. so using filepath and name for hql_query
    example my/file/path/myhivequery.hql
    :param hql_query: Hive query; string or file path
    :param type: f for file or e for string query
    """
    hql_query = " ".join(hql_query.splitlines())
    if type.upper() == "F":
        cmd = """hive  -f '%s'""" % hql_query
    else:
        cmd = """hive  -e '%s'""" % hql_query
    status, output = commands.getstatusoutput(cmd)
    if status == 0:
        print("Query Complete.\n")
    else:
        raise Exception("Hive Error. Code %s.\n" % status)


def table_exist(data_base, table_name):
    """
    check to see if hive table exist
    :param data_base: database name
    :param table_name: table name
    :return: boolean True if exist and False if does not exist
    """
    cmd = 'hive -S -e "USE {0}; SHOW TABLES LIKE '.format(data_base) + "'" + table_name + "'" + ';"'
    check = os.popen(cmd).read().strip().split("\t")
    if len(check) == 1 and len("".join(check)) != 0:
        return True
    else:
        return False


def delete_table(data_base, table_name, verbose=False):
    """
    drop a hive table
    :param data_base: database name
    :param table_name: table name
    :param verbose: print info
    :return:
    """
    cmd = 'hive -S -e "USE {0}; DROP TABLE IF EXISTS {1};"'.format(data_base, table_name)
    status, output = commands.getstatusoutput(cmd)
    if status == 0 and verbose:
        print("Dropped {0} table in {1} database.\n".format(table_name, data_base))
    elif status == 0:
        pass
    else:
        raise Exception("Hive Error. Code %s.\n" % status)


def get_schema(frame, data_base, table_name, comments=None, sep="\t"):
    """
    create scheme for creating a hive table
    :param frame: data frame
    :param data_base: database name
    :param table_name: table name
    :param comments: dictionary with comments about attribute ex. {'var1':'very good variables'}
    :param sep: delemintor
    :return: string; scheme generated by the dataframe
    """
    column_types = []
    dtypes = frame.dtypes
    for i, k in enumerate(dtypes.index):
        dt = dtypes[k]
        # print 'dtype', dt, dt.itemsize
        if str(dt.type) == pd.tslib.Timestamp:
            sqltype = 'TIMESTAMP'
        elif str(dt.type) == "<type 'numpy.datetime64'>":
            sqltype = PY2HIVE_TYPES['DATETIME']
        elif issubclass(dt.type, np.datetime64):
            sqltype = PY2HIVE_TYPES['DATETIME']
        elif issubclass(dt.type, np.integer):
            sqltype = PY2HIVE_TYPES['INT']
        elif issubclass(dt.type, np.bool_):
            sqltype = 'BOOLEAN'
        elif issubclass(dt.type, np.floating):
            sqltype = PY2HIVE_TYPES['FLOAT']
        else:
            sampl = frame[frame.columns[i]][0]
            # print 'other', type(sampl)
            if str(type(sampl)) == "<type 'datetime.datetime'>":
                sqltype = PY2HIVE_TYPES['DATETIME']
            elif str(type(sampl)) == "<type 'datetime.date'>":
                sqltype = PY2HIVE_TYPES['DATE']
            else:
                # size = 2 + max((len(str(a)) for a in frame[k]))
                # sqltype = PY2HIVE_TYPES['VARCHAR'] + '(?)'.replace('?', str(size))
                sqltype = PY2HIVE_TYPES['VARCHAR']
        colname = db_colname(k)
        if comments is not None and k in comments:
            column_types.append((colname, sqltype, comments[k].strip().upper()))
        else:
            column_types.append((colname, sqltype, ""))
    columns = ',\n  '.join("%s %s comment '%s'" % x for x in column_types)
    template_create = """USE {0}; CREATE TABLE %(name)s (
          %(columns)s
        ) ROW FORMAT DELIMITED FIELDS TERMINATED BY '{1}';"""
    create = template_create % {'name': table_name, 'columns': columns}
    return create.format(data_base, sep)


def write_frame(frame=None, data_base=None, table_name=None,comments=None, if_exists='replace', verbose=True):
    """
    write data into a hive table. can create table from scratch using replace or append
    or append data using append.
    if if_exist='append' and table does not exist then it will create the scheme to create the table and load the data
    :param frame: dataframe
    :param data_base: database name
    :param table_name: table name
    :param comments: dictionary with comments about attribute ex. {'var1':'very good variables'}
    :param if_exists: 'append' or 'replace'; replace will delete the table if exist. append will append data. DEFAULT 'replace'
    :param verbose:
    :return:
    """
    if data_base is None:
        raise Exception("Please provide database name as a parameter.\n")
    if table_name is None:
        raise Exception("Please provide database name as a parameter.\n")
    if if_exists.lower() == 'replace' and table_exist(data_base, table_name):
        delete_table(data_base, table_name, verbose)
    if if_exists.lower() in ('fail', 'replace') or (
                    if_exists.lower() == 'append' and not table_exist(data_base, table_name)):
        # create table
        schema = get_schema(frame, data_base, table_name,comments=comments)
        if verbose:
            print(schema)
        cmd = 'hive -e "{0}"'.format(schema)
        status, output = commands.getstatusoutput(cmd)
        if status == 0 and verbose:
            print("Table Schema {0} table for {1} database was created.\n".format(table_name, data_base))
        elif status == 0:
            pass
        else:
            raise Exception("Hive Error. Code {0}.\n"
                            "Table Schema {1} table for {2} database was"
                            " not created.\n".format(status, table_name, data_base))
    # upload via external file
    # create temp folder to store output and NZ Log
    mainDir = os.getcwd()
    workingDir = os.path.join(mainDir, "tempOUTPUT")
    logDir = os.path.join(mainDir, "hiveLog")
    tempFILE = os.path.join(workingDir, 'tempData.csv')
    if not os.path.exists(logDir):
        os.mkdir(logDir)
    if not os.path.exists(workingDir):
        os.mkdir(workingDir)
    else:
        if os.path.exists(tempFILE):
            os.remove(tempFILE)
    if verbose:
        print(frame.head())
    frame.to_csv(tempFILE, sep='\t', index=False, header=False)
    log_date = datetime.now().strftime("%Y%m%d_%I%M%S%p")
    log_file = '{2}/{0}_{1}_{3}.log'.format(data_base, table_name, logDir, log_date)
    cmd = """ "USE {0};
    LOAD DATA LOCAL INPATH '{2}' INTO  TABLE {1};" """.format(data_base, table_name, tempFILE)
    cmd = "hive -e " + cmd + " > {0} 2>&1".format(log_file)
    cmd = " ".join(cmd.splitlines())
    status, output = commands.getstatusoutput(cmd)
    if status == 0 and verbose:
        print("Data was inserted in {0} table in {1} database.\n".format(table_name, data_base))
        print("Log saved at {0}".format(log_file))
    elif status == 0:
        pass
    else:
        print("Error Log saved at {0}".format(log_file))
        raise Exception("Hive Error. Code %s.\n. Data failed to be inserted.\n" % status)
    os.remove(tempFILE)
    os.rmdir(workingDir)


def get_statistics(data_base=None, table_name=None, group_by=None):
    """
    generate statistics for each attribute in the table
    :param data_base: database name
    :param table_name: table name
    :param group_by: attributes to group by. list format ['col1']
    :return: dataframe
    """
    if data_base is None:
        raise Exception("Please provide database name as a parameter.\n")
    if table_name is None:
        raise Exception("Please provide database name as a parameter.\n")
    data_info = describe_hive(data_base=data_base, table_name=table_name)
    if group_by is not None:
        hql_query = "select {0} ,count(*) as total_records".format(",".join(group_by))
        data_info = data_info[~data_info.COL_NAME.isin(group_by)]
    else:
        hql_query = "select count(*) as total_records"
    for idx in data_info.index:
        if data_info.loc[idx]['DATA_TYPE'].upper() in ['DATE', 'TIMESTAMP', 'VARCHAR'] or \
                        data_info.loc[idx]['DATA_TYPE'].upper().split("(")[0] in ['VARCHAR']:
            hql_query += ",count({0}) as count_not_missing_{0}".format(data_info.loc[idx]['COL_NAME'])
            hql_query += ",sum(case when {0} is NULL then 1 else 0 end) as number_missing_{0}".format(
                data_info.loc[idx]['COL_NAME'])
            hql_query += ",count(distinct({0})) as distinct_count_{0}".format(data_info.loc[idx]['COL_NAME'])
            hql_query += ",min({0}) as min_{0}".format(data_info.loc[idx]['COL_NAME'])
            hql_query += ",max({0}) as max_{0}".format(data_info.loc[idx]['COL_NAME'])
        if data_info.loc[idx]['DATA_TYPE'].upper() in ['INT', 'BIGINT', 'DOUBLE']:
            hql_query += ",count({0}) as count_not_missing_{0}".format(data_info.loc[idx]['COL_NAME'])
            hql_query += ",sum(case when {0} is NULL then 1 else 0 end) as number_missing_{0}".format(
                data_info.loc[idx]['COL_NAME'])
            hql_query += ",sum({0}) as sum_{0}".format(data_info.loc[idx]['COL_NAME'])
            hql_query += ",min({0}) as min_{0}".format(data_info.loc[idx]['COL_NAME'])
            hql_query += ",avg({0}) as avg_{0}".format(data_info.loc[idx]['COL_NAME'])
            hql_query += ",max({0}) as max_{0}".format(data_info.loc[idx]['COL_NAME'])
    hql_query += " from {}.{}".format(data_base, table_name)
    if group_by is not None:
        hql_query += " group by {0} ".format(",".join(group_by))
    hql_query += ";"
    return query_read_hive(hql_query, data_base='KPMG', deliminator="\t", clean_columns=True)


if __name__ == "__main__":
    """
    test code
    make sure you are in a directory with read and write privileges
    """
    describe_hive('kpmg_ws','elink',None)
    sys.exit(0)
    header(statement="Testing Python Hive Functions")
    test_dir = "/opt/data/share05/sandbox/sandbox31/sm461x/reports/testing"
    # change directory
    if not os.path.exists(test_dir):
        os.mkdir(test_dir)
    os.chdir(test_dir)

    # clean up
    header(statement="Delete test tables if exist.")
    delete_table(data_base='KPMG_WS', table_name='py_test_abc')
    delete_table(data_base='KPMG_WS', table_name='TEST_PYHIVE')

    # create fake date for test
    header(statement="Creating data. 100 obs")
    final = pd.DataFrame(np.random.randn(100, 4), columns=list('ABCD'))
    final['DateTime'] = pd.date_range('1/1/2011', periods=len(final), freq='D')
    final['TimeStamp'] = pd.date_range('1/1/2011', periods=len(final), freq='S')
    final['Date'] = final['DateTime'].apply(lambda x: x.strftime("%Y-%m-%d"))
    final['Year'] = final['DateTime'].apply(lambda x: x.strftime("%Y"))
    final['A'] = final.A.astype(int)
    final['B'] = final.B.astype(str)
    data_types = final.dtypes
    print(data_types)

    # Check writing data to hive
    header(statement="Writing data. First 100 obs")
    my_table_comments ={'Year':"Year (in string format)","A":"Just a random Integer","TimeStamp":"Time stamp in seconds"}
    write_frame(final, data_base='KPMG_WS', table_name='TEST_PYHIVE',comments=my_table_comments, verbose=True)
    header(statement="Describe Data Function")
    data_info = describe_hive(data_base='KPMG_WS', table_name='TEST_PYHIVE', verbose=True)
    print(data_info)
    header(statement="Creating Descriptive Statistics")
    stats = get_statistics(data_base='KPMG_WS', table_name='TEST_PYHIVE', group_by=None)
    print(stats)
    del data_info, stats
    header(statement="Writing data appending. Create a new 250 obs")
    del final
    final = pd.DataFrame(np.random.randn(250, 4), columns=list('ABCD'))
    final['DateTime'] = pd.date_range('1/1/2012', periods=len(final), freq='D')
    final['TimeStamp'] = pd.date_range('1/1/2012', periods=len(final), freq='S')
    final['Date'] = final['DateTime'].apply(lambda x: x.strftime("%Y-%m-%d"))
    final['Year'] = final['DateTime'].apply(lambda x: x.strftime("%Y"))
    final['A'] = final.A.astype(int)
    final.loc[[1, 4, 6, 8], 'A'] = np.nan
    final['B'] = final.B.astype(str)
    header(statement="Appending new 250 obs")
    write_frame(final, data_base='KPMG_WS', table_name='TEST_PYHIVE', if_exists='append', verbose=True)
    header(statement="Describe Data Function")
    data_info = describe_hive(data_base='KPMG_WS', table_name='TEST_PYHIVE', verbose=True)
    print(data_info)
    # test group by
    header(statement="Creating Descriptive Statistics")
    stats_append = get_statistics(data_base='KPMG_WS', table_name='TEST_PYHIVE', group_by=['Year'])
    print(stats_append)
    del data_info

    # get data
    header(statement="read hive Function")
    data = read_hive(data_base='KPMG_WS', table_name='TEST_PYHIVE')
    # write to csv
    file_date_pattern = datetime.now().strftime("%Y%m%d_%I%M%S%p")
    file_name = "Test_read_hive" + "_" + file_date_pattern + ".csv"
    header(statement="to csv.{0}".format(file_name))
    data.to_csv(os.path.join(file_name), na_rep=" ", index=False)

    # get data
    header(statement="query_read_hive Function")
    my_hql = 'select * from TEST_PYHIVE where Date >= "2011-03-20";'
    data = query_read_hive(my_hql, data_base='KPMG_WS', deliminator="\t", clean_columns=True, add_pyudfs=None,
                           add_pickle=None)
    # write to csv
    file_date_pattern = datetime.now().strftime("%Y%m%d_%I%M%S%p")
    file_name = "query_read_hive" + "_" + file_date_pattern + ".csv"
    data.to_csv(os.path.join(file_name), na_rep=" ", index=False)

    # send query
    header(statement="send querying hive function")
    send_query("create table kpmg_ws.py_test_abc as select * from kpmg_ws.TEST_PYHIVE limit 10;", type='e')
    # get data
    header(statement="read hive Function")
    data1 = read_hive(data_base='KPMG_WS', table_name='py_test_abc')
    header(statement="Creating Descriptive Statistics")
    stats = get_statistics(data_base='KPMG_WS', table_name='py_test_abc', group_by=None)
    print(stats)
    # write to csv
    file_date_pattern = datetime.now().strftime("%Y%m%d_%I%M%S%p")
    file_name = "send_query" + "_" + file_date_pattern + ".csv"
    data1.to_csv(os.path.join(file_name), na_rep=" ", index=False)
    # excel sheets
    header(statement="Excel File")
    try:
        xls_writer = pd.ExcelWriter(os.path.join(test_dir, "ExcelWorkbook.xlsx"))
        data.to_excel(xls_writer, "data", index=False)
        data1.to_excel(xls_writer, "data1", index=False)
        stats.to_excel(xls_writer, 'stats', index=False)
        stats_append.to_excel(xls_writer, 'stats append', index=False)
        final.to_excel(xls_writer, "Original", index=False)
        xls_writer.save()
    except:
        "pandas verison conflict with openpxl has a bug so this is a work around"
        pd.core.format.header_style = None
        xls_writer = pd.ExcelWriter(os.path.join(test_dir, "ExcelWorkbook.xlsx"))
        xls_writer.date_format = None
        xls_writer.datetime_format = None
        data.to_excel(xls_writer, "data", index=False)
        data1.to_excel(xls_writer, "data1", index=False)
        stats.to_excel(xls_writer, 'stats', index=False)
        stats_append.to_excel(xls_writer, 'stats append', index=False)
        final.to_excel(xls_writer, "Original", index=False)
        xls_writer.save()
    # clean up
    delete_table(data_base='KPMG_WS', table_name='py_test_abc')
    delete_table(data_base='KPMG_WS', table_name='TEST_PYHIVE')
    print("Table {0} found {1}".format('py_test_abc', table_exist(data_base='KPMG_WS', table_name='py_test_abc')))
    print("Table {0} found {1}".format('TEST_PYHIVE', table_exist(data_base='KPMG_WS', table_name='TEST_PYHIVE')))

