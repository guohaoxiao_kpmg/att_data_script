hive -e "USE kpmg_ws;drop table GBADashBoardUnitCostHC; CREATE TABLE GBADashBoardUnitCostHC (
              FORCE_COUNT float,
  Month string,
  RC_Code string,
  City string,
  Work_State_Country_Desc string,
  Work_Region string,
  HR_Id string,
  EMPLOYEE_FIRST_NM string,
  EMPLOYEE_LAST_NM string,
  Job_Description string,
  Band string,
  Level string,
  Level_2A string,
  Level_3 string,
  Level_4 string,
  Description string,
  CBL_Element string,
  Band_Classification string,
  OL_VP_Level string,
  Dom_v_MoW string,
  VP_from_Focals string,
  AVP string,
  LOB1 string,
  LOB2 string,
  Dom_Glbl string,
  LOB string,
  Production_Non_Production_Worker string,
  FUNCTION1 string,
  vlookup string,
  WORK_TIME float,
  OTBASE float,
  Productive_Hours float,
  Non_Productive_Hours float,
  Total_Hours float,
  Hours_Found string,
  HC_Hours_Status string,
  Exception_Flag1 float,
  Exception_Flag2 float,
  F39 string,
  F40 string,
  Prod float,
  OT float,
  NP float,
  Prod_ string,
  NP_ string
             ) ;"




hadoop dfs -rmr hdfs://COREITKGMTNC20PROD01/sandbox/sandbox31/kpmg_ws/gbadashboardunitcosthc
sqoop  import -libjars '/usr/hdp/2.2.8.0-3150/sqoop/lib/sqljdbc4.jar' \
-m 6 \
--driver 'com.microsoft.sqlserver.jdbc.SQLServerDriver' \
--connect 'jdbc:sqlserver://CLDPRD0SQL01552.itservices.sbc.com\PD_ITMSPR01' \
--username 'ferrari' \
--password 'm@r1shka' \
--as-textfile \
--query 'select * from GBADashBoardUnitCostHC where $CONDITIONS' \
--target-dir 'hdfs://COREITKGMTNC20PROD01/sandbox/sandbox31/kpmg_ws/gbadashboardunitcosthc' \
--split-by 'FORCE_COUNT' \
--fields-terminated-by '\001' \
--null-string '\\N' \
--null-non-string '\\N' \
--hive-drop-import-delims


hive -e "USE kpmg_ws;drop table GBADashBoardUnitCostHCOLD; CREATE TABLE GBADashBoardUnitCostHCOLD (
              FORCE_COUNT float,
  Month string,
  RC_Code string,
  City string,
  Work_State_Country_Desc string,
  Work_Region string,
  HR_Id string,
  EMPLOYEE_FIRST_NM string,
  EMPLOYEE_LAST_NM string,
  Job_Description string,
  Band string,
  Level string,
  Level_2A string,
  Level_3 string,
  Level_4 string,
  Description string,
  CBL_Element string,
  Band_Classification string,
  OL_VP_Level string,
  Dom_v_MoW string,
  VP_from_Focals string,
  AVP string,
  LOB1 string,
  LOB2 string,
  Dom_Glbl string,
  LOB string,
  Production_Non_Production_Worker string,
  FUNCTION1 string,
  vlookup string,
  WORK_TIME float,
  OTBASE float,
  Productive_Hours float,
  Non_Productive_Hours float,
  Total_Hours float,
  Hours_Found string,
  HC_Hours_Status string,
  Exception_Flag1 float,
  Exception_Flag2 float,
  F39 string,
  F40 string,
  Prod float,
  OT float,
  NP float,
  Prod_ string,
  NP_ string
             ) ;"


hadoop dfs -rmr hdfs://COREITKGMTNC20PROD01/sandbox/sandbox31/kpmg_ws/gbadashboardunitcosthcold
sqoop  import -libjars '/usr/hdp/2.2.8.0-3150/sqoop/lib/sqljdbc4.jar' \
-m 6 \
--driver 'com.microsoft.sqlserver.jdbc.SQLServerDriver' \
--connect 'jdbc:sqlserver://CLDPRD0SQL01552.itservices.sbc.com\PD_ITMSPR01' \
--username 'ferrari' \
--password 'm@r1shka' \
--as-textfile \
--query 'select * from GBADashBoardUnitCostHCOLD where $CONDITIONS' \
--target-dir 'hdfs://COREITKGMTNC20PROD01/sandbox/sandbox31/kpmg_ws/gbadashboardunitcosthcold' \
--split-by 'FORCE_COUNT' \
--fields-terminated-by '\001' \
--null-string '\\N' \
--null-non-string '\\N' \
--hive-drop-import-delims



hive -e "USE kpmg_ws;drop table GBADashBoardUnitCostHCOLD1; CREATE TABLE GBADashBoardUnitCostHCOLD1 (
              FORCE_COUNT float,
  Month string,
  RC_Code string,
  City string,
  Work_State_Country_Desc string,
  Work_Region string,
  HR_Id string,
  EMPLOYEE_FIRST_NM string,
  EMPLOYEE_LAST_NM string,
  Job_Description string,
  Band string,
  Level string,
  Level_2A string,
  Level_3 string,
  Level_4 string,
  Description string,
  CBL_Element string,
  Band_Classification string,
  OL_VP_Level string,
  Dom_v_MoW string,
  VP_from_Focals string,
  AVP string,
  LOB1 string,
  LOB2 string,
  Dom_Glbl string,
  LOB string,
  Production_Non_Production_Worker string,
  FUNCTION1 string,
  vlookup string,
  WORK_TIME float,
  OTBASE float,
  Productive_Hours float,
  Non_Productive_Hours float,
  Total_Hours float,
  Hours_Found string,
  HC_Hours_Status string,
  Exception_Flag1 float,
  Exception_Flag2 float,
  F39 string,
  F40 string,
  Prod float,
  OT float,
  NP float,
  Prod_ string,
  NP_ string
             ) ;"

hadoop dfs -rmr hdfs://COREITKGMTNC20PROD01/sandbox/sandbox31/kpmg_ws/gbadashboardunitcosthcold1
sqoop  import -libjars '/usr/hdp/2.2.8.0-3150/sqoop/lib/sqljdbc4.jar' \
-m 6 \
--driver 'com.microsoft.sqlserver.jdbc.SQLServerDriver' \
--connect 'jdbc:sqlserver://CLDPRD0SQL01552.itservices.sbc.com\PD_ITMSPR01' \
--username 'ferrari' \
--password 'm@r1shka' \
--as-textfile \
--query 'select * from GBADashBoardUnitCostHCOLD1 where $CONDITIONS' \
--target-dir 'hdfs://COREITKGMTNC20PROD01/sandbox/sandbox31/kpmg_ws/gbadashboardunitcosthcold1' \
--split-by 'FORCE_COUNT' \
--fields-terminated-by '\001' \
--null-string '\\N' \
--null-non-string '\\N' \
--hive-drop-import-delims




hive -e "USE kpmg_ws;drop table GBALocationReport; CREATE TABLE GBALocationReport (
              id string,
  Sup6 string,
  Sup5 string,
  Sup4 string,
  Sup3 string,
  Sup2 string,
  Sup1 string,
  FORCE_COUNT float,
  RESPONSIBILITY_CD string,
  Dom_MoW__Phys_Loc_ string,
  CFO_Origin string,
  Segment string,
  Officer string,
  Officer1 string,
  VP string,
  Director_A string,
  EMPLOYEE_ID string,
  SBC_USER_ID string,
  EMPLOYEE_FIRST_NM string,
  EMPLOYEE_LAST_NM string,
  EMP_OFCL_WORK_LINE_1_ADDR string,
  EMP_OFCL_WORK_LINE_2_ADDR string,
  EMP_OFCL_WORK_ADDR_CITY_NM string,
  EMP_OFCL_WORK_ADDR_STATE_CD string,
  State_Country_Description string,
  Category__HC_LC_ string,
  Region string,
  EMPLOYMENT_STATUS_CD string,
  EMPLOYEE_JOB_DESC string,
  EMPLOYEE_JOB_TITLE_DESC string,
  Band string,
  Band_Classification string,
  Management_Level string,
  JOB_MARKET_ZONE_CD string,
  SUPERVISOR_SBC_USER_ID string,
  CBL_ELEMENT_CD string,
  CBL_ELEMENT_DESC string,
  EMPLOYEE_OVERTIME_EXEMPT_CD string,
  EMPLOYEE_SALARY_BARGAINED_CD string,
  NEW_HIRE_CD string,
  EMPLOYEE_NCS_DT string,
  EMPLOYMENT_CLASSIFICATION_CD string,
  SBC_COMPANY_CD string,
  SBC_COMPANY_NM string,
  Month string,
  Band_1 string,
  Summary_VP string,
  Vlook string,
  Type string,
  Function1 string,
  Summary_VP2 string,
  AVP_RC_Owner string,
  LOB_Summary string,
  LOB_Detail string,
  Country string,
  HCC_LCC string,
  Job_Function string,
  Job_Title_Category string,
  Supv string,
  Occupancy_Indicator string,
  Level_Detail string,
  Level_Detail2 string,
  Level_Price_Out string,
  Rate_Year float,
  Rate_LLR float,
  Cost_FY float,
  Cost_Mth float,
  cou string,
  lease string,
  list string
             ) ;"


hadoop dfs -rmr hdfs://COREITKGMTNC20PROD01/sandbox/sandbox31/kpmg_ws/gbalocationreport
sqoop  import -libjars '/usr/hdp/2.2.8.0-3150/sqoop/lib/sqljdbc4.jar' \
-m 6 \
--driver 'com.microsoft.sqlserver.jdbc.SQLServerDriver' \
--connect 'jdbc:sqlserver://CLDPRD0SQL01552.itservices.sbc.com\PD_ITMSPR01' \
--username 'ferrari' \
--password 'm@r1shka' \
--as-textfile \
--query 'select * from GBALocationReport where $CONDITIONS' \
--target-dir 'hdfs://COREITKGMTNC20PROD01/sandbox/sandbox31/kpmg_ws/gbalocationreport' \
--split-by 'id' \
--fields-terminated-by '\001' \
--null-string '\\N' \
--null-non-string '\\N' \





hadoop dfs -rmr hdfs://COREITKGMTNC20PROD01/sandbox/sandbox31/kpmg_ws/gbadashboardunitcostexpensedetail
sqoop  import -libjars '/usr/hdp/2.2.8.0-3150/sqoop/lib/sqljdbc4.jar' \
-m 6 \
--driver 'com.microsoft.sqlserver.jdbc.SQLServerDriver' \
--connect 'jdbc:sqlserver://CLDPRD0SQL01552.itservices.sbc.com\PD_ITMSPR01' \
--username 'ferrari' \
--password 'm@r1shka' \
--as-textfile \
--query 'select * from GBADashboardUnitCostExpenseDetail where $CONDITIONS' \
--target-dir 'hdfs://COREITKGMTNC20PROD01/sandbox/sandbox31/kpmg_ws/gbadashboardunitcostexpensedetail' \
--split-by 'MonthYear' \
--fields-terminated-by '\001' \
--null-string '\\N' \
--null-non-string '\\N' \
--hive-drop-import-delims

hadoop dfs -rmr hdfs://COREITKGMTNC20PROD01/sandbox/sandbox31/kpmg_ws/gbadashboardunitcostexpensedetailold
sqoop  import -libjars '/usr/hdp/2.2.8.0-3150/sqoop/lib/sqljdbc4.jar' \
-m 6 \
--driver 'com.microsoft.sqlserver.jdbc.SQLServerDriver' \
--connect 'jdbc:sqlserver://CLDPRD0SQL01552.itservices.sbc.com\PD_ITMSPR01' \
--username 'ferrari' \
--password 'm@r1shka' \
--as-textfile \
--query 'select * from GBADashboardUnitCostExpenseDetailOLD where $CONDITIONS' \
--target-dir 'hdfs://COREITKGMTNC20PROD01/sandbox/sandbox31/kpmg_ws/gbadashboardunitcostexpensedetailold' \
--split-by 'MonthYear'  \
--fields-terminated-by '\001' \
--null-string '\\N' \
--null-non-string '\\N' \
--hive-drop-import-delims

hadoop dfs -rmr hdfs://COREITKGMTNC20PROD01/sandbox/sandbox31/kpmg_ws/gbadashboardunitcostexpensedetailold1
sqoop  import -libjars '/usr/hdp/2.2.8.0-3150/sqoop/lib/sqljdbc4.jar' \
-m 6 \
--driver 'com.microsoft.sqlserver.jdbc.SQLServerDriver' \
--connect 'jdbc:sqlserver://CLDPRD0SQL01552.itservices.sbc.com\PD_ITMSPR01' \
--username 'ferrari' \
--password 'm@r1shka' \
--as-textfile \
--query 'select * from GBADashboardUnitCostExpenseDetailOLD1 where $CONDITIONS' \
--target-dir 'hdfs://COREITKGMTNC20PROD01/sandbox/sandbox31/kpmg_ws/gbadashboardunitcostexpensedetailold1' \
--split-by 'MonthYear'   \
--fields-terminated-by '\001' \
--null-string '\\N' \
--null-non-string '\\N' \
--hive-drop-import-delims









#16/06/26 21:42:03 ERROR tool.ImportTool: Imported Failed: Duplicate Column identifier specified: 'WFAFoundMTTR'
#they have two columns in it:
#'WFAFoundMTTR' ''WFA Found MTTR''
#looks like sqoop can not distinguish these two columns.
#but after I checking the result, they are not the sources tables, so we should be fine
#solution: create a view to rename.

#Tables:
#GBADashboardUnitCostFinal
#GBADashboardUnitCostFinalOLD
#GBADashboardUnitCostInitial

