set hive.exec.dynamic.partition.mode=nonstrict;
set hive.exec.dynamic.partition=true;
set hive.exec.max.dynamic.partitions.pernode=300;
set hive.exec.max.dynamic.partitions =10000000;
set hive.optimize.sort.dynamic.partition=true;

set fs.file.impl.disable.cache=false;
set fs.hdfs.impl.disable.cache=false;


insert into table kpmg_ws.elink_size2
PARTITION(start_month)
select e.*
 from kpmg.elink e
inner join kpmg.user_org u
on e.attuid=u.attuid
DISTRIBUTE BY rand();

