set hive.exec.dynamic.partition=true;
 set hive.exec.max.dynamic.partitions=20000;
set fs.file.impl.disable.cache=false;
set fs.hdfs.impl.disable.cache=false;

insert into table kpmg_ws.idesk_size2
PARTITION(active_window_start_date)
select idesk.*
  from kpmg.idesk_log_detail idesk
inner join kpmg.user_org u
on idesk.attuid=u.attuid
DISTRIBUTE BY active_window_start_date;

