set hive.cli.print.header=true;

use kpmg_ws;
insert overwrite local directory '/home/gc096s/guohao/bert_sum_uid'
row format delimited
fields terminated by '|'
select * from kpmg_ws.BERT_PROD_SUMMARY_uid;


insert overwrite local directory '/home/gc096s/guohao/bert_mul'
row format delimited
fields terminated by '|'
select ACTIVITYid,MEASURE_USED  from kpmg_ws.ert_msoc_activity_esm_re_dedup;

insert overwrite local directory '/home/gc096s/guohao/bert_sum'
row format delimited
fields terminated by '|'
select * from kpmg_ws.BERT_PROD_SUMMARY;


