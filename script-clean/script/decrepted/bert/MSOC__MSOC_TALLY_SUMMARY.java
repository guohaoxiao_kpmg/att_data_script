// ORM class for table 'MSOC..MSOC_TALLY_SUMMARY'
// WARNING: This class is AUTO-GENERATED. Modify at your own risk.
//
// Debug information:
// Generated date: Wed Mar 09 21:08:36 EST 2016
// For connector: org.apache.sqoop.manager.GenericJdbcManager
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapred.lib.db.DBWritable;
import com.cloudera.sqoop.lib.JdbcWritableBridge;
import com.cloudera.sqoop.lib.DelimiterSet;
import com.cloudera.sqoop.lib.FieldFormatter;
import com.cloudera.sqoop.lib.RecordParser;
import com.cloudera.sqoop.lib.BooleanParser;
import com.cloudera.sqoop.lib.BlobRef;
import com.cloudera.sqoop.lib.ClobRef;
import com.cloudera.sqoop.lib.LargeObjectLoader;
import com.cloudera.sqoop.lib.SqoopRecord;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class MSOC__MSOC_TALLY_SUMMARY extends SqoopRecord  implements DBWritable, Writable {
  private final int PROTOCOL_VERSION = 3;
  public int getClassFormatVersion() { return PROTOCOL_VERSION; }
  protected ResultSet __cur_result_set;
  private String CUID;
  public String get_CUID() {
    return CUID;
  }
  public void set_CUID(String CUID) {
    this.CUID = CUID;
  }
  public MSOC__MSOC_TALLY_SUMMARY with_CUID(String CUID) {
    this.CUID = CUID;
    return this;
  }
  private Integer ACTIVITYID;
  public Integer get_ACTIVITYID() {
    return ACTIVITYID;
  }
  public void set_ACTIVITYID(Integer ACTIVITYID) {
    this.ACTIVITYID = ACTIVITYID;
  }
  public MSOC__MSOC_TALLY_SUMMARY with_ACTIVITYID(Integer ACTIVITYID) {
    this.ACTIVITYID = ACTIVITYID;
    return this;
  }
  private String PERIOD_TIME;
  public String get_PERIOD_TIME() {
    return PERIOD_TIME;
  }
  public void set_PERIOD_TIME(String PERIOD_TIME) {
    this.PERIOD_TIME = PERIOD_TIME;
  }
  public MSOC__MSOC_TALLY_SUMMARY with_PERIOD_TIME(String PERIOD_TIME) {
    this.PERIOD_TIME = PERIOD_TIME;
    return this;
  }
  private java.sql.Timestamp PERIOD_DATE;
  public java.sql.Timestamp get_PERIOD_DATE() {
    return PERIOD_DATE;
  }
  public void set_PERIOD_DATE(java.sql.Timestamp PERIOD_DATE) {
    this.PERIOD_DATE = PERIOD_DATE;
  }
  public MSOC__MSOC_TALLY_SUMMARY with_PERIOD_DATE(java.sql.Timestamp PERIOD_DATE) {
    this.PERIOD_DATE = PERIOD_DATE;
    return this;
  }
  private Integer QUANTITY;
  public Integer get_QUANTITY() {
    return QUANTITY;
  }
  public void set_QUANTITY(Integer QUANTITY) {
    this.QUANTITY = QUANTITY;
  }
  public MSOC__MSOC_TALLY_SUMMARY with_QUANTITY(Integer QUANTITY) {
    this.QUANTITY = QUANTITY;
    return this;
  }
  private java.sql.Timestamp LOAD_TIME;
  public java.sql.Timestamp get_LOAD_TIME() {
    return LOAD_TIME;
  }
  public void set_LOAD_TIME(java.sql.Timestamp LOAD_TIME) {
    this.LOAD_TIME = LOAD_TIME;
  }
  public MSOC__MSOC_TALLY_SUMMARY with_LOAD_TIME(java.sql.Timestamp LOAD_TIME) {
    this.LOAD_TIME = LOAD_TIME;
    return this;
  }
  private String RC_CODE;
  public String get_RC_CODE() {
    return RC_CODE;
  }
  public void set_RC_CODE(String RC_CODE) {
    this.RC_CODE = RC_CODE;
  }
  public MSOC__MSOC_TALLY_SUMMARY with_RC_CODE(String RC_CODE) {
    this.RC_CODE = RC_CODE;
    return this;
  }
  private Long ROWID;
  public Long get_ROWID() {
    return ROWID;
  }
  public void set_ROWID(Long ROWID) {
    this.ROWID = ROWID;
  }
  public MSOC__MSOC_TALLY_SUMMARY with_ROWID(Long ROWID) {
    this.ROWID = ROWID;
    return this;
  }
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof MSOC__MSOC_TALLY_SUMMARY)) {
      return false;
    }
    MSOC__MSOC_TALLY_SUMMARY that = (MSOC__MSOC_TALLY_SUMMARY) o;
    boolean equal = true;
    equal = equal && (this.CUID == null ? that.CUID == null : this.CUID.equals(that.CUID));
    equal = equal && (this.ACTIVITYID == null ? that.ACTIVITYID == null : this.ACTIVITYID.equals(that.ACTIVITYID));
    equal = equal && (this.PERIOD_TIME == null ? that.PERIOD_TIME == null : this.PERIOD_TIME.equals(that.PERIOD_TIME));
    equal = equal && (this.PERIOD_DATE == null ? that.PERIOD_DATE == null : this.PERIOD_DATE.equals(that.PERIOD_DATE));
    equal = equal && (this.QUANTITY == null ? that.QUANTITY == null : this.QUANTITY.equals(that.QUANTITY));
    equal = equal && (this.LOAD_TIME == null ? that.LOAD_TIME == null : this.LOAD_TIME.equals(that.LOAD_TIME));
    equal = equal && (this.RC_CODE == null ? that.RC_CODE == null : this.RC_CODE.equals(that.RC_CODE));
    equal = equal && (this.ROWID == null ? that.ROWID == null : this.ROWID.equals(that.ROWID));
    return equal;
  }
  public boolean equals0(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof MSOC__MSOC_TALLY_SUMMARY)) {
      return false;
    }
    MSOC__MSOC_TALLY_SUMMARY that = (MSOC__MSOC_TALLY_SUMMARY) o;
    boolean equal = true;
    equal = equal && (this.CUID == null ? that.CUID == null : this.CUID.equals(that.CUID));
    equal = equal && (this.ACTIVITYID == null ? that.ACTIVITYID == null : this.ACTIVITYID.equals(that.ACTIVITYID));
    equal = equal && (this.PERIOD_TIME == null ? that.PERIOD_TIME == null : this.PERIOD_TIME.equals(that.PERIOD_TIME));
    equal = equal && (this.PERIOD_DATE == null ? that.PERIOD_DATE == null : this.PERIOD_DATE.equals(that.PERIOD_DATE));
    equal = equal && (this.QUANTITY == null ? that.QUANTITY == null : this.QUANTITY.equals(that.QUANTITY));
    equal = equal && (this.LOAD_TIME == null ? that.LOAD_TIME == null : this.LOAD_TIME.equals(that.LOAD_TIME));
    equal = equal && (this.RC_CODE == null ? that.RC_CODE == null : this.RC_CODE.equals(that.RC_CODE));
    equal = equal && (this.ROWID == null ? that.ROWID == null : this.ROWID.equals(that.ROWID));
    return equal;
  }
  public void readFields(ResultSet __dbResults) throws SQLException {
    this.__cur_result_set = __dbResults;
    this.CUID = JdbcWritableBridge.readString(1, __dbResults);
    this.ACTIVITYID = JdbcWritableBridge.readInteger(2, __dbResults);
    this.PERIOD_TIME = JdbcWritableBridge.readString(3, __dbResults);
    this.PERIOD_DATE = JdbcWritableBridge.readTimestamp(4, __dbResults);
    this.QUANTITY = JdbcWritableBridge.readInteger(5, __dbResults);
    this.LOAD_TIME = JdbcWritableBridge.readTimestamp(6, __dbResults);
    this.RC_CODE = JdbcWritableBridge.readString(7, __dbResults);
    this.ROWID = JdbcWritableBridge.readLong(8, __dbResults);
  }
  public void readFields0(ResultSet __dbResults) throws SQLException {
    this.CUID = JdbcWritableBridge.readString(1, __dbResults);
    this.ACTIVITYID = JdbcWritableBridge.readInteger(2, __dbResults);
    this.PERIOD_TIME = JdbcWritableBridge.readString(3, __dbResults);
    this.PERIOD_DATE = JdbcWritableBridge.readTimestamp(4, __dbResults);
    this.QUANTITY = JdbcWritableBridge.readInteger(5, __dbResults);
    this.LOAD_TIME = JdbcWritableBridge.readTimestamp(6, __dbResults);
    this.RC_CODE = JdbcWritableBridge.readString(7, __dbResults);
    this.ROWID = JdbcWritableBridge.readLong(8, __dbResults);
  }
  public void loadLargeObjects(LargeObjectLoader __loader)
      throws SQLException, IOException, InterruptedException {
  }
  public void loadLargeObjects0(LargeObjectLoader __loader)
      throws SQLException, IOException, InterruptedException {
  }
  public void write(PreparedStatement __dbStmt) throws SQLException {
    write(__dbStmt, 0);
  }

  public int write(PreparedStatement __dbStmt, int __off) throws SQLException {
    JdbcWritableBridge.writeString(CUID, 1 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeInteger(ACTIVITYID, 2 + __off, 4, __dbStmt);
    JdbcWritableBridge.writeString(PERIOD_TIME, 3 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeTimestamp(PERIOD_DATE, 4 + __off, 93, __dbStmt);
    JdbcWritableBridge.writeInteger(QUANTITY, 5 + __off, 4, __dbStmt);
    JdbcWritableBridge.writeTimestamp(LOAD_TIME, 6 + __off, 93, __dbStmt);
    JdbcWritableBridge.writeString(RC_CODE, 7 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeLong(ROWID, 8 + __off, -5, __dbStmt);
    return 8;
  }
  public void write0(PreparedStatement __dbStmt, int __off) throws SQLException {
    JdbcWritableBridge.writeString(CUID, 1 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeInteger(ACTIVITYID, 2 + __off, 4, __dbStmt);
    JdbcWritableBridge.writeString(PERIOD_TIME, 3 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeTimestamp(PERIOD_DATE, 4 + __off, 93, __dbStmt);
    JdbcWritableBridge.writeInteger(QUANTITY, 5 + __off, 4, __dbStmt);
    JdbcWritableBridge.writeTimestamp(LOAD_TIME, 6 + __off, 93, __dbStmt);
    JdbcWritableBridge.writeString(RC_CODE, 7 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeLong(ROWID, 8 + __off, -5, __dbStmt);
  }
  public void readFields(DataInput __dataIn) throws IOException {
this.readFields0(__dataIn);  }
  public void readFields0(DataInput __dataIn) throws IOException {
    if (__dataIn.readBoolean()) { 
        this.CUID = null;
    } else {
    this.CUID = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.ACTIVITYID = null;
    } else {
    this.ACTIVITYID = Integer.valueOf(__dataIn.readInt());
    }
    if (__dataIn.readBoolean()) { 
        this.PERIOD_TIME = null;
    } else {
    this.PERIOD_TIME = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.PERIOD_DATE = null;
    } else {
    this.PERIOD_DATE = new Timestamp(__dataIn.readLong());
    this.PERIOD_DATE.setNanos(__dataIn.readInt());
    }
    if (__dataIn.readBoolean()) { 
        this.QUANTITY = null;
    } else {
    this.QUANTITY = Integer.valueOf(__dataIn.readInt());
    }
    if (__dataIn.readBoolean()) { 
        this.LOAD_TIME = null;
    } else {
    this.LOAD_TIME = new Timestamp(__dataIn.readLong());
    this.LOAD_TIME.setNanos(__dataIn.readInt());
    }
    if (__dataIn.readBoolean()) { 
        this.RC_CODE = null;
    } else {
    this.RC_CODE = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.ROWID = null;
    } else {
    this.ROWID = Long.valueOf(__dataIn.readLong());
    }
  }
  public void write(DataOutput __dataOut) throws IOException {
    if (null == this.CUID) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, CUID);
    }
    if (null == this.ACTIVITYID) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.ACTIVITYID);
    }
    if (null == this.PERIOD_TIME) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, PERIOD_TIME);
    }
    if (null == this.PERIOD_DATE) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.PERIOD_DATE.getTime());
    __dataOut.writeInt(this.PERIOD_DATE.getNanos());
    }
    if (null == this.QUANTITY) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.QUANTITY);
    }
    if (null == this.LOAD_TIME) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.LOAD_TIME.getTime());
    __dataOut.writeInt(this.LOAD_TIME.getNanos());
    }
    if (null == this.RC_CODE) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, RC_CODE);
    }
    if (null == this.ROWID) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.ROWID);
    }
  }
  public void write0(DataOutput __dataOut) throws IOException {
    if (null == this.CUID) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, CUID);
    }
    if (null == this.ACTIVITYID) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.ACTIVITYID);
    }
    if (null == this.PERIOD_TIME) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, PERIOD_TIME);
    }
    if (null == this.PERIOD_DATE) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.PERIOD_DATE.getTime());
    __dataOut.writeInt(this.PERIOD_DATE.getNanos());
    }
    if (null == this.QUANTITY) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.QUANTITY);
    }
    if (null == this.LOAD_TIME) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.LOAD_TIME.getTime());
    __dataOut.writeInt(this.LOAD_TIME.getNanos());
    }
    if (null == this.RC_CODE) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, RC_CODE);
    }
    if (null == this.ROWID) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.ROWID);
    }
  }
  private static final DelimiterSet __outputDelimiters = new DelimiterSet((char) 1, (char) 10, (char) 0, (char) 0, false);
  public String toString() {
    return toString(__outputDelimiters, true);
  }
  public String toString(DelimiterSet delimiters) {
    return toString(delimiters, true);
  }
  public String toString(boolean useRecordDelim) {
    return toString(__outputDelimiters, useRecordDelim);
  }
  public String toString(DelimiterSet delimiters, boolean useRecordDelim) {
    StringBuilder __sb = new StringBuilder();
    char fieldDelim = delimiters.getFieldsTerminatedBy();
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(CUID==null?"\\N":CUID, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(ACTIVITYID==null?"\\N":"" + ACTIVITYID, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(PERIOD_TIME==null?"\\N":PERIOD_TIME, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(PERIOD_DATE==null?"\\N":"" + PERIOD_DATE, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(QUANTITY==null?"\\N":"" + QUANTITY, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(LOAD_TIME==null?"\\N":"" + LOAD_TIME, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(RC_CODE==null?"\\N":RC_CODE, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(ROWID==null?"\\N":"" + ROWID, delimiters));
    if (useRecordDelim) {
      __sb.append(delimiters.getLinesTerminatedBy());
    }
    return __sb.toString();
  }
  public void toString0(DelimiterSet delimiters, StringBuilder __sb, char fieldDelim) {
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(CUID==null?"\\N":CUID, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(ACTIVITYID==null?"\\N":"" + ACTIVITYID, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(PERIOD_TIME==null?"\\N":PERIOD_TIME, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(PERIOD_DATE==null?"\\N":"" + PERIOD_DATE, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(QUANTITY==null?"\\N":"" + QUANTITY, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(LOAD_TIME==null?"\\N":"" + LOAD_TIME, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(RC_CODE==null?"\\N":RC_CODE, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(ROWID==null?"\\N":"" + ROWID, delimiters));
  }
  private static final DelimiterSet __inputDelimiters = new DelimiterSet((char) 1, (char) 10, (char) 0, (char) 0, false);
  private RecordParser __parser;
  public void parse(Text __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(CharSequence __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(byte [] __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(char [] __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(ByteBuffer __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(CharBuffer __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  private void __loadFromFields(List<String> fields) {
    Iterator<String> __it = fields.listIterator();
    String __cur_str = null;
    try {
    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.CUID = null; } else {
      this.CUID = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.ACTIVITYID = null; } else {
      this.ACTIVITYID = Integer.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.PERIOD_TIME = null; } else {
      this.PERIOD_TIME = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.PERIOD_DATE = null; } else {
      this.PERIOD_DATE = java.sql.Timestamp.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.QUANTITY = null; } else {
      this.QUANTITY = Integer.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.LOAD_TIME = null; } else {
      this.LOAD_TIME = java.sql.Timestamp.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.RC_CODE = null; } else {
      this.RC_CODE = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.ROWID = null; } else {
      this.ROWID = Long.valueOf(__cur_str);
    }

    } catch (RuntimeException e) {    throw new RuntimeException("Can't parse input data: '" + __cur_str + "'", e);    }  }

  private void __loadFromFields0(Iterator<String> __it) {
    String __cur_str = null;
    try {
    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.CUID = null; } else {
      this.CUID = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.ACTIVITYID = null; } else {
      this.ACTIVITYID = Integer.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.PERIOD_TIME = null; } else {
      this.PERIOD_TIME = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.PERIOD_DATE = null; } else {
      this.PERIOD_DATE = java.sql.Timestamp.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.QUANTITY = null; } else {
      this.QUANTITY = Integer.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.LOAD_TIME = null; } else {
      this.LOAD_TIME = java.sql.Timestamp.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.RC_CODE = null; } else {
      this.RC_CODE = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.ROWID = null; } else {
      this.ROWID = Long.valueOf(__cur_str);
    }

    } catch (RuntimeException e) {    throw new RuntimeException("Can't parse input data: '" + __cur_str + "'", e);    }  }

  public Object clone() throws CloneNotSupportedException {
    MSOC__MSOC_TALLY_SUMMARY o = (MSOC__MSOC_TALLY_SUMMARY) super.clone();
    o.PERIOD_DATE = (o.PERIOD_DATE != null) ? (java.sql.Timestamp) o.PERIOD_DATE.clone() : null;
    o.LOAD_TIME = (o.LOAD_TIME != null) ? (java.sql.Timestamp) o.LOAD_TIME.clone() : null;
    return o;
  }

  public void clone0(MSOC__MSOC_TALLY_SUMMARY o) throws CloneNotSupportedException {
    o.PERIOD_DATE = (o.PERIOD_DATE != null) ? (java.sql.Timestamp) o.PERIOD_DATE.clone() : null;
    o.LOAD_TIME = (o.LOAD_TIME != null) ? (java.sql.Timestamp) o.LOAD_TIME.clone() : null;
  }

  public Map<String, Object> getFieldMap() {
    Map<String, Object> __sqoop$field_map = new TreeMap<String, Object>();
    __sqoop$field_map.put("CUID", this.CUID);
    __sqoop$field_map.put("ACTIVITYID", this.ACTIVITYID);
    __sqoop$field_map.put("PERIOD_TIME", this.PERIOD_TIME);
    __sqoop$field_map.put("PERIOD_DATE", this.PERIOD_DATE);
    __sqoop$field_map.put("QUANTITY", this.QUANTITY);
    __sqoop$field_map.put("LOAD_TIME", this.LOAD_TIME);
    __sqoop$field_map.put("RC_CODE", this.RC_CODE);
    __sqoop$field_map.put("ROWID", this.ROWID);
    return __sqoop$field_map;
  }

  public void getFieldMap0(Map<String, Object> __sqoop$field_map) {
    __sqoop$field_map.put("CUID", this.CUID);
    __sqoop$field_map.put("ACTIVITYID", this.ACTIVITYID);
    __sqoop$field_map.put("PERIOD_TIME", this.PERIOD_TIME);
    __sqoop$field_map.put("PERIOD_DATE", this.PERIOD_DATE);
    __sqoop$field_map.put("QUANTITY", this.QUANTITY);
    __sqoop$field_map.put("LOAD_TIME", this.LOAD_TIME);
    __sqoop$field_map.put("RC_CODE", this.RC_CODE);
    __sqoop$field_map.put("ROWID", this.ROWID);
  }

  public void setField(String __fieldName, Object __fieldVal) {
    if ("CUID".equals(__fieldName)) {
      this.CUID = (String) __fieldVal;
    }
    else    if ("ACTIVITYID".equals(__fieldName)) {
      this.ACTIVITYID = (Integer) __fieldVal;
    }
    else    if ("PERIOD_TIME".equals(__fieldName)) {
      this.PERIOD_TIME = (String) __fieldVal;
    }
    else    if ("PERIOD_DATE".equals(__fieldName)) {
      this.PERIOD_DATE = (java.sql.Timestamp) __fieldVal;
    }
    else    if ("QUANTITY".equals(__fieldName)) {
      this.QUANTITY = (Integer) __fieldVal;
    }
    else    if ("LOAD_TIME".equals(__fieldName)) {
      this.LOAD_TIME = (java.sql.Timestamp) __fieldVal;
    }
    else    if ("RC_CODE".equals(__fieldName)) {
      this.RC_CODE = (String) __fieldVal;
    }
    else    if ("ROWID".equals(__fieldName)) {
      this.ROWID = (Long) __fieldVal;
    }
    else {
      throw new RuntimeException("No such field: " + __fieldName);
    }
  }
  public boolean setField0(String __fieldName, Object __fieldVal) {
    if ("CUID".equals(__fieldName)) {
      this.CUID = (String) __fieldVal;
      return true;
    }
    else    if ("ACTIVITYID".equals(__fieldName)) {
      this.ACTIVITYID = (Integer) __fieldVal;
      return true;
    }
    else    if ("PERIOD_TIME".equals(__fieldName)) {
      this.PERIOD_TIME = (String) __fieldVal;
      return true;
    }
    else    if ("PERIOD_DATE".equals(__fieldName)) {
      this.PERIOD_DATE = (java.sql.Timestamp) __fieldVal;
      return true;
    }
    else    if ("QUANTITY".equals(__fieldName)) {
      this.QUANTITY = (Integer) __fieldVal;
      return true;
    }
    else    if ("LOAD_TIME".equals(__fieldName)) {
      this.LOAD_TIME = (java.sql.Timestamp) __fieldVal;
      return true;
    }
    else    if ("RC_CODE".equals(__fieldName)) {
      this.RC_CODE = (String) __fieldVal;
      return true;
    }
    else    if ("ROWID".equals(__fieldName)) {
      this.ROWID = (Long) __fieldVal;
      return true;
    }
    else {
      return false;    }
  }
}
