use kpmg_ws;


drop table BERT_PROD_SUMMARY;
create table BERT_PROD_SUMMARY as 
select trim(UID) as uid, split(cast(PERIOD_DATE as string)," ")[0]  as DATE, d.ACTIVITYID,sum(s.QUANTITY) as quantity_total,sum(s.QUANTITY*d.MEASURE_USED) as NORMALIZED_VOLUME from 
tmp_bert_msoc_tally_summary s
inner join  
(select * from
tmp_bert_msoc_employee
where trim(TITLE)="NP_TPC_TEMS_MIDWEST"
) e
on s.cuid=e.cuid
inner join 
ert_msoc_activity_esm_re_dedup d
on s.ACTIVITYID = d.ACTIVITYID
group by uid,split(cast(PERIOD_DATE as string)," ")[0],d.ACTIVITYID;


drop table BERT_PROD_SUMMARY_uid;
create table BERT_PROD_SUMMARY_uid as
select uid,date,sum(NORMALIZED_VOLUME) as total_NORMALIZED_VOLUME,collect_list(element) as collection
from 
(
select uid,date,NORMALIZED_VOLUME,concat(cast(ACTIVITYID as string),"-",cast(quantity_total as string)) as element
from
BERT_PROD_SUMMARY)x
group by uid,date;


