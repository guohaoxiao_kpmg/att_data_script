#! /bin/bash

# /* -------------------------------------------------------------------------
# * Request:    ATT -- Service Assurance PxQ -- POC
# *             On Sandbox31, scoop from SQL Server the needed tables.
# *             and run the SQL logic to build table GBADashboardUnitCostFinal
# * Analyst:    John Liao
# * Step:       
# * Log:
# *   2016/06/28   John Liao     Initial
# * -----------------------------------------------------------
# */

MailList="guohaoxiao@kpmg.com"


TktSystemWFA='WFA'
TktSystemBMP='BMP'
TktSystemAOTS='AOTS'
TechTouchedALL='All'
TechTouchedWFA='1'
TechTouchedBMP='Touched'
TechTouchedAOTS='Yes'
 

hive -e "  USE KPMG_WS; 
  drop table if exists tmp_LOB;
  create table tmp_LOB
  as
  select distinct
         LOB
        ,Year
        ,month(from_unixtime(unix_timestamp(month,'MM/dd/yyyy'))) as MonthNumber
  from GBADashboardData
  Where Month!='All' 
    and Month is not null 
    and (TktSystem='${TktSystemWFA}' 
      or TktSystem='${TktSystemBMP}'
      or TktSystem='${TktSystemAOTS}'
        ) 
    and TechTouched='${TechTouchedALL}'
 union all
  select LOB
       , YEAR
       , MonthNumber  
  from GBADashboardUnitCostAdditionalLOB
  ; 
  
  drop table if exists tmp_VolumeForecastTouched;
     
  create table tmp_VolumeForecastTouched
  as
  Select GBADashboardUnitCostMapping.LOB
        ,Year
        ,month(from_unixtime(unix_timestamp(month,'MM/dd/yyyy'))) as MonthNumber
        ,VolumeForecastTouched
  from GBADashboardUnitCostForecastTouched
    left join 
       GBADashboardUnitCostMapping
     on GBADashboardUnitCostForecastTouched.LOB=GBADashboardUnitCostMapping.ForecastTouched
  where GBADashboardUnitCostMapping.Ticketing is not null 
    and GBADashboardUnitCostMapping.Ticketing !='NO DATA'
  order by GBADashboardUnitCostMapping.LOB,Year,MonthNumber
  ;

  drop table if exists tmp_NicheVolumes;
  
  create table tmp_NicheVolumes
  AS
  select LOB
       , Year
       , Month         as MonthNumber
       , WD_KPI
       , WD_KPI_Values
       , WD_KPI_Names
  FROM GBADashboardDataNicheValues
  where GBADashboardDataNicheValues.WD_KPI='WD Total'
  ;
  
  drop table if exists tmp_DWTWeights;

  create table tmp_DWTWeights
  AS
  Select GBADashboardUnitCostMapping.LOB
        ,Year
        ,DWTWeights
  from GBADashboardUnitCostDWT
    left join 
       GBADashboardUnitCostMapping
      on GBADashboardUnitCostDWT.LOB=GBADashboardUnitCostMapping.ForecastTouched
  where GBADashboardUnitCostMapping.Ticketing is not null 
    and GBADashboardUnitCostMapping.Ticketing !='NO DATA'
  order by GBADashboardUnitCostMapping.LOB,Year
  ;          
            
  drop table if exists tmp_BMPCutVolume;
     
  create table tmp_BMPCutVolume
  AS
  Select LOB
        ,Year
        ,month(from_unixtime(unix_timestamp(month,'MM/dd/yyyy'))) as MonthNumber
        ,Tickets as BMPCutVolume
  from GBADashboardDataCUT
  Where Month!='All' 
    and Month is not null 
    and TktSystem='${TktSystemBMP}'
    and TechTouched='${TechTouchedALL}'
  ;
  
  drop table if exists tmp_WFACutVolume;
            
  create table tmp_WFACutVolume
  AS
  Select LOB
        ,Year
        ,month(from_unixtime(unix_timestamp(month,'MM/dd/yyyy'))) as MonthNumber
        ,Tickets as WFACutVolume
  from GBADashboardDataCUT
  Where Month!='All' 
    and Month is not null 
    and TktSystem='${TktSystemWFA}' 
    and TechTouched='${TechTouchedALL}'
  ;

  drop table if exists tmp_WFAFromBMPVolume;
  
  create table tmp_WFAFromBMPVolume
  as
  Select LOB
        ,Year
        ,month(from_unixtime(unix_timestamp(month,'MM/dd/yyyy'))) as MonthNumber
        ,Tickets as WFAFromBMPVolume
  from GBADashboardDataATX
  Where Month!='All' 
    and Month is not null 
    and TktSystem='${TktSystemWFA}' 
    and TechTouched='${TechTouchedALL}'
  ;

  drop table if exists tmp_WFAFromBMPTouchedVolume
  ;
  
  create table tmp_WFAFromBMPTouchedVolume
  as
  Select LOB
        ,Year
        ,month(from_unixtime(unix_timestamp(month,'MM/dd/yyyy'))) as MonthNumber
        ,Tickets as WFAFromBMPTouchedVolume
  from GBADashboardDataATX
  Where Month!='All' 
    and Month is not null 
    and TktSystem='${TktSystemWFA}' 
    and TechTouched='${TechTouchedWFA}'
  ;
  

  drop table if exists tmp_WFAFromCarrierVolume
  ;
  
  create table tmp_WFAFromCarrierVolume
  as
  Select LOB
        ,Year
        ,month(from_unixtime(unix_timestamp(month,'MM/dd/yyyy'))) as MonthNumber
        ,Tickets as WFAFromCarrierVolume
  from GBADashboardDataCarrier
  Where Month!='All' 
  and Month is not null 
  and TktSystem='${TktSystemWFA}' 
  and TechTouched='${TechTouchedALL}'
  ;
  

  drop table if exists tmp_WFAFromCarrierTouchedVolume
  ;
  
  create table tmp_WFAFromCarrierTouchedVolume
  as
  Select LOB
        ,Year
        ,month(from_unixtime(unix_timestamp(month,'MM/dd/yyyy'))) as MonthNumber
        ,Tickets as WFAFromCarrierTouchedVolume
  from GBADashboardDataCarrier
  Where Month!='All' 
  and Month is not null 
  and TktSystem='${TktSystemWFA}' 
  and TechTouched='$TechTouchedWFA}'
  ;
  
  drop table if exists tmp_WFATouched
  ;
  
  create table tmp_WFATouched
  as
  Select LOB
        ,Year
        ,month(from_unixtime(unix_timestamp(month,'MM/dd/yyyy'))) as MonthNumber
        ,Tickets as WFATouchedVolume
  from GBADashboardData
  Where Month!='All' 
    and Month is not null 
    and TktSystem='${TktSystemWFA}' 
    and TechTouched='$TechTouchedWFA}'
  ;
  
  drop table if exists tmp_BMPTouched
  ;
  
  create table tmp_BMPTouched
  as
  Select LOB
        ,Year
        ,month(from_unixtime(unix_timestamp(month,'MM/dd/yyyy'))) as MonthNumber
        ,Tickets as BMPTouchedVolume
  from GBADashboardData
  Where Month!='All' 
    and Month is not null 
    and TktSystem='${TktSystemBMP}' 
    and TechTouched='${TechTouchedBMP}'
  ;
  
  drop table if exists tmp_AOTSTouched
  ;
  
  create table tmp_AOTSTouched
  as
  Select LOB
        ,Year
        ,month(from_unixtime(unix_timestamp(month,'MM/dd/yyyy'))) as MonthNumber
        ,Tickets as AOTSTouchedVolume
  from GBADashboardData
  Where Month!='All' 
    and Month is not null
    and TktSystem='${TktSystemAOTS}'
    and TechTouched='${TechTouchedAOTS}'
  ;
  
  drop table if exists tmp_WFA
  ;
    
  create table tmp_WFA
  as
  Select LOB
        ,Year
        ,month(from_unixtime(unix_timestamp(month,'MM/dd/yyyy'))) as MonthNumber
        ,Tickets as WFAVolume
        ,MeasureTickets as WFAMeasuredVolume
        ,MTTR  as WFAMTTR
        ,FoundMTTR as WFAFoundMTTR
        ,SVB as WFASVB
        ,RepeatPer as WFARepeatPer
        ,ReworkPer as WFAReworkPer
        ,ChronicPer as WFAChronicPer
        ,TotalTouches as WFATouches
  from GBADashboardData
  Where Month!='All' 
    and Month is not null 
    and TktSystem='${TktSystemWFA}' 
    and TechTouched='${TechTouchedALL}'
  ;
  
  drop table if exists tmp_BMP
  ;
  
  create table tmp_BMP
  as
  Select LOB
        ,Year
        ,month(from_unixtime(unix_timestamp(month,'MM/dd/yyyy'))) as MonthNumber
        ,Tickets as BMPVolume
        ,MeasureTickets BMPMeasuredVolume
        ,MTTR  as BMPMTTR
        ,FoundMTTR as BMPFoundMTTR
        ,SVB as BMPSVB
        ,RepeatPer as BMPRepeatPer
        ,ReworkPer as BMPReworkPer
        ,ChronicPer as BMPChronicPer
        ,TotalTouches as BMPTouches
  from GBADashboardData
  Where Month!='All' 
  and Month is not null 
  and TktSystem='${TktSystemBMP}' 
  and TechTouched='${TechTouchedALL}'
  ;
   
  drop table if exists tmp_AOTS
  ;
  
  create table tmp_AOTS
  as
  Select LOB
        ,Year
        ,month(from_unixtime(unix_timestamp(month,'MM/dd/yyyy'))) as MonthNumber
        ,Tickets as AOTSVolume
        ,MeasureTickets as AOTSMeasuredVolume
        ,MTTR  as AOTSMTTR
        ,FoundMTTR as AOTSFoundMTTR
        ,SVB as AOTSSVB 
        ,RepeatPer AS AOTSRepeatPer
        ,ReworkPer as AOTSReworkPer
        ,ChronicPer as AOTSChronicPer
        ,TotalTouches as AOTSTouches
  from GBADashboardData
  Where Month!='All' 
    and Month is not null 
    and TktSystem='${TktSystemAOTS}' 
    and TechTouched='${TechTouchedALL}'
  ; 

  drop table if exists tmp_HCandHours
  ;
  
  Create table tmp_HCandHours
  as
  select GBADashboardUnitCostMapping.Ticketing                   as LOB
        ,year(from_unixtime(unix_timestamp(month,'MM-yyyy')))    as year
        ,month(from_unixtime(unix_timestamp(month,'MM-yyyy')))   as MonthNumber
        ,sum(FORCE_COUNT)                                        as InitialHeadcount
        ,SUM(OTBASE)                                             as OvertimeHours
        ,Sum(Productive_Hours)                                   as InitialProductiveHours 
        ,Sum(Non_Productive_Hours)                               as InitialNonProductiveHours 
  from GBADashBoardUnitCostHC    
    left join 
       GBADashboardUnitCostMapping
     on GBADashBoardUnitCostHC.LOB2=GBADashboardUnitCostMapping.HC
  where Production_Non_Production_Worker in ('Production', 'Non-Production')
    and GBADashboardUnitCostMapping.Ticketing is not null 
    and GBADashboardUnitCostMapping.Ticketing !='NO DATA'
  group by GBADashboardUnitCostMapping.Ticketing 
          ,year(from_unixtime(unix_timestamp(month,'MM-yyyy')))
          ,month(from_unixtime(unix_timestamp(month,'MM-yyyy')))
  ;

  drop table if exists tmp_HCandHoursProductive
  ;
  
  Create table tmp_HCandHoursProductive
  as
  select GBADashboardUnitCostMapping.Ticketing                   as LOB
        ,year(from_unixtime(unix_timestamp(month,'MM-yyyy')))    as year
        ,month(from_unixtime(unix_timestamp(month,'MM-yyyy')))   as MonthNumber
        ,sum(FORCE_COUNT)                                        as InitialProductiveHeadcount 
        ,Sum(Productive_Hours)                                   as InitialProductiveHoursProductiveHC 
  from GBADashBoardUnitCostHC    
    left join 
       GBADashboardUnitCostMapping
     on GBADashBoardUnitCostHC.LOB2=GBADashboardUnitCostMapping.HC
  where Production_Non_Production_Worker='Production'
    and GBADashboardUnitCostMapping.Ticketing is not null 
    and GBADashboardUnitCostMapping.Ticketing !='NO DATA'
  group by GBADashboardUnitCostMapping.Ticketing 
          ,year(from_unixtime(unix_timestamp(month,'MM-yyyy')))
          ,month(from_unixtime(unix_timestamp(month,'MM-yyyy')))
  ;
  
  drop table if exists tmp_Exp
  ;
  
  Create table tmp_Exp
  as
  select GBADashboardUnitCostMapping.Ticketing                   as LOB
        ,year(from_unixtime(unix_timestamp(monthyear,'MM-yyyy')))    as year
        ,month(from_unixtime(unix_timestamp(monthyear,'MM-yyyy')))   as MonthNumber
        ,sum(coalesce(cast(jan as double),0.0) + coalesce(cast(feb as double),0.0) + coalesce(cast(mar as double),0.0)
           + coalesce(cast(apr as double),0.0) + coalesce(cast(may as double),0.0) + coalesce(cast(jun as double),0.0)
           + coalesce(cast(jul as double),0.0) + coalesce(cast(aug as double),0.0) + coalesce(cast(sep as double),0.0)
           + coalesce(cast(oct as double),0.0) + coalesce(cast(nov as double),0.0) + coalesce(cast(dec as double),0.0)
         )                                                       as InitialExpense
  from GBADashboardUnitCostExpenseDetail
  left join 
       GBADashboardUnitCostMapping
    on GBADashboardUnitCostExpenseDetail.LOB1=GBADashboardUnitCostMapping.COST
  where GBADashboardUnitCostMapping.Ticketing is not null 
    and GBADashboardUnitCostMapping.Ticketing !='NO DATA'
  group by  GBADashboardUnitCostMapping.Ticketing                
           ,year(from_unixtime(unix_timestamp(monthyear,'MM-yyyy'))) 
           ,month(from_unixtime(unix_timestamp(monthyear,'MM-yyyy')))
  ;
 
  drop table if exists tmp_LoandedOut
  ;
  
  Create table tmp_LoandedOut
  as
  Select LOB
        ,Year
        ,MonthNumber
        ,LoanedOutHours as LoandedOutHours
        ,TrainingHours
        ,LOBLoandedTo
  from GBADashboardUnitCostLoanedAndTraining
  ;
  
  drop table if exists tmp_Quality
  ;
  
  Create table tmp_Quality
  as
  select GBADashboardUnitCostMapping.Ticketing                                               as LOB
        ,SAGCSCQualityCombinedAudit.Year
        ,month(from_unixtime(unix_timestamp(SAGCSCQualityCombinedAudit.Month, 'MMMMM')))     as MonthNumber
        ,COUNT(distinct concat(TechATTUID,TicketNumber))                                     as QualityReviewVolume 
        ,Round(AVG(Score),2)                                                                 as QualityReviewScore 
  from SAGCSCQualityCombinedAudit
    left join 
       attextractpeople 
     on SAGCSCQualityCombinedAudit.TechATTUID=attextractpeople.ATTUID
    left join 
       LOBCostCenterMappingsFinance 
     on attextractpeople.CostCenter=LOBCostCenterMappingsFinance.RC
    left join 
       GBADashboardUnitCostMapping 
     on LOBCostCenterMappingsFinance.LOB=GBADashboardUnitCostMapping.COST
  where GBADashboardUnitCostMapping.Ticketing is not null 
    and GBADashboardUnitCostMapping.Ticketing!='NO DATA'
  group by GBADashboardUnitCostMapping.Ticketing
          ,SAGCSCQualityCombinedAudit.Year
          ,month(from_unixtime(unix_timestamp(SAGCSCQualityCombinedAudit.Month, 'MMMMM')))
  ;

  drop table if exists tmp_Productivity
  ;
  
  -----------------------------------------------------------------------------------
  -- We can simplify the logic here:
  -- join to LOBCostCenterMappingsFinance and GBADashboardUnitCostMapping is to 
  -- get a valid GBADashboardUnitCostMapping.Ticketing and
  -- apply condition GBADashboardUnitCostMapping.Ticketing!='NO DATA'
  -- We better do inner joins
  -- Also, ALLMaster.datadate needs to be -1 to -12 months from current date
  -- we can do it together, no need to union 12 sub-queries together
  ------------------------------------------------------------------------------------
  Create table tmp_Productivity
  as
  select GBADashboardUnitCostMapping.Ticketing                                            as LOB
        ,LOBCostCenterMappingsFinance.Year                                                as year
        ,month(from_unixtime(unix_timestamp(ALLMaster.DataDate,'yyyy/MM/dd')))            as MonthNumber
        ,SUM(AllMaster.Agent_Count)                                                       as activities
  FROM ALLMaster
      ,LOBCostCenterMappingsFinance 
      ,GBADashboardUnitCostMapping 
  where ALLMaster.blank is not null 
    and ALLMaster.Groupings='.Total' 
    and ALLMaster.Last_Act='.Total'  
    and ALLMaster.CostCenter=LOBCostCenterMappingsFinance.RC
    and LOBCostCenterMappingsFinance.LOB=GBADashboardUnitCostMapping.COST
    and GBADashboardUnitCostMapping.Ticketing is not null 
    and GBADashboardUnitCostMapping.Ticketing!='NO DATA'
    and year(from_unixtime(unix_timestamp(ALLMaster.DataDate,'yyyy/MM/dd'))) = LOBCostCenterMappingsFinance.Year 
    and month(from_unixtime(unix_timestamp(ALLMaster.DataDate,'yyyy/MM/dd'))) = month(from_unixtime(unix_timestamp(LOBCostCenterMappingsFinance.Month, 'MMM')))
    and (year(from_unixtime(unix_timestamp(ALLMaster.DataDate,'yyyy/MM/dd'))) - year(from_unixtime(unix_timestamp())))*12 + 
        (month(from_unixtime(unix_timestamp(ALLMaster.DataDate,'yyyy/MM/dd'))) - month(from_unixtime(unix_timestamp()))) between -13 and -1
  group by GBADashboardUnitCostMapping.Ticketing                                
          ,LOBCostCenterMappingsFinance.Year                                    
          ,month(from_unixtime(unix_timestamp(ALLMaster.DataDate,'yyyy/MM/dd')))
  ;
  

  drop table if exists tmp_Targets
  ;
   
  Create table tmp_Targets
  as
  select LOB
        ,cast(wfa_found_mttr      AS double)       AS wfa_found_mttr     
        ,cast(WFA_SVB             AS double)       AS WFA_SVB            
        ,CAST(WFA_Repeat_percent  AS double)       AS WFA_Repeat_percent 
        ,CAST(BMP_Found_MTTR      AS double)       AS BMP_Found_MTTR     
        ,CAST(BMP_SVB             AS double)       AS BMP_SVB            
        ,CAST(BMP_Repeat_percent  AS double)       AS BMP_Repeat_percent 
        ,CAST(AOTS_Found_MTTR     AS double)       AS AOTS_Found_MTTR    
        ,CAST(AOTS_SVB            AS double)       AS AOTS_SVB           
        ,CAST(AOTS_Repeat_percent AS double)       AS AOTS_Repeat_percent
  from GBADashboardUnitCostTargets
  ;
 
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
------ BREAK POINT: combine update part together, because all  LoanedInHours is not null --------
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------

  drop table if exists tmp_DataInitial
  ;
  
  CREATE TABLE tmp_DataInitial
  as 
  select tmp_LOB.LOB
        ,tmp_LOB.Year 
        ,tmp_LOB.MonthNumber 
	    ,tmp_VolumeForecastTouched.VolumeForecastTouched
        ,tmp_WFA.WFAVolume
        ,tmp_WFATouched.WFATouchedVolume
        ,tmp_WFA.WFAMeasuredVolume
        ,tmp_WFACutVolume.WFACutVolume
        ,tmp_WFAFromBMPVolume.WFAFromBMPVolume
        ,tmp_WFAFromBMPTouchedVolume.WFAFromBMPTouchedVolume
        ,tmp_WFAFromCarrierVolume.WFAFromCarrierVolume                  as WFACarrierVolume
        ,tmp_WFA.WFAMTTR
        ,tmp_WFA.WFAFoundMTTR
        ,tmp_WFA.WFASVB
        ,tmp_WFA.WFARepeatPer
        ,tmp_WFA.WFAReworkPer
        ,tmp_WFA.WFAChronicPer
        ,tmp_WFA.WFATouches
        ,tmp_BMP.BMPVolume
        ,tmp_BMPTouched.BMPTouchedVolume
        ,tmp_BMP.BMPMeasuredVolume
        ,tmp_BMP.BMPMTTR
        ,tmp_BMP.BMPFoundMTTR
        ,tmp_BMP.BMPSVB
        ,tmp_BMP.BMPRepeatPer
        ,tmp_BMP.BMPReworkPer
        ,tmp_BMP.BMPChronicPer
        ,tmp_BMP.BMPTouches
        ,tmp_AOTS.AOTSVolume
        ,tmp_AOTSTouched.AOTSTouchedVolume
        ,tmp_AOTS.AOTSMeasuredVolume
        ,tmp_AOTS.AOTSMTTR
        ,tmp_AOTS.AOTSFoundMTTR
        ,tmp_AOTS.AOTSSVB
        ,tmp_AOTS.AOTSRepeatPer
        ,tmp_AOTS.AOTSReworkPer
        ,tmp_AOTS.AOTSChronicPer
        ,tmp_AOTS.AOTSTouches
        ,coalesce(tmp_WFA.WFAVolume,0)+coalesce(tmp_BMP.BMPVolume,0)+coalesce(tmp_AOTS.AOTSVolume,0)+coalesce(tmp_NicheVolumes.WD_KPI_Values,0)    as AllVolume
        ,coalesce(tmp_WFATouched.WFATouchedVolume,0)+coalesce(tmp_BMPTouched.BMPTouchedVolume,0)+coalesce(tmp_AOTSTouched.AOTSTouchedVolume,0)
         +coalesce(tmp_NicheVolumes.WD_KPI_Values,0)                                                                                               as AllTouchedVolume
        ,coalesce(tmp_WFA.WFAMeasuredVolume,0)+coalesce(tmp_BMP.BMPMeasuredVolume,0)+coalesce(tmp_AOTS.AOTSMeasuredVolume,0)                       as AllMeasuredVolume
        ,(coalesce(tmp_WFA.WFAVolume,0)+coalesce(tmp_BMP.BMPVolume,0)+coalesce(tmp_AOTS.AOTSVolume,0)+coalesce(tmp_NicheVolumes.WD_KPI_Values,0))
         -coalesce(tmp_WFAFromBMPVolume.WFAFromBMPVolume,0)-coalesce(tmp_WFAFromCarrierVolume.WFAFromCarrierVolume,0)                              as AllIncidentVolume
        ,(coalesce(tmp_WFATouched.WFATouchedVolume,0)+coalesce(tmp_BMPTouched.BMPTouchedVolume,0)+coalesce(tmp_AOTSTouched.AOTSTouchedVolume,0)
         +coalesce(tmp_NicheVolumes.WD_KPI_Values,0))-coalesce(tmp_WFAFromBMPTouchedVolume.WFAFromBMPTouchedVolume,0)
         -coalesce(tmp_WFAFromCarrierTouchedVolume.WFAFromCarrierTouchedVolume,0)                                                                  as AllIncidentTouchedVolume
        ,tmp_HCandHours.InitialHeadcount
        ,tmp_Exp.InitialExpense
        ,tmp_HCandHours.InitialProductiveHours
        ,tmp_HCandHours.InitialNonProductiveHours
        ,tmp_HCandHours.OvertimeHours
        ,tmp_LoandedOut.LoandedOutHours
        ,tmp_LoandedOut.LOBLoandedTo
        ,(tmp_LoandedOut.LoandedOutHours)                                                                                                                                   as LoandedInHours
        ,tmp_LoandedOut.TrainingHours

        ,coalesce(round(tmp_HCandHoursProductive.InitialProductiveHoursProductiveHC/if(tmp_HCandHoursProductive.InitialProductiveHeadcount!=0,tmp_HCandHoursProductive.InitialProductiveHeadcount,null),2),0)   as AverageProdHrsPerHC
        ,coalesce(round(tmp_Exp.InitialExpense/if(tmp_HCandHours.InitialHeadcount!=0,tmp_HCandHours.InitialHeadcount,null),2),0)                                                     as AverageExpPerHC
        ,coalesce(round(tmp_LoandedOut.LoandedOutHours/if((round(tmp_HCandHoursProductive.InitialProductiveHoursProductiveHC/
         if(tmp_HCandHoursProductive.InitialProductiveHeadcount!=0,tmp_HCandHoursProductive.InitialProductiveHeadcount,null),2))!=0,(round(tmp_HCandHoursProductive.InitialProductiveHoursProductiveHC/
         if(tmp_HCandHoursProductive.InitialProductiveHeadcount!=0,tmp_HCandHoursProductive.InitialProductiveHeadcount,null),2)),null),2),0)                                                                as LoanedHC
        ,(coalesce((round(tmp_Exp.InitialExpense/if(tmp_HCandHours.InitialHeadcount!=0,tmp_HCandHours.InitialHeadcount,null),2))*
         round(tmp_LoandedOut.LoandedOutHours/if((round(tmp_HCandHoursProductive.InitialProductiveHoursProductiveHC/
         if(tmp_HCandHoursProductive.InitialProductiveHeadcount!=0,tmp_HCandHoursProductive.InitialProductiveHeadcount,null),2))!=0,(round(tmp_HCandHoursProductive.InitialProductiveHoursProductiveHC/
         if(tmp_HCandHoursProductive.InitialProductiveHeadcount!=0,tmp_HCandHoursProductive.InitialProductiveHeadcount,null),2)),null),2),0))                                                                as LoandedExp
        ,tmp_Quality.QualityReviewVolume
        ,tmp_Quality.QualityReviewScore
        ,tmp_Productivity.Activities
        ,coalesce(round(tmp_Productivity.Activities/if((coalesce(tmp_WFATouched.WFATouchedVolume,0)+
         coalesce(tmp_BMPTouched.BMPTouchedVolume,0)+coalesce(tmp_AOTSTouched.AOTSTouchedVolume,0)+
         coalesce(tmp_NicheVolumes.WD_KPI_Values,0))!=0,(coalesce(tmp_WFATouched.WFATouchedVolume,0)+
         coalesce(tmp_BMPTouched.BMPTouchedVolume,0)+coalesce(tmp_AOTSTouched.AOTSTouchedVolume,0)+
         coalesce(tmp_NicheVolumes.WD_KPI_Values,0)),null),2),0)                                                                                      AS ActivitiesPerTouchedTicket
        ,''                                                                                                                                        as AutoPickCount
        ,''                                                                                                                                        as TotalClaims
        ,''                                                                                                                                        as  AutoPickPercentage
        ,tmp_BMPCutVolume.BMPCutVolume
        ,tmp_Targets.WFA_Found_MTTR
        ,tmp_Targets.WFA_SVB
        ,tmp_Targets.WFA_Repeat_percent
        ,tmp_Targets.BMP_Found_MTTR
        ,tmp_Targets.BMP_SVB
        ,tmp_Targets.BMP_Repeat_percent
        ,tmp_Targets.AOTS_Found_MTTR
        ,tmp_Targets.AOTS_SVB
        ,tmp_Targets.AOTS_Repeat_percent
        ,tmp_HCandHoursProductive.InitialProductiveHeadcount                                                                                       as InitialProductiveHeadcount
        ,tmp_HCandHoursProductive.InitialProductiveHoursProductiveHC                                                                               as InitialProductiveHoursProductiveHC
        ,tmp_HCandHoursProductive.InitialProductiveHoursProductiveHC/if(tmp_HCandHoursProductive.InitialProductiveHeadcount!=0,tmp_HCandHoursProductive.InitialProductiveHeadcount,null)                 as AvgProductiveHoursProductiveHCPerProductiveHeadcount
        ,coalesce(tmp_BMPCutVolume.BMPCutVolume,0)+ coalesce(tmp_WFACutVolume.WFACutVolume,0)                                                      as TotalCutVolume
        ,tmp_DWTWeights.DWTWeights
        ,tmp_WFAFromCarrierTouchedVolume.WFAFromCarrierTouchedVolume  
 from tmp_LOB
    left join 
       tmp_WFA on tmp_LOB.LOB=tmp_WFA.LOB and tmp_LOB.Year=tmp_WFA.Year and tmp_LOB.MonthNumber=tmp_WFA.MonthNumber
    left join 
       tmp_BMP on tmp_LOB.LOB=tmp_BMP.LOB and tmp_LOB.Year=tmp_BMP.Year and tmp_LOB.MonthNumber=tmp_BMP.MonthNumber
    left join 
       tmp_AOTS on tmp_LOB.LOB=tmp_AOTS.LOB and tmp_LOB.Year=tmp_AOTS.Year and tmp_LOB.MonthNumber=tmp_AOTS.MonthNumber
    left join
       tmp_WFATouched on tmp_LOB.LOB=tmp_WFATouched.LOB and tmp_LOB.Year=tmp_WFATouched.Year and tmp_LOB.MonthNumber=tmp_WFATouched.MonthNumber
    left join
       tmp_BMPTouched on tmp_LOB.LOB=tmp_BMPTouched.LOB and tmp_LOB.Year=tmp_BMPTouched.Year and tmp_LOB.MonthNumber=tmp_BMPTouched.MonthNumber
    left join
       tmp_AOTSTouched on tmp_LOB.LOB=tmp_AOTSTouched.LOB and tmp_LOB.Year=tmp_AOTSTouched.Year and tmp_LOB.MonthNumber=tmp_AOTSTouched.MonthNumber
    left join
       tmp_WFACutVolume on tmp_LOB.LOB=tmp_WFACutVolume.LOB and tmp_LOB.Year=tmp_WFACutVolume.Year and tmp_LOB.MonthNumber=tmp_WFACutVolume.MonthNumber
    left join
       tmp_WFAFromBMPVolume on tmp_LOB.LOB=tmp_WFAFromBMPVolume.LOB and tmp_LOB.Year=tmp_WFAFromBMPVolume.Year and tmp_LOB.MonthNumber=tmp_WFAFromBMPVolume.MonthNumber
     left join
        tmp_WFAFromBMPTouchedVolume on tmp_LOB.LOB=tmp_WFAFromBMPTouchedVolume.LOB and tmp_LOB.Year=tmp_WFAFromBMPTouchedVolume.Year and tmp_LOB.MonthNumber=tmp_WFAFromBMPTouchedVolume.MonthNumber
     left join
        tmp_HCandHours on tmp_LOB.LOB=tmp_HCandHours.LOB and tmp_LOB.Year=tmp_HCandHours.Year and tmp_LOB.MonthNumber=tmp_HCandHours.MonthNumber
     left join
        tmp_Exp on tmp_LOB.LOB=tmp_Exp.LOB and tmp_LOB.Year=tmp_Exp.Year and tmp_LOB.MonthNumber=tmp_Exp.MonthNumber
     left join
        tmp_LoandedOut on tmp_LOB.LOB=tmp_LoandedOut.LOB and tmp_LOB.Year=tmp_LoandedOut.Year and tmp_LOB.MonthNumber=tmp_LoandedOut.MonthNumber
     left join
        tmp_Quality on tmp_LOB.LOB=tmp_Quality.LOB and tmp_LOB.Year=tmp_Quality.Year and tmp_LOB.MonthNumber=tmp_Quality.MonthNumber
     left join
        tmp_Productivity on tmp_LOB.LOB=tmp_Productivity.LOB and tmp_LOB.Year=tmp_Productivity.Year and tmp_LOB.MonthNumber=tmp_Productivity.MonthNumber
     left join
        tmp_Targets on tmp_LOB.LOB=tmp_Targets.LOB 
     left join
        tmp_BMPCutVolume on tmp_LOB.LOB=tmp_BMPCutVolume.LOB and tmp_LOB.Year=tmp_BMPCutVolume.Year and tmp_LOB.MonthNumber=tmp_BMPCutVolume.MonthNumber
     left join
        tmp_HCandHoursProductive on tmp_LOB.LOB=tmp_HCandHoursProductive.LOB and tmp_LOB.Year=tmp_HCandHoursProductive.Year and tmp_LOB.MonthNumber=tmp_HCandHoursProductive.MonthNumber
     left join 
        tmp_VolumeForecastTouched on tmp_LOB.LOB=tmp_VolumeForecastTouched.LOB and tmp_LOB.Year=tmp_VolumeForecastTouched.Year and tmp_LOB.MonthNumber=tmp_VolumeForecastTouched.MonthNumber
     left join
        tmp_DWTWeights on tmp_LOB.LOB=tmp_DWTWeights.LOB and tmp_LOB.Year=tmp_DWTWeights.Year
     left join 
        tmp_NicheVolumes on tmp_LOB.LOB=tmp_NicheVolumes.LOB and tmp_LOB.Year=tmp_NicheVolumes.Year and tmp_LOB.MonthNumber=tmp_NicheVolumes.MonthNumber
     left join
        tmp_WFAFromCarrierVolume on tmp_LOB.LOB=tmp_WFAFromCarrierVolume.LOB and tmp_LOB.Year=tmp_WFAFromCarrierVolume.Year and tmp_LOB.MonthNumber=tmp_WFAFromCarrierVolume.MonthNumber
     left join 
        tmp_WFAFromCarrierTouchedVolume on tmp_LOB.LOB=tmp_WFAFromCarrierTouchedVolume.LOB and tmp_LOB.Year=tmp_WFAFromCarrierTouchedVolume.Year and tmp_LOB.MonthNumber=tmp_WFAFromCarrierTouchedVolume.MonthNumber
     ;
    
drop table GBADashboardUnitCostInitial;
create table GBADashboardUnitCostInitial as
select
  i.lob,
  i.year,
  i.monthnumber,
  i.volumeforecasttouched,
  i.wfavolume,
  i.wfatouchedvolume,
  i.wfameasuredvolume,
  i.wfacutvolume,
  i.wfafrombmpvolume,
  i.wfafrombmptouchedvolume,
  i.wfacarriervolume,
  i.wfamttr,
  i.wfafoundmttr,
  i.wfasvb,
  i.wfarepeatper,
  i.wfareworkper,
  i.wfachronicper,
  i.wfatouches,
  i.bmpvolume,
  i.bmptouchedvolume,
  i.bmpmeasuredvolume,
  i.bmpmttr,
  i.bmpfoundmttr,
  i.bmpsvb,
  i.bmprepeatper,
  i.bmpreworkper,
  i.bmpchronicper,
  i.bmptouches,
  i.aotsvolume,
  i.aotstouchedvolume,
  i.aotsmeasuredvolume,
  i.aotsmttr,
  i.aotsfoundmttr,
  i.aotssvb,
  i.aotsrepeatper,
  i.aotsreworkper,
  i.aotschronicper,
  i.aotstouches,
  i.allvolume,
  i.alltouchedvolume,
  i.allmeasuredvolume,
  i.allincidentvolume,
  i.allincidenttouchedvolume,
  i.initialheadcount,
  i.initialexpense,
  i.initialproductivehours,
  i.initialnonproductivehours,
  i.overtimehours,
  i.loandedouthours,
  i.lobloandedto,
  if(t.loandedinhours is null,i.loandedinhours,-t.loandedouthours) as loandedinhours,
  i.traininghours,
  i.averageprodhrsperhc,
  i.averageexpperhc,
  if(t.loanedhc is null,i.loanedhc,-t.loanedhc) as loanedhc,
  if(t.loandedexp is null, i.loandedexp,-t.loandedexp) as loandedexp,
  i.qualityreviewvolume,
  i.qualityreviewscore,
  i.activities,
  i.activitiespertouchedticket,
  i.autopickcount,
  i.totalclaims,
  i.autopickpercentage,
  i.bmpcutvolume,
  i.wfa_found_mttr,
  i.wfa_svb,
  i.wfa_repeat_percent,
  i.bmp_found_mttr,
  i.bmp_svb,
  i.bmp_repeat_percent,
  i.aots_found_mttr,
  i.aots_svb,
  i.aots_repeat_percent,
  i.initialproductiveheadcount,
  i.initialproductivehoursproductivehc,
  i.avgproductivehoursproductivehcperproductiveheadcount,
  i.totalcutvolume,
  i.dwtweights,
  i.wfafromcarriertouchedvolume
from tmp_DataInitial i
left join (select * from tmp_DataInitial
where LoandedInHours is not null ) t
on t.LOBLoandedTo=i.LOB
     and i.MonthNumber=t.MonthNumber
     and i.Year=t.Year;


--delete overtimeshour
--change VolumeForecast to VolumeForecastTouched
drop table GBADashboardUnitCostFinal1;
create table GBADashboardUnitCostFinal1
as
select 
if(LOB in ('DS0','DS1','DS0_DS1'),'DS0-DS1', if(LOB in ('DCMC ANNAPOLIS','DCMC AUSTIN','DCMC CHICAGO','DCMC COLUMBUS'),'Dedicated',if (LOB='Virtual GCSC - Custom' or LOB='Virtual GCSC - Standard','Virtual GCSC - Standard/Custom',LOB) )) as LOB
,Year 
,MonthNumber 
,avg(VolumeForecastTouched) as ForecastTouchedVolume
,sum(WFAVolume)/if(count(WFAVolume)!=0,count(WFAVolume),null) as WFAVolume
,avg(WFATouchedVolume ) as WFATouchedVolume
,avg(WFAMeasuredVolume ) as WFAMeasuredVolume
,avg(WFACutVolume  ) as WFACutVolume 
,avg(WFAFromBMPVolume ) as WFAFromBMPVolume
,avg(WFAFromBMPTouchedVolume ) as WFAFromBMPTouchedVolume
,avg(WFACarrierVolume ) as WFACarrierVolume
,avg(WFAMTTR ) as WFAMTTR
,avg(WFAFoundMTTR ) as WFAFoundMTTR
,avg(WFASVB ) as WFASVB
,avg(WFARepeatPer ) as WFARepeatPer
,avg(WFAReworkPer ) as WFAReworkPer
,avg(WFAChronicPer ) as WFAChronicPer
,avg(WFATouches ) as WFATouches
,avg(BMPVolume ) as BMPVolume
,avg(BMPTouchedVolume ) as BMPTouchedVolume
,avg(BMPMeasuredVolume ) as BMPMeasuredVolume
,avg(BMPMTTR ) as BMPMTTR
,avg(BMPFoundMTTR ) as BMPFoundMTTR
,avg(BMPSVB ) as BMPSVB
,avg(BMPRepeatPer ) as BMPRepeatPer
,avg(BMPReworkPer ) as BMPReworkPer
,avg(BMPChronicPer ) as BMPChronicPer
,avg(BMPTouches ) as BMPTouches
,avg(AOTSVolume ) as AOTSVolume
,avg(AOTSTouchedVolume ) as AOTSTouchedVolume
,avg(AOTSMeasuredVolume ) as AOTSMeasuredVolume
,avg(AOTSMTTR ) as AOTSMTTR
,avg(AOTSFoundMTTR ) as AOTSFoundMTTR
,avg(AOTSSVB ) as AOTSSVB
,avg(AOTSRepeatPer ) as AOTSRepeatPer
,avg(AOTSReworkPer ) as AOTSReworkPer
,avg(AOTSChronicPer ) as AOTSChronicPer
,avg(AOTSTouches ) as AOTSTouches
,avg(AllVolume ) as AllVolume
,avg(AllTouchedVolume ) as AllTouchedVolume
,avg(AllMeasuredVolume ) as AllMeasuredVolume
,avg(AllIncidentVolume ) as AllIncidentVolume
,avg(AllIncidentTouchedVolume ) as AllIncidentTouchedVolume
,avg(InitialHeadcount)  as InitialHeadcount
,avg(InitialExpense ) as InitialExpense
,avg(InitialProductiveHours ) as InitialProductiveHours
,avg(InitialNonProductiveHours ) as InitialNonProductiveHours

,avg(OvertimeHours) as overtimehrs
,sum(LoandedOutHours) as LoandedOutHours

,Sum(LoandedInHours ) as LoandedInHours
,avg(TrainingHours ) as TrainingHours
,avg(AverageProdHrsPerHC ) as AverageProdHrsPerHC
--,avg(AverageCostPerHC )
, 0 as AverageCostPerHC
,sum(LoanedHC ) as LoanedHC
,avg(LoandedExp) as LoandedExp
,avg(QualityReviewVolume ) as QualityReviewVolume
,avg(QualityReviewScore ) as QualityReviewScore
--,avg(ProductivityActivities ) 
,avg(Activities ) as ProductivityActivities
,avg(ActivitiesPerTouchedTicket ) as ActivitiesPerTouchedTicket
,coalesce(avg(InitialHeadcount),0)-coalesce(sum(LoanedHC),0)-coalesce(round(avg(TrainingHours)/if(avg(AverageProdHrsPerHC)!=0,avg(AverageProdHrsPerHC),null),2),0) as FinalHeadcount
,coalesce(avg(InitialExpense),0)-coalesce(avg(LoandedExp),0) as FinalExpense
,coalesce(avg(InitialProductiveHours),0)-coalesce(avg(TrainingHours),0)-coalesce(avg(LoandedOutHours),0)-coalesce(avg(LoandedInHours),0)  as FinalProductiveHours
,avg(InitialNonProductiveHours) as FinalNonProductiveHours
,coalesce(round((coalesce(avg(InitialExpense),0)-coalesce(avg(LoandedExp),0))/if(avg(AllVolume)!=0,avg(AllVolume),null),2),0) as UnitCostAllTickets
,coalesce(round((coalesce(avg(InitialExpense),0)-coalesce(avg(LoandedExp),0))/if(avg(AllTouchedVolume)!=0,avg(AllTouchedVolume),null),2),0) as UnitCostTouchedTickets
,coalesce(round((coalesce(avg(InitialProductiveHours),0)-coalesce(avg(TrainingHours),0)-coalesce(avg(LoandedOutHours),0)-coalesce(avg(LoandedInHours),0))/if(avg(AllTouchedVolume)!=0,avg(AllTouchedVolume),null),2),0) as DWT

,coalesce(round((coalesce(avg(InitialExpense),0)-coalesce(avg(LoandedExp),0))/if(avg(AllIncidentVolume)!=0,avg(AllIncidentVolume),null),2),0) as UnitCostAllIncidents
,coalesce(round((coalesce(avg(InitialExpense),0)-coalesce(avg(LoandedExp),0))/if(avg(AllIncidentTouchedVolume)!=0,avg(AllIncidentTouchedVolume),null),2),0) as UnitCostTouchedIncidents
,coalesce(round((coalesce(avg(InitialProductiveHours),0)-coalesce(avg(TrainingHours),0)-coalesce(avg(LoandedOutHours),0)-coalesce(avg(LoandedInHours),0))/if(avg(AllIncidentTouchedVolume)!=0,avg(AllIncidentTouchedVolume),null),2),0) as DWTIncidents
,avg(AutoPickCount) as AutoPickCount
,avg(TotalClaims) as TotalClaims
,avg(AutoPickPercentage) as AutoPickPercentage
,avg(BMPCutVolume) as BMPCutVolume
,avg(WFA_Found_MTTR ) as WFA_Found_MTTR
,avg(WFA_SVB) as WFA_SVB
,avg(WFA_Repeat_percent ) as WFA_Repeat_percent
,avg(BMP_Found_MTTR ) as BMP_Found_MTTR
,avg(BMP_SVB ) as BMP_SVB
,avg(BMP_Repeat_percent ) as BMP_Repeat_percent
,avg(AOTS_Found_MTTR ) as AOTS_Found_MTTR
,avg(AOTS_SVB ) as AOTS_SVB
,avg(AOTS_Repeat_percent) as AOTS_Repeat_percent


,avg(InitialProductiveHeadcount) as InitialProductiveHeadcount
,avg(InitialProductiveHoursProductiveHC ) as InitialProductiveHoursProductiveHC
,avg(AvgProductiveHoursProductiveHCPerProductiveHeadcount) as AvgProductiveHoursProductiveHCPerProductiveHeadcount
,avg(TotalCutVolume) as TotalCutVolume

,coalesce(avg(InitialProductiveHoursProductiveHC),0)-coalesce(avg(TrainingHours),0)-coalesce(avg(LoandedOutHours),0)-coalesce(avg(LoandedInHours),0) as FinalProductionHCProductiveHrs
,OvertimeHours/if(coalesce(avg(InitialProductiveHoursProductiveHC),0)-coalesce(avg(TrainingHours),0)-coalesce(avg(LoandedOutHours),0)-coalesce(avg(LoandedInHours),0)!=0,coalesce(avg(InitialProductiveHoursProductiveHC),0)-coalesce(avg(TrainingHours),0)-coalesce(avg(LoandedOutHours),0)-coalesce(avg(LoandedInHours),0),null) as OvertimePer
,coalesce(avg(WFATouches),0)+coalesce(avg(BMPTouches),0)+coalesce(avg(AOTSTouches),0) as TotalTouches
,round((coalesce(avg(InitialExpense),0)-coalesce(avg(LoandedExp),0))/if(coalesce(avg(WFATouches),0)+coalesce(avg(BMPTouches),0)+coalesce(avg(AOTSTouches),0)!=0,coalesce(avg(WFATouches),0)+coalesce(avg(BMPTouches),0)+coalesce(avg(AOTSTouches),0),null),2) as CostPerTouch
--,round(avg(ProductivityActivities)/if(avg(InitialProductiveHeadcount)!=0,avg(InitialProductiveHeadcount),null),2) as ActivitiesPerProdHC
,round(avg(Activities)/if(avg(InitialProductiveHeadcount)!=0,avg(InitialProductiveHeadcount),null),2) as ActivitiesPerProdHC
,coalesce(avg(InitialProductiveHeadcount),0)-coalesce(sum(LoanedHC),0) -coalesce(round(avg(TrainingHours)/if(avg(AverageProdHrsPerHC)!=0,avg(AverageProdHrsPerHC),null),2),0)as FinalProductionHC
,(avg(VolumeForecastTouched)*avg(DWTWeights)) as ForecastTouchedVolumeWeighted
,(avg(AllTouchedVolume)*avg(DWTWeights)) as TouchedVolumeWeighted
,round((coalesce(avg(InitialExpense),0)-coalesce(avg(LoandedExp),0))/if((avg(AllTouchedVolume)*avg(DWTWeights))!=0,(avg(AllTouchedVolume)*avg(DWTWeights)),null),2) as UnitCostPerTouchedVolumeWeighted

,round(avg(OvertimeHours)/if(avg(AverageProdHrsPerHC)!=0,avg(AverageProdHrsPerHC),null),2) as OvertimeHeadcount
,round(avg(TrainingHours)/if(avg(AverageProdHrsPerHC)!=0,avg(AverageProdHrsPerHC),null),2) as TrainingHeadcount
,coalesce(round((coalesce(avg(InitialProductiveHoursProductiveHC),0)-coalesce(avg(TrainingHours),0)-coalesce(avg(LoandedOutHours),0)-coalesce(avg(LoandedInHours),0))/if(avg(AllTouchedVolume)!=0,avg(AllTouchedVolume),null),2),0) as DWT_production
,round(avg(AllTouchedVolume )/if(avg(AllIncidentTouchedVolume )!=0,avg(AllIncidentTouchedVolume ),null),2) as TouchedTicketsPerTouchedIncident
,round(avg(QualityReviewVolume )/if(avg(InitialProductiveHeadcount)!=0,avg(InitialProductiveHeadcount),null),2) as QualityReviewsPerInitialProductionHC
--,avg(WFACarrierTouchedVolume )
,avg(WFACarrierVolume) as WFACarrierTouchedVolume
from 
GBADashboardUnitCostInitial
group by LOB,Year,MonthNumber,OvertimeHours
;

drop table GBADashboardUnitCostFinal;
create table  GBADashboardUnitCostFinal
as
select 
LOB
,Year
,MonthNumber
,SUM(ForecastTouchedVolume) as VolumneForecastTouched
,sum(WFAVolume) as WFAVolume
,SUM(WFATouchedVolume) as WFATouchedVolume
,SUM(WFAMeasuredVolume) as WFAMeasuredVolume
,SUM(WFACutVolume) as WFACutVolume
,SUM(WFAFromBMPVolume) as WFAFromBMPVolume
,SUM(WFAFromBMPTouchedVolume) as WFAFromBMPTouchedVolume
,SUM(WFACarrierVolume) as WFACarrierVolume
,'' as WFAMTTR
,'' as WFAFoundMTTR
,'' as WFASVB
,'' as WFARepeatPer
,'' as WFAReworkPer
,'' as WFAChronicPer
,sum(WFATouches ) as WFATouches
,sum(BMPVolume) as BMPVolume
,sum(BMPTouchedVolume) as BMPTouchedVolume
,sum(BMPMeasuredVolume) as BMPMeasuredVolume
,'' as BMPMTTR
,'' as BMPFoundMTTR
,'' as BMPSVB
,'' as BMPRepeatPer
,'' as BMPReworkPer
,'' as BMPChronicPer
,sum(BMPTouches) as BMPTouches
,sum(AOTSVolume) as AOTSVolume
,sum(AOTSTouchedVolume) as AOTSTouchedVolume
,sum(AOTSMeasuredVolume) as AOTSMeasuredVolume
,'' asAOTSMTTR
,'' asAOTSFoundMTTR
,'' asAOTSSVB
,'' asAOTSRepeatPer
,'' asAOTSReworkPer
,'' asAOTSChronicPer
,sum(AOTSTouches) as AOTSTouches
,sum(AllVolume) as AllVolume
,sum(AllTouchedVolume) as AllTouchedVolume
,sum(AllMeasuredVolume) as AllMeasuredVolume
,sum(AllIncidentVolume) as AllIncidentVolume
,sum(AllIncidentTouchedVolume) as AllIncidentTouchedVolume
--,sum(InitialAllHeadcount)
,sum(InitialHeadcount) as InitialHeadcount
,sum(InitialExpense) as InitialExpense
,sum(InitialProductiveHours) as InitialProductiveHours
,sum(InitialNonProductiveHours) as InitialNonProductiveHours
,sum(Overtimehrs) as Overtimehrs
,sum(LoandedOutHours) as LoandedOutHours
,sum(LoandedInHours) as LoandedInHours
,sum(TrainingHours) as TrainingHours
,round(SUM(InitialProductiveHoursProductiveHC)/if(sum(InitialProductiveHeadcount)!=0,sum(InitialProductiveHeadcount),null),0) as AverageProdHrsPerHC
--,round(sum(InitialExpense)/if(sum(InitialAllHeadcount)!=0,sum(InitialAllHeadcount),null),0)
,round(sum(InitialExpense)/if(sum(InitialHeadcount)!=0,sum(InitialHeadcount),null),0) as AverageCostPerHC

,sum(LoanedHC) as LoanedHC
,sum(LoandedExp) as LoandedExp
,sum(QualityReviewVolume) as QualityReviewVolume
,avg(QualityReviewScore) as QualityReviewScore
,sum(ProductivityActivities) as ProductivityActivities
,round(sum(ProductivityActivities)/if(sum(AllTouchedVolume)!=0,sum(AllTouchedVolume),null),0) as ActivitiesPerTouchedTicket
--,sum(FinalAllHeadcount) 
,sum(FinalHeadcount) as FinalHeadcount
,sum(FinalExpense) as FinalExpense
--,sum(FinalAllProductiveHours)
,sum(FinalProductiveHours) as FinalProductiveHours
--,sum(FinalAllNonProductiveHours)
,sum(FinalNonProductiveHours) as FinalNonProductiveHours

,round((SUM(InitialExpense)-SUM( LoandedExp))/if(sum(AllVolume)!=0,sum(AllVolume),null),2)as UnitCostAllTickets
,round((SUM(InitialExpense)-SUM( LoandedExp))/if(sum(AllTouchedVolume)!=0,sum(AllTouchedVolume),null),2)as UnitCostTouchedTickets
--,round((sum(FinalAllProductiveHours))/if(sum(AllTouchedVolume)!=0,sum(AllTouchedVolume),null),2) as DWT
,round((sum(FinalProductiveHours))/if(sum(AllTouchedVolume)!=0,sum(AllTouchedVolume),null),2) as DWT

,round((SUM(InitialExpense)-SUM( LoandedExp))/if(sum(AllIncidentVolume)!=0,sum(AllIncidentVolume),null),2)as UnitCostAllIncidents
,round((SUM(InitialExpense)-SUM( LoandedExp))/if(sum(AllIncidentTouchedVolume)!=0,sum(AllIncidentTouchedVolume),null),2)as UnitCostTouchedIncidents
--,round(sum(FinalAllProductiveHours)/if(sum(AllIncidentTouchedVolume)!=0,sum(AllIncidentTouchedVolume),null),2) as DWTTouchedIncidents
,round(sum(FinalProductiveHours)/if(sum(AllIncidentTouchedVolume)!=0,sum(AllIncidentTouchedVolume),null),2) as DWTTouchedIncidents

,'' as AutoPickCount
,'' as TotalClaims
,'' as AutoPickPercentage
,sum(BMPCutVolume)
,'' as WFA_Found_MTTR 
,'' as WFA_SVB
,'' as WFA_Repeat_percent
,'' as BMP_Found_MTTR 
,'' as BMP_SVB
,'' as BMP_Repeat_percent
,'' as AOTS_Found_MTTR 
,'' as AOTS_SVB
,'' as AOTS_Repeat_percent
,sum(InitialProductiveHeadcount) as InitialProductiveHeadcount
,sum(InitialProductiveHoursProductiveHC) as InitialProductiveHoursProductiveHC
,round(SUM(InitialProductiveHoursProductiveHC)/if(sum(InitialProductiveHeadcount)!=0,sum(InitialProductiveHeadcount),null),0) as AvgProductiveHoursProductiveHCPerProductiveHeadcount
,sum(TotalCutVolume)
,(sum(InitialProductiveHoursProductiveHC)-SUM(TrainingHours)-sum(LoandedOutHours)-sum(LoandedInHours))as FinalProductionHCProductiveHrs
,sum(OvertimeHrs)/if(coalesce(sum(InitialProductiveHoursProductiveHC),0)-coalesce(sum(TrainingHours),0)-coalesce(sum(LoandedOutHours),0)-coalesce(sum(LoandedInHours),0)!=0,coalesce(sum(InitialProductiveHoursProductiveHC),0)-coalesce(sum(TrainingHours),0)-coalesce(sum(LoandedOutHours),0)-coalesce(sum(LoandedInHours),0),null)  as OvertimePer
,sum(TotalTouches)
, round(sum(FinalExpense)/if(sum(TotalTouches)!=0,sum(TotalTouches),null),2) as CostPerTouch
,sum(ProductivityActivities)/if(sum(InitialProductiveHeadcount)!=0,sum(InitialProductiveHeadcount),null) as ActivitiesPerProdHC
,sum(FinalProductionHC) as FinalProductionHC
,sum(ForecastTouchedVolumeWeighted) as ForecastTouchedVolumeWeighted
,sum(TouchedVolumeWeighted) as TouchedVolumeWeighted
,round((SUM(InitialExpense)-SUM( LoandedExp))/if(sum(TouchedVolumeWeighted)!=0,sum(TouchedVolumeWeighted),null),2)as UnitCostPerTouchedVolumeWeighted
,sum(OvertimeHeadcount) as OvertimeHeadcount
,sum(TrainingHeadcount) as TrainingHeadcount
,round((sum(coalesce(InitialProductiveHoursProductiveHC,0))-SUM(coalesce(TrainingHours,0))-sum(coalesce(LoandedOutHours,0))-sum(coalesce(LoandedInHours,0)))/if(sum(AllTouchedVolume)!=0,sum(AllTouchedVolume),null),2) as DWT_Production
,ROUND(sum(AllTouchedVolume)/if(sum(AllIncidentTouchedVolume)!=0,sum(AllIncidentTouchedVolume),null),2) as TouchedTicketsPerTouchedIncident
,ROUND(sum(QualityReviewVolume)/if(sum(InitialProductiveHeadcount)!=0,sum(InitialProductiveHeadcount),null),2) AS QualityReviewsPerInitialProductionHC
,SUM(WFACarrierTouchedVolume) as WFACarrierTouchedVolume

FROM GBADashboardUnitCostFinal1
where LOB in ('DS0-DS1','Dedicated','Virtual GCSC - Standard/Custom')
GROUP BY 
LOB,
Year
,MonthNumber
union all
select *
FROM GBADashboardUnitCostFinal1
where LOB not in ('DS0-DS1','Dedicated','Virtual GCSC - Standard/Custom')

;
"




