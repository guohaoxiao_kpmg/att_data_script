// ORM class for table 'SDA_Stage.dbo.Organization'
// WARNING: This class is AUTO-GENERATED. Modify at your own risk.
//
// Debug information:
// Generated date: Wed May 25 08:45:32 EDT 2016
// For connector: org.apache.sqoop.manager.GenericJdbcManager
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapred.lib.db.DBWritable;
import com.cloudera.sqoop.lib.JdbcWritableBridge;
import com.cloudera.sqoop.lib.DelimiterSet;
import com.cloudera.sqoop.lib.FieldFormatter;
import com.cloudera.sqoop.lib.RecordParser;
import com.cloudera.sqoop.lib.BooleanParser;
import com.cloudera.sqoop.lib.BlobRef;
import com.cloudera.sqoop.lib.ClobRef;
import com.cloudera.sqoop.lib.LargeObjectLoader;
import com.cloudera.sqoop.lib.SqoopRecord;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class SDA_Stage_dbo_Organization extends SqoopRecord  implements DBWritable, Writable {
  private final int PROTOCOL_VERSION = 3;
  public int getClassFormatVersion() { return PROTOCOL_VERSION; }
  protected ResultSet __cur_result_set;
  private String attuid;
  public String get_attuid() {
    return attuid;
  }
  public void set_attuid(String attuid) {
    this.attuid = attuid;
  }
  public SDA_Stage_dbo_Organization with_attuid(String attuid) {
    this.attuid = attuid;
    return this;
  }
  private String name;
  public String get_name() {
    return name;
  }
  public void set_name(String name) {
    this.name = name;
  }
  public SDA_Stage_dbo_Organization with_name(String name) {
    this.name = name;
    return this;
  }
  private String first_name;
  public String get_first_name() {
    return first_name;
  }
  public void set_first_name(String first_name) {
    this.first_name = first_name;
  }
  public SDA_Stage_dbo_Organization with_first_name(String first_name) {
    this.first_name = first_name;
    return this;
  }
  private String last_name;
  public String get_last_name() {
    return last_name;
  }
  public void set_last_name(String last_name) {
    this.last_name = last_name;
  }
  public SDA_Stage_dbo_Organization with_last_name(String last_name) {
    this.last_name = last_name;
    return this;
  }
  private String department;
  public String get_department() {
    return department;
  }
  public void set_department(String department) {
    this.department = department;
  }
  public SDA_Stage_dbo_Organization with_department(String department) {
    this.department = department;
    return this;
  }
  private String city;
  public String get_city() {
    return city;
  }
  public void set_city(String city) {
    this.city = city;
  }
  public SDA_Stage_dbo_Organization with_city(String city) {
    this.city = city;
    return this;
  }
  private String state;
  public String get_state() {
    return state;
  }
  public void set_state(String state) {
    this.state = state;
  }
  public SDA_Stage_dbo_Organization with_state(String state) {
    this.state = state;
    return this;
  }
  private String country;
  public String get_country() {
    return country;
  }
  public void set_country(String country) {
    this.country = country;
  }
  public SDA_Stage_dbo_Organization with_country(String country) {
    this.country = country;
    return this;
  }
  private String level;
  public String get_level() {
    return level;
  }
  public void set_level(String level) {
    this.level = level;
  }
  public SDA_Stage_dbo_Organization with_level(String level) {
    this.level = level;
    return this;
  }
  private String job_title_code;
  public String get_job_title_code() {
    return job_title_code;
  }
  public void set_job_title_code(String job_title_code) {
    this.job_title_code = job_title_code;
  }
  public SDA_Stage_dbo_Organization with_job_title_code(String job_title_code) {
    this.job_title_code = job_title_code;
    return this;
  }
  private String job_title_name;
  public String get_job_title_name() {
    return job_title_name;
  }
  public void set_job_title_name(String job_title_name) {
    this.job_title_name = job_title_name;
  }
  public SDA_Stage_dbo_Organization with_job_title_name(String job_title_name) {
    this.job_title_name = job_title_name;
    return this;
  }
  private String svp_attuid;
  public String get_svp_attuid() {
    return svp_attuid;
  }
  public void set_svp_attuid(String svp_attuid) {
    this.svp_attuid = svp_attuid;
  }
  public SDA_Stage_dbo_Organization with_svp_attuid(String svp_attuid) {
    this.svp_attuid = svp_attuid;
    return this;
  }
  private String svp_name;
  public String get_svp_name() {
    return svp_name;
  }
  public void set_svp_name(String svp_name) {
    this.svp_name = svp_name;
  }
  public SDA_Stage_dbo_Organization with_svp_name(String svp_name) {
    this.svp_name = svp_name;
    return this;
  }
  private String svp_job_title;
  public String get_svp_job_title() {
    return svp_job_title;
  }
  public void set_svp_job_title(String svp_job_title) {
    this.svp_job_title = svp_job_title;
  }
  public SDA_Stage_dbo_Organization with_svp_job_title(String svp_job_title) {
    this.svp_job_title = svp_job_title;
    return this;
  }
  private String vp_attuid;
  public String get_vp_attuid() {
    return vp_attuid;
  }
  public void set_vp_attuid(String vp_attuid) {
    this.vp_attuid = vp_attuid;
  }
  public SDA_Stage_dbo_Organization with_vp_attuid(String vp_attuid) {
    this.vp_attuid = vp_attuid;
    return this;
  }
  private String vp_name;
  public String get_vp_name() {
    return vp_name;
  }
  public void set_vp_name(String vp_name) {
    this.vp_name = vp_name;
  }
  public SDA_Stage_dbo_Organization with_vp_name(String vp_name) {
    this.vp_name = vp_name;
    return this;
  }
  private String vp_job_title;
  public String get_vp_job_title() {
    return vp_job_title;
  }
  public void set_vp_job_title(String vp_job_title) {
    this.vp_job_title = vp_job_title;
  }
  public SDA_Stage_dbo_Organization with_vp_job_title(String vp_job_title) {
    this.vp_job_title = vp_job_title;
    return this;
  }
  private String avp_attuid;
  public String get_avp_attuid() {
    return avp_attuid;
  }
  public void set_avp_attuid(String avp_attuid) {
    this.avp_attuid = avp_attuid;
  }
  public SDA_Stage_dbo_Organization with_avp_attuid(String avp_attuid) {
    this.avp_attuid = avp_attuid;
    return this;
  }
  private String avp_name;
  public String get_avp_name() {
    return avp_name;
  }
  public void set_avp_name(String avp_name) {
    this.avp_name = avp_name;
  }
  public SDA_Stage_dbo_Organization with_avp_name(String avp_name) {
    this.avp_name = avp_name;
    return this;
  }
  private String avp_job_title;
  public String get_avp_job_title() {
    return avp_job_title;
  }
  public void set_avp_job_title(String avp_job_title) {
    this.avp_job_title = avp_job_title;
  }
  public SDA_Stage_dbo_Organization with_avp_job_title(String avp_job_title) {
    this.avp_job_title = avp_job_title;
    return this;
  }
  private String director_attuid;
  public String get_director_attuid() {
    return director_attuid;
  }
  public void set_director_attuid(String director_attuid) {
    this.director_attuid = director_attuid;
  }
  public SDA_Stage_dbo_Organization with_director_attuid(String director_attuid) {
    this.director_attuid = director_attuid;
    return this;
  }
  private String director_name;
  public String get_director_name() {
    return director_name;
  }
  public void set_director_name(String director_name) {
    this.director_name = director_name;
  }
  public SDA_Stage_dbo_Organization with_director_name(String director_name) {
    this.director_name = director_name;
    return this;
  }
  private String director_job_title;
  public String get_director_job_title() {
    return director_job_title;
  }
  public void set_director_job_title(String director_job_title) {
    this.director_job_title = director_job_title;
  }
  public SDA_Stage_dbo_Organization with_director_job_title(String director_job_title) {
    this.director_job_title = director_job_title;
    return this;
  }
  private String area_manager_attuid;
  public String get_area_manager_attuid() {
    return area_manager_attuid;
  }
  public void set_area_manager_attuid(String area_manager_attuid) {
    this.area_manager_attuid = area_manager_attuid;
  }
  public SDA_Stage_dbo_Organization with_area_manager_attuid(String area_manager_attuid) {
    this.area_manager_attuid = area_manager_attuid;
    return this;
  }
  private String area_manager_name;
  public String get_area_manager_name() {
    return area_manager_name;
  }
  public void set_area_manager_name(String area_manager_name) {
    this.area_manager_name = area_manager_name;
  }
  public SDA_Stage_dbo_Organization with_area_manager_name(String area_manager_name) {
    this.area_manager_name = area_manager_name;
    return this;
  }
  private String area_manager_job_title;
  public String get_area_manager_job_title() {
    return area_manager_job_title;
  }
  public void set_area_manager_job_title(String area_manager_job_title) {
    this.area_manager_job_title = area_manager_job_title;
  }
  public SDA_Stage_dbo_Organization with_area_manager_job_title(String area_manager_job_title) {
    this.area_manager_job_title = area_manager_job_title;
    return this;
  }
  private String manager_attuid;
  public String get_manager_attuid() {
    return manager_attuid;
  }
  public void set_manager_attuid(String manager_attuid) {
    this.manager_attuid = manager_attuid;
  }
  public SDA_Stage_dbo_Organization with_manager_attuid(String manager_attuid) {
    this.manager_attuid = manager_attuid;
    return this;
  }
  private String manager_name;
  public String get_manager_name() {
    return manager_name;
  }
  public void set_manager_name(String manager_name) {
    this.manager_name = manager_name;
  }
  public SDA_Stage_dbo_Organization with_manager_name(String manager_name) {
    this.manager_name = manager_name;
    return this;
  }
  private String manager_job_title;
  public String get_manager_job_title() {
    return manager_job_title;
  }
  public void set_manager_job_title(String manager_job_title) {
    this.manager_job_title = manager_job_title;
  }
  public SDA_Stage_dbo_Organization with_manager_job_title(String manager_job_title) {
    this.manager_job_title = manager_job_title;
    return this;
  }
  private String non_manager_supervisor_attuid;
  public String get_non_manager_supervisor_attuid() {
    return non_manager_supervisor_attuid;
  }
  public void set_non_manager_supervisor_attuid(String non_manager_supervisor_attuid) {
    this.non_manager_supervisor_attuid = non_manager_supervisor_attuid;
  }
  public SDA_Stage_dbo_Organization with_non_manager_supervisor_attuid(String non_manager_supervisor_attuid) {
    this.non_manager_supervisor_attuid = non_manager_supervisor_attuid;
    return this;
  }
  private String non_manager_supervisor_name;
  public String get_non_manager_supervisor_name() {
    return non_manager_supervisor_name;
  }
  public void set_non_manager_supervisor_name(String non_manager_supervisor_name) {
    this.non_manager_supervisor_name = non_manager_supervisor_name;
  }
  public SDA_Stage_dbo_Organization with_non_manager_supervisor_name(String non_manager_supervisor_name) {
    this.non_manager_supervisor_name = non_manager_supervisor_name;
    return this;
  }
  private String non_manager_supervisor_job_title;
  public String get_non_manager_supervisor_job_title() {
    return non_manager_supervisor_job_title;
  }
  public void set_non_manager_supervisor_job_title(String non_manager_supervisor_job_title) {
    this.non_manager_supervisor_job_title = non_manager_supervisor_job_title;
  }
  public SDA_Stage_dbo_Organization with_non_manager_supervisor_job_title(String non_manager_supervisor_job_title) {
    this.non_manager_supervisor_job_title = non_manager_supervisor_job_title;
    return this;
  }
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof SDA_Stage_dbo_Organization)) {
      return false;
    }
    SDA_Stage_dbo_Organization that = (SDA_Stage_dbo_Organization) o;
    boolean equal = true;
    equal = equal && (this.attuid == null ? that.attuid == null : this.attuid.equals(that.attuid));
    equal = equal && (this.name == null ? that.name == null : this.name.equals(that.name));
    equal = equal && (this.first_name == null ? that.first_name == null : this.first_name.equals(that.first_name));
    equal = equal && (this.last_name == null ? that.last_name == null : this.last_name.equals(that.last_name));
    equal = equal && (this.department == null ? that.department == null : this.department.equals(that.department));
    equal = equal && (this.city == null ? that.city == null : this.city.equals(that.city));
    equal = equal && (this.state == null ? that.state == null : this.state.equals(that.state));
    equal = equal && (this.country == null ? that.country == null : this.country.equals(that.country));
    equal = equal && (this.level == null ? that.level == null : this.level.equals(that.level));
    equal = equal && (this.job_title_code == null ? that.job_title_code == null : this.job_title_code.equals(that.job_title_code));
    equal = equal && (this.job_title_name == null ? that.job_title_name == null : this.job_title_name.equals(that.job_title_name));
    equal = equal && (this.svp_attuid == null ? that.svp_attuid == null : this.svp_attuid.equals(that.svp_attuid));
    equal = equal && (this.svp_name == null ? that.svp_name == null : this.svp_name.equals(that.svp_name));
    equal = equal && (this.svp_job_title == null ? that.svp_job_title == null : this.svp_job_title.equals(that.svp_job_title));
    equal = equal && (this.vp_attuid == null ? that.vp_attuid == null : this.vp_attuid.equals(that.vp_attuid));
    equal = equal && (this.vp_name == null ? that.vp_name == null : this.vp_name.equals(that.vp_name));
    equal = equal && (this.vp_job_title == null ? that.vp_job_title == null : this.vp_job_title.equals(that.vp_job_title));
    equal = equal && (this.avp_attuid == null ? that.avp_attuid == null : this.avp_attuid.equals(that.avp_attuid));
    equal = equal && (this.avp_name == null ? that.avp_name == null : this.avp_name.equals(that.avp_name));
    equal = equal && (this.avp_job_title == null ? that.avp_job_title == null : this.avp_job_title.equals(that.avp_job_title));
    equal = equal && (this.director_attuid == null ? that.director_attuid == null : this.director_attuid.equals(that.director_attuid));
    equal = equal && (this.director_name == null ? that.director_name == null : this.director_name.equals(that.director_name));
    equal = equal && (this.director_job_title == null ? that.director_job_title == null : this.director_job_title.equals(that.director_job_title));
    equal = equal && (this.area_manager_attuid == null ? that.area_manager_attuid == null : this.area_manager_attuid.equals(that.area_manager_attuid));
    equal = equal && (this.area_manager_name == null ? that.area_manager_name == null : this.area_manager_name.equals(that.area_manager_name));
    equal = equal && (this.area_manager_job_title == null ? that.area_manager_job_title == null : this.area_manager_job_title.equals(that.area_manager_job_title));
    equal = equal && (this.manager_attuid == null ? that.manager_attuid == null : this.manager_attuid.equals(that.manager_attuid));
    equal = equal && (this.manager_name == null ? that.manager_name == null : this.manager_name.equals(that.manager_name));
    equal = equal && (this.manager_job_title == null ? that.manager_job_title == null : this.manager_job_title.equals(that.manager_job_title));
    equal = equal && (this.non_manager_supervisor_attuid == null ? that.non_manager_supervisor_attuid == null : this.non_manager_supervisor_attuid.equals(that.non_manager_supervisor_attuid));
    equal = equal && (this.non_manager_supervisor_name == null ? that.non_manager_supervisor_name == null : this.non_manager_supervisor_name.equals(that.non_manager_supervisor_name));
    equal = equal && (this.non_manager_supervisor_job_title == null ? that.non_manager_supervisor_job_title == null : this.non_manager_supervisor_job_title.equals(that.non_manager_supervisor_job_title));
    return equal;
  }
  public boolean equals0(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof SDA_Stage_dbo_Organization)) {
      return false;
    }
    SDA_Stage_dbo_Organization that = (SDA_Stage_dbo_Organization) o;
    boolean equal = true;
    equal = equal && (this.attuid == null ? that.attuid == null : this.attuid.equals(that.attuid));
    equal = equal && (this.name == null ? that.name == null : this.name.equals(that.name));
    equal = equal && (this.first_name == null ? that.first_name == null : this.first_name.equals(that.first_name));
    equal = equal && (this.last_name == null ? that.last_name == null : this.last_name.equals(that.last_name));
    equal = equal && (this.department == null ? that.department == null : this.department.equals(that.department));
    equal = equal && (this.city == null ? that.city == null : this.city.equals(that.city));
    equal = equal && (this.state == null ? that.state == null : this.state.equals(that.state));
    equal = equal && (this.country == null ? that.country == null : this.country.equals(that.country));
    equal = equal && (this.level == null ? that.level == null : this.level.equals(that.level));
    equal = equal && (this.job_title_code == null ? that.job_title_code == null : this.job_title_code.equals(that.job_title_code));
    equal = equal && (this.job_title_name == null ? that.job_title_name == null : this.job_title_name.equals(that.job_title_name));
    equal = equal && (this.svp_attuid == null ? that.svp_attuid == null : this.svp_attuid.equals(that.svp_attuid));
    equal = equal && (this.svp_name == null ? that.svp_name == null : this.svp_name.equals(that.svp_name));
    equal = equal && (this.svp_job_title == null ? that.svp_job_title == null : this.svp_job_title.equals(that.svp_job_title));
    equal = equal && (this.vp_attuid == null ? that.vp_attuid == null : this.vp_attuid.equals(that.vp_attuid));
    equal = equal && (this.vp_name == null ? that.vp_name == null : this.vp_name.equals(that.vp_name));
    equal = equal && (this.vp_job_title == null ? that.vp_job_title == null : this.vp_job_title.equals(that.vp_job_title));
    equal = equal && (this.avp_attuid == null ? that.avp_attuid == null : this.avp_attuid.equals(that.avp_attuid));
    equal = equal && (this.avp_name == null ? that.avp_name == null : this.avp_name.equals(that.avp_name));
    equal = equal && (this.avp_job_title == null ? that.avp_job_title == null : this.avp_job_title.equals(that.avp_job_title));
    equal = equal && (this.director_attuid == null ? that.director_attuid == null : this.director_attuid.equals(that.director_attuid));
    equal = equal && (this.director_name == null ? that.director_name == null : this.director_name.equals(that.director_name));
    equal = equal && (this.director_job_title == null ? that.director_job_title == null : this.director_job_title.equals(that.director_job_title));
    equal = equal && (this.area_manager_attuid == null ? that.area_manager_attuid == null : this.area_manager_attuid.equals(that.area_manager_attuid));
    equal = equal && (this.area_manager_name == null ? that.area_manager_name == null : this.area_manager_name.equals(that.area_manager_name));
    equal = equal && (this.area_manager_job_title == null ? that.area_manager_job_title == null : this.area_manager_job_title.equals(that.area_manager_job_title));
    equal = equal && (this.manager_attuid == null ? that.manager_attuid == null : this.manager_attuid.equals(that.manager_attuid));
    equal = equal && (this.manager_name == null ? that.manager_name == null : this.manager_name.equals(that.manager_name));
    equal = equal && (this.manager_job_title == null ? that.manager_job_title == null : this.manager_job_title.equals(that.manager_job_title));
    equal = equal && (this.non_manager_supervisor_attuid == null ? that.non_manager_supervisor_attuid == null : this.non_manager_supervisor_attuid.equals(that.non_manager_supervisor_attuid));
    equal = equal && (this.non_manager_supervisor_name == null ? that.non_manager_supervisor_name == null : this.non_manager_supervisor_name.equals(that.non_manager_supervisor_name));
    equal = equal && (this.non_manager_supervisor_job_title == null ? that.non_manager_supervisor_job_title == null : this.non_manager_supervisor_job_title.equals(that.non_manager_supervisor_job_title));
    return equal;
  }
  public void readFields(ResultSet __dbResults) throws SQLException {
    this.__cur_result_set = __dbResults;
    this.attuid = JdbcWritableBridge.readString(1, __dbResults);
    this.name = JdbcWritableBridge.readString(2, __dbResults);
    this.first_name = JdbcWritableBridge.readString(3, __dbResults);
    this.last_name = JdbcWritableBridge.readString(4, __dbResults);
    this.department = JdbcWritableBridge.readString(5, __dbResults);
    this.city = JdbcWritableBridge.readString(6, __dbResults);
    this.state = JdbcWritableBridge.readString(7, __dbResults);
    this.country = JdbcWritableBridge.readString(8, __dbResults);
    this.level = JdbcWritableBridge.readString(9, __dbResults);
    this.job_title_code = JdbcWritableBridge.readString(10, __dbResults);
    this.job_title_name = JdbcWritableBridge.readString(11, __dbResults);
    this.svp_attuid = JdbcWritableBridge.readString(12, __dbResults);
    this.svp_name = JdbcWritableBridge.readString(13, __dbResults);
    this.svp_job_title = JdbcWritableBridge.readString(14, __dbResults);
    this.vp_attuid = JdbcWritableBridge.readString(15, __dbResults);
    this.vp_name = JdbcWritableBridge.readString(16, __dbResults);
    this.vp_job_title = JdbcWritableBridge.readString(17, __dbResults);
    this.avp_attuid = JdbcWritableBridge.readString(18, __dbResults);
    this.avp_name = JdbcWritableBridge.readString(19, __dbResults);
    this.avp_job_title = JdbcWritableBridge.readString(20, __dbResults);
    this.director_attuid = JdbcWritableBridge.readString(21, __dbResults);
    this.director_name = JdbcWritableBridge.readString(22, __dbResults);
    this.director_job_title = JdbcWritableBridge.readString(23, __dbResults);
    this.area_manager_attuid = JdbcWritableBridge.readString(24, __dbResults);
    this.area_manager_name = JdbcWritableBridge.readString(25, __dbResults);
    this.area_manager_job_title = JdbcWritableBridge.readString(26, __dbResults);
    this.manager_attuid = JdbcWritableBridge.readString(27, __dbResults);
    this.manager_name = JdbcWritableBridge.readString(28, __dbResults);
    this.manager_job_title = JdbcWritableBridge.readString(29, __dbResults);
    this.non_manager_supervisor_attuid = JdbcWritableBridge.readString(30, __dbResults);
    this.non_manager_supervisor_name = JdbcWritableBridge.readString(31, __dbResults);
    this.non_manager_supervisor_job_title = JdbcWritableBridge.readString(32, __dbResults);
  }
  public void readFields0(ResultSet __dbResults) throws SQLException {
    this.attuid = JdbcWritableBridge.readString(1, __dbResults);
    this.name = JdbcWritableBridge.readString(2, __dbResults);
    this.first_name = JdbcWritableBridge.readString(3, __dbResults);
    this.last_name = JdbcWritableBridge.readString(4, __dbResults);
    this.department = JdbcWritableBridge.readString(5, __dbResults);
    this.city = JdbcWritableBridge.readString(6, __dbResults);
    this.state = JdbcWritableBridge.readString(7, __dbResults);
    this.country = JdbcWritableBridge.readString(8, __dbResults);
    this.level = JdbcWritableBridge.readString(9, __dbResults);
    this.job_title_code = JdbcWritableBridge.readString(10, __dbResults);
    this.job_title_name = JdbcWritableBridge.readString(11, __dbResults);
    this.svp_attuid = JdbcWritableBridge.readString(12, __dbResults);
    this.svp_name = JdbcWritableBridge.readString(13, __dbResults);
    this.svp_job_title = JdbcWritableBridge.readString(14, __dbResults);
    this.vp_attuid = JdbcWritableBridge.readString(15, __dbResults);
    this.vp_name = JdbcWritableBridge.readString(16, __dbResults);
    this.vp_job_title = JdbcWritableBridge.readString(17, __dbResults);
    this.avp_attuid = JdbcWritableBridge.readString(18, __dbResults);
    this.avp_name = JdbcWritableBridge.readString(19, __dbResults);
    this.avp_job_title = JdbcWritableBridge.readString(20, __dbResults);
    this.director_attuid = JdbcWritableBridge.readString(21, __dbResults);
    this.director_name = JdbcWritableBridge.readString(22, __dbResults);
    this.director_job_title = JdbcWritableBridge.readString(23, __dbResults);
    this.area_manager_attuid = JdbcWritableBridge.readString(24, __dbResults);
    this.area_manager_name = JdbcWritableBridge.readString(25, __dbResults);
    this.area_manager_job_title = JdbcWritableBridge.readString(26, __dbResults);
    this.manager_attuid = JdbcWritableBridge.readString(27, __dbResults);
    this.manager_name = JdbcWritableBridge.readString(28, __dbResults);
    this.manager_job_title = JdbcWritableBridge.readString(29, __dbResults);
    this.non_manager_supervisor_attuid = JdbcWritableBridge.readString(30, __dbResults);
    this.non_manager_supervisor_name = JdbcWritableBridge.readString(31, __dbResults);
    this.non_manager_supervisor_job_title = JdbcWritableBridge.readString(32, __dbResults);
  }
  public void loadLargeObjects(LargeObjectLoader __loader)
      throws SQLException, IOException, InterruptedException {
  }
  public void loadLargeObjects0(LargeObjectLoader __loader)
      throws SQLException, IOException, InterruptedException {
  }
  public void write(PreparedStatement __dbStmt) throws SQLException {
    write(__dbStmt, 0);
  }

  public int write(PreparedStatement __dbStmt, int __off) throws SQLException {
    JdbcWritableBridge.writeString(attuid, 1 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(name, 2 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(first_name, 3 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(last_name, 4 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(department, 5 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(city, 6 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(state, 7 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(country, 8 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(level, 9 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(job_title_code, 10 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(job_title_name, 11 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(svp_attuid, 12 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(svp_name, 13 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(svp_job_title, 14 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(vp_attuid, 15 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(vp_name, 16 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(vp_job_title, 17 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(avp_attuid, 18 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(avp_name, 19 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(avp_job_title, 20 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(director_attuid, 21 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(director_name, 22 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(director_job_title, 23 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(area_manager_attuid, 24 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(area_manager_name, 25 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(area_manager_job_title, 26 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(manager_attuid, 27 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(manager_name, 28 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(manager_job_title, 29 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(non_manager_supervisor_attuid, 30 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(non_manager_supervisor_name, 31 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(non_manager_supervisor_job_title, 32 + __off, 12, __dbStmt);
    return 32;
  }
  public void write0(PreparedStatement __dbStmt, int __off) throws SQLException {
    JdbcWritableBridge.writeString(attuid, 1 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(name, 2 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(first_name, 3 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(last_name, 4 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(department, 5 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(city, 6 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(state, 7 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(country, 8 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(level, 9 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(job_title_code, 10 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(job_title_name, 11 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(svp_attuid, 12 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(svp_name, 13 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(svp_job_title, 14 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(vp_attuid, 15 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(vp_name, 16 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(vp_job_title, 17 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(avp_attuid, 18 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(avp_name, 19 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(avp_job_title, 20 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(director_attuid, 21 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(director_name, 22 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(director_job_title, 23 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(area_manager_attuid, 24 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(area_manager_name, 25 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(area_manager_job_title, 26 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(manager_attuid, 27 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(manager_name, 28 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(manager_job_title, 29 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(non_manager_supervisor_attuid, 30 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(non_manager_supervisor_name, 31 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(non_manager_supervisor_job_title, 32 + __off, 12, __dbStmt);
  }
  public void readFields(DataInput __dataIn) throws IOException {
this.readFields0(__dataIn);  }
  public void readFields0(DataInput __dataIn) throws IOException {
    if (__dataIn.readBoolean()) { 
        this.attuid = null;
    } else {
    this.attuid = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.name = null;
    } else {
    this.name = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.first_name = null;
    } else {
    this.first_name = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.last_name = null;
    } else {
    this.last_name = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.department = null;
    } else {
    this.department = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.city = null;
    } else {
    this.city = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.state = null;
    } else {
    this.state = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.country = null;
    } else {
    this.country = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.level = null;
    } else {
    this.level = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.job_title_code = null;
    } else {
    this.job_title_code = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.job_title_name = null;
    } else {
    this.job_title_name = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.svp_attuid = null;
    } else {
    this.svp_attuid = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.svp_name = null;
    } else {
    this.svp_name = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.svp_job_title = null;
    } else {
    this.svp_job_title = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.vp_attuid = null;
    } else {
    this.vp_attuid = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.vp_name = null;
    } else {
    this.vp_name = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.vp_job_title = null;
    } else {
    this.vp_job_title = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.avp_attuid = null;
    } else {
    this.avp_attuid = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.avp_name = null;
    } else {
    this.avp_name = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.avp_job_title = null;
    } else {
    this.avp_job_title = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.director_attuid = null;
    } else {
    this.director_attuid = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.director_name = null;
    } else {
    this.director_name = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.director_job_title = null;
    } else {
    this.director_job_title = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.area_manager_attuid = null;
    } else {
    this.area_manager_attuid = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.area_manager_name = null;
    } else {
    this.area_manager_name = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.area_manager_job_title = null;
    } else {
    this.area_manager_job_title = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.manager_attuid = null;
    } else {
    this.manager_attuid = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.manager_name = null;
    } else {
    this.manager_name = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.manager_job_title = null;
    } else {
    this.manager_job_title = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.non_manager_supervisor_attuid = null;
    } else {
    this.non_manager_supervisor_attuid = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.non_manager_supervisor_name = null;
    } else {
    this.non_manager_supervisor_name = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.non_manager_supervisor_job_title = null;
    } else {
    this.non_manager_supervisor_job_title = Text.readString(__dataIn);
    }
  }
  public void write(DataOutput __dataOut) throws IOException {
    if (null == this.attuid) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, attuid);
    }
    if (null == this.name) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, name);
    }
    if (null == this.first_name) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, first_name);
    }
    if (null == this.last_name) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, last_name);
    }
    if (null == this.department) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, department);
    }
    if (null == this.city) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, city);
    }
    if (null == this.state) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, state);
    }
    if (null == this.country) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, country);
    }
    if (null == this.level) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, level);
    }
    if (null == this.job_title_code) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, job_title_code);
    }
    if (null == this.job_title_name) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, job_title_name);
    }
    if (null == this.svp_attuid) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, svp_attuid);
    }
    if (null == this.svp_name) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, svp_name);
    }
    if (null == this.svp_job_title) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, svp_job_title);
    }
    if (null == this.vp_attuid) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, vp_attuid);
    }
    if (null == this.vp_name) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, vp_name);
    }
    if (null == this.vp_job_title) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, vp_job_title);
    }
    if (null == this.avp_attuid) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, avp_attuid);
    }
    if (null == this.avp_name) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, avp_name);
    }
    if (null == this.avp_job_title) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, avp_job_title);
    }
    if (null == this.director_attuid) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, director_attuid);
    }
    if (null == this.director_name) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, director_name);
    }
    if (null == this.director_job_title) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, director_job_title);
    }
    if (null == this.area_manager_attuid) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, area_manager_attuid);
    }
    if (null == this.area_manager_name) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, area_manager_name);
    }
    if (null == this.area_manager_job_title) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, area_manager_job_title);
    }
    if (null == this.manager_attuid) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, manager_attuid);
    }
    if (null == this.manager_name) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, manager_name);
    }
    if (null == this.manager_job_title) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, manager_job_title);
    }
    if (null == this.non_manager_supervisor_attuid) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, non_manager_supervisor_attuid);
    }
    if (null == this.non_manager_supervisor_name) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, non_manager_supervisor_name);
    }
    if (null == this.non_manager_supervisor_job_title) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, non_manager_supervisor_job_title);
    }
  }
  public void write0(DataOutput __dataOut) throws IOException {
    if (null == this.attuid) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, attuid);
    }
    if (null == this.name) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, name);
    }
    if (null == this.first_name) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, first_name);
    }
    if (null == this.last_name) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, last_name);
    }
    if (null == this.department) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, department);
    }
    if (null == this.city) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, city);
    }
    if (null == this.state) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, state);
    }
    if (null == this.country) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, country);
    }
    if (null == this.level) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, level);
    }
    if (null == this.job_title_code) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, job_title_code);
    }
    if (null == this.job_title_name) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, job_title_name);
    }
    if (null == this.svp_attuid) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, svp_attuid);
    }
    if (null == this.svp_name) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, svp_name);
    }
    if (null == this.svp_job_title) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, svp_job_title);
    }
    if (null == this.vp_attuid) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, vp_attuid);
    }
    if (null == this.vp_name) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, vp_name);
    }
    if (null == this.vp_job_title) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, vp_job_title);
    }
    if (null == this.avp_attuid) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, avp_attuid);
    }
    if (null == this.avp_name) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, avp_name);
    }
    if (null == this.avp_job_title) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, avp_job_title);
    }
    if (null == this.director_attuid) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, director_attuid);
    }
    if (null == this.director_name) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, director_name);
    }
    if (null == this.director_job_title) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, director_job_title);
    }
    if (null == this.area_manager_attuid) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, area_manager_attuid);
    }
    if (null == this.area_manager_name) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, area_manager_name);
    }
    if (null == this.area_manager_job_title) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, area_manager_job_title);
    }
    if (null == this.manager_attuid) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, manager_attuid);
    }
    if (null == this.manager_name) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, manager_name);
    }
    if (null == this.manager_job_title) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, manager_job_title);
    }
    if (null == this.non_manager_supervisor_attuid) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, non_manager_supervisor_attuid);
    }
    if (null == this.non_manager_supervisor_name) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, non_manager_supervisor_name);
    }
    if (null == this.non_manager_supervisor_job_title) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, non_manager_supervisor_job_title);
    }
  }
  private static final DelimiterSet __outputDelimiters = new DelimiterSet((char) 1, (char) 10, (char) 0, (char) 0, false);
  public String toString() {
    return toString(__outputDelimiters, true);
  }
  public String toString(DelimiterSet delimiters) {
    return toString(delimiters, true);
  }
  public String toString(boolean useRecordDelim) {
    return toString(__outputDelimiters, useRecordDelim);
  }
  public String toString(DelimiterSet delimiters, boolean useRecordDelim) {
    StringBuilder __sb = new StringBuilder();
    char fieldDelim = delimiters.getFieldsTerminatedBy();
    __sb.append(FieldFormatter.escapeAndEnclose(attuid==null?"null":attuid, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(name==null?"null":name, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(first_name==null?"null":first_name, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(last_name==null?"null":last_name, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(department==null?"null":department, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(city==null?"null":city, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(state==null?"null":state, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(country==null?"null":country, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(level==null?"null":level, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(job_title_code==null?"null":job_title_code, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(job_title_name==null?"null":job_title_name, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(svp_attuid==null?"null":svp_attuid, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(svp_name==null?"null":svp_name, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(svp_job_title==null?"null":svp_job_title, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(vp_attuid==null?"null":vp_attuid, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(vp_name==null?"null":vp_name, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(vp_job_title==null?"null":vp_job_title, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(avp_attuid==null?"null":avp_attuid, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(avp_name==null?"null":avp_name, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(avp_job_title==null?"null":avp_job_title, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(director_attuid==null?"null":director_attuid, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(director_name==null?"null":director_name, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(director_job_title==null?"null":director_job_title, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(area_manager_attuid==null?"null":area_manager_attuid, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(area_manager_name==null?"null":area_manager_name, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(area_manager_job_title==null?"null":area_manager_job_title, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(manager_attuid==null?"null":manager_attuid, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(manager_name==null?"null":manager_name, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(manager_job_title==null?"null":manager_job_title, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(non_manager_supervisor_attuid==null?"null":non_manager_supervisor_attuid, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(non_manager_supervisor_name==null?"null":non_manager_supervisor_name, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(non_manager_supervisor_job_title==null?"null":non_manager_supervisor_job_title, delimiters));
    if (useRecordDelim) {
      __sb.append(delimiters.getLinesTerminatedBy());
    }
    return __sb.toString();
  }
  public void toString0(DelimiterSet delimiters, StringBuilder __sb, char fieldDelim) {
    __sb.append(FieldFormatter.escapeAndEnclose(attuid==null?"null":attuid, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(name==null?"null":name, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(first_name==null?"null":first_name, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(last_name==null?"null":last_name, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(department==null?"null":department, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(city==null?"null":city, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(state==null?"null":state, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(country==null?"null":country, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(level==null?"null":level, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(job_title_code==null?"null":job_title_code, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(job_title_name==null?"null":job_title_name, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(svp_attuid==null?"null":svp_attuid, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(svp_name==null?"null":svp_name, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(svp_job_title==null?"null":svp_job_title, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(vp_attuid==null?"null":vp_attuid, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(vp_name==null?"null":vp_name, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(vp_job_title==null?"null":vp_job_title, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(avp_attuid==null?"null":avp_attuid, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(avp_name==null?"null":avp_name, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(avp_job_title==null?"null":avp_job_title, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(director_attuid==null?"null":director_attuid, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(director_name==null?"null":director_name, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(director_job_title==null?"null":director_job_title, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(area_manager_attuid==null?"null":area_manager_attuid, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(area_manager_name==null?"null":area_manager_name, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(area_manager_job_title==null?"null":area_manager_job_title, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(manager_attuid==null?"null":manager_attuid, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(manager_name==null?"null":manager_name, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(manager_job_title==null?"null":manager_job_title, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(non_manager_supervisor_attuid==null?"null":non_manager_supervisor_attuid, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(non_manager_supervisor_name==null?"null":non_manager_supervisor_name, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(non_manager_supervisor_job_title==null?"null":non_manager_supervisor_job_title, delimiters));
  }
  private static final DelimiterSet __inputDelimiters = new DelimiterSet((char) 1, (char) 10, (char) 0, (char) 0, false);
  private RecordParser __parser;
  public void parse(Text __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(CharSequence __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(byte [] __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(char [] __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(ByteBuffer __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(CharBuffer __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  private void __loadFromFields(List<String> fields) {
    Iterator<String> __it = fields.listIterator();
    String __cur_str = null;
    try {
    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.attuid = null; } else {
      this.attuid = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.name = null; } else {
      this.name = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.first_name = null; } else {
      this.first_name = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.last_name = null; } else {
      this.last_name = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.department = null; } else {
      this.department = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.city = null; } else {
      this.city = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.state = null; } else {
      this.state = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.country = null; } else {
      this.country = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.level = null; } else {
      this.level = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.job_title_code = null; } else {
      this.job_title_code = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.job_title_name = null; } else {
      this.job_title_name = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.svp_attuid = null; } else {
      this.svp_attuid = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.svp_name = null; } else {
      this.svp_name = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.svp_job_title = null; } else {
      this.svp_job_title = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.vp_attuid = null; } else {
      this.vp_attuid = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.vp_name = null; } else {
      this.vp_name = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.vp_job_title = null; } else {
      this.vp_job_title = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.avp_attuid = null; } else {
      this.avp_attuid = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.avp_name = null; } else {
      this.avp_name = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.avp_job_title = null; } else {
      this.avp_job_title = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.director_attuid = null; } else {
      this.director_attuid = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.director_name = null; } else {
      this.director_name = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.director_job_title = null; } else {
      this.director_job_title = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.area_manager_attuid = null; } else {
      this.area_manager_attuid = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.area_manager_name = null; } else {
      this.area_manager_name = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.area_manager_job_title = null; } else {
      this.area_manager_job_title = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.manager_attuid = null; } else {
      this.manager_attuid = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.manager_name = null; } else {
      this.manager_name = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.manager_job_title = null; } else {
      this.manager_job_title = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.non_manager_supervisor_attuid = null; } else {
      this.non_manager_supervisor_attuid = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.non_manager_supervisor_name = null; } else {
      this.non_manager_supervisor_name = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.non_manager_supervisor_job_title = null; } else {
      this.non_manager_supervisor_job_title = __cur_str;
    }

    } catch (RuntimeException e) {    throw new RuntimeException("Can't parse input data: '" + __cur_str + "'", e);    }  }

  private void __loadFromFields0(Iterator<String> __it) {
    String __cur_str = null;
    try {
    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.attuid = null; } else {
      this.attuid = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.name = null; } else {
      this.name = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.first_name = null; } else {
      this.first_name = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.last_name = null; } else {
      this.last_name = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.department = null; } else {
      this.department = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.city = null; } else {
      this.city = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.state = null; } else {
      this.state = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.country = null; } else {
      this.country = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.level = null; } else {
      this.level = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.job_title_code = null; } else {
      this.job_title_code = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.job_title_name = null; } else {
      this.job_title_name = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.svp_attuid = null; } else {
      this.svp_attuid = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.svp_name = null; } else {
      this.svp_name = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.svp_job_title = null; } else {
      this.svp_job_title = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.vp_attuid = null; } else {
      this.vp_attuid = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.vp_name = null; } else {
      this.vp_name = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.vp_job_title = null; } else {
      this.vp_job_title = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.avp_attuid = null; } else {
      this.avp_attuid = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.avp_name = null; } else {
      this.avp_name = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.avp_job_title = null; } else {
      this.avp_job_title = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.director_attuid = null; } else {
      this.director_attuid = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.director_name = null; } else {
      this.director_name = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.director_job_title = null; } else {
      this.director_job_title = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.area_manager_attuid = null; } else {
      this.area_manager_attuid = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.area_manager_name = null; } else {
      this.area_manager_name = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.area_manager_job_title = null; } else {
      this.area_manager_job_title = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.manager_attuid = null; } else {
      this.manager_attuid = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.manager_name = null; } else {
      this.manager_name = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.manager_job_title = null; } else {
      this.manager_job_title = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.non_manager_supervisor_attuid = null; } else {
      this.non_manager_supervisor_attuid = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.non_manager_supervisor_name = null; } else {
      this.non_manager_supervisor_name = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.non_manager_supervisor_job_title = null; } else {
      this.non_manager_supervisor_job_title = __cur_str;
    }

    } catch (RuntimeException e) {    throw new RuntimeException("Can't parse input data: '" + __cur_str + "'", e);    }  }

  public Object clone() throws CloneNotSupportedException {
    SDA_Stage_dbo_Organization o = (SDA_Stage_dbo_Organization) super.clone();
    return o;
  }

  public void clone0(SDA_Stage_dbo_Organization o) throws CloneNotSupportedException {
  }

  public Map<String, Object> getFieldMap() {
    Map<String, Object> __sqoop$field_map = new TreeMap<String, Object>();
    __sqoop$field_map.put("attuid", this.attuid);
    __sqoop$field_map.put("name", this.name);
    __sqoop$field_map.put("first_name", this.first_name);
    __sqoop$field_map.put("last_name", this.last_name);
    __sqoop$field_map.put("department", this.department);
    __sqoop$field_map.put("city", this.city);
    __sqoop$field_map.put("state", this.state);
    __sqoop$field_map.put("country", this.country);
    __sqoop$field_map.put("level", this.level);
    __sqoop$field_map.put("job_title_code", this.job_title_code);
    __sqoop$field_map.put("job_title_name", this.job_title_name);
    __sqoop$field_map.put("svp_attuid", this.svp_attuid);
    __sqoop$field_map.put("svp_name", this.svp_name);
    __sqoop$field_map.put("svp_job_title", this.svp_job_title);
    __sqoop$field_map.put("vp_attuid", this.vp_attuid);
    __sqoop$field_map.put("vp_name", this.vp_name);
    __sqoop$field_map.put("vp_job_title", this.vp_job_title);
    __sqoop$field_map.put("avp_attuid", this.avp_attuid);
    __sqoop$field_map.put("avp_name", this.avp_name);
    __sqoop$field_map.put("avp_job_title", this.avp_job_title);
    __sqoop$field_map.put("director_attuid", this.director_attuid);
    __sqoop$field_map.put("director_name", this.director_name);
    __sqoop$field_map.put("director_job_title", this.director_job_title);
    __sqoop$field_map.put("area_manager_attuid", this.area_manager_attuid);
    __sqoop$field_map.put("area_manager_name", this.area_manager_name);
    __sqoop$field_map.put("area_manager_job_title", this.area_manager_job_title);
    __sqoop$field_map.put("manager_attuid", this.manager_attuid);
    __sqoop$field_map.put("manager_name", this.manager_name);
    __sqoop$field_map.put("manager_job_title", this.manager_job_title);
    __sqoop$field_map.put("non_manager_supervisor_attuid", this.non_manager_supervisor_attuid);
    __sqoop$field_map.put("non_manager_supervisor_name", this.non_manager_supervisor_name);
    __sqoop$field_map.put("non_manager_supervisor_job_title", this.non_manager_supervisor_job_title);
    return __sqoop$field_map;
  }

  public void getFieldMap0(Map<String, Object> __sqoop$field_map) {
    __sqoop$field_map.put("attuid", this.attuid);
    __sqoop$field_map.put("name", this.name);
    __sqoop$field_map.put("first_name", this.first_name);
    __sqoop$field_map.put("last_name", this.last_name);
    __sqoop$field_map.put("department", this.department);
    __sqoop$field_map.put("city", this.city);
    __sqoop$field_map.put("state", this.state);
    __sqoop$field_map.put("country", this.country);
    __sqoop$field_map.put("level", this.level);
    __sqoop$field_map.put("job_title_code", this.job_title_code);
    __sqoop$field_map.put("job_title_name", this.job_title_name);
    __sqoop$field_map.put("svp_attuid", this.svp_attuid);
    __sqoop$field_map.put("svp_name", this.svp_name);
    __sqoop$field_map.put("svp_job_title", this.svp_job_title);
    __sqoop$field_map.put("vp_attuid", this.vp_attuid);
    __sqoop$field_map.put("vp_name", this.vp_name);
    __sqoop$field_map.put("vp_job_title", this.vp_job_title);
    __sqoop$field_map.put("avp_attuid", this.avp_attuid);
    __sqoop$field_map.put("avp_name", this.avp_name);
    __sqoop$field_map.put("avp_job_title", this.avp_job_title);
    __sqoop$field_map.put("director_attuid", this.director_attuid);
    __sqoop$field_map.put("director_name", this.director_name);
    __sqoop$field_map.put("director_job_title", this.director_job_title);
    __sqoop$field_map.put("area_manager_attuid", this.area_manager_attuid);
    __sqoop$field_map.put("area_manager_name", this.area_manager_name);
    __sqoop$field_map.put("area_manager_job_title", this.area_manager_job_title);
    __sqoop$field_map.put("manager_attuid", this.manager_attuid);
    __sqoop$field_map.put("manager_name", this.manager_name);
    __sqoop$field_map.put("manager_job_title", this.manager_job_title);
    __sqoop$field_map.put("non_manager_supervisor_attuid", this.non_manager_supervisor_attuid);
    __sqoop$field_map.put("non_manager_supervisor_name", this.non_manager_supervisor_name);
    __sqoop$field_map.put("non_manager_supervisor_job_title", this.non_manager_supervisor_job_title);
  }

  public void setField(String __fieldName, Object __fieldVal) {
    if ("attuid".equals(__fieldName)) {
      this.attuid = (String) __fieldVal;
    }
    else    if ("name".equals(__fieldName)) {
      this.name = (String) __fieldVal;
    }
    else    if ("first_name".equals(__fieldName)) {
      this.first_name = (String) __fieldVal;
    }
    else    if ("last_name".equals(__fieldName)) {
      this.last_name = (String) __fieldVal;
    }
    else    if ("department".equals(__fieldName)) {
      this.department = (String) __fieldVal;
    }
    else    if ("city".equals(__fieldName)) {
      this.city = (String) __fieldVal;
    }
    else    if ("state".equals(__fieldName)) {
      this.state = (String) __fieldVal;
    }
    else    if ("country".equals(__fieldName)) {
      this.country = (String) __fieldVal;
    }
    else    if ("level".equals(__fieldName)) {
      this.level = (String) __fieldVal;
    }
    else    if ("job_title_code".equals(__fieldName)) {
      this.job_title_code = (String) __fieldVal;
    }
    else    if ("job_title_name".equals(__fieldName)) {
      this.job_title_name = (String) __fieldVal;
    }
    else    if ("svp_attuid".equals(__fieldName)) {
      this.svp_attuid = (String) __fieldVal;
    }
    else    if ("svp_name".equals(__fieldName)) {
      this.svp_name = (String) __fieldVal;
    }
    else    if ("svp_job_title".equals(__fieldName)) {
      this.svp_job_title = (String) __fieldVal;
    }
    else    if ("vp_attuid".equals(__fieldName)) {
      this.vp_attuid = (String) __fieldVal;
    }
    else    if ("vp_name".equals(__fieldName)) {
      this.vp_name = (String) __fieldVal;
    }
    else    if ("vp_job_title".equals(__fieldName)) {
      this.vp_job_title = (String) __fieldVal;
    }
    else    if ("avp_attuid".equals(__fieldName)) {
      this.avp_attuid = (String) __fieldVal;
    }
    else    if ("avp_name".equals(__fieldName)) {
      this.avp_name = (String) __fieldVal;
    }
    else    if ("avp_job_title".equals(__fieldName)) {
      this.avp_job_title = (String) __fieldVal;
    }
    else    if ("director_attuid".equals(__fieldName)) {
      this.director_attuid = (String) __fieldVal;
    }
    else    if ("director_name".equals(__fieldName)) {
      this.director_name = (String) __fieldVal;
    }
    else    if ("director_job_title".equals(__fieldName)) {
      this.director_job_title = (String) __fieldVal;
    }
    else    if ("area_manager_attuid".equals(__fieldName)) {
      this.area_manager_attuid = (String) __fieldVal;
    }
    else    if ("area_manager_name".equals(__fieldName)) {
      this.area_manager_name = (String) __fieldVal;
    }
    else    if ("area_manager_job_title".equals(__fieldName)) {
      this.area_manager_job_title = (String) __fieldVal;
    }
    else    if ("manager_attuid".equals(__fieldName)) {
      this.manager_attuid = (String) __fieldVal;
    }
    else    if ("manager_name".equals(__fieldName)) {
      this.manager_name = (String) __fieldVal;
    }
    else    if ("manager_job_title".equals(__fieldName)) {
      this.manager_job_title = (String) __fieldVal;
    }
    else    if ("non_manager_supervisor_attuid".equals(__fieldName)) {
      this.non_manager_supervisor_attuid = (String) __fieldVal;
    }
    else    if ("non_manager_supervisor_name".equals(__fieldName)) {
      this.non_manager_supervisor_name = (String) __fieldVal;
    }
    else    if ("non_manager_supervisor_job_title".equals(__fieldName)) {
      this.non_manager_supervisor_job_title = (String) __fieldVal;
    }
    else {
      throw new RuntimeException("No such field: " + __fieldName);
    }
  }
  public boolean setField0(String __fieldName, Object __fieldVal) {
    if ("attuid".equals(__fieldName)) {
      this.attuid = (String) __fieldVal;
      return true;
    }
    else    if ("name".equals(__fieldName)) {
      this.name = (String) __fieldVal;
      return true;
    }
    else    if ("first_name".equals(__fieldName)) {
      this.first_name = (String) __fieldVal;
      return true;
    }
    else    if ("last_name".equals(__fieldName)) {
      this.last_name = (String) __fieldVal;
      return true;
    }
    else    if ("department".equals(__fieldName)) {
      this.department = (String) __fieldVal;
      return true;
    }
    else    if ("city".equals(__fieldName)) {
      this.city = (String) __fieldVal;
      return true;
    }
    else    if ("state".equals(__fieldName)) {
      this.state = (String) __fieldVal;
      return true;
    }
    else    if ("country".equals(__fieldName)) {
      this.country = (String) __fieldVal;
      return true;
    }
    else    if ("level".equals(__fieldName)) {
      this.level = (String) __fieldVal;
      return true;
    }
    else    if ("job_title_code".equals(__fieldName)) {
      this.job_title_code = (String) __fieldVal;
      return true;
    }
    else    if ("job_title_name".equals(__fieldName)) {
      this.job_title_name = (String) __fieldVal;
      return true;
    }
    else    if ("svp_attuid".equals(__fieldName)) {
      this.svp_attuid = (String) __fieldVal;
      return true;
    }
    else    if ("svp_name".equals(__fieldName)) {
      this.svp_name = (String) __fieldVal;
      return true;
    }
    else    if ("svp_job_title".equals(__fieldName)) {
      this.svp_job_title = (String) __fieldVal;
      return true;
    }
    else    if ("vp_attuid".equals(__fieldName)) {
      this.vp_attuid = (String) __fieldVal;
      return true;
    }
    else    if ("vp_name".equals(__fieldName)) {
      this.vp_name = (String) __fieldVal;
      return true;
    }
    else    if ("vp_job_title".equals(__fieldName)) {
      this.vp_job_title = (String) __fieldVal;
      return true;
    }
    else    if ("avp_attuid".equals(__fieldName)) {
      this.avp_attuid = (String) __fieldVal;
      return true;
    }
    else    if ("avp_name".equals(__fieldName)) {
      this.avp_name = (String) __fieldVal;
      return true;
    }
    else    if ("avp_job_title".equals(__fieldName)) {
      this.avp_job_title = (String) __fieldVal;
      return true;
    }
    else    if ("director_attuid".equals(__fieldName)) {
      this.director_attuid = (String) __fieldVal;
      return true;
    }
    else    if ("director_name".equals(__fieldName)) {
      this.director_name = (String) __fieldVal;
      return true;
    }
    else    if ("director_job_title".equals(__fieldName)) {
      this.director_job_title = (String) __fieldVal;
      return true;
    }
    else    if ("area_manager_attuid".equals(__fieldName)) {
      this.area_manager_attuid = (String) __fieldVal;
      return true;
    }
    else    if ("area_manager_name".equals(__fieldName)) {
      this.area_manager_name = (String) __fieldVal;
      return true;
    }
    else    if ("area_manager_job_title".equals(__fieldName)) {
      this.area_manager_job_title = (String) __fieldVal;
      return true;
    }
    else    if ("manager_attuid".equals(__fieldName)) {
      this.manager_attuid = (String) __fieldVal;
      return true;
    }
    else    if ("manager_name".equals(__fieldName)) {
      this.manager_name = (String) __fieldVal;
      return true;
    }
    else    if ("manager_job_title".equals(__fieldName)) {
      this.manager_job_title = (String) __fieldVal;
      return true;
    }
    else    if ("non_manager_supervisor_attuid".equals(__fieldName)) {
      this.non_manager_supervisor_attuid = (String) __fieldVal;
      return true;
    }
    else    if ("non_manager_supervisor_name".equals(__fieldName)) {
      this.non_manager_supervisor_name = (String) __fieldVal;
      return true;
    }
    else    if ("non_manager_supervisor_job_title".equals(__fieldName)) {
      this.non_manager_supervisor_job_title = (String) __fieldVal;
      return true;
    }
    else {
      return false;    }
  }
}
