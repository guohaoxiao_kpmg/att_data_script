// ORM class for table 'idesktemp.dbo.gowalters'
// WARNING: This class is AUTO-GENERATED. Modify at your own risk.
//
// Debug information:
// Generated date: Fri Jun 24 12:15:58 EDT 2016
// For connector: org.apache.sqoop.manager.GenericJdbcManager
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapred.lib.db.DBWritable;
import com.cloudera.sqoop.lib.JdbcWritableBridge;
import com.cloudera.sqoop.lib.DelimiterSet;
import com.cloudera.sqoop.lib.FieldFormatter;
import com.cloudera.sqoop.lib.RecordParser;
import com.cloudera.sqoop.lib.BooleanParser;
import com.cloudera.sqoop.lib.BlobRef;
import com.cloudera.sqoop.lib.ClobRef;
import com.cloudera.sqoop.lib.LargeObjectLoader;
import com.cloudera.sqoop.lib.SqoopRecord;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class idesktemp_dbo_gowalters extends SqoopRecord  implements DBWritable, Writable {
  private final int PROTOCOL_VERSION = 3;
  public int getClassFormatVersion() { return PROTOCOL_VERSION; }
  protected ResultSet __cur_result_set;
  private String OrderIdentifier;
  public String get_OrderIdentifier() {
    return OrderIdentifier;
  }
  public void set_OrderIdentifier(String OrderIdentifier) {
    this.OrderIdentifier = OrderIdentifier;
  }
  public idesktemp_dbo_gowalters with_OrderIdentifier(String OrderIdentifier) {
    this.OrderIdentifier = OrderIdentifier;
    return this;
  }
  private String ServiceType;
  public String get_ServiceType() {
    return ServiceType;
  }
  public void set_ServiceType(String ServiceType) {
    this.ServiceType = ServiceType;
  }
  public idesktemp_dbo_gowalters with_ServiceType(String ServiceType) {
    this.ServiceType = ServiceType;
    return this;
  }
  private String OrderIssuedDate;
  public String get_OrderIssuedDate() {
    return OrderIssuedDate;
  }
  public void set_OrderIssuedDate(String OrderIssuedDate) {
    this.OrderIssuedDate = OrderIssuedDate;
  }
  public idesktemp_dbo_gowalters with_OrderIssuedDate(String OrderIssuedDate) {
    this.OrderIssuedDate = OrderIssuedDate;
    return this;
  }
  private String OrderCompletedDate;
  public String get_OrderCompletedDate() {
    return OrderCompletedDate;
  }
  public void set_OrderCompletedDate(String OrderCompletedDate) {
    this.OrderCompletedDate = OrderCompletedDate;
  }
  public idesktemp_dbo_gowalters with_OrderCompletedDate(String OrderCompletedDate) {
    this.OrderCompletedDate = OrderCompletedDate;
    return this;
  }
  private String product;
  public String get_product() {
    return product;
  }
  public void set_product(String product) {
    this.product = product;
  }
  public idesktemp_dbo_gowalters with_product(String product) {
    this.product = product;
    return this;
  }
  private String DMSproduct;
  public String get_DMSproduct() {
    return DMSproduct;
  }
  public void set_DMSproduct(String DMSproduct) {
    this.DMSproduct = DMSproduct;
  }
  public idesktemp_dbo_gowalters with_DMSproduct(String DMSproduct) {
    this.DMSproduct = DMSproduct;
    return this;
  }
  private String Order_creator;
  public String get_Order_creator() {
    return Order_creator;
  }
  public void set_Order_creator(String Order_creator) {
    this.Order_creator = Order_creator;
  }
  public idesktemp_dbo_gowalters with_Order_creator(String Order_creator) {
    this.Order_creator = Order_creator;
    return this;
  }
  private String Order_manager;
  public String get_Order_manager() {
    return Order_manager;
  }
  public void set_Order_manager(String Order_manager) {
    this.Order_manager = Order_manager;
  }
  public idesktemp_dbo_gowalters with_Order_manager(String Order_manager) {
    this.Order_manager = Order_manager;
    return this;
  }
  private String Speed_grouping;
  public String get_Speed_grouping() {
    return Speed_grouping;
  }
  public void set_Speed_grouping(String Speed_grouping) {
    this.Speed_grouping = Speed_grouping;
  }
  public idesktemp_dbo_gowalters with_Speed_grouping(String Speed_grouping) {
    this.Speed_grouping = Speed_grouping;
    return this;
  }
  private String tag;
  public String get_tag() {
    return tag;
  }
  public void set_tag(String tag) {
    this.tag = tag;
  }
  public idesktemp_dbo_gowalters with_tag(String tag) {
    this.tag = tag;
    return this;
  }
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof idesktemp_dbo_gowalters)) {
      return false;
    }
    idesktemp_dbo_gowalters that = (idesktemp_dbo_gowalters) o;
    boolean equal = true;
    equal = equal && (this.OrderIdentifier == null ? that.OrderIdentifier == null : this.OrderIdentifier.equals(that.OrderIdentifier));
    equal = equal && (this.ServiceType == null ? that.ServiceType == null : this.ServiceType.equals(that.ServiceType));
    equal = equal && (this.OrderIssuedDate == null ? that.OrderIssuedDate == null : this.OrderIssuedDate.equals(that.OrderIssuedDate));
    equal = equal && (this.OrderCompletedDate == null ? that.OrderCompletedDate == null : this.OrderCompletedDate.equals(that.OrderCompletedDate));
    equal = equal && (this.product == null ? that.product == null : this.product.equals(that.product));
    equal = equal && (this.DMSproduct == null ? that.DMSproduct == null : this.DMSproduct.equals(that.DMSproduct));
    equal = equal && (this.Order_creator == null ? that.Order_creator == null : this.Order_creator.equals(that.Order_creator));
    equal = equal && (this.Order_manager == null ? that.Order_manager == null : this.Order_manager.equals(that.Order_manager));
    equal = equal && (this.Speed_grouping == null ? that.Speed_grouping == null : this.Speed_grouping.equals(that.Speed_grouping));
    equal = equal && (this.tag == null ? that.tag == null : this.tag.equals(that.tag));
    return equal;
  }
  public boolean equals0(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof idesktemp_dbo_gowalters)) {
      return false;
    }
    idesktemp_dbo_gowalters that = (idesktemp_dbo_gowalters) o;
    boolean equal = true;
    equal = equal && (this.OrderIdentifier == null ? that.OrderIdentifier == null : this.OrderIdentifier.equals(that.OrderIdentifier));
    equal = equal && (this.ServiceType == null ? that.ServiceType == null : this.ServiceType.equals(that.ServiceType));
    equal = equal && (this.OrderIssuedDate == null ? that.OrderIssuedDate == null : this.OrderIssuedDate.equals(that.OrderIssuedDate));
    equal = equal && (this.OrderCompletedDate == null ? that.OrderCompletedDate == null : this.OrderCompletedDate.equals(that.OrderCompletedDate));
    equal = equal && (this.product == null ? that.product == null : this.product.equals(that.product));
    equal = equal && (this.DMSproduct == null ? that.DMSproduct == null : this.DMSproduct.equals(that.DMSproduct));
    equal = equal && (this.Order_creator == null ? that.Order_creator == null : this.Order_creator.equals(that.Order_creator));
    equal = equal && (this.Order_manager == null ? that.Order_manager == null : this.Order_manager.equals(that.Order_manager));
    equal = equal && (this.Speed_grouping == null ? that.Speed_grouping == null : this.Speed_grouping.equals(that.Speed_grouping));
    equal = equal && (this.tag == null ? that.tag == null : this.tag.equals(that.tag));
    return equal;
  }
  public void readFields(ResultSet __dbResults) throws SQLException {
    this.__cur_result_set = __dbResults;
    this.OrderIdentifier = JdbcWritableBridge.readString(1, __dbResults);
    this.ServiceType = JdbcWritableBridge.readString(2, __dbResults);
    this.OrderIssuedDate = JdbcWritableBridge.readString(3, __dbResults);
    this.OrderCompletedDate = JdbcWritableBridge.readString(4, __dbResults);
    this.product = JdbcWritableBridge.readString(5, __dbResults);
    this.DMSproduct = JdbcWritableBridge.readString(6, __dbResults);
    this.Order_creator = JdbcWritableBridge.readString(7, __dbResults);
    this.Order_manager = JdbcWritableBridge.readString(8, __dbResults);
    this.Speed_grouping = JdbcWritableBridge.readString(9, __dbResults);
    this.tag = JdbcWritableBridge.readString(10, __dbResults);
  }
  public void readFields0(ResultSet __dbResults) throws SQLException {
    this.OrderIdentifier = JdbcWritableBridge.readString(1, __dbResults);
    this.ServiceType = JdbcWritableBridge.readString(2, __dbResults);
    this.OrderIssuedDate = JdbcWritableBridge.readString(3, __dbResults);
    this.OrderCompletedDate = JdbcWritableBridge.readString(4, __dbResults);
    this.product = JdbcWritableBridge.readString(5, __dbResults);
    this.DMSproduct = JdbcWritableBridge.readString(6, __dbResults);
    this.Order_creator = JdbcWritableBridge.readString(7, __dbResults);
    this.Order_manager = JdbcWritableBridge.readString(8, __dbResults);
    this.Speed_grouping = JdbcWritableBridge.readString(9, __dbResults);
    this.tag = JdbcWritableBridge.readString(10, __dbResults);
  }
  public void loadLargeObjects(LargeObjectLoader __loader)
      throws SQLException, IOException, InterruptedException {
  }
  public void loadLargeObjects0(LargeObjectLoader __loader)
      throws SQLException, IOException, InterruptedException {
  }
  public void write(PreparedStatement __dbStmt) throws SQLException {
    write(__dbStmt, 0);
  }

  public int write(PreparedStatement __dbStmt, int __off) throws SQLException {
    JdbcWritableBridge.writeString(OrderIdentifier, 1 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(ServiceType, 2 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(OrderIssuedDate, 3 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(OrderCompletedDate, 4 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(product, 5 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(DMSproduct, 6 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(Order_creator, 7 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(Order_manager, 8 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(Speed_grouping, 9 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(tag, 10 + __off, 12, __dbStmt);
    return 10;
  }
  public void write0(PreparedStatement __dbStmt, int __off) throws SQLException {
    JdbcWritableBridge.writeString(OrderIdentifier, 1 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(ServiceType, 2 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(OrderIssuedDate, 3 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(OrderCompletedDate, 4 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(product, 5 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(DMSproduct, 6 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(Order_creator, 7 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(Order_manager, 8 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(Speed_grouping, 9 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(tag, 10 + __off, 12, __dbStmt);
  }
  public void readFields(DataInput __dataIn) throws IOException {
this.readFields0(__dataIn);  }
  public void readFields0(DataInput __dataIn) throws IOException {
    if (__dataIn.readBoolean()) { 
        this.OrderIdentifier = null;
    } else {
    this.OrderIdentifier = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.ServiceType = null;
    } else {
    this.ServiceType = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.OrderIssuedDate = null;
    } else {
    this.OrderIssuedDate = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.OrderCompletedDate = null;
    } else {
    this.OrderCompletedDate = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.product = null;
    } else {
    this.product = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.DMSproduct = null;
    } else {
    this.DMSproduct = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.Order_creator = null;
    } else {
    this.Order_creator = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.Order_manager = null;
    } else {
    this.Order_manager = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.Speed_grouping = null;
    } else {
    this.Speed_grouping = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.tag = null;
    } else {
    this.tag = Text.readString(__dataIn);
    }
  }
  public void write(DataOutput __dataOut) throws IOException {
    if (null == this.OrderIdentifier) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, OrderIdentifier);
    }
    if (null == this.ServiceType) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, ServiceType);
    }
    if (null == this.OrderIssuedDate) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, OrderIssuedDate);
    }
    if (null == this.OrderCompletedDate) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, OrderCompletedDate);
    }
    if (null == this.product) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, product);
    }
    if (null == this.DMSproduct) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, DMSproduct);
    }
    if (null == this.Order_creator) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, Order_creator);
    }
    if (null == this.Order_manager) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, Order_manager);
    }
    if (null == this.Speed_grouping) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, Speed_grouping);
    }
    if (null == this.tag) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, tag);
    }
  }
  public void write0(DataOutput __dataOut) throws IOException {
    if (null == this.OrderIdentifier) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, OrderIdentifier);
    }
    if (null == this.ServiceType) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, ServiceType);
    }
    if (null == this.OrderIssuedDate) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, OrderIssuedDate);
    }
    if (null == this.OrderCompletedDate) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, OrderCompletedDate);
    }
    if (null == this.product) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, product);
    }
    if (null == this.DMSproduct) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, DMSproduct);
    }
    if (null == this.Order_creator) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, Order_creator);
    }
    if (null == this.Order_manager) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, Order_manager);
    }
    if (null == this.Speed_grouping) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, Speed_grouping);
    }
    if (null == this.tag) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, tag);
    }
  }
  private static final DelimiterSet __outputDelimiters = new DelimiterSet((char) 1, (char) 10, (char) 0, (char) 0, false);
  public String toString() {
    return toString(__outputDelimiters, true);
  }
  public String toString(DelimiterSet delimiters) {
    return toString(delimiters, true);
  }
  public String toString(boolean useRecordDelim) {
    return toString(__outputDelimiters, useRecordDelim);
  }
  public String toString(DelimiterSet delimiters, boolean useRecordDelim) {
    StringBuilder __sb = new StringBuilder();
    char fieldDelim = delimiters.getFieldsTerminatedBy();
    __sb.append(FieldFormatter.escapeAndEnclose(OrderIdentifier==null?"null":OrderIdentifier, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(ServiceType==null?"null":ServiceType, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(OrderIssuedDate==null?"null":OrderIssuedDate, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(OrderCompletedDate==null?"null":OrderCompletedDate, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(product==null?"null":product, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(DMSproduct==null?"null":DMSproduct, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(Order_creator==null?"null":Order_creator, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(Order_manager==null?"null":Order_manager, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(Speed_grouping==null?"null":Speed_grouping, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(tag==null?"null":tag, delimiters));
    if (useRecordDelim) {
      __sb.append(delimiters.getLinesTerminatedBy());
    }
    return __sb.toString();
  }
  public void toString0(DelimiterSet delimiters, StringBuilder __sb, char fieldDelim) {
    __sb.append(FieldFormatter.escapeAndEnclose(OrderIdentifier==null?"null":OrderIdentifier, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(ServiceType==null?"null":ServiceType, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(OrderIssuedDate==null?"null":OrderIssuedDate, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(OrderCompletedDate==null?"null":OrderCompletedDate, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(product==null?"null":product, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(DMSproduct==null?"null":DMSproduct, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(Order_creator==null?"null":Order_creator, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(Order_manager==null?"null":Order_manager, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(Speed_grouping==null?"null":Speed_grouping, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(tag==null?"null":tag, delimiters));
  }
  private static final DelimiterSet __inputDelimiters = new DelimiterSet((char) 1, (char) 10, (char) 0, (char) 0, false);
  private RecordParser __parser;
  public void parse(Text __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(CharSequence __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(byte [] __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(char [] __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(ByteBuffer __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(CharBuffer __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  private void __loadFromFields(List<String> fields) {
    Iterator<String> __it = fields.listIterator();
    String __cur_str = null;
    try {
    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.OrderIdentifier = null; } else {
      this.OrderIdentifier = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.ServiceType = null; } else {
      this.ServiceType = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.OrderIssuedDate = null; } else {
      this.OrderIssuedDate = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.OrderCompletedDate = null; } else {
      this.OrderCompletedDate = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.product = null; } else {
      this.product = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.DMSproduct = null; } else {
      this.DMSproduct = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.Order_creator = null; } else {
      this.Order_creator = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.Order_manager = null; } else {
      this.Order_manager = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.Speed_grouping = null; } else {
      this.Speed_grouping = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.tag = null; } else {
      this.tag = __cur_str;
    }

    } catch (RuntimeException e) {    throw new RuntimeException("Can't parse input data: '" + __cur_str + "'", e);    }  }

  private void __loadFromFields0(Iterator<String> __it) {
    String __cur_str = null;
    try {
    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.OrderIdentifier = null; } else {
      this.OrderIdentifier = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.ServiceType = null; } else {
      this.ServiceType = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.OrderIssuedDate = null; } else {
      this.OrderIssuedDate = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.OrderCompletedDate = null; } else {
      this.OrderCompletedDate = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.product = null; } else {
      this.product = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.DMSproduct = null; } else {
      this.DMSproduct = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.Order_creator = null; } else {
      this.Order_creator = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.Order_manager = null; } else {
      this.Order_manager = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.Speed_grouping = null; } else {
      this.Speed_grouping = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.tag = null; } else {
      this.tag = __cur_str;
    }

    } catch (RuntimeException e) {    throw new RuntimeException("Can't parse input data: '" + __cur_str + "'", e);    }  }

  public Object clone() throws CloneNotSupportedException {
    idesktemp_dbo_gowalters o = (idesktemp_dbo_gowalters) super.clone();
    return o;
  }

  public void clone0(idesktemp_dbo_gowalters o) throws CloneNotSupportedException {
  }

  public Map<String, Object> getFieldMap() {
    Map<String, Object> __sqoop$field_map = new TreeMap<String, Object>();
    __sqoop$field_map.put("OrderIdentifier", this.OrderIdentifier);
    __sqoop$field_map.put("ServiceType", this.ServiceType);
    __sqoop$field_map.put("OrderIssuedDate", this.OrderIssuedDate);
    __sqoop$field_map.put("OrderCompletedDate", this.OrderCompletedDate);
    __sqoop$field_map.put("product", this.product);
    __sqoop$field_map.put("DMSproduct", this.DMSproduct);
    __sqoop$field_map.put("Order_creator", this.Order_creator);
    __sqoop$field_map.put("Order_manager", this.Order_manager);
    __sqoop$field_map.put("Speed_grouping", this.Speed_grouping);
    __sqoop$field_map.put("tag", this.tag);
    return __sqoop$field_map;
  }

  public void getFieldMap0(Map<String, Object> __sqoop$field_map) {
    __sqoop$field_map.put("OrderIdentifier", this.OrderIdentifier);
    __sqoop$field_map.put("ServiceType", this.ServiceType);
    __sqoop$field_map.put("OrderIssuedDate", this.OrderIssuedDate);
    __sqoop$field_map.put("OrderCompletedDate", this.OrderCompletedDate);
    __sqoop$field_map.put("product", this.product);
    __sqoop$field_map.put("DMSproduct", this.DMSproduct);
    __sqoop$field_map.put("Order_creator", this.Order_creator);
    __sqoop$field_map.put("Order_manager", this.Order_manager);
    __sqoop$field_map.put("Speed_grouping", this.Speed_grouping);
    __sqoop$field_map.put("tag", this.tag);
  }

  public void setField(String __fieldName, Object __fieldVal) {
    if ("OrderIdentifier".equals(__fieldName)) {
      this.OrderIdentifier = (String) __fieldVal;
    }
    else    if ("ServiceType".equals(__fieldName)) {
      this.ServiceType = (String) __fieldVal;
    }
    else    if ("OrderIssuedDate".equals(__fieldName)) {
      this.OrderIssuedDate = (String) __fieldVal;
    }
    else    if ("OrderCompletedDate".equals(__fieldName)) {
      this.OrderCompletedDate = (String) __fieldVal;
    }
    else    if ("product".equals(__fieldName)) {
      this.product = (String) __fieldVal;
    }
    else    if ("DMSproduct".equals(__fieldName)) {
      this.DMSproduct = (String) __fieldVal;
    }
    else    if ("Order_creator".equals(__fieldName)) {
      this.Order_creator = (String) __fieldVal;
    }
    else    if ("Order_manager".equals(__fieldName)) {
      this.Order_manager = (String) __fieldVal;
    }
    else    if ("Speed_grouping".equals(__fieldName)) {
      this.Speed_grouping = (String) __fieldVal;
    }
    else    if ("tag".equals(__fieldName)) {
      this.tag = (String) __fieldVal;
    }
    else {
      throw new RuntimeException("No such field: " + __fieldName);
    }
  }
  public boolean setField0(String __fieldName, Object __fieldVal) {
    if ("OrderIdentifier".equals(__fieldName)) {
      this.OrderIdentifier = (String) __fieldVal;
      return true;
    }
    else    if ("ServiceType".equals(__fieldName)) {
      this.ServiceType = (String) __fieldVal;
      return true;
    }
    else    if ("OrderIssuedDate".equals(__fieldName)) {
      this.OrderIssuedDate = (String) __fieldVal;
      return true;
    }
    else    if ("OrderCompletedDate".equals(__fieldName)) {
      this.OrderCompletedDate = (String) __fieldVal;
      return true;
    }
    else    if ("product".equals(__fieldName)) {
      this.product = (String) __fieldVal;
      return true;
    }
    else    if ("DMSproduct".equals(__fieldName)) {
      this.DMSproduct = (String) __fieldVal;
      return true;
    }
    else    if ("Order_creator".equals(__fieldName)) {
      this.Order_creator = (String) __fieldVal;
      return true;
    }
    else    if ("Order_manager".equals(__fieldName)) {
      this.Order_manager = (String) __fieldVal;
      return true;
    }
    else    if ("Speed_grouping".equals(__fieldName)) {
      this.Speed_grouping = (String) __fieldVal;
      return true;
    }
    else    if ("tag".equals(__fieldName)) {
      this.tag = (String) __fieldVal;
      return true;
    }
    else {
      return false;    }
  }
}
