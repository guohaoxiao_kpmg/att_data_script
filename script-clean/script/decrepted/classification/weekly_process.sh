elink_base=$(hive -e "select max(date) as date from kpmg_ws.elink_max_date;")
elink_max=$(hive -e "select max(calendar_day) as date from kpmg.elink_standard;")

idesk_base=$(hive -e "select max(date) as date from kpmg_ws.idesk_max_date;")
idesk_max=$(hive -e "select max(regexp_replace(active_window_start_date,'-','')) as date from kpmg.idesk_log_detail;")



if [[ $idesk_base -eq $idesk_max || $elink_base -eq $elink_max ]]
then
  messageStr="elink or idesk is not updated"
else
message="elink date updated to :"$max" last updated to:"$base
messageStr=`printf 'elink updated successfully %s \n' ${message}`
fi
echo -e ${messageStr}
echo -e ${messageStr} | mailx -s "elink date check" ${MailList}



