DELETE FILE udf.py;
DELETE FILE rule.txt;
add file udf.py;
add file rule.txt;

--drop table kpmg_ws.temp;
--create table kpmg_ws.temp 
--as 
--select app_name,duration_in_sec,window_title,site,url
--from
--kpmg.idesk_log_detail_standardized limit 1000;


select  TRANSFORM(app_name,duration_in_sec,window_title,site,url) USING 'python udf.py' as (set string)
from
kpmg_ws.temp;
