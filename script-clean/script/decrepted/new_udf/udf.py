#!/usr/bin/python

import sys
import string
import hashlib
import re

class Ideskfeature(object):
    """ app_name, duration_insec,window_tile,site,url to indentify classification
    """
    app_name=""
    duration_min=""
    duration_max=""
    window_title=""
    site=""
    url=""
    classfication=""

    re_app_name=""
    re_window_title=""
    re_site=""
    re_url=""


    def __init__(self, app_name, duration_min, duration_max, window_title, site, url, classfication):

        self.app_name = "^" + app_name+"$"

        if duration_min=="N":
            self.duration_min="-1";
        else:
            self.duration_min = duration_min
        if duration_max=="N":
            self.duration_max = sys.maxint
        else:
            self.duration_max = duration_max

        self.window_title = "^" + window_title + "$"
        self.site = "^" + site + "$"
        self.url = "^" + url + "$"
        self.classfication=classfication

        self.re_app_name = re.compile(self.app_name)
        self.re_window_title = re.compile(self.window_title)
        self.re_site = re.compile(self.site)
        self.re_url = re.compile(self.url)


    def match(self, app_name, duration, window_title, site, url):
        if self.re_app_name.match(app_name) and self.re_window_title.match(window_title) and self.re_site.match(site) and self.re_url.match(url) and float(duration) > float(self.duration_min) and float(duration) < float(self.duration_max):
            return self.classfication
        else :
            return None



list=[]
for line in open("rule.txt") :
    elements = line.strip("\n").split("\t")
    feature= Ideskfeature(elements[0], elements[1], elements[2] , elements[3], elements[4],elements[5],elements[6])
    list.append(feature)

while True:
    line=sys.stdin.readline()
    if not line:
        break
    elements = line.strip("\n").split("\t")
    getResult=0
    for feature in list:
        if len(elements) <= 4:
            print "less thaN4 "
        if len(elements) > 4 and feature.match(elements[0], elements[1], elements[2] , elements[3], elements[4]) is not None:
            print feature.classfication
            getResult=1
            break
    if getResult==0:
        print "NOT_CLASSIFIED"

