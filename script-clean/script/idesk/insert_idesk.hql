--set hive.execution.engine=tez;
use kpmg_ws;
drop table temp_de_dup_idesk;
create table temp_de_dup_idesk
as
select *, row_number() over( partition by attuid,end_ts,start_ts order by create_date desc) as rank
from
kpmg_ws.temp_idesk_log;


insert into table kpmg.idesk_log_detail
PARTITION(active_window_start_date)
select 
  upper(`attuid`),
  upper(`win_title`),
  upper(`proc_desc`),
  upper(`url_desc`),
  upper(`app_desc`),
  `create_date`,
  `modify_date`,
  `end_ts`,
  upper(`local_tz`),
  upper(`mac_id`),
  upper(`copy_app`),
  upper(`copy_title`),
  upper(`copy_ts`),
  upper(`paste_app`),
  upper(`paste_title`),
  upper(`paste_ts`),
  `start_ts`,
  `duration_secs`
,to_date(start_ts) as active_window_start_date
from kpmg_ws.temp_de_dup_idesk
where rank=1;

drop table temp_idesk;
create table temp_idesk
as
select *, row_number() over( partition by attuid,active_window_end_timetamp,active_window_end_timetamp order by create_date desc) as rank
from
kpmg.idesk_log_detail;

use kpmg;
drop table idesk_log_detail;

CREATE TABLE kpmg.idesk_log_detail
(
  `attuid` string,
  `window_title` string,
  `site` string,
  `url` string,
  `app_name` string,
  `create_date` string,
  `modify_date` string,
  `active_window_end_timetamp` timestamp,
  `time_zone` string,
  `machine_id` string,
  `copy_app` string,
  `copy_title` string,
  `copy_ts` timestamp,
  `paste_app` string,
  `paste_title` string,
  `paste_ts` timestamp,
  `active_window_start_timetamp` timestamp,
  `duration_in_sec` double
)
partitioned by (active_window_start_date   string)
ROW format delimited fields terminated by "|"
stored as orc
location '/sandbox/sandbox31/kpmg/standard/idesk_log_detail'
;


set hive.exec.dynamic.partition.mode=nonstrict;
set hive.exec.dynamic.partition=true;
set hive.exec.max.dynamic.partitions.pernode=300;
set hive.exec.max.dynamic.partitions =10000000;
set hive.optimize.sort.dynamic.partition=true;

set fs.file.impl.disable.cache=false;
set fs.hdfs.impl.disable.cache=false;
insert into table kpmg.idesk_log_detail
PARTITION(active_window_start_date)
select
  attuid,
  window_title,
  site,
  url,
  app_name,
  create_date,
  modify_date,
  active_window_end_timetamp,
  time_zone,
  machine_id,
  copy_app,
  copy_title,
  copy_ts,
  paste_app,
  paste_title,
  paste_ts,
  active_window_start_timetamp,
  duration_in_sec,
  active_window_start_date
from kpmg_ws.temp_idesk
where rank=1;

