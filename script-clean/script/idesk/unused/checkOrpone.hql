use kpmg;

select distinct i.attuid
from kpmg.idesk_log_detail i
left  join USER_ORG u
on i.attuid=u.attuid
where u.attuid is null
;

select active_window_start_date, count(*)
from kpmg.idesk_log_detail i
left  join USER_ORG u
on i.attuid=u.attuid
where u.attuid is null
group by 
active_window_start_date;


