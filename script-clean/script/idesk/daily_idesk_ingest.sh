#! /bin/bash
# * ----------------------------------------------------------------------------------
# * Project:         ATT
# *   Console script
# *
# * Log:
# *   
# *    2015/11/30    Guohao xiao     Sqoop Idesk data into hive table and partition it
# * ----------------------------------------------------------------------------------
echo "LinkNumber1+"|kinit
start=$1
end=$2

echo $start
echo $end

hadoop dfs -rmr /sandbox/sandbox31/kpmg_ws/tmp/idesk_log


sqoop  import \
-libjars "/usr/hdp/current/sqoop-client/lib/sqljdbc4.jar" \
--connect "jdbc:sqlserver://cldprd0sql01682.itservices.sbc.com\PD_ECCRIX01" \
--username hadoop_reader \
--password 'w!tmkLi83aB.' \
--as-textfile \
--target-dir '/sandbox/sandbox31/kpmg_ws/tmp/idesk_log' \
-m 20 \
--split-by attuid \
--query 'select attuid,win_title,proc_desc,url_desc,app_desc,create_date,modify_date,end_ts,local_tz,mac_id,copy_app,copy_title,copy_ts,paste_app,paste_title,paste_ts,start_ts,duration_secs from FIDO.dbo.sdpa_dm_app_detail_usage_view where create_date > convert(datetime,convert(char(8), '$start'), 111) and create_date < convert(datetime, convert(char(8), '$end'), 111) and $CONDITIONS' \
--null-string '\\N' \
--null-non-string '\\N' \
--fields-terminated-by '\001' \
--hive-drop-import-delims ;
#--connection-manager 'org.apache.sqoop.manager.SQLServerManager';

hadoop dfs -mkdir "/sandbox/sandbox31/backup/idesk_backup/$end"
hadoop dfs -cp "/sandbox/sandbox31/kpmg_ws/tmp/idesk_log/*" "/sandbox/sandbox31/backup/idesk_backup/$end/"

hive -f /home/gc096s/guohao/script/idesk/insert_idesk.hql
hadoop dfs -chmod -R 755 /sandbox/sandbox31/kpmg/standard/idesk*

echo "weekly idesk downloads count:"

output=$(hive -e "select to_date(start_ts) as date1,count(*) from kpmg_ws.temp_de_dup_idesk group by to_date(start_ts) order by date1;")

MailList="guohaoxiao@kpmg.com,smuthersbaugh@kpmg.com,tbaweja@kpmg.com"
messageStr=`printf '%s \n' ${output}` 
echo -e ${messageStr} 
echo -e ${messageStr} | mailx -s "IDesk new ingestion distribution" ${MailList}



#base=$(hive -e "select max(date) as date from kpmg.base;")
base=$(hive -e "select max(date1) as date1 from kpmg_ws.idesk_max_date;")
max=$(hive -e "select max(regexp_replace(active_window_start_date,'-','')) as date1 from kpmg.idesk_log_detail;")
hive -e "
use kpmg_ws;
drop table idesk_max_date;
create table  idesk_max_date as
select max(regexp_replace(active_window_start_date,'-','')) as date1 from kpmg.idesk_log_detail;
"

if [[ $base -eq $max ]]
then 
  messageStr="idesk  process not succeded"
  hive -e "
use kpmg_ws;
drop table idesk_status;
create table  idesk_status
(
status string
);
insert into idesk_status values('failed')
"



else 
  hive -e "
use kpmg_ws;
drop table idesk_status;
create table  idesk_status
(
status string
);
insert into idesk_status values('ok')
"


message="idesk updated to : $max last updated to date: $base"
messageStr=`printf '%s \n' ${message}`
fi
echo -e ${messageStr}
echo -e ${messageStr} | mailx -s "IDesk date check" ${MailList}


