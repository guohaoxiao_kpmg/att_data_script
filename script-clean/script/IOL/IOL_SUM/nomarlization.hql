set hive.cli.print.header=true;

drop table kpmg_ws.temp_freq;
drop table kpmg_ws.temp_density;
drop table kpmg_ws.IOL_weight;
create table kpmg_ws.temp_freq as
select type,event,sum(if(week='1',count,0)) as mon,sum(if(week='2',count,0)) as Tue,sum(if(week='3',count,0)) as Wed,sum(if(week='4',count,0)) as Thu,sum(if(week='5',count,0)) as Fri, sum(count) as sum, sum(count)/5 as avg,rank() over( partition by type order by sum(count) desc) as rank
from 
(
select type,  from_unixtime(unix_timestamp(to_date(completed_date),'yyyy-MM-dd'),'u') as week,event,count(*) as count
from kpmg.iol
where completed_date is not null and event is not null and from_unixtime(unix_timestamp(to_date(completed_date),'yyyy-MM-dd'),'u')!='6' and from_unixtime(unix_timestamp(to_date(completed_date),'yyyy-MM-dd'),'u')!='7'
group by from_unixtime(unix_timestamp(to_date(completed_date),'yyyy-MM-dd'),'u'),event,type) x
group by event,type
;

create table kpmg_ws.temp_density as
select type,event,sum(if(week='1',count,0)) as mon,sum(if(week='2',count,0)) as Tue,sum(if(week='3',count,0)) as Wed,sum(if(week='4',count,0)) as Thu,sum(if(week='5',count,0)) as Fri,
 sum(count) as sum,sum(count)/5 as avg,rank() over( partition by type order by sum(count) desc) as rank
from
(
select type, from_unixtime(unix_timestamp(to_date(completed_date),'yyyy-MM-dd'),'u') as week,event,count(distinct attuid) as count
from kpmg.iol
where completed_date is not null and event is not null and from_unixtime(unix_timestamp(to_date(completed_date),'yyyy-MM-dd'),'u')!='6' and from_unixtime(unix_timestamp(to_date(completed_date),'yyyy-MM-dd'),'u')!='7'
group by from_unixtime(unix_timestamp(to_date(completed_date),'yyyy-MM-dd'),'u'),event,type) x
group by event,type
;

select * from kpmg_ws.temp_freq;
select * from kpmg_ws.temp_density;

create table kpmg_ws.IOL_weight
as
select f.type as type,f.event as event,f.avg as  freq_avg,d.avg as den_avg,f.avg/d.avg as avg,f.rank as frank,d.rank as drank,f.rank+d.rank as total,(max+1)/(f.avg/d.avg+1) as mul  from
kpmg_ws.temp_freq f
inner join kpmg_ws.temp_density d
on f.event=d.event and f.type=d.type,
(
select type,max(avg) as max
from
(
select f.type as type,f.event as event,f.avg as  freq_avg,d.avg as den_avg,f.avg/d.avg as avg,f.rank as frank,d.rank as drank,f.rank+d.rank as total from
kpmg_ws.temp_freq f
inner join kpmg_ws.temp_density d
on f.event=d.event and f.type=d.type)x
group by type
)y
where y.type=f.type
;


select * from kpmg_ws.IOL_weight;
