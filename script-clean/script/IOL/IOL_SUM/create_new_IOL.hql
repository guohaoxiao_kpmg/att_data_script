drop table kpmg_ws.IOL_new;
CREATE TABLE kpmg_ws.IOL_new
(
attuid string,
USO string,
event string,
completed_date timestamp
)
ROW format delimited fields terminated by ","
location '/sandbox/sandbox31/kpmg_ws/IOL_new'
;

