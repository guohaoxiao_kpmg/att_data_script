add file udf2.py;
drop table kpmg.iol_sum;
drop table kpmg_ws.iol_sum_performance;
create table kpmg_ws.iol_sum_performance
as
select attuid,to_date(completed_date),event,uso,count(*)
from kpmg.iol
group by attuid,to_date(completed_date),event,uso;


create table kpmg.iol_sum
as
select attuid,substr(date,6,2),date,total_task,normalized_volume,set
from
(
select TRANSFORM(attuid, to_date(completed_date),count(*),sum(mul),collect_list(l.event)) USING 'python udf2.py' as (attuid string,date string,total_task int,normalized_volume double,set map<string,int>)
from kpmg.iol l
inner join kpmg_ws.IOL_weight w
on l.event=w.event and l.type=w.type
group by attuid, to_date(completed_date)
)x;
