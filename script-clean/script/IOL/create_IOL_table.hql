drop table kpmg_ws.IOL;
CREATE TABLE kpmg_ws.IOL
(
attuid string,
type string,
USO string,
event string,
completed_date timestamp
)
ROW format delimited fields terminated by ","
location '/sandbox/sandbox31/kpmg_ws/IOL'
;

drop table kpmg.IOL;
CREATE External TABLE kpmg.IOL
(
attuid string,
type string,
USO string,
event string,
completed_date timestamp
)
partitioned by (date  string)
ROW format delimited fields terminated by "|"
stored as orc
location '/sandbox/sandbox31/kpmg/standard/IOL'
;

