
drop table kpmg_ws.idesk_duplicated;

create table kpmg_ws.idesk_duplicated
as
select attuid,
  window_title,
  site,
  url,
  app_name,
  modify_date,
  active_window_end_timetamp,
  time_zone,
  machine_id,
  copy_app,
  copy_title,
  copy_ts,
  paste_app,
  paste_title,
  paste_ts,
  active_window_start_timetamp,
  split(element,"\t")[0] as duration_in_sec,
  split(element,"\t")[1] as create_date
from
(
select * from 
(
select 
  attuid,
  window_title,
  site,
  url,
  app_name,
  modify_date,
  active_window_end_timetamp,
  time_zone,
  machine_id,
  copy_app,
  copy_title,
  copy_ts,
  paste_app,
  paste_title,
  paste_ts,
  active_window_start_timetamp,
  collect_list(concat(duration_in_sec,"\t",create_date)) as duration_list
from kpmg.idesk_log_detail
group by 
attuid,
  window_title,
  site,
  url,
  app_name,
  modify_date,
  active_window_end_timetamp,
  time_zone,
  machine_id,
  copy_app,
  copy_title,
  copy_ts,
  paste_app,
  paste_title,
  paste_ts,
  active_window_start_timetamp
)y where size(duration_list) >1
)x
LATERAL VIEW explode(duration_list) x AS element;
