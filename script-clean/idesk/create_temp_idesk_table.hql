use kpmg_ws;
drop table temp_idesk_log;
CREATE TABLE `kpmg_ws.temp_idesk_log`(
  `attuid` string,
  `win_title` string,
  `proc_desc` string,
  `url_desc` string,
  `app_desc` string,
  `create_date` string,
  `modify_date` string,
  `end_ts` string,
  `local_tz` string,
  `mac_id` string,
  `copy_app` string,
  `copy_title` string,
  `copy_ts` string,
  `paste_app` string,
  `paste_title` string,
  `paste_ts` string,
  `start_ts` string,
  `duration_secs` double)
ROW FORMAT DELIMITED
  FIELDS TERMINATED BY '\001'
  LINES TERMINATED BY '\n'
STORED AS INPUTFORMAT
  'org.apache.hadoop.mapred.TextInputFormat'
OUTPUTFORMAT
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  'hdfs://COREITKGMTNC20PROD01/sandbox/sandbox31/kpmg_ws/tmp/idesk_log'
;
