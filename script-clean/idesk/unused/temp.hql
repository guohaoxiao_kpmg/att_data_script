use kpmg_ws;
set hive.exec.dynamic.partition.mode=nonstrict;
set hive.exec.dynamic.partition=true;
set hive.exec.max.dynamic.partitions.pernode=300;
set hive.exec.max.dynamic.partitions =10000000;
set hive.optimize.sort.dynamic.partition=true;

set fs.file.impl.disable.cache=false;
set fs.hdfs.impl.disable.cache=false;

drop table idesk_log_detail_20160216;

CREATE TABLE idesk_log_detail_20160216
(
  `attuid` string,
  `window_title` string,
  `site` string,
  `url` string,
  `app_name` string,
  `create_date` string,
  `modify_date` string,
  `active_window_end_timetamp` timestamp,
  `time_zone` string,
  `machine_id` string,
  `copy_app` string,
  `copy_title` string,
  `copy_ts` timestamp,
  `paste_app` string,
  `paste_title` string,
  `paste_ts` timestamp,
  `active_window_start_timetamp` timestamp,
  `duration_in_sec` double
)
partitioned by (active_window_start_date   string)
ROW format delimited fields terminated by "|"
stored as orc
;

insert into table idesk_log_detail_20160216
PARTITION(active_window_start_date)
select
  attuid,
  window_title,
  site,
  url,
  app_name,
  create_date,
  modify_date,
  active_window_end_timetamp,
  time_zone,
  machine_id,
  copy_app,
  copy_title,
  copy_ts,
  paste_app,
  paste_title,
  paste_ts,
  active_window_start_timetamp,
  duration_in_sec,
  active_window_start_date
from kpmg_ws.temp_idesk
where rank=1;

