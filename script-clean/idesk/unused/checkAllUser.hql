insert overwrite local directory '/home/gc096s/guohao/idesk_user'
row format delimited
fields terminated by ','
select a.*,u.VP_NAME,w.active
from 
(select attuid from kpmg.idesk_log_detail
group by attuid) a
left join kpmg.user_org u
on a.attuid=u.attuid
left join kpmg_ws.whitelist w
on a.attuid=w.attuid;
