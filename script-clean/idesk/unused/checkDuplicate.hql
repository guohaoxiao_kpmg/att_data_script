select active_window_start_date,count(*)
from
(
select
 attuid, window_title, site, url, app_name, create_date, modify_date, active_window_end_timetamp, time_zone, machine_id, copy_app, copy_title, copy_ts, paste_app, paste_title, paste_ts, active_window_start_timetamp, duration_in_sec, active_window_start_date, count(*) as c
from kpmg.idesk_log_detail
group by 
attuid, window_title, site, url, app_name, create_date, modify_date, active_window_end_timetamp, time_zone, machine_id, copy_app, copy_title, copy_ts, paste_app, paste_title, paste_ts, active_window_start_timetamp, duration_in_sec, active_window_start_date
)x 
where x.c > 1
group by  active_window_start_date
;
