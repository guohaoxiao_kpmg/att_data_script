set hive.exec.dynamic.partition.mode=nonstrict;
set hive.exec.dynamic.partition=true;
set hive.exec.max.dynamic.partitions.pernode=300;
set hive.exec.max.dynamic.partitions =10000000;
set hive.optimize.sort.dynamic.partition=true;

set fs.file.impl.disable.cache=false;
set fs.hdfs.impl.disable.cache=false;


insert into table kpmg.idesk_log_detail
PARTITION(active_window_start_date)
select 
  upper(`attuid`),
  upper(`window_title`),
  upper(`site`),
  upper(`url`),
  upper(`app_name`),
  upper(`create_date`),
  upper(`modify_date`),
  `active_window_end_timetamp`,
  upper(`time_zone`),
  upper(`machine_id`),
  upper(`copy_app`),
  upper(`copy_title`),
  `copy_ts`,
  upper(`paste_app`),
  upper(`paste_title`),
  `paste_ts`,
  `active_window_start_timetamp`,
  `duration_in_sec`,
  active_window_start_date
from kpmg_ws.idesk_log_detail_standardized;

