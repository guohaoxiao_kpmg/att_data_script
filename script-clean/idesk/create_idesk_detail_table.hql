set hive.exec.dynamic.partition.mode=nonstrict;
set hive.exec.dynamic.partition=true;
set hive.exec.max.dynamic.partitions.pernode=300;
set hive.exec.max.dynamic.partitions =10000000;
set hive.optimize.sort.dynamic.partition=true;

set fs.file.impl.disable.cache=false;
set fs.hdfs.impl.disable.cache=false;


--alter table kpmg.idesk_log_detail_standardized drop partition(active_window_start_date > 0);

--drop table kpmg.idesk_log_detail_standardized;

CREATE external TABLE kpmg.idesk_log_detail
(
  `attuid` string,
  `window_title` string,
  `site` string,
  `url` string,
  `app_name` string,
  `create_date` string,
  `modify_date` string,
  `active_window_end_timetamp` timestamp,
  `time_zone` string,
  `machine_id` string,
  `copy_app` string,
  `copy_title` string,
  `copy_ts` timestamp,
  `paste_app` string,
  `paste_title` string,
  `paste_ts` timestamp,
  `active_window_start_timetamp` timestamp,
  `duration_in_sec` double
)
partitioned by (active_window_start_date   string)
ROW format delimited fields terminated by "|"
stored as orc
location '/sandbox/sandbox31/kpmg/standard/idesk_log_detail'
;

