use kpmg_ws;
add file /home/gc096s/guohao/script/idat_sum/udf.py;

drop table temp_lookup_completed_lssd;
create table  temp_lookup_completed_lssd as
select ncon,USO_Originator_HRID as uid,"history" as name,row
from
(
select x.ncon,
--"" as USO_Originator_HRID
y.USO_Originator_HRID

,row_number() over(partition by x.ncon order by y.aadate desc) as row
from
(
select * from kpmg_ws.idat_lssd_scorecard_details_uso_mpr_uso_completed_dedup
)x
inner join
(
select ncon,USO_Originator_HRID,aadate from kpmg_ws.idat_lssd_scorecard_details_uso_mpr_uso_completed_dedup
where USO_Originator_HRID is not null
group by  ncon,USO_Originator_HRID,aadate
)y
on x.ncon=y.ncon
)z

union all

select ncon,uid,"webphone" as name,row
from
(
select  x.ncon,u.handle as uid,row_number() over(partition by x.ncon order by u.handle) as row
from
(
select x.ncon,regexp_replace(split(x.ncon," ")[size(split(x.ncon," "))-1],"-","") as phone
from
(
select * from kpmg_ws.idat_lssd_scorecard_details_uso_mpr_uso_completed_dedup
)x
left join
(
select ncon,USO_Originator_HRID from kpmg_ws.idat_lssd_scorecard_details_uso_mpr_uso_completed_dedup
where USO_Originator_HRID is not null
group by ncon,USO_Originator_HRID
)y
on x.ncon=y.ncon
where y.ncon is null
group by x.ncon
)x
left join  webphone_orig u
on split(u.telephone_number," ")[size(split(u.telephone_number," "))-1]=x.phone
where
split(u.telephone_number," ")[size(split(u.telephone_number," "))-1] is not null and
instr(x.ncon,u.first_name)>0 and instr(x.ncon,u.last_name) >0 and instr(x.ncon,u.first_name) is not null and instr(x.ncon,u.last_name)  is not null
)result;

drop table temp_lookup_issued_lssd;
create table  temp_lookup_issued_lssd as
select ncon,USO_Originator_HRID as uid,"history" as name,row
from
(
select x.ncon,
--"" as USO_Originator_HRID
y.USO_Originator_HRID

,row_number() over(partition by x.ncon order by y.aadate desc) as row
from
(
select * from kpmg_ws.idat_lssd_scorecard_details_uso_mpr_uso_issued_dedup
)x
inner join
(
select ncon,USO_Originator_HRID,aadate from kpmg_ws.idat_lssd_scorecard_details_uso_mpr_uso_issued_dedup
where USO_Originator_HRID is not null
group by  ncon,USO_Originator_HRID,aadate
)y
on x.ncon=y.ncon
)z

union all

select ncon,uid,"webphone" as name,row
from
(
select  x.ncon,u.handle as uid,row_number() over(partition by x.ncon order by u.handle) as row
from
(
select x.ncon,regexp_replace(split(x.ncon," ")[size(split(x.ncon," "))-1],"-","") as phone
from
(
select * from kpmg_ws.idat_lssd_scorecard_details_uso_mpr_uso_issued_dedup
)x
left join
(
select ncon,USO_Originator_HRID from kpmg_ws.idat_lssd_scorecard_details_uso_mpr_uso_issued_dedup
where USO_Originator_HRID is not null
group by ncon,USO_Originator_HRID
)y
on x.ncon=y.ncon
where y.ncon is null
group by x.ncon
)x
left join  webphone_orig u
on split(u.telephone_number," ")[size(split(u.telephone_number," "))-1]=x.phone
where
split(u.telephone_number," ")[size(split(u.telephone_number," "))-1] is not null and
instr(x.ncon,u.first_name)>0 and instr(x.ncon,u.last_name) >0 and instr(x.ncon,u.first_name) is not null and instr(x.ncon,u.last_name)  is not null
)result;



drop table temp_lookup_completed;
create table  temp_lookup_completed as
select ncon,USO_Originator_HRID as uid,"history" as name,row
from
(
select x.ncon,y.USO_Originator_HRID,row_number() over(partition by x.ncon order by y.aadate desc) as row
from
(
select * from kpmg_ws.idat_hssd_scorecard_ethernet_linedata_mpr_uso_completed_dedup
)x
inner join
(
select ncon,USO_Originator_HRID,aadate from kpmg_ws.idat_hssd_scorecard_ethernet_linedata_mpr_uso_completed_dedup
where USO_Originator_HRID is not null
group by  ncon,USO_Originator_HRID,aadate
)y
on x.ncon=y.ncon
)z

union all

select ncon,uid,"webphone" as name,row
from
(
select  x.ncon,u.handle as uid,row_number() over(partition by x.ncon order by u.handle) as row
from
(
select x.ncon,regexp_replace(split(x.ncon," ")[size(split(x.ncon," "))-1],"-","") as phone
from
(
select * from kpmg_ws.idat_hssd_scorecard_ethernet_linedata_mpr_uso_completed_dedup
)x
left join
(
select ncon,USO_Originator_HRID from kpmg_ws.idat_hssd_scorecard_ethernet_linedata_mpr_uso_completed_dedup
where USO_Originator_HRID is not null
group by ncon,USO_Originator_HRID
)y
on x.ncon=y.ncon
where y.ncon is null
group by x.ncon
)x
left join  webphone_orig u
on split(u.telephone_number," ")[size(split(u.telephone_number," "))-1]=x.phone
where
split(u.telephone_number," ")[size(split(u.telephone_number," "))-1] is not null and
instr(x.ncon,u.first_name)>0 and instr(x.ncon,u.last_name) >0 and instr(x.ncon,u.first_name) is not null and instr(x.ncon,u.last_name)  is not null
)result;







drop table temp_lookup_issued;
create table  temp_lookup_issued as
select ncon,USO_Originator_HRID as uid,"history" as name,row
from
(
select x.ncon,y.USO_Originator_HRID,row_number() over(partition by x.ncon order by y.aadate desc) as row
from
(
select * from kpmg_ws.idat_hssd_scorecard_ethernet_linedata_mpr_uso_issued_dedup
)x
inner join
(
select ncon,USO_Originator_HRID,aadate from kpmg_ws.idat_hssd_scorecard_ethernet_linedata_mpr_uso_issued_dedup
where USO_Originator_HRID is not null
group by  ncon,USO_Originator_HRID,aadate
)y
on x.ncon=y.ncon
)z

union all

select ncon,uid,"webphone" as name,row
from
(
select  x.ncon,u.handle as uid,row_number() over(partition by x.ncon order by u.handle) as row
from
(
select x.ncon,regexp_replace(split(x.ncon," ")[size(split(x.ncon," "))-1],"-","") as phone
from
(
select * from kpmg_ws.idat_hssd_scorecard_ethernet_linedata_mpr_uso_issued_dedup
)x
left join
(
select ncon,USO_Originator_HRID from kpmg_ws.idat_hssd_scorecard_ethernet_linedata_mpr_uso_issued_dedup
where USO_Originator_HRID is not null
group by ncon,USO_Originator_HRID
)y
on x.ncon=y.ncon
where y.ncon is null
group by x.ncon
)x
left join  webphone_orig u
on split(u.telephone_number," ")[size(split(u.telephone_number," "))-1]=x.phone
where
split(u.telephone_number," ")[size(split(u.telephone_number," "))-1] is not null and
instr(x.ncon,u.first_name)>0 and instr(x.ncon,u.last_name) >0 and instr(x.ncon,u.first_name) is not null and instr(x.ncon,u.last_name)  is not null
)result;






drop table Global_Ordering_Prod_Volume;
create table Global_Ordering_Prod_Volume
as
select i.USO as uso, i.circuit_id,date_add('1899-12-30',cast(aadate as int)) as order_creation_date, null as Order_Completion_Date, main_order_type,product,i.ncon as NCON_USO_Issued,null as ncon_uso_completed,uid as USO_Originator_HRID_issued, null  as USO_Originator_HRID_completed,temp.uid  as order_creator,temp.uid  as order_manager, speed 
--,OCO1_Jep
from IDAT_HSSD_Scorecard_Ethernet_Linedata_MPR_USO_issued_dedup i
left join
(select * from temp_lookup_issued where row=1) temp
on i.ncon=temp.ncon
where product = "EaAVPN" and temp.uid is not null
union all 
select i.USO as uso,i.circuit_id, null as order_creation_date,  date_add('1899-12-30',cast(DD_Comp as int)) as Order_Completion_Date, main_order_type,product,null as NCON_USO_Issued,i.ncon as ncon_uso_completed,null  as USO_Originator_HRID_issued, uid  as USO_Originator_HRID_completed,uid  as order_creator,uid  as order_manager, speed
--,OCO1_Jep
from IDAT_HSSD_Scorecard_Ethernet_Linedata_MPR_USO_completed_dedup i
left join 
(select * from temp_lookup_completed where row=1) temp
on i.ncon=temp.ncon 
where product = "EaAVPN" and temp.uid is not null

union all
select i.USO as uso,i.circuit_id, null as order_creation_date,  date_add('1899-12-30',cast(DD_Comp as int)) as Order_Completion_Date, main_order_type,product,null as NCON_USO_Issued,i.ncon as ncon_uso_completed,null  as USO_Originator_HRID_issued, uid  as USO_Originator_HRID_completed,uid  as order_creator,uid  as order_manager, speed
--, "" as OCO1_Jep
from  kpmg_ws.idat_lssd_scorecard_details_uso_mpr_uso_completed_dedup i
left join
(select * from temp_lookup_completed_lssd where row=1) temp
on i.ncon=temp.ncon
where product in ("AVPN","FLEX","MIS") and temp.uid is not null

union all
select i.USO as uso, i.circuit_id,date_add('1899-12-30',cast(aadate as int)) as order_creation_date, null as Order_Completion_Date, main_order_type,product,i.ncon as NCON_USO_Issued,null as ncon_uso_completed,uid as USO_Originator_HRID_issued, null  as USO_Originator_HRID_completed,temp.uid  as order_creator,temp.uid  as order_manager, speed
--,"" as OCO1_Jep
from kpmg_ws.idat_lssd_scorecard_details_uso_mpr_uso_issued_dedup i
left join
(select * from temp_lookup_issued_lssd where row=1) temp
on i.ncon=temp.ncon
where product in ("AVPN","FLEX","MIS") and temp.uid is not null

;
