use kpmg_ws;

drop table Global_Ordering_Prod_Volume;
create table Global_Ordering_Prod_Volume
as
select i.USO as uso, i.circuit_id,date_add('1899-12-30',cast(aadate as int)) as order_creation_date, null as Order_Completion_Date, main_order_type,product,ncon as NCON_USO_Issued,null as ncon_uso_completed,uid as USO_Originator_HRID_issued, null  as USO_Originator_HRID_completed,temp.uid  as order_creator,temp.uid  as order_manager
from IDAT_HSSD_Scorecard_Ethernet_Linedata_MPR_USO_issued i
left join temp_lookup_issued temp
on i.uso=temp.uso and i.circuit_id=temp.circuit_id
where product = "EaAVPN"
union all
select i.USO as uso,i.circuit_id, null as order_creation_date,  date_add('1899-12-30',cast(DD_Comp as int)) as Order_Completion_Date, main_order_type,product,null as NCON_USO_Issued,ncon as ncon_uso_completed,null  as USO_Originator_HRID_issued, uid  as USO_Originator_HRID_completed,uid  as order_creator,uid  as order_manager
from IDAT_HSSD_Scorecard_Ethernet_Linedata_MPR_USO_completed i
left join temp_lookup_com temp
on i.uso=temp.uso and i.circuit_id=temp.circuit_id
where product = "EaAVPN"
;


drop table Global_Ordering_Prod_Volume_no_uid;
create table Global_Ordering_Prod_Volume_no_uid
as
select i.ncon,i.USO as uso, i.circuit_id,date_add('1899-12-30',cast(aadate as int)) as order_creation_date, null as Order_Completion_Date, main_order_type,product,ncon as NCON_USO_Issued,null as ncon_uso_completed,uid as USO_Originator_HRID_issued, null  as USO_Originator_HRID_completed,temp.uid  as order_creator,temp.uid  as order_manager
from IDAT_HSSD_Scorecard_Ethernet_Linedata_MPR_USO_issued i
left join temp_lookup_issued temp
on i.uso=temp.uso and i.circuit_id=temp.circuit_id
where product = "EaAVPN" and temp.uid is null
union all
select i.ncon,i.USO as uso,i.circuit_id, null as order_creation_date,  date_add('1899-12-30',cast(DD_Comp as int)) as Order_Completion_Date, main_order_type,product,null as NCON_USO_Issued,ncon as ncon_uso_completed,null  as USO_Originator_HRID_issued, uid  as USO_Originator_HRID_completed,uid  as order_creator,uid  as order_manager
from IDAT_HSSD_Scorecard_Ethernet_Linedata_MPR_USO_completed i
left join temp_lookup_com temp
on i.uso=temp.uso and i.circuit_id=temp.circuit_id
where product = "EaAVPN" and temp.uid is null
;

