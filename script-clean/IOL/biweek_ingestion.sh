#! /bin/bash
# * ----------------------------------------------------------------------------------
# * Project:         ATT
# *   Console script
# *
# * Log:
# *
# *    2015/11/30    Guohao xiao    copy IOL data from local directory and load it into
# *                                 a partitioned table
# * ----------------------------------------------------------------------------------

hadoop dfs -mkdir /sandbox/sandbox31/backup/IOL_backup/
hadoop dfs -mv /sandbox/sandbox31/kpmg_ws/IOL/* /sandbox/sandbox31/backup/IOL_backup/
hadoop dfs -rmr /sandbox/sandbox31/kpmg_ws/IOL/*
hadoop dfs -copyFromLocal /opt/data/share05/sandbox/sandbox31/IOL/*.csv /sandbox/sandbox31/kpmg_ws/IOL/

hive -f /home/gc096s/guohao/script/IOL/insert_IOL.hql

hadoop dfs -chmod -R 755 /sandbox/sandbox31/kpmg/standard/IOL*


