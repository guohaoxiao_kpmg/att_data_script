set hive.exec.dynamic.partition.mode=nonstrict;
set hive.exec.dynamic.partition=true;
set hive.exec.max.dynamic.partitions.pernode=300;
set hive.exec.max.dynamic.partitions =10000000;
set hive.optimize.sort.dynamic.partition=true;

set fs.file.impl.disable.cache=false;
set fs.hdfs.impl.disable.cache=false;


insert into table kpmg.IOL
PARTITION(date)
select 
upper(attuid),
upper(type),
upper(uso),
upper(event),
completed_date,
to_date(completed_date) as date
  from kpmg_ws.IOL;
