drop table kpmg_ws.elink;
CREATE TABLE kpmg_ws.elink
(
attuid string,
calendar_day string,
calendar_month string,
actual_time double,
org_unit string,
time_type string,
time_type_descr string
)
ROW format delimited fields terminated by "|"
location '/sandbox/sandbox31/kpmg_ws/elink'
;

--drop table kpmg.elink;
drop table kpmg.elink_standardized;
CREATE TABLE kpmg.elink_standardized
(
attuid string,
calendar_day string,
calendar_month string,
actual_time double,
org_unit string,
time_type string,
time_type_descr string
)
partitioned by (start_month   string)
ROW format delimited fields terminated by "|"
stored as orc
location '/sandbox/sandbox31/kpmg/standard/elink_standardized'
;

