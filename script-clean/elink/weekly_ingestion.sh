#! /bin/bash
# * ----------------------------------------------------------------------------------
# * Project:         ATT
# *   Console script
# *
# * Log:
# *
# *    2015/11/30    Guohao xiao     Sqoop Idesk data into hive table and partition it
# * ----------------------------------------------------------------------------------
echo "LinkNumber1+"|kinit

start=$1

end=$2

echo $start
echo $end



hadoop dfs -rmr /sandbox/sandbox31/kpmg_ws/elink_feed

sqoop import \
--connect "jdbc:oracle:thin:@bwp.ffdc.sbc.com:1527:BWP" \
--username m97363 \
--password LmcqwgkDzz8ys^  \
--as-textfile \
--target-dir '/sandbox/sandbox31/kpmg_ws/elink_feed' \
-m 20 \
--split-by ATTUID \
--query 'select ATTUID,CALENDAR_DAY,CAL_YEAR_MONTH,ACTUAL_TIME,ORG_UNIT,TIME_TYPE,TIME_TYPE_DESCR from SAPR3.Z_ZPT_1178_2_V where $CONDITIONS' \
--null-string '' \
--null-non-string '' \
--fields-terminated-by '|' \
--hive-drop-import-delims \
--connection-manager 'org.apache.sqoop.manager.OracleManager';


#hadoop dfs -mkdir /sandbox/sandbox31/backup/elink_backup/$start
#hadoop dfs -mv /sandbox/sandbox31/kpmg_ws/elink/* /sandbox/sandbox31/backup/elink_backup/$start



hadoop dfs -rmr /sandbox/sandbox31/kpmg_ws/elink/*
hadoop dfs -mv /sandbox/sandbox31/kpmg_ws/elink_feed/part*  /sandbox/sandbox31/kpmg_ws/elink/
hadoop dfs -rmr /sandbox/sandbox31/kpmg_ws/elink_feed/*
date=$(hive -e "select min(calendar_day) from  kpmg_ws.elink")
echo $date


hive -f /home/gc096s/guohao/script/elink/insert_elink.hql -hiveconf date=$date

hive  -f  /home/gc096s/guohao/script/elink/create_elink_elink_sum.hql
hadoop dfs -chmod -R 755 /sandbox/sandbox31/kpmg/standard/elink*


output=$(hive -e "select calendar_day as date1,count(*) from kpmg.elink_standard where calendar_day >$start and  calendar_day<=$end  group by calendar_day order by date1;")

MailList="guohaoxiao@kpmg.com,smuthersbaugh@kpmg.com,tbaweja@kpmg.com"
messageStr=`printf '%s \n' ${output}`
echo -e ${messageStr}
echo -e ${messageStr} | mailx -s "elink new ingestion distribution" ${MailList}


#base=$(hive -e "select max(date) as date from kpmg.base;")
base=$(hive -e "select max(date1) as date1 from kpmg_ws.elink_max_date;")
max=$(hive -e "select max(calendar_day) as date1 from kpmg.elink_standard;")
hive -e "
use kpmg_ws;
drop table elink_max_date;

create table elink_max_date as 
select max(calendar_day) as date1 from kpmg.elink_standard;
"

if [[ $base -eq $max ]]
then
  messageStr="elink is not updated"
  hive -e "
use kpmg_ws;
drop table elink_status;
create table  elink_status
(
status string
);
insert into elink_status values('failed')
"

else
message="elink date updated to : $max last updated to: $base"
messageStr=`printf ' %s \n' ${message}`
  hive -e "
use kpmg_ws;
drop table elink_status;
create table  elink_status
(
status string
);
insert into elink_status values('ok')
"

fi
echo -e ${messageStr}
echo -e ${messageStr} | mailx -s "elink date check" ${MailList}



