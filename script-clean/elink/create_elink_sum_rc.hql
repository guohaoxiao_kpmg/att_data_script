use kpmg_ws;
drop table elink_sum_rc;

create table elink_sum_rc as
select * 
from
(
select attuid,JOB_TITLE_NAME,costcenter,calendar_day,time_type,sum(actual_time) as total_time, if(costcenter in ('6NUEACD30', '6NUEACD31', '6NUEACD33', '6NUEACD34'),'LNS',if(costcenter in ('ACUEACD50', 'EYUEACD50', 'EYUEACD54', 'QYUF2CF00', 'QYUF2CFC0', 'S0UF2CA00', 'S0UF2CAA0'),'Austin','Other')) as Lobs
from
(
select e.attuid,JOB_TITLE_NAME,costcenter,calendar_day,actual_time,time_type
from kpmg.elink_standard e 
left join kpmg.web_phone o 
on upper(e.attuid) = upper(o.attuid)
)x
group by attuid, calendar_day,costcenter,JOB_TITLE_NAME,time_type
)y
where Lobs!='Other';
