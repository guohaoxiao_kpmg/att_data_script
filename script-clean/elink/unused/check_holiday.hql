use kpmg_ws;
select count(*) ,sum(if(x.absence_time=y.absence_time+8,1,0))
from 
(
select * from kpmg.elink_sum e
inner join holiday h
on e.calendar_day=regexp_replace(h.date,"-","")
)x
inner join 
(
select * from kpmg_ws.elink_sum_old e
inner join holiday h
on e.calendar_day=regexp_replace(h.date,"-","")
)y
on x.attuid=y.attuid and x.calendar_day=y.calendar_day
;

