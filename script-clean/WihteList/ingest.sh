#! /bin/bash
# * ----------------------------------------------------------------------------------
# * Project:         ATT
# *   Console script
# *
# * Log:
# *   
# *    2015/11/30    Guohao xiao     Sqoop Idesk data into hive table and partition it
# * ----------------------------------------------------------------------------------
echo "LinkNumber1+"|kinit


hadoop dfs -rmr /sandbox/sandbox31/kpmg_ws/whitelist


sqoop  import \
-libjars "/usr/hdp/current/sqoop-client/lib/sqljdbc4.jar" \
--connect "jdbc:sqlserver://cldprd0sql01682.itservices.sbc.com\PD_ECCRIX01" \
--username hadoop_reader \
--password 'w!tmkLi83aB.' \
--as-textfile \
--target-dir '/sandbox/sandbox31/kpmg_ws/whitelist' \
-m 20 \
--split-by attuid \
--query 'select * from FIDO.dbo.sdpa_whitelist_view where $CONDITIONS' \
--null-string '\\N' \
--null-non-string '\\N' \
--fields-terminated-by '|' \
--hive-drop-import-delims ;
#--connection-manager 'org.apache.sqoop.manager.SQLServerManager';

