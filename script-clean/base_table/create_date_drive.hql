drop table kpmg_ws.date_drive;
CREATE TABLE kpmg_ws.date_drive
(
formatted_date string,
month int,
year int,
quarter int,
week int,
dayofweek int,
dayofmonth int,
dayofquarter int,
dayofyear int,
week_name String
)
ROW format delimited fields terminated by "|"
location '/sandbox/sandbox31/kpmg_ws/date_drive'
;

