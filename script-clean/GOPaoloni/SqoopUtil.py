import os

"""

sqoop  import \
-libjars "/usr/hdp/current/sqoop-client/lib/sqljdbc4.jar" \
--connect "jdbc:sqlserver://cldprd0sql01682.itservices.sbc.com\PD_ECCRIX01" \
--username hadoop_reader \
--password 'w!tmkLi83aB.' \
--as-textfile \
--target-dir '/sandbox/sandbox31/kpmg_ws/tmp/idesk_log' \
-m 20 \
--split-by attuid \
--query 'select attuid,win_title,proc_desc,url_desc,app_desc,create_date,modify_date,end_ts,local_tz,mac_id,copy_app,copy_title,copy_ts,paste_app,paste_title,paste_ts,start_ts,duration_secs from FIDO.dbo.sdpa_dm_app_detail_usage_view where create_date > convert(datetime,convert(char(8), '$start'), 111) and create_date < convert(datetime, convert(char(8), '$end'), 111) and $CONDITIONS' \
--null-string '\\N' \
--null-non-string '\\N' \
--fields-terminated-by '\001' \
--hive-drop-import-delims ;


"""
class SqoopUtil:
    def __init__(self):
        print 'init'

    def configure(self,libjars,driver,connect,username,password,mapper):
        self.libjars=libjars
        self.driver=driver
        self.connect=connect
        self.username=username
        self.password=password
        self.mapper=mapper

    """
    sqoop  eval \
    -libjars "/usr/hdp/2.2.8.0-3150/sqoop/lib/jtds-1.3.1.jar"                                                                        \
    --driver          "net.sourceforge.jtds.jdbc.Driver"                                                                                          \
    --connect         "jdbc:jtds:sqlserver://MOSTLS1ABSPMD06.itservices.sbc.com;instance=PD_MANDMX01;databaseName=views_user;domain=itservices"   \
    --username        jl7392                                                                                                                      \
    --password        '@tlanta1050'                                                                                                               \
    --query 'select top 1 * from  views_user..vw_IDAT_LSSD_Linedata_OrdersCompleted_YTD' \


    sqoop export -libjars "/usr/hdp/2.2.8.0-3150/sqoop/lib/sqljdbc4.jar" \
    -Dsqoop.export.records.per.statement=1000 \
    -Dsqoop.export.statements.per.transaction=1000 \
    -m 4 \
    --driver "com.microsoft.sqlserver.jdbc.SQLServerDriver" \
    --connect "jdbc:sqlserver://Clddev0sql01683.itservices.sbc.com\DD_ECCRIX01" \
    --username iDESK-REPORTUSER \
    --password 'du3!Tax3s' \
    --table "idesktemp.dbo.gowalters" \
    --export-dir "hdfs://COREITKGMTNC20PROD01/sandbox/sandbox31/kpmg_ws/gowalters" \
    --fields-terminated-by "\\001"                                                \
    --lines-terminated-by "\\n"                                                   \
    --input-null-string '\\N'                                                           \
    --input-null-non-string '\\N'                                                       \
    --batch


    """
    def buildStatement_int(self,prefix,value):
         return prefix+" " +str(value)+" \\\n"

    def buildStatement(self,prefix,value):
        return prefix+" \'" +value+"\' \\\n"
    def buildStatement_double(self,prefix,value):
         return prefix+" \"" +value+"\" \\\n"

    def query(self,query):
        queryStatement="sqoop  eval \\\n"
        queryStatement=self.buildStatement(queryStatement+"-libjars",self.libjars)
        #queryStatement=self.buildStatement_int(queryStatement+"-m",mapper)
        queryStatement=self.buildStatement(queryStatement+"--driver",self.driver)
        queryStatement=self.buildStatement(queryStatement+"--connect",self.connect)
        queryStatement=self.buildStatement(queryStatement+"--username",self.username)
        queryStatement=self.buildStatement(queryStatement+"--password",self.password)
        queryStatement=self.buildStatement(queryStatement+"--query",query)        
        print queryStatement[0:-2]
        result=os.popen(queryStatement).read()
        print "result:"
        print result

    def export(self,table,export_dir,fields_teminator,lines_teminator,null_string,null_non_string,mapper):
        queryStatement=""
        queryStatement=self.buildStatement(queryStatement+"sqoop  export -libjars",self.libjars)
        queryStatement=self.buildStatement_int(queryStatement+"-m",mapper)
        queryStatement=self.buildStatement(queryStatement+"--driver",self.driver)
        queryStatement=self.buildStatement(queryStatement+"--connect",self.connect)
        queryStatement=self.buildStatement(queryStatement+"--username",self.username)
        queryStatement=self.buildStatement(queryStatement+"--password",self.password)
        queryStatement=self.buildStatement(queryStatement+"--table",table)
        queryStatement=self.buildStatement(queryStatement+"--export-dir",export_dir)
        queryStatement=self.buildStatement_double(queryStatement+"--fields-terminated-by",fields_teminator)
        queryStatement=self.buildStatement_double(queryStatement+"--lines-terminated-by",lines_teminator)
        queryStatement=self.buildStatement(queryStatement+"--input-null-string",null_string)
        queryStatement=self.buildStatement(queryStatement+"--input-null-non-string",null_non_string)
        queryStatement=queryStatement+"--batch"
        print queryStatement
        result=os.popen(queryStatement).read()
        print "result:"
        print result

    def importToHive(self,target_dir,split_by,mapper,query,null_string,null_non_string,fields_teminator):
        queryStatement="hadoop dfs -rmr "+target_dir+"\n"
        queryStatement=self.buildStatement(queryStatement+"sqoop  import -libjars",self.libjars)
        queryStatement=self.buildStatement_int(queryStatement+"-m",mapper)
        queryStatement=self.buildStatement(queryStatement+"--driver",self.driver)
        queryStatement=self.buildStatement(queryStatement+"--connect",self.connect)
        queryStatement=self.buildStatement(queryStatement+"--username",self.username)
        queryStatement=self.buildStatement(queryStatement+"--password",self.password)
        queryStatement=queryStatement+"--as-textfile \\\n"
        queryStatement=self.buildStatement(queryStatement+"--query",query)
        queryStatement=self.buildStatement(queryStatement+"--target-dir",target_dir)
        queryStatement=self.buildStatement(queryStatement+"--split-by",split_by)
        queryStatement=self.buildStatement(queryStatement+"--fields-terminated-by",fields_teminator)
        queryStatement=self.buildStatement(queryStatement+"--null-string",null_string)
        queryStatement=self.buildStatement(queryStatement+"--null-non-string",null_non_string)
        queryStatement=queryStatement+"--hive-drop-import-delims"
        print queryStatement
        result=os.popen(queryStatement).read()
        print "result:"
        print result

    def getTableDescription(self,db,table):
        queryStatement=""
        queryStatement=self.buildStatement(queryStatement+"sqoop eval -libjars",self.libjars)
        queryStatement=self.buildStatement(queryStatement+"--driver",self.driver)
        queryStatement=self.buildStatement(queryStatement+"--connect",self.connect)
        queryStatement=self.buildStatement(queryStatement+"--username",self.username)
        queryStatement=self.buildStatement(queryStatement+"--password",self.password)
        queryStatement=self.buildStatement_double(queryStatement+"--query","use "+db+";select * from information_schema.columns where table_name=\'"+table+"\' order by ordinal_position")
        queryStatement=queryStatement[0:-2]
        print  queryStatement
        result=os.popen(queryStatement).read()
        print "result:"
        set=[]
        for line in result.split("\n"):
            
            if "|" in line:
              set.append(line.split("|")[4].strip()+"\t"+line.split("|")[8].strip())  
        return set
        
        
    
    def createTable(self,table,colDic):
        cmd="create table "+table+"(\n"
        for key in colDic:
            cmd=cmd+key+" "+colDic[key]+","
        cmd=cmd[0:-1]+")"
        self.query(cmd)
    def interactiveMode(self):
        cmd=""
        while cmd!="quit" and cmd!="q":
            cmd = str(raw_input("input your query, type q to quit:\n"))
            if cmd!="q":
                self.query(cmd)   
       




#test=SqoopUtil()
#test.configure("/usr/hdp/2.2.8.0-3150/sqoop/lib/jtds-1.3.1.jar", "net.sourceforge.jtds.jdbc.Driver",'jdbc:jtds:sqlserver://MOSTLS1ABSPMD06.itservices.sbc.com;instance=PD_MANDMX01;databaseName=views_user;domain=itservices','jl7392','@tlanta1050',6 )
#test.configure("/usr/hdp/2.2.8.0-3150/sqoop/lib/sqljdbc4.jar","com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://Clddev0sql01683.itservices.sbc.com\DD_ECCRIX01","iDESK-REPORTUSER","du3!Tax3s",6)
#colDic={"test":"char","test2":"int"}
#test.createTable("IDesktemp.dbo.test2",colDic)
#test.query("select top 1 * from IDesktemp.dbo.webphone") 
        

      












    

    
