#! /bin/bash
# * ----------------------------------------------------------------------------------
# * Project:         ATT
# *   Console script
# *
# * Log:
# *   
# *    2015/11/30    Guohao xiao     Sqoop Idesk data into hive table and partition it
# * ----------------------------------------------------------------------------------
echo "LinkNumber1+"|kinit

echo $start
echo $end

hadoop dfs -rmr /sandbox/sandbox31/kpmg_ws/client_classification


sqoop  import \
-libjars "/usr/hdp/current/sqoop-client/lib/sqljdbc4.jar" \
--connect "jdbc:sqlserver://cldprd0sql01682.itservices.sbc.com\PD_ECCRIX01" \
--username hadoop_reader \
--password 'w!tmkLi83aB.' \
--as-textfile \
--target-dir '/sandbox/sandbox31/kpmg_ws/client_classification' \
-m 20 \
--split-by modify_date \
--query 'select app_desc,l1cat_desc,l2cat_desc,modify_date from FIDO.dbo.sdpa_app_category_view where $CONDITIONS' \
--null-string '\\N' \
--null-non-string '\\N' \
--fields-terminated-by '|' \
--hive-drop-import-delims ;
#--connection-manager 'org.apache.sqoop.manager.SQLServerManager';
echo "weekly client_classification downloads count:"

output=$(hive -e "select to_date(updateddate) as date1,count(*) from kpmg_ws.client_classification group by to_date(updateddate) order by date1;")

MailList="guohaoxiao@kpmg.com,smuthersbaugh@kpmg.com"
messageStr=`printf '%s \n' ${output}` 
echo -e ${messageStr} 
echo -e ${messageStr} | mailx -s "client classification new ingestion distribution" ${MailList}

