drop table kpmg_ws.client_classification;
CREATE TABLE kpmg_ws.client_classification
(
Application String,
L1_Category String,
L2_Category String,
UpdatedDate String

)
ROW format delimited fields terminated by "|"
location '/sandbox/sandbox31/kpmg_ws/client_classification'
;


