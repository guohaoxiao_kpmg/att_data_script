CREATE external TABLE kpmg.raw_idesk_log_new
(
  svc_id                 int,    
  user_id                      string,
  machine_id                   string,
  window_title                 string,
  proc_desc                    string,
  url_desc                     string,
  app_desc                     string,
  active_window_start_timetamp timestamp,
  active_window_end_timetamp   timestamp,
  duration_in_sec              double, 
  time_zone                    string,
  report_run_timestamp         timestamp,
  domain                       string,
  processor                    string,
  ram                          string,
  os_version                   string,
  sys_type                     string,
  copy_app                     string,
  copy_title                   string,
  copy_ts                      string,
  paste_app                    string,
  paste_title                  string,
  paste_ts                     string
)
ROW format delimited fields terminated by "|"
location '/user/gc096s/raw_idesk_processed_new'
;
