CREATE TABLE kpmg_ws.IOL_size2
(
attuid string,
type string,
USO string,
event string,
completed_date timestamp
)
partitioned by (date  string)
ROW format delimited fields terminated by "|"
location '/sandbox/sandbox31/kpmg_ws/IOL_size2'
;

