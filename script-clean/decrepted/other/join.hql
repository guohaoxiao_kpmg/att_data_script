
use kpmg_ws;
insert overwrite local directory '/home/gc096s/guohao/data'
row format delimited
fields terminated by '|'
select ATTUID,Type,data.USO,Event,Completed,data.Circuit_ID,USO_Circuit_ID,Main_Order_Type,Product,Project_Text,Speed,Ethernet_Switched_Ind,Single_Control_Plane_Ind,Pon_Newest_In_Region_Ind,spreadsheet

from data
left join 
(
select USO,Circuit_ID,Main_Order_Type,Product,Project_Text,Speed,Ethernet_Switched_Ind,Single_Control_Plane_Ind,Pon_Newest_In_Region_Ind,spreadsheet
from
(
select USO,Circuit_ID,Main_Order_Type,Product,Project_Text,Speed,Ethernet_Switched_Ind,Single_Control_Plane_Ind,Pon_Newest_In_Region_Ind,"compeleted" as spreadsheet
from idat_hssd_scorecard_ethernet_linedata_mpr_uso_completed
union all
select USO,Circuit_ID,Main_Order_Type,Product,Project_Text,Speed,Ethernet_Switched_Ind,Single_Control_Plane_Ind,Pon_Newest_In_Region_Ind,"cancelled" as spreadsheet
from idat_hssd_scorecard_ethernet_linedata_mpr_uso_cancelled
union all
select USO,Circuit_ID,Main_Order_Type,Product,Project_Text,Speed,Ethernet_Switched_Ind,Single_Control_Plane_Ind,Pon_Newest_In_Region_Ind,"issued" as spreadsheet
from idat_hssd_scorecard_ethernet_linedata_mpr_uso_issued
union all
select USO,Circuit_ID,Main_Order_Type,Product,Project_Text,Speed,Ethernet_Switched_Ind,Single_Control_Plane_Ind,Pon_Newest_In_Region_Ind,"wip" as spreadsheet
from idat_hssd_scorecard_ethernet_linedata_mpr_uso_wip
)x
)y
on data.USO_Circuit_ID=concat(y.USO,"-",y.Circuit_ID)
group by  ATTUID,Type,data.USO,Event,Completed,data.Circuit_ID,USO_Circuit_ID,Main_Order_Type,Product,Project_Text,Speed,Ethernet_Switched_Ind,Single_Control_Plane_Ind,Pon_Newest_In_Region_Ind,spreadsheet;


