set hive.exec.dynamic.partition.mode=nonstrict;
set hive.exec.dynamic.partition=true;
set hive.exec.max.dynamic.partitions.pernode=300;
set hive.exec.max.dynamic.partitions =10000000;
set hive.optimize.sort.dynamic.partition=true;

set fs.file.impl.disable.cache=false;
set fs.hdfs.impl.disable.cache=false;


--alter table kpmg.idesk_log_detail_standardized drop partition(active_window_start_date > 0);

--drop table kpmg.idesk_log_detail_standardized;

CREATE external TABLE kpmg_ws.data
(
ATTUID	String,
Type   String,
USO	String,
Event	String,
Completed	String,
Circuit_ID	String,
USO_Circuit_ID	String
)
ROW format delimited fields terminated by "|"
;

