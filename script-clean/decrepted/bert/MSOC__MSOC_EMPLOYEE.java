// ORM class for table 'MSOC..MSOC_EMPLOYEE'
// WARNING: This class is AUTO-GENERATED. Modify at your own risk.
//
// Debug information:
// Generated date: Wed Mar 09 21:07:33 EST 2016
// For connector: org.apache.sqoop.manager.GenericJdbcManager
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapred.lib.db.DBWritable;
import com.cloudera.sqoop.lib.JdbcWritableBridge;
import com.cloudera.sqoop.lib.DelimiterSet;
import com.cloudera.sqoop.lib.FieldFormatter;
import com.cloudera.sqoop.lib.RecordParser;
import com.cloudera.sqoop.lib.BooleanParser;
import com.cloudera.sqoop.lib.BlobRef;
import com.cloudera.sqoop.lib.ClobRef;
import com.cloudera.sqoop.lib.LargeObjectLoader;
import com.cloudera.sqoop.lib.SqoopRecord;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class MSOC__MSOC_EMPLOYEE extends SqoopRecord  implements DBWritable, Writable {
  private final int PROTOCOL_VERSION = 3;
  public int getClassFormatVersion() { return PROTOCOL_VERSION; }
  protected ResultSet __cur_result_set;
  private String FULL_NAME;
  public String get_FULL_NAME() {
    return FULL_NAME;
  }
  public void set_FULL_NAME(String FULL_NAME) {
    this.FULL_NAME = FULL_NAME;
  }
  public MSOC__MSOC_EMPLOYEE with_FULL_NAME(String FULL_NAME) {
    this.FULL_NAME = FULL_NAME;
    return this;
  }
  private String TELEPHONE;
  public String get_TELEPHONE() {
    return TELEPHONE;
  }
  public void set_TELEPHONE(String TELEPHONE) {
    this.TELEPHONE = TELEPHONE;
  }
  public MSOC__MSOC_EMPLOYEE with_TELEPHONE(String TELEPHONE) {
    this.TELEPHONE = TELEPHONE;
    return this;
  }
  private String TITLE;
  public String get_TITLE() {
    return TITLE;
  }
  public void set_TITLE(String TITLE) {
    this.TITLE = TITLE;
  }
  public MSOC__MSOC_EMPLOYEE with_TITLE(String TITLE) {
    this.TITLE = TITLE;
    return this;
  }
  private String CUID;
  public String get_CUID() {
    return CUID;
  }
  public void set_CUID(String CUID) {
    this.CUID = CUID;
  }
  public MSOC__MSOC_EMPLOYEE with_CUID(String CUID) {
    this.CUID = CUID;
    return this;
  }
  private String RC_CODE;
  public String get_RC_CODE() {
    return RC_CODE;
  }
  public void set_RC_CODE(String RC_CODE) {
    this.RC_CODE = RC_CODE;
  }
  public MSOC__MSOC_EMPLOYEE with_RC_CODE(String RC_CODE) {
    this.RC_CODE = RC_CODE;
    return this;
  }
  private String ORGANIZATION;
  public String get_ORGANIZATION() {
    return ORGANIZATION;
  }
  public void set_ORGANIZATION(String ORGANIZATION) {
    this.ORGANIZATION = ORGANIZATION;
  }
  public MSOC__MSOC_EMPLOYEE with_ORGANIZATION(String ORGANIZATION) {
    this.ORGANIZATION = ORGANIZATION;
    return this;
  }
  private Integer PWORD;
  public Integer get_PWORD() {
    return PWORD;
  }
  public void set_PWORD(Integer PWORD) {
    this.PWORD = PWORD;
  }
  public MSOC__MSOC_EMPLOYEE with_PWORD(Integer PWORD) {
    this.PWORD = PWORD;
    return this;
  }
  private String EMAIL;
  public String get_EMAIL() {
    return EMAIL;
  }
  public void set_EMAIL(String EMAIL) {
    this.EMAIL = EMAIL;
  }
  public MSOC__MSOC_EMPLOYEE with_EMAIL(String EMAIL) {
    this.EMAIL = EMAIL;
    return this;
  }
  private java.sql.Timestamp LOAD_TIME;
  public java.sql.Timestamp get_LOAD_TIME() {
    return LOAD_TIME;
  }
  public void set_LOAD_TIME(java.sql.Timestamp LOAD_TIME) {
    this.LOAD_TIME = LOAD_TIME;
  }
  public MSOC__MSOC_EMPLOYEE with_LOAD_TIME(java.sql.Timestamp LOAD_TIME) {
    this.LOAD_TIME = LOAD_TIME;
    return this;
  }
  private String MANAGER;
  public String get_MANAGER() {
    return MANAGER;
  }
  public void set_MANAGER(String MANAGER) {
    this.MANAGER = MANAGER;
  }
  public MSOC__MSOC_EMPLOYEE with_MANAGER(String MANAGER) {
    this.MANAGER = MANAGER;
    return this;
  }
  private String MANAGER_RC;
  public String get_MANAGER_RC() {
    return MANAGER_RC;
  }
  public void set_MANAGER_RC(String MANAGER_RC) {
    this.MANAGER_RC = MANAGER_RC;
  }
  public MSOC__MSOC_EMPLOYEE with_MANAGER_RC(String MANAGER_RC) {
    this.MANAGER_RC = MANAGER_RC;
    return this;
  }
  private String SUPERVISOR;
  public String get_SUPERVISOR() {
    return SUPERVISOR;
  }
  public void set_SUPERVISOR(String SUPERVISOR) {
    this.SUPERVISOR = SUPERVISOR;
  }
  public MSOC__MSOC_EMPLOYEE with_SUPERVISOR(String SUPERVISOR) {
    this.SUPERVISOR = SUPERVISOR;
    return this;
  }
  private String SUPERVISOR_RC;
  public String get_SUPERVISOR_RC() {
    return SUPERVISOR_RC;
  }
  public void set_SUPERVISOR_RC(String SUPERVISOR_RC) {
    this.SUPERVISOR_RC = SUPERVISOR_RC;
  }
  public MSOC__MSOC_EMPLOYEE with_SUPERVISOR_RC(String SUPERVISOR_RC) {
    this.SUPERVISOR_RC = SUPERVISOR_RC;
    return this;
  }
  private String DIRECTOR;
  public String get_DIRECTOR() {
    return DIRECTOR;
  }
  public void set_DIRECTOR(String DIRECTOR) {
    this.DIRECTOR = DIRECTOR;
  }
  public MSOC__MSOC_EMPLOYEE with_DIRECTOR(String DIRECTOR) {
    this.DIRECTOR = DIRECTOR;
    return this;
  }
  private String DIRECTOR_RC;
  public String get_DIRECTOR_RC() {
    return DIRECTOR_RC;
  }
  public void set_DIRECTOR_RC(String DIRECTOR_RC) {
    this.DIRECTOR_RC = DIRECTOR_RC;
  }
  public MSOC__MSOC_EMPLOYEE with_DIRECTOR_RC(String DIRECTOR_RC) {
    this.DIRECTOR_RC = DIRECTOR_RC;
    return this;
  }
  private String DATA_SOURCE;
  public String get_DATA_SOURCE() {
    return DATA_SOURCE;
  }
  public void set_DATA_SOURCE(String DATA_SOURCE) {
    this.DATA_SOURCE = DATA_SOURCE;
  }
  public MSOC__MSOC_EMPLOYEE with_DATA_SOURCE(String DATA_SOURCE) {
    this.DATA_SOURCE = DATA_SOURCE;
    return this;
  }
  private String LEVEL_NAME;
  public String get_LEVEL_NAME() {
    return LEVEL_NAME;
  }
  public void set_LEVEL_NAME(String LEVEL_NAME) {
    this.LEVEL_NAME = LEVEL_NAME;
  }
  public MSOC__MSOC_EMPLOYEE with_LEVEL_NAME(String LEVEL_NAME) {
    this.LEVEL_NAME = LEVEL_NAME;
    return this;
  }
  private String TIME_ZONE;
  public String get_TIME_ZONE() {
    return TIME_ZONE;
  }
  public void set_TIME_ZONE(String TIME_ZONE) {
    this.TIME_ZONE = TIME_ZONE;
  }
  public MSOC__MSOC_EMPLOYEE with_TIME_ZONE(String TIME_ZONE) {
    this.TIME_ZONE = TIME_ZONE;
    return this;
  }
  private Integer LEVEL_ID;
  public Integer get_LEVEL_ID() {
    return LEVEL_ID;
  }
  public void set_LEVEL_ID(Integer LEVEL_ID) {
    this.LEVEL_ID = LEVEL_ID;
  }
  public MSOC__MSOC_EMPLOYEE with_LEVEL_ID(Integer LEVEL_ID) {
    this.LEVEL_ID = LEVEL_ID;
    return this;
  }
  private Integer SUPERVISORY_INDICATOR;
  public Integer get_SUPERVISORY_INDICATOR() {
    return SUPERVISORY_INDICATOR;
  }
  public void set_SUPERVISORY_INDICATOR(Integer SUPERVISORY_INDICATOR) {
    this.SUPERVISORY_INDICATOR = SUPERVISORY_INDICATOR;
  }
  public MSOC__MSOC_EMPLOYEE with_SUPERVISORY_INDICATOR(Integer SUPERVISORY_INDICATOR) {
    this.SUPERVISORY_INDICATOR = SUPERVISORY_INDICATOR;
    return this;
  }
  private Integer ROWID;
  public Integer get_ROWID() {
    return ROWID;
  }
  public void set_ROWID(Integer ROWID) {
    this.ROWID = ROWID;
  }
  public MSOC__MSOC_EMPLOYEE with_ROWID(Integer ROWID) {
    this.ROWID = ROWID;
    return this;
  }
  private String WAGESCALE;
  public String get_WAGESCALE() {
    return WAGESCALE;
  }
  public void set_WAGESCALE(String WAGESCALE) {
    this.WAGESCALE = WAGESCALE;
  }
  public MSOC__MSOC_EMPLOYEE with_WAGESCALE(String WAGESCALE) {
    this.WAGESCALE = WAGESCALE;
    return this;
  }
  private String RANK_ORDER_NUMBER;
  public String get_RANK_ORDER_NUMBER() {
    return RANK_ORDER_NUMBER;
  }
  public void set_RANK_ORDER_NUMBER(String RANK_ORDER_NUMBER) {
    this.RANK_ORDER_NUMBER = RANK_ORDER_NUMBER;
  }
  public MSOC__MSOC_EMPLOYEE with_RANK_ORDER_NUMBER(String RANK_ORDER_NUMBER) {
    this.RANK_ORDER_NUMBER = RANK_ORDER_NUMBER;
    return this;
  }
  private java.sql.Timestamp START_DATE;
  public java.sql.Timestamp get_START_DATE() {
    return START_DATE;
  }
  public void set_START_DATE(java.sql.Timestamp START_DATE) {
    this.START_DATE = START_DATE;
  }
  public MSOC__MSOC_EMPLOYEE with_START_DATE(java.sql.Timestamp START_DATE) {
    this.START_DATE = START_DATE;
    return this;
  }
  private java.sql.Timestamp END_DATE;
  public java.sql.Timestamp get_END_DATE() {
    return END_DATE;
  }
  public void set_END_DATE(java.sql.Timestamp END_DATE) {
    this.END_DATE = END_DATE;
  }
  public MSOC__MSOC_EMPLOYEE with_END_DATE(java.sql.Timestamp END_DATE) {
    this.END_DATE = END_DATE;
    return this;
  }
  private java.math.BigDecimal RATE_PER_HOUR;
  public java.math.BigDecimal get_RATE_PER_HOUR() {
    return RATE_PER_HOUR;
  }
  public void set_RATE_PER_HOUR(java.math.BigDecimal RATE_PER_HOUR) {
    this.RATE_PER_HOUR = RATE_PER_HOUR;
  }
  public MSOC__MSOC_EMPLOYEE with_RATE_PER_HOUR(java.math.BigDecimal RATE_PER_HOUR) {
    this.RATE_PER_HOUR = RATE_PER_HOUR;
    return this;
  }
  private String LOGIN_ID;
  public String get_LOGIN_ID() {
    return LOGIN_ID;
  }
  public void set_LOGIN_ID(String LOGIN_ID) {
    this.LOGIN_ID = LOGIN_ID;
  }
  public MSOC__MSOC_EMPLOYEE with_LOGIN_ID(String LOGIN_ID) {
    this.LOGIN_ID = LOGIN_ID;
    return this;
  }
  private String PROFILE_NAME;
  public String get_PROFILE_NAME() {
    return PROFILE_NAME;
  }
  public void set_PROFILE_NAME(String PROFILE_NAME) {
    this.PROFILE_NAME = PROFILE_NAME;
  }
  public MSOC__MSOC_EMPLOYEE with_PROFILE_NAME(String PROFILE_NAME) {
    this.PROFILE_NAME = PROFILE_NAME;
    return this;
  }
  private String UID;
  public String get_UID() {
    return UID;
  }
  public void set_UID(String UID) {
    this.UID = UID;
  }
  public MSOC__MSOC_EMPLOYEE with_UID(String UID) {
    this.UID = UID;
    return this;
  }
  private String LAST_NAME;
  public String get_LAST_NAME() {
    return LAST_NAME;
  }
  public void set_LAST_NAME(String LAST_NAME) {
    this.LAST_NAME = LAST_NAME;
  }
  public MSOC__MSOC_EMPLOYEE with_LAST_NAME(String LAST_NAME) {
    this.LAST_NAME = LAST_NAME;
    return this;
  }
  private String FIRST_NAME;
  public String get_FIRST_NAME() {
    return FIRST_NAME;
  }
  public void set_FIRST_NAME(String FIRST_NAME) {
    this.FIRST_NAME = FIRST_NAME;
  }
  public MSOC__MSOC_EMPLOYEE with_FIRST_NAME(String FIRST_NAME) {
    this.FIRST_NAME = FIRST_NAME;
    return this;
  }
  private String MIDDLE_INITIAL;
  public String get_MIDDLE_INITIAL() {
    return MIDDLE_INITIAL;
  }
  public void set_MIDDLE_INITIAL(String MIDDLE_INITIAL) {
    this.MIDDLE_INITIAL = MIDDLE_INITIAL;
  }
  public MSOC__MSOC_EMPLOYEE with_MIDDLE_INITIAL(String MIDDLE_INITIAL) {
    this.MIDDLE_INITIAL = MIDDLE_INITIAL;
    return this;
  }
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof MSOC__MSOC_EMPLOYEE)) {
      return false;
    }
    MSOC__MSOC_EMPLOYEE that = (MSOC__MSOC_EMPLOYEE) o;
    boolean equal = true;
    equal = equal && (this.FULL_NAME == null ? that.FULL_NAME == null : this.FULL_NAME.equals(that.FULL_NAME));
    equal = equal && (this.TELEPHONE == null ? that.TELEPHONE == null : this.TELEPHONE.equals(that.TELEPHONE));
    equal = equal && (this.TITLE == null ? that.TITLE == null : this.TITLE.equals(that.TITLE));
    equal = equal && (this.CUID == null ? that.CUID == null : this.CUID.equals(that.CUID));
    equal = equal && (this.RC_CODE == null ? that.RC_CODE == null : this.RC_CODE.equals(that.RC_CODE));
    equal = equal && (this.ORGANIZATION == null ? that.ORGANIZATION == null : this.ORGANIZATION.equals(that.ORGANIZATION));
    equal = equal && (this.PWORD == null ? that.PWORD == null : this.PWORD.equals(that.PWORD));
    equal = equal && (this.EMAIL == null ? that.EMAIL == null : this.EMAIL.equals(that.EMAIL));
    equal = equal && (this.LOAD_TIME == null ? that.LOAD_TIME == null : this.LOAD_TIME.equals(that.LOAD_TIME));
    equal = equal && (this.MANAGER == null ? that.MANAGER == null : this.MANAGER.equals(that.MANAGER));
    equal = equal && (this.MANAGER_RC == null ? that.MANAGER_RC == null : this.MANAGER_RC.equals(that.MANAGER_RC));
    equal = equal && (this.SUPERVISOR == null ? that.SUPERVISOR == null : this.SUPERVISOR.equals(that.SUPERVISOR));
    equal = equal && (this.SUPERVISOR_RC == null ? that.SUPERVISOR_RC == null : this.SUPERVISOR_RC.equals(that.SUPERVISOR_RC));
    equal = equal && (this.DIRECTOR == null ? that.DIRECTOR == null : this.DIRECTOR.equals(that.DIRECTOR));
    equal = equal && (this.DIRECTOR_RC == null ? that.DIRECTOR_RC == null : this.DIRECTOR_RC.equals(that.DIRECTOR_RC));
    equal = equal && (this.DATA_SOURCE == null ? that.DATA_SOURCE == null : this.DATA_SOURCE.equals(that.DATA_SOURCE));
    equal = equal && (this.LEVEL_NAME == null ? that.LEVEL_NAME == null : this.LEVEL_NAME.equals(that.LEVEL_NAME));
    equal = equal && (this.TIME_ZONE == null ? that.TIME_ZONE == null : this.TIME_ZONE.equals(that.TIME_ZONE));
    equal = equal && (this.LEVEL_ID == null ? that.LEVEL_ID == null : this.LEVEL_ID.equals(that.LEVEL_ID));
    equal = equal && (this.SUPERVISORY_INDICATOR == null ? that.SUPERVISORY_INDICATOR == null : this.SUPERVISORY_INDICATOR.equals(that.SUPERVISORY_INDICATOR));
    equal = equal && (this.ROWID == null ? that.ROWID == null : this.ROWID.equals(that.ROWID));
    equal = equal && (this.WAGESCALE == null ? that.WAGESCALE == null : this.WAGESCALE.equals(that.WAGESCALE));
    equal = equal && (this.RANK_ORDER_NUMBER == null ? that.RANK_ORDER_NUMBER == null : this.RANK_ORDER_NUMBER.equals(that.RANK_ORDER_NUMBER));
    equal = equal && (this.START_DATE == null ? that.START_DATE == null : this.START_DATE.equals(that.START_DATE));
    equal = equal && (this.END_DATE == null ? that.END_DATE == null : this.END_DATE.equals(that.END_DATE));
    equal = equal && (this.RATE_PER_HOUR == null ? that.RATE_PER_HOUR == null : this.RATE_PER_HOUR.equals(that.RATE_PER_HOUR));
    equal = equal && (this.LOGIN_ID == null ? that.LOGIN_ID == null : this.LOGIN_ID.equals(that.LOGIN_ID));
    equal = equal && (this.PROFILE_NAME == null ? that.PROFILE_NAME == null : this.PROFILE_NAME.equals(that.PROFILE_NAME));
    equal = equal && (this.UID == null ? that.UID == null : this.UID.equals(that.UID));
    equal = equal && (this.LAST_NAME == null ? that.LAST_NAME == null : this.LAST_NAME.equals(that.LAST_NAME));
    equal = equal && (this.FIRST_NAME == null ? that.FIRST_NAME == null : this.FIRST_NAME.equals(that.FIRST_NAME));
    equal = equal && (this.MIDDLE_INITIAL == null ? that.MIDDLE_INITIAL == null : this.MIDDLE_INITIAL.equals(that.MIDDLE_INITIAL));
    return equal;
  }
  public boolean equals0(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof MSOC__MSOC_EMPLOYEE)) {
      return false;
    }
    MSOC__MSOC_EMPLOYEE that = (MSOC__MSOC_EMPLOYEE) o;
    boolean equal = true;
    equal = equal && (this.FULL_NAME == null ? that.FULL_NAME == null : this.FULL_NAME.equals(that.FULL_NAME));
    equal = equal && (this.TELEPHONE == null ? that.TELEPHONE == null : this.TELEPHONE.equals(that.TELEPHONE));
    equal = equal && (this.TITLE == null ? that.TITLE == null : this.TITLE.equals(that.TITLE));
    equal = equal && (this.CUID == null ? that.CUID == null : this.CUID.equals(that.CUID));
    equal = equal && (this.RC_CODE == null ? that.RC_CODE == null : this.RC_CODE.equals(that.RC_CODE));
    equal = equal && (this.ORGANIZATION == null ? that.ORGANIZATION == null : this.ORGANIZATION.equals(that.ORGANIZATION));
    equal = equal && (this.PWORD == null ? that.PWORD == null : this.PWORD.equals(that.PWORD));
    equal = equal && (this.EMAIL == null ? that.EMAIL == null : this.EMAIL.equals(that.EMAIL));
    equal = equal && (this.LOAD_TIME == null ? that.LOAD_TIME == null : this.LOAD_TIME.equals(that.LOAD_TIME));
    equal = equal && (this.MANAGER == null ? that.MANAGER == null : this.MANAGER.equals(that.MANAGER));
    equal = equal && (this.MANAGER_RC == null ? that.MANAGER_RC == null : this.MANAGER_RC.equals(that.MANAGER_RC));
    equal = equal && (this.SUPERVISOR == null ? that.SUPERVISOR == null : this.SUPERVISOR.equals(that.SUPERVISOR));
    equal = equal && (this.SUPERVISOR_RC == null ? that.SUPERVISOR_RC == null : this.SUPERVISOR_RC.equals(that.SUPERVISOR_RC));
    equal = equal && (this.DIRECTOR == null ? that.DIRECTOR == null : this.DIRECTOR.equals(that.DIRECTOR));
    equal = equal && (this.DIRECTOR_RC == null ? that.DIRECTOR_RC == null : this.DIRECTOR_RC.equals(that.DIRECTOR_RC));
    equal = equal && (this.DATA_SOURCE == null ? that.DATA_SOURCE == null : this.DATA_SOURCE.equals(that.DATA_SOURCE));
    equal = equal && (this.LEVEL_NAME == null ? that.LEVEL_NAME == null : this.LEVEL_NAME.equals(that.LEVEL_NAME));
    equal = equal && (this.TIME_ZONE == null ? that.TIME_ZONE == null : this.TIME_ZONE.equals(that.TIME_ZONE));
    equal = equal && (this.LEVEL_ID == null ? that.LEVEL_ID == null : this.LEVEL_ID.equals(that.LEVEL_ID));
    equal = equal && (this.SUPERVISORY_INDICATOR == null ? that.SUPERVISORY_INDICATOR == null : this.SUPERVISORY_INDICATOR.equals(that.SUPERVISORY_INDICATOR));
    equal = equal && (this.ROWID == null ? that.ROWID == null : this.ROWID.equals(that.ROWID));
    equal = equal && (this.WAGESCALE == null ? that.WAGESCALE == null : this.WAGESCALE.equals(that.WAGESCALE));
    equal = equal && (this.RANK_ORDER_NUMBER == null ? that.RANK_ORDER_NUMBER == null : this.RANK_ORDER_NUMBER.equals(that.RANK_ORDER_NUMBER));
    equal = equal && (this.START_DATE == null ? that.START_DATE == null : this.START_DATE.equals(that.START_DATE));
    equal = equal && (this.END_DATE == null ? that.END_DATE == null : this.END_DATE.equals(that.END_DATE));
    equal = equal && (this.RATE_PER_HOUR == null ? that.RATE_PER_HOUR == null : this.RATE_PER_HOUR.equals(that.RATE_PER_HOUR));
    equal = equal && (this.LOGIN_ID == null ? that.LOGIN_ID == null : this.LOGIN_ID.equals(that.LOGIN_ID));
    equal = equal && (this.PROFILE_NAME == null ? that.PROFILE_NAME == null : this.PROFILE_NAME.equals(that.PROFILE_NAME));
    equal = equal && (this.UID == null ? that.UID == null : this.UID.equals(that.UID));
    equal = equal && (this.LAST_NAME == null ? that.LAST_NAME == null : this.LAST_NAME.equals(that.LAST_NAME));
    equal = equal && (this.FIRST_NAME == null ? that.FIRST_NAME == null : this.FIRST_NAME.equals(that.FIRST_NAME));
    equal = equal && (this.MIDDLE_INITIAL == null ? that.MIDDLE_INITIAL == null : this.MIDDLE_INITIAL.equals(that.MIDDLE_INITIAL));
    return equal;
  }
  public void readFields(ResultSet __dbResults) throws SQLException {
    this.__cur_result_set = __dbResults;
    this.FULL_NAME = JdbcWritableBridge.readString(1, __dbResults);
    this.TELEPHONE = JdbcWritableBridge.readString(2, __dbResults);
    this.TITLE = JdbcWritableBridge.readString(3, __dbResults);
    this.CUID = JdbcWritableBridge.readString(4, __dbResults);
    this.RC_CODE = JdbcWritableBridge.readString(5, __dbResults);
    this.ORGANIZATION = JdbcWritableBridge.readString(6, __dbResults);
    this.PWORD = JdbcWritableBridge.readInteger(7, __dbResults);
    this.EMAIL = JdbcWritableBridge.readString(8, __dbResults);
    this.LOAD_TIME = JdbcWritableBridge.readTimestamp(9, __dbResults);
    this.MANAGER = JdbcWritableBridge.readString(10, __dbResults);
    this.MANAGER_RC = JdbcWritableBridge.readString(11, __dbResults);
    this.SUPERVISOR = JdbcWritableBridge.readString(12, __dbResults);
    this.SUPERVISOR_RC = JdbcWritableBridge.readString(13, __dbResults);
    this.DIRECTOR = JdbcWritableBridge.readString(14, __dbResults);
    this.DIRECTOR_RC = JdbcWritableBridge.readString(15, __dbResults);
    this.DATA_SOURCE = JdbcWritableBridge.readString(16, __dbResults);
    this.LEVEL_NAME = JdbcWritableBridge.readString(17, __dbResults);
    this.TIME_ZONE = JdbcWritableBridge.readString(18, __dbResults);
    this.LEVEL_ID = JdbcWritableBridge.readInteger(19, __dbResults);
    this.SUPERVISORY_INDICATOR = JdbcWritableBridge.readInteger(20, __dbResults);
    this.ROWID = JdbcWritableBridge.readInteger(21, __dbResults);
    this.WAGESCALE = JdbcWritableBridge.readString(22, __dbResults);
    this.RANK_ORDER_NUMBER = JdbcWritableBridge.readString(23, __dbResults);
    this.START_DATE = JdbcWritableBridge.readTimestamp(24, __dbResults);
    this.END_DATE = JdbcWritableBridge.readTimestamp(25, __dbResults);
    this.RATE_PER_HOUR = JdbcWritableBridge.readBigDecimal(26, __dbResults);
    this.LOGIN_ID = JdbcWritableBridge.readString(27, __dbResults);
    this.PROFILE_NAME = JdbcWritableBridge.readString(28, __dbResults);
    this.UID = JdbcWritableBridge.readString(29, __dbResults);
    this.LAST_NAME = JdbcWritableBridge.readString(30, __dbResults);
    this.FIRST_NAME = JdbcWritableBridge.readString(31, __dbResults);
    this.MIDDLE_INITIAL = JdbcWritableBridge.readString(32, __dbResults);
  }
  public void readFields0(ResultSet __dbResults) throws SQLException {
    this.FULL_NAME = JdbcWritableBridge.readString(1, __dbResults);
    this.TELEPHONE = JdbcWritableBridge.readString(2, __dbResults);
    this.TITLE = JdbcWritableBridge.readString(3, __dbResults);
    this.CUID = JdbcWritableBridge.readString(4, __dbResults);
    this.RC_CODE = JdbcWritableBridge.readString(5, __dbResults);
    this.ORGANIZATION = JdbcWritableBridge.readString(6, __dbResults);
    this.PWORD = JdbcWritableBridge.readInteger(7, __dbResults);
    this.EMAIL = JdbcWritableBridge.readString(8, __dbResults);
    this.LOAD_TIME = JdbcWritableBridge.readTimestamp(9, __dbResults);
    this.MANAGER = JdbcWritableBridge.readString(10, __dbResults);
    this.MANAGER_RC = JdbcWritableBridge.readString(11, __dbResults);
    this.SUPERVISOR = JdbcWritableBridge.readString(12, __dbResults);
    this.SUPERVISOR_RC = JdbcWritableBridge.readString(13, __dbResults);
    this.DIRECTOR = JdbcWritableBridge.readString(14, __dbResults);
    this.DIRECTOR_RC = JdbcWritableBridge.readString(15, __dbResults);
    this.DATA_SOURCE = JdbcWritableBridge.readString(16, __dbResults);
    this.LEVEL_NAME = JdbcWritableBridge.readString(17, __dbResults);
    this.TIME_ZONE = JdbcWritableBridge.readString(18, __dbResults);
    this.LEVEL_ID = JdbcWritableBridge.readInteger(19, __dbResults);
    this.SUPERVISORY_INDICATOR = JdbcWritableBridge.readInteger(20, __dbResults);
    this.ROWID = JdbcWritableBridge.readInteger(21, __dbResults);
    this.WAGESCALE = JdbcWritableBridge.readString(22, __dbResults);
    this.RANK_ORDER_NUMBER = JdbcWritableBridge.readString(23, __dbResults);
    this.START_DATE = JdbcWritableBridge.readTimestamp(24, __dbResults);
    this.END_DATE = JdbcWritableBridge.readTimestamp(25, __dbResults);
    this.RATE_PER_HOUR = JdbcWritableBridge.readBigDecimal(26, __dbResults);
    this.LOGIN_ID = JdbcWritableBridge.readString(27, __dbResults);
    this.PROFILE_NAME = JdbcWritableBridge.readString(28, __dbResults);
    this.UID = JdbcWritableBridge.readString(29, __dbResults);
    this.LAST_NAME = JdbcWritableBridge.readString(30, __dbResults);
    this.FIRST_NAME = JdbcWritableBridge.readString(31, __dbResults);
    this.MIDDLE_INITIAL = JdbcWritableBridge.readString(32, __dbResults);
  }
  public void loadLargeObjects(LargeObjectLoader __loader)
      throws SQLException, IOException, InterruptedException {
  }
  public void loadLargeObjects0(LargeObjectLoader __loader)
      throws SQLException, IOException, InterruptedException {
  }
  public void write(PreparedStatement __dbStmt) throws SQLException {
    write(__dbStmt, 0);
  }

  public int write(PreparedStatement __dbStmt, int __off) throws SQLException {
    JdbcWritableBridge.writeString(FULL_NAME, 1 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(TELEPHONE, 2 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(TITLE, 3 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(CUID, 4 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(RC_CODE, 5 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(ORGANIZATION, 6 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeInteger(PWORD, 7 + __off, 4, __dbStmt);
    JdbcWritableBridge.writeString(EMAIL, 8 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeTimestamp(LOAD_TIME, 9 + __off, 93, __dbStmt);
    JdbcWritableBridge.writeString(MANAGER, 10 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(MANAGER_RC, 11 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(SUPERVISOR, 12 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(SUPERVISOR_RC, 13 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(DIRECTOR, 14 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(DIRECTOR_RC, 15 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(DATA_SOURCE, 16 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(LEVEL_NAME, 17 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(TIME_ZONE, 18 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeInteger(LEVEL_ID, 19 + __off, 4, __dbStmt);
    JdbcWritableBridge.writeInteger(SUPERVISORY_INDICATOR, 20 + __off, 4, __dbStmt);
    JdbcWritableBridge.writeInteger(ROWID, 21 + __off, 4, __dbStmt);
    JdbcWritableBridge.writeString(WAGESCALE, 22 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(RANK_ORDER_NUMBER, 23 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeTimestamp(START_DATE, 24 + __off, 93, __dbStmt);
    JdbcWritableBridge.writeTimestamp(END_DATE, 25 + __off, 93, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(RATE_PER_HOUR, 26 + __off, 3, __dbStmt);
    JdbcWritableBridge.writeString(LOGIN_ID, 27 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(PROFILE_NAME, 28 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(UID, 29 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(LAST_NAME, 30 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(FIRST_NAME, 31 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(MIDDLE_INITIAL, 32 + __off, 1, __dbStmt);
    return 32;
  }
  public void write0(PreparedStatement __dbStmt, int __off) throws SQLException {
    JdbcWritableBridge.writeString(FULL_NAME, 1 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(TELEPHONE, 2 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(TITLE, 3 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(CUID, 4 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(RC_CODE, 5 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(ORGANIZATION, 6 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeInteger(PWORD, 7 + __off, 4, __dbStmt);
    JdbcWritableBridge.writeString(EMAIL, 8 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeTimestamp(LOAD_TIME, 9 + __off, 93, __dbStmt);
    JdbcWritableBridge.writeString(MANAGER, 10 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(MANAGER_RC, 11 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(SUPERVISOR, 12 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(SUPERVISOR_RC, 13 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(DIRECTOR, 14 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(DIRECTOR_RC, 15 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(DATA_SOURCE, 16 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(LEVEL_NAME, 17 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(TIME_ZONE, 18 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeInteger(LEVEL_ID, 19 + __off, 4, __dbStmt);
    JdbcWritableBridge.writeInteger(SUPERVISORY_INDICATOR, 20 + __off, 4, __dbStmt);
    JdbcWritableBridge.writeInteger(ROWID, 21 + __off, 4, __dbStmt);
    JdbcWritableBridge.writeString(WAGESCALE, 22 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(RANK_ORDER_NUMBER, 23 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeTimestamp(START_DATE, 24 + __off, 93, __dbStmt);
    JdbcWritableBridge.writeTimestamp(END_DATE, 25 + __off, 93, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(RATE_PER_HOUR, 26 + __off, 3, __dbStmt);
    JdbcWritableBridge.writeString(LOGIN_ID, 27 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(PROFILE_NAME, 28 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(UID, 29 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(LAST_NAME, 30 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(FIRST_NAME, 31 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(MIDDLE_INITIAL, 32 + __off, 1, __dbStmt);
  }
  public void readFields(DataInput __dataIn) throws IOException {
this.readFields0(__dataIn);  }
  public void readFields0(DataInput __dataIn) throws IOException {
    if (__dataIn.readBoolean()) { 
        this.FULL_NAME = null;
    } else {
    this.FULL_NAME = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.TELEPHONE = null;
    } else {
    this.TELEPHONE = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.TITLE = null;
    } else {
    this.TITLE = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.CUID = null;
    } else {
    this.CUID = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.RC_CODE = null;
    } else {
    this.RC_CODE = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.ORGANIZATION = null;
    } else {
    this.ORGANIZATION = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.PWORD = null;
    } else {
    this.PWORD = Integer.valueOf(__dataIn.readInt());
    }
    if (__dataIn.readBoolean()) { 
        this.EMAIL = null;
    } else {
    this.EMAIL = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.LOAD_TIME = null;
    } else {
    this.LOAD_TIME = new Timestamp(__dataIn.readLong());
    this.LOAD_TIME.setNanos(__dataIn.readInt());
    }
    if (__dataIn.readBoolean()) { 
        this.MANAGER = null;
    } else {
    this.MANAGER = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.MANAGER_RC = null;
    } else {
    this.MANAGER_RC = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.SUPERVISOR = null;
    } else {
    this.SUPERVISOR = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.SUPERVISOR_RC = null;
    } else {
    this.SUPERVISOR_RC = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.DIRECTOR = null;
    } else {
    this.DIRECTOR = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.DIRECTOR_RC = null;
    } else {
    this.DIRECTOR_RC = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.DATA_SOURCE = null;
    } else {
    this.DATA_SOURCE = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.LEVEL_NAME = null;
    } else {
    this.LEVEL_NAME = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.TIME_ZONE = null;
    } else {
    this.TIME_ZONE = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.LEVEL_ID = null;
    } else {
    this.LEVEL_ID = Integer.valueOf(__dataIn.readInt());
    }
    if (__dataIn.readBoolean()) { 
        this.SUPERVISORY_INDICATOR = null;
    } else {
    this.SUPERVISORY_INDICATOR = Integer.valueOf(__dataIn.readInt());
    }
    if (__dataIn.readBoolean()) { 
        this.ROWID = null;
    } else {
    this.ROWID = Integer.valueOf(__dataIn.readInt());
    }
    if (__dataIn.readBoolean()) { 
        this.WAGESCALE = null;
    } else {
    this.WAGESCALE = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.RANK_ORDER_NUMBER = null;
    } else {
    this.RANK_ORDER_NUMBER = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.START_DATE = null;
    } else {
    this.START_DATE = new Timestamp(__dataIn.readLong());
    this.START_DATE.setNanos(__dataIn.readInt());
    }
    if (__dataIn.readBoolean()) { 
        this.END_DATE = null;
    } else {
    this.END_DATE = new Timestamp(__dataIn.readLong());
    this.END_DATE.setNanos(__dataIn.readInt());
    }
    if (__dataIn.readBoolean()) { 
        this.RATE_PER_HOUR = null;
    } else {
    this.RATE_PER_HOUR = com.cloudera.sqoop.lib.BigDecimalSerializer.readFields(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.LOGIN_ID = null;
    } else {
    this.LOGIN_ID = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.PROFILE_NAME = null;
    } else {
    this.PROFILE_NAME = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.UID = null;
    } else {
    this.UID = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.LAST_NAME = null;
    } else {
    this.LAST_NAME = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.FIRST_NAME = null;
    } else {
    this.FIRST_NAME = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.MIDDLE_INITIAL = null;
    } else {
    this.MIDDLE_INITIAL = Text.readString(__dataIn);
    }
  }
  public void write(DataOutput __dataOut) throws IOException {
    if (null == this.FULL_NAME) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, FULL_NAME);
    }
    if (null == this.TELEPHONE) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, TELEPHONE);
    }
    if (null == this.TITLE) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, TITLE);
    }
    if (null == this.CUID) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, CUID);
    }
    if (null == this.RC_CODE) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, RC_CODE);
    }
    if (null == this.ORGANIZATION) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, ORGANIZATION);
    }
    if (null == this.PWORD) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.PWORD);
    }
    if (null == this.EMAIL) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, EMAIL);
    }
    if (null == this.LOAD_TIME) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.LOAD_TIME.getTime());
    __dataOut.writeInt(this.LOAD_TIME.getNanos());
    }
    if (null == this.MANAGER) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, MANAGER);
    }
    if (null == this.MANAGER_RC) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, MANAGER_RC);
    }
    if (null == this.SUPERVISOR) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, SUPERVISOR);
    }
    if (null == this.SUPERVISOR_RC) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, SUPERVISOR_RC);
    }
    if (null == this.DIRECTOR) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, DIRECTOR);
    }
    if (null == this.DIRECTOR_RC) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, DIRECTOR_RC);
    }
    if (null == this.DATA_SOURCE) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, DATA_SOURCE);
    }
    if (null == this.LEVEL_NAME) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, LEVEL_NAME);
    }
    if (null == this.TIME_ZONE) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, TIME_ZONE);
    }
    if (null == this.LEVEL_ID) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.LEVEL_ID);
    }
    if (null == this.SUPERVISORY_INDICATOR) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.SUPERVISORY_INDICATOR);
    }
    if (null == this.ROWID) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.ROWID);
    }
    if (null == this.WAGESCALE) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, WAGESCALE);
    }
    if (null == this.RANK_ORDER_NUMBER) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, RANK_ORDER_NUMBER);
    }
    if (null == this.START_DATE) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.START_DATE.getTime());
    __dataOut.writeInt(this.START_DATE.getNanos());
    }
    if (null == this.END_DATE) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.END_DATE.getTime());
    __dataOut.writeInt(this.END_DATE.getNanos());
    }
    if (null == this.RATE_PER_HOUR) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.RATE_PER_HOUR, __dataOut);
    }
    if (null == this.LOGIN_ID) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, LOGIN_ID);
    }
    if (null == this.PROFILE_NAME) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, PROFILE_NAME);
    }
    if (null == this.UID) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, UID);
    }
    if (null == this.LAST_NAME) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, LAST_NAME);
    }
    if (null == this.FIRST_NAME) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, FIRST_NAME);
    }
    if (null == this.MIDDLE_INITIAL) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, MIDDLE_INITIAL);
    }
  }
  public void write0(DataOutput __dataOut) throws IOException {
    if (null == this.FULL_NAME) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, FULL_NAME);
    }
    if (null == this.TELEPHONE) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, TELEPHONE);
    }
    if (null == this.TITLE) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, TITLE);
    }
    if (null == this.CUID) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, CUID);
    }
    if (null == this.RC_CODE) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, RC_CODE);
    }
    if (null == this.ORGANIZATION) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, ORGANIZATION);
    }
    if (null == this.PWORD) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.PWORD);
    }
    if (null == this.EMAIL) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, EMAIL);
    }
    if (null == this.LOAD_TIME) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.LOAD_TIME.getTime());
    __dataOut.writeInt(this.LOAD_TIME.getNanos());
    }
    if (null == this.MANAGER) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, MANAGER);
    }
    if (null == this.MANAGER_RC) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, MANAGER_RC);
    }
    if (null == this.SUPERVISOR) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, SUPERVISOR);
    }
    if (null == this.SUPERVISOR_RC) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, SUPERVISOR_RC);
    }
    if (null == this.DIRECTOR) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, DIRECTOR);
    }
    if (null == this.DIRECTOR_RC) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, DIRECTOR_RC);
    }
    if (null == this.DATA_SOURCE) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, DATA_SOURCE);
    }
    if (null == this.LEVEL_NAME) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, LEVEL_NAME);
    }
    if (null == this.TIME_ZONE) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, TIME_ZONE);
    }
    if (null == this.LEVEL_ID) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.LEVEL_ID);
    }
    if (null == this.SUPERVISORY_INDICATOR) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.SUPERVISORY_INDICATOR);
    }
    if (null == this.ROWID) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.ROWID);
    }
    if (null == this.WAGESCALE) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, WAGESCALE);
    }
    if (null == this.RANK_ORDER_NUMBER) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, RANK_ORDER_NUMBER);
    }
    if (null == this.START_DATE) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.START_DATE.getTime());
    __dataOut.writeInt(this.START_DATE.getNanos());
    }
    if (null == this.END_DATE) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.END_DATE.getTime());
    __dataOut.writeInt(this.END_DATE.getNanos());
    }
    if (null == this.RATE_PER_HOUR) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.RATE_PER_HOUR, __dataOut);
    }
    if (null == this.LOGIN_ID) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, LOGIN_ID);
    }
    if (null == this.PROFILE_NAME) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, PROFILE_NAME);
    }
    if (null == this.UID) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, UID);
    }
    if (null == this.LAST_NAME) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, LAST_NAME);
    }
    if (null == this.FIRST_NAME) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, FIRST_NAME);
    }
    if (null == this.MIDDLE_INITIAL) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, MIDDLE_INITIAL);
    }
  }
  private static final DelimiterSet __outputDelimiters = new DelimiterSet((char) 1, (char) 10, (char) 0, (char) 0, false);
  public String toString() {
    return toString(__outputDelimiters, true);
  }
  public String toString(DelimiterSet delimiters) {
    return toString(delimiters, true);
  }
  public String toString(boolean useRecordDelim) {
    return toString(__outputDelimiters, useRecordDelim);
  }
  public String toString(DelimiterSet delimiters, boolean useRecordDelim) {
    StringBuilder __sb = new StringBuilder();
    char fieldDelim = delimiters.getFieldsTerminatedBy();
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(FULL_NAME==null?"\\N":FULL_NAME, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(TELEPHONE==null?"\\N":TELEPHONE, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(TITLE==null?"\\N":TITLE, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(CUID==null?"\\N":CUID, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(RC_CODE==null?"\\N":RC_CODE, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(ORGANIZATION==null?"\\N":ORGANIZATION, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(PWORD==null?"\\N":"" + PWORD, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(EMAIL==null?"\\N":EMAIL, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(LOAD_TIME==null?"\\N":"" + LOAD_TIME, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(MANAGER==null?"\\N":MANAGER, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(MANAGER_RC==null?"\\N":MANAGER_RC, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(SUPERVISOR==null?"\\N":SUPERVISOR, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(SUPERVISOR_RC==null?"\\N":SUPERVISOR_RC, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(DIRECTOR==null?"\\N":DIRECTOR, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(DIRECTOR_RC==null?"\\N":DIRECTOR_RC, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(DATA_SOURCE==null?"\\N":DATA_SOURCE, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(LEVEL_NAME==null?"\\N":LEVEL_NAME, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(TIME_ZONE==null?"\\N":TIME_ZONE, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(LEVEL_ID==null?"\\N":"" + LEVEL_ID, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(SUPERVISORY_INDICATOR==null?"\\N":"" + SUPERVISORY_INDICATOR, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(ROWID==null?"\\N":"" + ROWID, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(WAGESCALE==null?"\\N":WAGESCALE, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(RANK_ORDER_NUMBER==null?"\\N":RANK_ORDER_NUMBER, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(START_DATE==null?"\\N":"" + START_DATE, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(END_DATE==null?"\\N":"" + END_DATE, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(RATE_PER_HOUR==null?"\\N":RATE_PER_HOUR.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(LOGIN_ID==null?"\\N":LOGIN_ID, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(PROFILE_NAME==null?"\\N":PROFILE_NAME, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(UID==null?"\\N":UID, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(LAST_NAME==null?"\\N":LAST_NAME, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(FIRST_NAME==null?"\\N":FIRST_NAME, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(MIDDLE_INITIAL==null?"\\N":MIDDLE_INITIAL, delimiters));
    if (useRecordDelim) {
      __sb.append(delimiters.getLinesTerminatedBy());
    }
    return __sb.toString();
  }
  public void toString0(DelimiterSet delimiters, StringBuilder __sb, char fieldDelim) {
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(FULL_NAME==null?"\\N":FULL_NAME, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(TELEPHONE==null?"\\N":TELEPHONE, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(TITLE==null?"\\N":TITLE, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(CUID==null?"\\N":CUID, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(RC_CODE==null?"\\N":RC_CODE, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(ORGANIZATION==null?"\\N":ORGANIZATION, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(PWORD==null?"\\N":"" + PWORD, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(EMAIL==null?"\\N":EMAIL, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(LOAD_TIME==null?"\\N":"" + LOAD_TIME, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(MANAGER==null?"\\N":MANAGER, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(MANAGER_RC==null?"\\N":MANAGER_RC, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(SUPERVISOR==null?"\\N":SUPERVISOR, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(SUPERVISOR_RC==null?"\\N":SUPERVISOR_RC, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(DIRECTOR==null?"\\N":DIRECTOR, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(DIRECTOR_RC==null?"\\N":DIRECTOR_RC, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(DATA_SOURCE==null?"\\N":DATA_SOURCE, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(LEVEL_NAME==null?"\\N":LEVEL_NAME, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(TIME_ZONE==null?"\\N":TIME_ZONE, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(LEVEL_ID==null?"\\N":"" + LEVEL_ID, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(SUPERVISORY_INDICATOR==null?"\\N":"" + SUPERVISORY_INDICATOR, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(ROWID==null?"\\N":"" + ROWID, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(WAGESCALE==null?"\\N":WAGESCALE, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(RANK_ORDER_NUMBER==null?"\\N":RANK_ORDER_NUMBER, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(START_DATE==null?"\\N":"" + START_DATE, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(END_DATE==null?"\\N":"" + END_DATE, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(RATE_PER_HOUR==null?"\\N":RATE_PER_HOUR.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(LOGIN_ID==null?"\\N":LOGIN_ID, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(PROFILE_NAME==null?"\\N":PROFILE_NAME, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(UID==null?"\\N":UID, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(LAST_NAME==null?"\\N":LAST_NAME, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(FIRST_NAME==null?"\\N":FIRST_NAME, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(MIDDLE_INITIAL==null?"\\N":MIDDLE_INITIAL, delimiters));
  }
  private static final DelimiterSet __inputDelimiters = new DelimiterSet((char) 1, (char) 10, (char) 0, (char) 0, false);
  private RecordParser __parser;
  public void parse(Text __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(CharSequence __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(byte [] __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(char [] __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(ByteBuffer __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(CharBuffer __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  private void __loadFromFields(List<String> fields) {
    Iterator<String> __it = fields.listIterator();
    String __cur_str = null;
    try {
    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.FULL_NAME = null; } else {
      this.FULL_NAME = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.TELEPHONE = null; } else {
      this.TELEPHONE = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.TITLE = null; } else {
      this.TITLE = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.CUID = null; } else {
      this.CUID = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.RC_CODE = null; } else {
      this.RC_CODE = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.ORGANIZATION = null; } else {
      this.ORGANIZATION = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.PWORD = null; } else {
      this.PWORD = Integer.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.EMAIL = null; } else {
      this.EMAIL = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.LOAD_TIME = null; } else {
      this.LOAD_TIME = java.sql.Timestamp.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.MANAGER = null; } else {
      this.MANAGER = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.MANAGER_RC = null; } else {
      this.MANAGER_RC = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.SUPERVISOR = null; } else {
      this.SUPERVISOR = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.SUPERVISOR_RC = null; } else {
      this.SUPERVISOR_RC = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.DIRECTOR = null; } else {
      this.DIRECTOR = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.DIRECTOR_RC = null; } else {
      this.DIRECTOR_RC = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.DATA_SOURCE = null; } else {
      this.DATA_SOURCE = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.LEVEL_NAME = null; } else {
      this.LEVEL_NAME = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.TIME_ZONE = null; } else {
      this.TIME_ZONE = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.LEVEL_ID = null; } else {
      this.LEVEL_ID = Integer.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.SUPERVISORY_INDICATOR = null; } else {
      this.SUPERVISORY_INDICATOR = Integer.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.ROWID = null; } else {
      this.ROWID = Integer.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.WAGESCALE = null; } else {
      this.WAGESCALE = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.RANK_ORDER_NUMBER = null; } else {
      this.RANK_ORDER_NUMBER = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.START_DATE = null; } else {
      this.START_DATE = java.sql.Timestamp.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.END_DATE = null; } else {
      this.END_DATE = java.sql.Timestamp.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.RATE_PER_HOUR = null; } else {
      this.RATE_PER_HOUR = new java.math.BigDecimal(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.LOGIN_ID = null; } else {
      this.LOGIN_ID = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.PROFILE_NAME = null; } else {
      this.PROFILE_NAME = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.UID = null; } else {
      this.UID = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.LAST_NAME = null; } else {
      this.LAST_NAME = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.FIRST_NAME = null; } else {
      this.FIRST_NAME = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.MIDDLE_INITIAL = null; } else {
      this.MIDDLE_INITIAL = __cur_str;
    }

    } catch (RuntimeException e) {    throw new RuntimeException("Can't parse input data: '" + __cur_str + "'", e);    }  }

  private void __loadFromFields0(Iterator<String> __it) {
    String __cur_str = null;
    try {
    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.FULL_NAME = null; } else {
      this.FULL_NAME = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.TELEPHONE = null; } else {
      this.TELEPHONE = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.TITLE = null; } else {
      this.TITLE = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.CUID = null; } else {
      this.CUID = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.RC_CODE = null; } else {
      this.RC_CODE = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.ORGANIZATION = null; } else {
      this.ORGANIZATION = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.PWORD = null; } else {
      this.PWORD = Integer.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.EMAIL = null; } else {
      this.EMAIL = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.LOAD_TIME = null; } else {
      this.LOAD_TIME = java.sql.Timestamp.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.MANAGER = null; } else {
      this.MANAGER = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.MANAGER_RC = null; } else {
      this.MANAGER_RC = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.SUPERVISOR = null; } else {
      this.SUPERVISOR = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.SUPERVISOR_RC = null; } else {
      this.SUPERVISOR_RC = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.DIRECTOR = null; } else {
      this.DIRECTOR = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.DIRECTOR_RC = null; } else {
      this.DIRECTOR_RC = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.DATA_SOURCE = null; } else {
      this.DATA_SOURCE = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.LEVEL_NAME = null; } else {
      this.LEVEL_NAME = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.TIME_ZONE = null; } else {
      this.TIME_ZONE = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.LEVEL_ID = null; } else {
      this.LEVEL_ID = Integer.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.SUPERVISORY_INDICATOR = null; } else {
      this.SUPERVISORY_INDICATOR = Integer.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.ROWID = null; } else {
      this.ROWID = Integer.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.WAGESCALE = null; } else {
      this.WAGESCALE = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.RANK_ORDER_NUMBER = null; } else {
      this.RANK_ORDER_NUMBER = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.START_DATE = null; } else {
      this.START_DATE = java.sql.Timestamp.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.END_DATE = null; } else {
      this.END_DATE = java.sql.Timestamp.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.RATE_PER_HOUR = null; } else {
      this.RATE_PER_HOUR = new java.math.BigDecimal(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.LOGIN_ID = null; } else {
      this.LOGIN_ID = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.PROFILE_NAME = null; } else {
      this.PROFILE_NAME = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.UID = null; } else {
      this.UID = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.LAST_NAME = null; } else {
      this.LAST_NAME = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.FIRST_NAME = null; } else {
      this.FIRST_NAME = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.MIDDLE_INITIAL = null; } else {
      this.MIDDLE_INITIAL = __cur_str;
    }

    } catch (RuntimeException e) {    throw new RuntimeException("Can't parse input data: '" + __cur_str + "'", e);    }  }

  public Object clone() throws CloneNotSupportedException {
    MSOC__MSOC_EMPLOYEE o = (MSOC__MSOC_EMPLOYEE) super.clone();
    o.LOAD_TIME = (o.LOAD_TIME != null) ? (java.sql.Timestamp) o.LOAD_TIME.clone() : null;
    o.START_DATE = (o.START_DATE != null) ? (java.sql.Timestamp) o.START_DATE.clone() : null;
    o.END_DATE = (o.END_DATE != null) ? (java.sql.Timestamp) o.END_DATE.clone() : null;
    return o;
  }

  public void clone0(MSOC__MSOC_EMPLOYEE o) throws CloneNotSupportedException {
    o.LOAD_TIME = (o.LOAD_TIME != null) ? (java.sql.Timestamp) o.LOAD_TIME.clone() : null;
    o.START_DATE = (o.START_DATE != null) ? (java.sql.Timestamp) o.START_DATE.clone() : null;
    o.END_DATE = (o.END_DATE != null) ? (java.sql.Timestamp) o.END_DATE.clone() : null;
  }

  public Map<String, Object> getFieldMap() {
    Map<String, Object> __sqoop$field_map = new TreeMap<String, Object>();
    __sqoop$field_map.put("FULL_NAME", this.FULL_NAME);
    __sqoop$field_map.put("TELEPHONE", this.TELEPHONE);
    __sqoop$field_map.put("TITLE", this.TITLE);
    __sqoop$field_map.put("CUID", this.CUID);
    __sqoop$field_map.put("RC_CODE", this.RC_CODE);
    __sqoop$field_map.put("ORGANIZATION", this.ORGANIZATION);
    __sqoop$field_map.put("PWORD", this.PWORD);
    __sqoop$field_map.put("EMAIL", this.EMAIL);
    __sqoop$field_map.put("LOAD_TIME", this.LOAD_TIME);
    __sqoop$field_map.put("MANAGER", this.MANAGER);
    __sqoop$field_map.put("MANAGER_RC", this.MANAGER_RC);
    __sqoop$field_map.put("SUPERVISOR", this.SUPERVISOR);
    __sqoop$field_map.put("SUPERVISOR_RC", this.SUPERVISOR_RC);
    __sqoop$field_map.put("DIRECTOR", this.DIRECTOR);
    __sqoop$field_map.put("DIRECTOR_RC", this.DIRECTOR_RC);
    __sqoop$field_map.put("DATA_SOURCE", this.DATA_SOURCE);
    __sqoop$field_map.put("LEVEL_NAME", this.LEVEL_NAME);
    __sqoop$field_map.put("TIME_ZONE", this.TIME_ZONE);
    __sqoop$field_map.put("LEVEL_ID", this.LEVEL_ID);
    __sqoop$field_map.put("SUPERVISORY_INDICATOR", this.SUPERVISORY_INDICATOR);
    __sqoop$field_map.put("ROWID", this.ROWID);
    __sqoop$field_map.put("WAGESCALE", this.WAGESCALE);
    __sqoop$field_map.put("RANK_ORDER_NUMBER", this.RANK_ORDER_NUMBER);
    __sqoop$field_map.put("START_DATE", this.START_DATE);
    __sqoop$field_map.put("END_DATE", this.END_DATE);
    __sqoop$field_map.put("RATE_PER_HOUR", this.RATE_PER_HOUR);
    __sqoop$field_map.put("LOGIN_ID", this.LOGIN_ID);
    __sqoop$field_map.put("PROFILE_NAME", this.PROFILE_NAME);
    __sqoop$field_map.put("UID", this.UID);
    __sqoop$field_map.put("LAST_NAME", this.LAST_NAME);
    __sqoop$field_map.put("FIRST_NAME", this.FIRST_NAME);
    __sqoop$field_map.put("MIDDLE_INITIAL", this.MIDDLE_INITIAL);
    return __sqoop$field_map;
  }

  public void getFieldMap0(Map<String, Object> __sqoop$field_map) {
    __sqoop$field_map.put("FULL_NAME", this.FULL_NAME);
    __sqoop$field_map.put("TELEPHONE", this.TELEPHONE);
    __sqoop$field_map.put("TITLE", this.TITLE);
    __sqoop$field_map.put("CUID", this.CUID);
    __sqoop$field_map.put("RC_CODE", this.RC_CODE);
    __sqoop$field_map.put("ORGANIZATION", this.ORGANIZATION);
    __sqoop$field_map.put("PWORD", this.PWORD);
    __sqoop$field_map.put("EMAIL", this.EMAIL);
    __sqoop$field_map.put("LOAD_TIME", this.LOAD_TIME);
    __sqoop$field_map.put("MANAGER", this.MANAGER);
    __sqoop$field_map.put("MANAGER_RC", this.MANAGER_RC);
    __sqoop$field_map.put("SUPERVISOR", this.SUPERVISOR);
    __sqoop$field_map.put("SUPERVISOR_RC", this.SUPERVISOR_RC);
    __sqoop$field_map.put("DIRECTOR", this.DIRECTOR);
    __sqoop$field_map.put("DIRECTOR_RC", this.DIRECTOR_RC);
    __sqoop$field_map.put("DATA_SOURCE", this.DATA_SOURCE);
    __sqoop$field_map.put("LEVEL_NAME", this.LEVEL_NAME);
    __sqoop$field_map.put("TIME_ZONE", this.TIME_ZONE);
    __sqoop$field_map.put("LEVEL_ID", this.LEVEL_ID);
    __sqoop$field_map.put("SUPERVISORY_INDICATOR", this.SUPERVISORY_INDICATOR);
    __sqoop$field_map.put("ROWID", this.ROWID);
    __sqoop$field_map.put("WAGESCALE", this.WAGESCALE);
    __sqoop$field_map.put("RANK_ORDER_NUMBER", this.RANK_ORDER_NUMBER);
    __sqoop$field_map.put("START_DATE", this.START_DATE);
    __sqoop$field_map.put("END_DATE", this.END_DATE);
    __sqoop$field_map.put("RATE_PER_HOUR", this.RATE_PER_HOUR);
    __sqoop$field_map.put("LOGIN_ID", this.LOGIN_ID);
    __sqoop$field_map.put("PROFILE_NAME", this.PROFILE_NAME);
    __sqoop$field_map.put("UID", this.UID);
    __sqoop$field_map.put("LAST_NAME", this.LAST_NAME);
    __sqoop$field_map.put("FIRST_NAME", this.FIRST_NAME);
    __sqoop$field_map.put("MIDDLE_INITIAL", this.MIDDLE_INITIAL);
  }

  public void setField(String __fieldName, Object __fieldVal) {
    if ("FULL_NAME".equals(__fieldName)) {
      this.FULL_NAME = (String) __fieldVal;
    }
    else    if ("TELEPHONE".equals(__fieldName)) {
      this.TELEPHONE = (String) __fieldVal;
    }
    else    if ("TITLE".equals(__fieldName)) {
      this.TITLE = (String) __fieldVal;
    }
    else    if ("CUID".equals(__fieldName)) {
      this.CUID = (String) __fieldVal;
    }
    else    if ("RC_CODE".equals(__fieldName)) {
      this.RC_CODE = (String) __fieldVal;
    }
    else    if ("ORGANIZATION".equals(__fieldName)) {
      this.ORGANIZATION = (String) __fieldVal;
    }
    else    if ("PWORD".equals(__fieldName)) {
      this.PWORD = (Integer) __fieldVal;
    }
    else    if ("EMAIL".equals(__fieldName)) {
      this.EMAIL = (String) __fieldVal;
    }
    else    if ("LOAD_TIME".equals(__fieldName)) {
      this.LOAD_TIME = (java.sql.Timestamp) __fieldVal;
    }
    else    if ("MANAGER".equals(__fieldName)) {
      this.MANAGER = (String) __fieldVal;
    }
    else    if ("MANAGER_RC".equals(__fieldName)) {
      this.MANAGER_RC = (String) __fieldVal;
    }
    else    if ("SUPERVISOR".equals(__fieldName)) {
      this.SUPERVISOR = (String) __fieldVal;
    }
    else    if ("SUPERVISOR_RC".equals(__fieldName)) {
      this.SUPERVISOR_RC = (String) __fieldVal;
    }
    else    if ("DIRECTOR".equals(__fieldName)) {
      this.DIRECTOR = (String) __fieldVal;
    }
    else    if ("DIRECTOR_RC".equals(__fieldName)) {
      this.DIRECTOR_RC = (String) __fieldVal;
    }
    else    if ("DATA_SOURCE".equals(__fieldName)) {
      this.DATA_SOURCE = (String) __fieldVal;
    }
    else    if ("LEVEL_NAME".equals(__fieldName)) {
      this.LEVEL_NAME = (String) __fieldVal;
    }
    else    if ("TIME_ZONE".equals(__fieldName)) {
      this.TIME_ZONE = (String) __fieldVal;
    }
    else    if ("LEVEL_ID".equals(__fieldName)) {
      this.LEVEL_ID = (Integer) __fieldVal;
    }
    else    if ("SUPERVISORY_INDICATOR".equals(__fieldName)) {
      this.SUPERVISORY_INDICATOR = (Integer) __fieldVal;
    }
    else    if ("ROWID".equals(__fieldName)) {
      this.ROWID = (Integer) __fieldVal;
    }
    else    if ("WAGESCALE".equals(__fieldName)) {
      this.WAGESCALE = (String) __fieldVal;
    }
    else    if ("RANK_ORDER_NUMBER".equals(__fieldName)) {
      this.RANK_ORDER_NUMBER = (String) __fieldVal;
    }
    else    if ("START_DATE".equals(__fieldName)) {
      this.START_DATE = (java.sql.Timestamp) __fieldVal;
    }
    else    if ("END_DATE".equals(__fieldName)) {
      this.END_DATE = (java.sql.Timestamp) __fieldVal;
    }
    else    if ("RATE_PER_HOUR".equals(__fieldName)) {
      this.RATE_PER_HOUR = (java.math.BigDecimal) __fieldVal;
    }
    else    if ("LOGIN_ID".equals(__fieldName)) {
      this.LOGIN_ID = (String) __fieldVal;
    }
    else    if ("PROFILE_NAME".equals(__fieldName)) {
      this.PROFILE_NAME = (String) __fieldVal;
    }
    else    if ("UID".equals(__fieldName)) {
      this.UID = (String) __fieldVal;
    }
    else    if ("LAST_NAME".equals(__fieldName)) {
      this.LAST_NAME = (String) __fieldVal;
    }
    else    if ("FIRST_NAME".equals(__fieldName)) {
      this.FIRST_NAME = (String) __fieldVal;
    }
    else    if ("MIDDLE_INITIAL".equals(__fieldName)) {
      this.MIDDLE_INITIAL = (String) __fieldVal;
    }
    else {
      throw new RuntimeException("No such field: " + __fieldName);
    }
  }
  public boolean setField0(String __fieldName, Object __fieldVal) {
    if ("FULL_NAME".equals(__fieldName)) {
      this.FULL_NAME = (String) __fieldVal;
      return true;
    }
    else    if ("TELEPHONE".equals(__fieldName)) {
      this.TELEPHONE = (String) __fieldVal;
      return true;
    }
    else    if ("TITLE".equals(__fieldName)) {
      this.TITLE = (String) __fieldVal;
      return true;
    }
    else    if ("CUID".equals(__fieldName)) {
      this.CUID = (String) __fieldVal;
      return true;
    }
    else    if ("RC_CODE".equals(__fieldName)) {
      this.RC_CODE = (String) __fieldVal;
      return true;
    }
    else    if ("ORGANIZATION".equals(__fieldName)) {
      this.ORGANIZATION = (String) __fieldVal;
      return true;
    }
    else    if ("PWORD".equals(__fieldName)) {
      this.PWORD = (Integer) __fieldVal;
      return true;
    }
    else    if ("EMAIL".equals(__fieldName)) {
      this.EMAIL = (String) __fieldVal;
      return true;
    }
    else    if ("LOAD_TIME".equals(__fieldName)) {
      this.LOAD_TIME = (java.sql.Timestamp) __fieldVal;
      return true;
    }
    else    if ("MANAGER".equals(__fieldName)) {
      this.MANAGER = (String) __fieldVal;
      return true;
    }
    else    if ("MANAGER_RC".equals(__fieldName)) {
      this.MANAGER_RC = (String) __fieldVal;
      return true;
    }
    else    if ("SUPERVISOR".equals(__fieldName)) {
      this.SUPERVISOR = (String) __fieldVal;
      return true;
    }
    else    if ("SUPERVISOR_RC".equals(__fieldName)) {
      this.SUPERVISOR_RC = (String) __fieldVal;
      return true;
    }
    else    if ("DIRECTOR".equals(__fieldName)) {
      this.DIRECTOR = (String) __fieldVal;
      return true;
    }
    else    if ("DIRECTOR_RC".equals(__fieldName)) {
      this.DIRECTOR_RC = (String) __fieldVal;
      return true;
    }
    else    if ("DATA_SOURCE".equals(__fieldName)) {
      this.DATA_SOURCE = (String) __fieldVal;
      return true;
    }
    else    if ("LEVEL_NAME".equals(__fieldName)) {
      this.LEVEL_NAME = (String) __fieldVal;
      return true;
    }
    else    if ("TIME_ZONE".equals(__fieldName)) {
      this.TIME_ZONE = (String) __fieldVal;
      return true;
    }
    else    if ("LEVEL_ID".equals(__fieldName)) {
      this.LEVEL_ID = (Integer) __fieldVal;
      return true;
    }
    else    if ("SUPERVISORY_INDICATOR".equals(__fieldName)) {
      this.SUPERVISORY_INDICATOR = (Integer) __fieldVal;
      return true;
    }
    else    if ("ROWID".equals(__fieldName)) {
      this.ROWID = (Integer) __fieldVal;
      return true;
    }
    else    if ("WAGESCALE".equals(__fieldName)) {
      this.WAGESCALE = (String) __fieldVal;
      return true;
    }
    else    if ("RANK_ORDER_NUMBER".equals(__fieldName)) {
      this.RANK_ORDER_NUMBER = (String) __fieldVal;
      return true;
    }
    else    if ("START_DATE".equals(__fieldName)) {
      this.START_DATE = (java.sql.Timestamp) __fieldVal;
      return true;
    }
    else    if ("END_DATE".equals(__fieldName)) {
      this.END_DATE = (java.sql.Timestamp) __fieldVal;
      return true;
    }
    else    if ("RATE_PER_HOUR".equals(__fieldName)) {
      this.RATE_PER_HOUR = (java.math.BigDecimal) __fieldVal;
      return true;
    }
    else    if ("LOGIN_ID".equals(__fieldName)) {
      this.LOGIN_ID = (String) __fieldVal;
      return true;
    }
    else    if ("PROFILE_NAME".equals(__fieldName)) {
      this.PROFILE_NAME = (String) __fieldVal;
      return true;
    }
    else    if ("UID".equals(__fieldName)) {
      this.UID = (String) __fieldVal;
      return true;
    }
    else    if ("LAST_NAME".equals(__fieldName)) {
      this.LAST_NAME = (String) __fieldVal;
      return true;
    }
    else    if ("FIRST_NAME".equals(__fieldName)) {
      this.FIRST_NAME = (String) __fieldVal;
      return true;
    }
    else    if ("MIDDLE_INITIAL".equals(__fieldName)) {
      this.MIDDLE_INITIAL = (String) __fieldVal;
      return true;
    }
    else {
      return false;    }
  }
}
