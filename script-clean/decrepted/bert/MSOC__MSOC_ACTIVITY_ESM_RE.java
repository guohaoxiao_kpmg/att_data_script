// ORM class for table 'MSOC..MSOC_ACTIVITY_ESM_RE'
// WARNING: This class is AUTO-GENERATED. Modify at your own risk.
//
// Debug information:
// Generated date: Thu Feb 25 13:24:57 EST 2016
// For connector: org.apache.sqoop.manager.GenericJdbcManager
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapred.lib.db.DBWritable;
import com.cloudera.sqoop.lib.JdbcWritableBridge;
import com.cloudera.sqoop.lib.DelimiterSet;
import com.cloudera.sqoop.lib.FieldFormatter;
import com.cloudera.sqoop.lib.RecordParser;
import com.cloudera.sqoop.lib.BooleanParser;
import com.cloudera.sqoop.lib.BlobRef;
import com.cloudera.sqoop.lib.ClobRef;
import com.cloudera.sqoop.lib.LargeObjectLoader;
import com.cloudera.sqoop.lib.SqoopRecord;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class MSOC__MSOC_ACTIVITY_ESM_RE extends SqoopRecord  implements DBWritable, Writable {
  private final int PROTOCOL_VERSION = 3;
  public int getClassFormatVersion() { return PROTOCOL_VERSION; }
  protected ResultSet __cur_result_set;
  private Integer ROWID;
  public Integer get_ROWID() {
    return ROWID;
  }
  public void set_ROWID(Integer ROWID) {
    this.ROWID = ROWID;
  }
  public MSOC__MSOC_ACTIVITY_ESM_RE with_ROWID(Integer ROWID) {
    this.ROWID = ROWID;
    return this;
  }
  private Integer ACTIVITYID;
  public Integer get_ACTIVITYID() {
    return ACTIVITYID;
  }
  public void set_ACTIVITYID(Integer ACTIVITYID) {
    this.ACTIVITYID = ACTIVITYID;
  }
  public MSOC__MSOC_ACTIVITY_ESM_RE with_ACTIVITYID(Integer ACTIVITYID) {
    this.ACTIVITYID = ACTIVITYID;
    return this;
  }
  private java.math.BigDecimal ESM;
  public java.math.BigDecimal get_ESM() {
    return ESM;
  }
  public void set_ESM(java.math.BigDecimal ESM) {
    this.ESM = ESM;
  }
  public MSOC__MSOC_ACTIVITY_ESM_RE with_ESM(java.math.BigDecimal ESM) {
    this.ESM = ESM;
    return this;
  }
  private java.math.BigDecimal RE;
  public java.math.BigDecimal get_RE() {
    return RE;
  }
  public void set_RE(java.math.BigDecimal RE) {
    this.RE = RE;
  }
  public MSOC__MSOC_ACTIVITY_ESM_RE with_RE(java.math.BigDecimal RE) {
    this.RE = RE;
    return this;
  }
  private String ESM_USED;
  public String get_ESM_USED() {
    return ESM_USED;
  }
  public void set_ESM_USED(String ESM_USED) {
    this.ESM_USED = ESM_USED;
  }
  public MSOC__MSOC_ACTIVITY_ESM_RE with_ESM_USED(String ESM_USED) {
    this.ESM_USED = ESM_USED;
    return this;
  }
  private java.math.BigDecimal MEASURE_USED;
  public java.math.BigDecimal get_MEASURE_USED() {
    return MEASURE_USED;
  }
  public void set_MEASURE_USED(java.math.BigDecimal MEASURE_USED) {
    this.MEASURE_USED = MEASURE_USED;
  }
  public MSOC__MSOC_ACTIVITY_ESM_RE with_MEASURE_USED(java.math.BigDecimal MEASURE_USED) {
    this.MEASURE_USED = MEASURE_USED;
    return this;
  }
  private java.sql.Timestamp START_DATE;
  public java.sql.Timestamp get_START_DATE() {
    return START_DATE;
  }
  public void set_START_DATE(java.sql.Timestamp START_DATE) {
    this.START_DATE = START_DATE;
  }
  public MSOC__MSOC_ACTIVITY_ESM_RE with_START_DATE(java.sql.Timestamp START_DATE) {
    this.START_DATE = START_DATE;
    return this;
  }
  private java.sql.Timestamp END_DATE;
  public java.sql.Timestamp get_END_DATE() {
    return END_DATE;
  }
  public void set_END_DATE(java.sql.Timestamp END_DATE) {
    this.END_DATE = END_DATE;
  }
  public MSOC__MSOC_ACTIVITY_ESM_RE with_END_DATE(java.sql.Timestamp END_DATE) {
    this.END_DATE = END_DATE;
    return this;
  }
  private String SUPERVISOR_ESTIMATE;
  public String get_SUPERVISOR_ESTIMATE() {
    return SUPERVISOR_ESTIMATE;
  }
  public void set_SUPERVISOR_ESTIMATE(String SUPERVISOR_ESTIMATE) {
    this.SUPERVISOR_ESTIMATE = SUPERVISOR_ESTIMATE;
  }
  public MSOC__MSOC_ACTIVITY_ESM_RE with_SUPERVISOR_ESTIMATE(String SUPERVISOR_ESTIMATE) {
    this.SUPERVISOR_ESTIMATE = SUPERVISOR_ESTIMATE;
    return this;
  }
  private java.sql.Timestamp SUPERVISOR_ESTIMATE_CHANGE_DATE;
  public java.sql.Timestamp get_SUPERVISOR_ESTIMATE_CHANGE_DATE() {
    return SUPERVISOR_ESTIMATE_CHANGE_DATE;
  }
  public void set_SUPERVISOR_ESTIMATE_CHANGE_DATE(java.sql.Timestamp SUPERVISOR_ESTIMATE_CHANGE_DATE) {
    this.SUPERVISOR_ESTIMATE_CHANGE_DATE = SUPERVISOR_ESTIMATE_CHANGE_DATE;
  }
  public MSOC__MSOC_ACTIVITY_ESM_RE with_SUPERVISOR_ESTIMATE_CHANGE_DATE(java.sql.Timestamp SUPERVISOR_ESTIMATE_CHANGE_DATE) {
    this.SUPERVISOR_ESTIMATE_CHANGE_DATE = SUPERVISOR_ESTIMATE_CHANGE_DATE;
    return this;
  }
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof MSOC__MSOC_ACTIVITY_ESM_RE)) {
      return false;
    }
    MSOC__MSOC_ACTIVITY_ESM_RE that = (MSOC__MSOC_ACTIVITY_ESM_RE) o;
    boolean equal = true;
    equal = equal && (this.ROWID == null ? that.ROWID == null : this.ROWID.equals(that.ROWID));
    equal = equal && (this.ACTIVITYID == null ? that.ACTIVITYID == null : this.ACTIVITYID.equals(that.ACTIVITYID));
    equal = equal && (this.ESM == null ? that.ESM == null : this.ESM.equals(that.ESM));
    equal = equal && (this.RE == null ? that.RE == null : this.RE.equals(that.RE));
    equal = equal && (this.ESM_USED == null ? that.ESM_USED == null : this.ESM_USED.equals(that.ESM_USED));
    equal = equal && (this.MEASURE_USED == null ? that.MEASURE_USED == null : this.MEASURE_USED.equals(that.MEASURE_USED));
    equal = equal && (this.START_DATE == null ? that.START_DATE == null : this.START_DATE.equals(that.START_DATE));
    equal = equal && (this.END_DATE == null ? that.END_DATE == null : this.END_DATE.equals(that.END_DATE));
    equal = equal && (this.SUPERVISOR_ESTIMATE == null ? that.SUPERVISOR_ESTIMATE == null : this.SUPERVISOR_ESTIMATE.equals(that.SUPERVISOR_ESTIMATE));
    equal = equal && (this.SUPERVISOR_ESTIMATE_CHANGE_DATE == null ? that.SUPERVISOR_ESTIMATE_CHANGE_DATE == null : this.SUPERVISOR_ESTIMATE_CHANGE_DATE.equals(that.SUPERVISOR_ESTIMATE_CHANGE_DATE));
    return equal;
  }
  public boolean equals0(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof MSOC__MSOC_ACTIVITY_ESM_RE)) {
      return false;
    }
    MSOC__MSOC_ACTIVITY_ESM_RE that = (MSOC__MSOC_ACTIVITY_ESM_RE) o;
    boolean equal = true;
    equal = equal && (this.ROWID == null ? that.ROWID == null : this.ROWID.equals(that.ROWID));
    equal = equal && (this.ACTIVITYID == null ? that.ACTIVITYID == null : this.ACTIVITYID.equals(that.ACTIVITYID));
    equal = equal && (this.ESM == null ? that.ESM == null : this.ESM.equals(that.ESM));
    equal = equal && (this.RE == null ? that.RE == null : this.RE.equals(that.RE));
    equal = equal && (this.ESM_USED == null ? that.ESM_USED == null : this.ESM_USED.equals(that.ESM_USED));
    equal = equal && (this.MEASURE_USED == null ? that.MEASURE_USED == null : this.MEASURE_USED.equals(that.MEASURE_USED));
    equal = equal && (this.START_DATE == null ? that.START_DATE == null : this.START_DATE.equals(that.START_DATE));
    equal = equal && (this.END_DATE == null ? that.END_DATE == null : this.END_DATE.equals(that.END_DATE));
    equal = equal && (this.SUPERVISOR_ESTIMATE == null ? that.SUPERVISOR_ESTIMATE == null : this.SUPERVISOR_ESTIMATE.equals(that.SUPERVISOR_ESTIMATE));
    equal = equal && (this.SUPERVISOR_ESTIMATE_CHANGE_DATE == null ? that.SUPERVISOR_ESTIMATE_CHANGE_DATE == null : this.SUPERVISOR_ESTIMATE_CHANGE_DATE.equals(that.SUPERVISOR_ESTIMATE_CHANGE_DATE));
    return equal;
  }
  public void readFields(ResultSet __dbResults) throws SQLException {
    this.__cur_result_set = __dbResults;
    this.ROWID = JdbcWritableBridge.readInteger(1, __dbResults);
    this.ACTIVITYID = JdbcWritableBridge.readInteger(2, __dbResults);
    this.ESM = JdbcWritableBridge.readBigDecimal(3, __dbResults);
    this.RE = JdbcWritableBridge.readBigDecimal(4, __dbResults);
    this.ESM_USED = JdbcWritableBridge.readString(5, __dbResults);
    this.MEASURE_USED = JdbcWritableBridge.readBigDecimal(6, __dbResults);
    this.START_DATE = JdbcWritableBridge.readTimestamp(7, __dbResults);
    this.END_DATE = JdbcWritableBridge.readTimestamp(8, __dbResults);
    this.SUPERVISOR_ESTIMATE = JdbcWritableBridge.readString(9, __dbResults);
    this.SUPERVISOR_ESTIMATE_CHANGE_DATE = JdbcWritableBridge.readTimestamp(10, __dbResults);
  }
  public void readFields0(ResultSet __dbResults) throws SQLException {
    this.ROWID = JdbcWritableBridge.readInteger(1, __dbResults);
    this.ACTIVITYID = JdbcWritableBridge.readInteger(2, __dbResults);
    this.ESM = JdbcWritableBridge.readBigDecimal(3, __dbResults);
    this.RE = JdbcWritableBridge.readBigDecimal(4, __dbResults);
    this.ESM_USED = JdbcWritableBridge.readString(5, __dbResults);
    this.MEASURE_USED = JdbcWritableBridge.readBigDecimal(6, __dbResults);
    this.START_DATE = JdbcWritableBridge.readTimestamp(7, __dbResults);
    this.END_DATE = JdbcWritableBridge.readTimestamp(8, __dbResults);
    this.SUPERVISOR_ESTIMATE = JdbcWritableBridge.readString(9, __dbResults);
    this.SUPERVISOR_ESTIMATE_CHANGE_DATE = JdbcWritableBridge.readTimestamp(10, __dbResults);
  }
  public void loadLargeObjects(LargeObjectLoader __loader)
      throws SQLException, IOException, InterruptedException {
  }
  public void loadLargeObjects0(LargeObjectLoader __loader)
      throws SQLException, IOException, InterruptedException {
  }
  public void write(PreparedStatement __dbStmt) throws SQLException {
    write(__dbStmt, 0);
  }

  public int write(PreparedStatement __dbStmt, int __off) throws SQLException {
    JdbcWritableBridge.writeInteger(ROWID, 1 + __off, 4, __dbStmt);
    JdbcWritableBridge.writeInteger(ACTIVITYID, 2 + __off, 4, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(ESM, 3 + __off, 3, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(RE, 4 + __off, 3, __dbStmt);
    JdbcWritableBridge.writeString(ESM_USED, 5 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(MEASURE_USED, 6 + __off, 3, __dbStmt);
    JdbcWritableBridge.writeTimestamp(START_DATE, 7 + __off, 93, __dbStmt);
    JdbcWritableBridge.writeTimestamp(END_DATE, 8 + __off, 93, __dbStmt);
    JdbcWritableBridge.writeString(SUPERVISOR_ESTIMATE, 9 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeTimestamp(SUPERVISOR_ESTIMATE_CHANGE_DATE, 10 + __off, 93, __dbStmt);
    return 10;
  }
  public void write0(PreparedStatement __dbStmt, int __off) throws SQLException {
    JdbcWritableBridge.writeInteger(ROWID, 1 + __off, 4, __dbStmt);
    JdbcWritableBridge.writeInteger(ACTIVITYID, 2 + __off, 4, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(ESM, 3 + __off, 3, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(RE, 4 + __off, 3, __dbStmt);
    JdbcWritableBridge.writeString(ESM_USED, 5 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(MEASURE_USED, 6 + __off, 3, __dbStmt);
    JdbcWritableBridge.writeTimestamp(START_DATE, 7 + __off, 93, __dbStmt);
    JdbcWritableBridge.writeTimestamp(END_DATE, 8 + __off, 93, __dbStmt);
    JdbcWritableBridge.writeString(SUPERVISOR_ESTIMATE, 9 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeTimestamp(SUPERVISOR_ESTIMATE_CHANGE_DATE, 10 + __off, 93, __dbStmt);
  }
  public void readFields(DataInput __dataIn) throws IOException {
this.readFields0(__dataIn);  }
  public void readFields0(DataInput __dataIn) throws IOException {
    if (__dataIn.readBoolean()) { 
        this.ROWID = null;
    } else {
    this.ROWID = Integer.valueOf(__dataIn.readInt());
    }
    if (__dataIn.readBoolean()) { 
        this.ACTIVITYID = null;
    } else {
    this.ACTIVITYID = Integer.valueOf(__dataIn.readInt());
    }
    if (__dataIn.readBoolean()) { 
        this.ESM = null;
    } else {
    this.ESM = com.cloudera.sqoop.lib.BigDecimalSerializer.readFields(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.RE = null;
    } else {
    this.RE = com.cloudera.sqoop.lib.BigDecimalSerializer.readFields(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.ESM_USED = null;
    } else {
    this.ESM_USED = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.MEASURE_USED = null;
    } else {
    this.MEASURE_USED = com.cloudera.sqoop.lib.BigDecimalSerializer.readFields(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.START_DATE = null;
    } else {
    this.START_DATE = new Timestamp(__dataIn.readLong());
    this.START_DATE.setNanos(__dataIn.readInt());
    }
    if (__dataIn.readBoolean()) { 
        this.END_DATE = null;
    } else {
    this.END_DATE = new Timestamp(__dataIn.readLong());
    this.END_DATE.setNanos(__dataIn.readInt());
    }
    if (__dataIn.readBoolean()) { 
        this.SUPERVISOR_ESTIMATE = null;
    } else {
    this.SUPERVISOR_ESTIMATE = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.SUPERVISOR_ESTIMATE_CHANGE_DATE = null;
    } else {
    this.SUPERVISOR_ESTIMATE_CHANGE_DATE = new Timestamp(__dataIn.readLong());
    this.SUPERVISOR_ESTIMATE_CHANGE_DATE.setNanos(__dataIn.readInt());
    }
  }
  public void write(DataOutput __dataOut) throws IOException {
    if (null == this.ROWID) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.ROWID);
    }
    if (null == this.ACTIVITYID) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.ACTIVITYID);
    }
    if (null == this.ESM) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.ESM, __dataOut);
    }
    if (null == this.RE) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.RE, __dataOut);
    }
    if (null == this.ESM_USED) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, ESM_USED);
    }
    if (null == this.MEASURE_USED) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.MEASURE_USED, __dataOut);
    }
    if (null == this.START_DATE) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.START_DATE.getTime());
    __dataOut.writeInt(this.START_DATE.getNanos());
    }
    if (null == this.END_DATE) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.END_DATE.getTime());
    __dataOut.writeInt(this.END_DATE.getNanos());
    }
    if (null == this.SUPERVISOR_ESTIMATE) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, SUPERVISOR_ESTIMATE);
    }
    if (null == this.SUPERVISOR_ESTIMATE_CHANGE_DATE) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.SUPERVISOR_ESTIMATE_CHANGE_DATE.getTime());
    __dataOut.writeInt(this.SUPERVISOR_ESTIMATE_CHANGE_DATE.getNanos());
    }
  }
  public void write0(DataOutput __dataOut) throws IOException {
    if (null == this.ROWID) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.ROWID);
    }
    if (null == this.ACTIVITYID) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.ACTIVITYID);
    }
    if (null == this.ESM) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.ESM, __dataOut);
    }
    if (null == this.RE) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.RE, __dataOut);
    }
    if (null == this.ESM_USED) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, ESM_USED);
    }
    if (null == this.MEASURE_USED) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.MEASURE_USED, __dataOut);
    }
    if (null == this.START_DATE) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.START_DATE.getTime());
    __dataOut.writeInt(this.START_DATE.getNanos());
    }
    if (null == this.END_DATE) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.END_DATE.getTime());
    __dataOut.writeInt(this.END_DATE.getNanos());
    }
    if (null == this.SUPERVISOR_ESTIMATE) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, SUPERVISOR_ESTIMATE);
    }
    if (null == this.SUPERVISOR_ESTIMATE_CHANGE_DATE) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.SUPERVISOR_ESTIMATE_CHANGE_DATE.getTime());
    __dataOut.writeInt(this.SUPERVISOR_ESTIMATE_CHANGE_DATE.getNanos());
    }
  }
  private static final DelimiterSet __outputDelimiters = new DelimiterSet((char) 1, (char) 10, (char) 0, (char) 0, false);
  public String toString() {
    return toString(__outputDelimiters, true);
  }
  public String toString(DelimiterSet delimiters) {
    return toString(delimiters, true);
  }
  public String toString(boolean useRecordDelim) {
    return toString(__outputDelimiters, useRecordDelim);
  }
  public String toString(DelimiterSet delimiters, boolean useRecordDelim) {
    StringBuilder __sb = new StringBuilder();
    char fieldDelim = delimiters.getFieldsTerminatedBy();
    __sb.append(FieldFormatter.escapeAndEnclose(ROWID==null?"\\N":"" + ROWID, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(ACTIVITYID==null?"\\N":"" + ACTIVITYID, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(ESM==null?"\\N":ESM.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(RE==null?"\\N":RE.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(ESM_USED==null?"\\N":ESM_USED, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(MEASURE_USED==null?"\\N":MEASURE_USED.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(START_DATE==null?"\\N":"" + START_DATE, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(END_DATE==null?"\\N":"" + END_DATE, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(SUPERVISOR_ESTIMATE==null?"\\N":SUPERVISOR_ESTIMATE, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(SUPERVISOR_ESTIMATE_CHANGE_DATE==null?"\\N":"" + SUPERVISOR_ESTIMATE_CHANGE_DATE, delimiters));
    if (useRecordDelim) {
      __sb.append(delimiters.getLinesTerminatedBy());
    }
    return __sb.toString();
  }
  public void toString0(DelimiterSet delimiters, StringBuilder __sb, char fieldDelim) {
    __sb.append(FieldFormatter.escapeAndEnclose(ROWID==null?"\\N":"" + ROWID, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(ACTIVITYID==null?"\\N":"" + ACTIVITYID, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(ESM==null?"\\N":ESM.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(RE==null?"\\N":RE.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(ESM_USED==null?"\\N":ESM_USED, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(MEASURE_USED==null?"\\N":MEASURE_USED.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(START_DATE==null?"\\N":"" + START_DATE, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(END_DATE==null?"\\N":"" + END_DATE, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(SUPERVISOR_ESTIMATE==null?"\\N":SUPERVISOR_ESTIMATE, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(SUPERVISOR_ESTIMATE_CHANGE_DATE==null?"\\N":"" + SUPERVISOR_ESTIMATE_CHANGE_DATE, delimiters));
  }
  private static final DelimiterSet __inputDelimiters = new DelimiterSet((char) 1, (char) 10, (char) 0, (char) 0, false);
  private RecordParser __parser;
  public void parse(Text __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(CharSequence __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(byte [] __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(char [] __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(ByteBuffer __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(CharBuffer __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  private void __loadFromFields(List<String> fields) {
    Iterator<String> __it = fields.listIterator();
    String __cur_str = null;
    try {
    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.ROWID = null; } else {
      this.ROWID = Integer.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.ACTIVITYID = null; } else {
      this.ACTIVITYID = Integer.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.ESM = null; } else {
      this.ESM = new java.math.BigDecimal(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.RE = null; } else {
      this.RE = new java.math.BigDecimal(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.ESM_USED = null; } else {
      this.ESM_USED = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.MEASURE_USED = null; } else {
      this.MEASURE_USED = new java.math.BigDecimal(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.START_DATE = null; } else {
      this.START_DATE = java.sql.Timestamp.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.END_DATE = null; } else {
      this.END_DATE = java.sql.Timestamp.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.SUPERVISOR_ESTIMATE = null; } else {
      this.SUPERVISOR_ESTIMATE = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.SUPERVISOR_ESTIMATE_CHANGE_DATE = null; } else {
      this.SUPERVISOR_ESTIMATE_CHANGE_DATE = java.sql.Timestamp.valueOf(__cur_str);
    }

    } catch (RuntimeException e) {    throw new RuntimeException("Can't parse input data: '" + __cur_str + "'", e);    }  }

  private void __loadFromFields0(Iterator<String> __it) {
    String __cur_str = null;
    try {
    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.ROWID = null; } else {
      this.ROWID = Integer.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.ACTIVITYID = null; } else {
      this.ACTIVITYID = Integer.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.ESM = null; } else {
      this.ESM = new java.math.BigDecimal(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.RE = null; } else {
      this.RE = new java.math.BigDecimal(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.ESM_USED = null; } else {
      this.ESM_USED = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.MEASURE_USED = null; } else {
      this.MEASURE_USED = new java.math.BigDecimal(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.START_DATE = null; } else {
      this.START_DATE = java.sql.Timestamp.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.END_DATE = null; } else {
      this.END_DATE = java.sql.Timestamp.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.SUPERVISOR_ESTIMATE = null; } else {
      this.SUPERVISOR_ESTIMATE = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.SUPERVISOR_ESTIMATE_CHANGE_DATE = null; } else {
      this.SUPERVISOR_ESTIMATE_CHANGE_DATE = java.sql.Timestamp.valueOf(__cur_str);
    }

    } catch (RuntimeException e) {    throw new RuntimeException("Can't parse input data: '" + __cur_str + "'", e);    }  }

  public Object clone() throws CloneNotSupportedException {
    MSOC__MSOC_ACTIVITY_ESM_RE o = (MSOC__MSOC_ACTIVITY_ESM_RE) super.clone();
    o.START_DATE = (o.START_DATE != null) ? (java.sql.Timestamp) o.START_DATE.clone() : null;
    o.END_DATE = (o.END_DATE != null) ? (java.sql.Timestamp) o.END_DATE.clone() : null;
    o.SUPERVISOR_ESTIMATE_CHANGE_DATE = (o.SUPERVISOR_ESTIMATE_CHANGE_DATE != null) ? (java.sql.Timestamp) o.SUPERVISOR_ESTIMATE_CHANGE_DATE.clone() : null;
    return o;
  }

  public void clone0(MSOC__MSOC_ACTIVITY_ESM_RE o) throws CloneNotSupportedException {
    o.START_DATE = (o.START_DATE != null) ? (java.sql.Timestamp) o.START_DATE.clone() : null;
    o.END_DATE = (o.END_DATE != null) ? (java.sql.Timestamp) o.END_DATE.clone() : null;
    o.SUPERVISOR_ESTIMATE_CHANGE_DATE = (o.SUPERVISOR_ESTIMATE_CHANGE_DATE != null) ? (java.sql.Timestamp) o.SUPERVISOR_ESTIMATE_CHANGE_DATE.clone() : null;
  }

  public Map<String, Object> getFieldMap() {
    Map<String, Object> __sqoop$field_map = new TreeMap<String, Object>();
    __sqoop$field_map.put("ROWID", this.ROWID);
    __sqoop$field_map.put("ACTIVITYID", this.ACTIVITYID);
    __sqoop$field_map.put("ESM", this.ESM);
    __sqoop$field_map.put("RE", this.RE);
    __sqoop$field_map.put("ESM_USED", this.ESM_USED);
    __sqoop$field_map.put("MEASURE_USED", this.MEASURE_USED);
    __sqoop$field_map.put("START_DATE", this.START_DATE);
    __sqoop$field_map.put("END_DATE", this.END_DATE);
    __sqoop$field_map.put("SUPERVISOR_ESTIMATE", this.SUPERVISOR_ESTIMATE);
    __sqoop$field_map.put("SUPERVISOR_ESTIMATE_CHANGE_DATE", this.SUPERVISOR_ESTIMATE_CHANGE_DATE);
    return __sqoop$field_map;
  }

  public void getFieldMap0(Map<String, Object> __sqoop$field_map) {
    __sqoop$field_map.put("ROWID", this.ROWID);
    __sqoop$field_map.put("ACTIVITYID", this.ACTIVITYID);
    __sqoop$field_map.put("ESM", this.ESM);
    __sqoop$field_map.put("RE", this.RE);
    __sqoop$field_map.put("ESM_USED", this.ESM_USED);
    __sqoop$field_map.put("MEASURE_USED", this.MEASURE_USED);
    __sqoop$field_map.put("START_DATE", this.START_DATE);
    __sqoop$field_map.put("END_DATE", this.END_DATE);
    __sqoop$field_map.put("SUPERVISOR_ESTIMATE", this.SUPERVISOR_ESTIMATE);
    __sqoop$field_map.put("SUPERVISOR_ESTIMATE_CHANGE_DATE", this.SUPERVISOR_ESTIMATE_CHANGE_DATE);
  }

  public void setField(String __fieldName, Object __fieldVal) {
    if ("ROWID".equals(__fieldName)) {
      this.ROWID = (Integer) __fieldVal;
    }
    else    if ("ACTIVITYID".equals(__fieldName)) {
      this.ACTIVITYID = (Integer) __fieldVal;
    }
    else    if ("ESM".equals(__fieldName)) {
      this.ESM = (java.math.BigDecimal) __fieldVal;
    }
    else    if ("RE".equals(__fieldName)) {
      this.RE = (java.math.BigDecimal) __fieldVal;
    }
    else    if ("ESM_USED".equals(__fieldName)) {
      this.ESM_USED = (String) __fieldVal;
    }
    else    if ("MEASURE_USED".equals(__fieldName)) {
      this.MEASURE_USED = (java.math.BigDecimal) __fieldVal;
    }
    else    if ("START_DATE".equals(__fieldName)) {
      this.START_DATE = (java.sql.Timestamp) __fieldVal;
    }
    else    if ("END_DATE".equals(__fieldName)) {
      this.END_DATE = (java.sql.Timestamp) __fieldVal;
    }
    else    if ("SUPERVISOR_ESTIMATE".equals(__fieldName)) {
      this.SUPERVISOR_ESTIMATE = (String) __fieldVal;
    }
    else    if ("SUPERVISOR_ESTIMATE_CHANGE_DATE".equals(__fieldName)) {
      this.SUPERVISOR_ESTIMATE_CHANGE_DATE = (java.sql.Timestamp) __fieldVal;
    }
    else {
      throw new RuntimeException("No such field: " + __fieldName);
    }
  }
  public boolean setField0(String __fieldName, Object __fieldVal) {
    if ("ROWID".equals(__fieldName)) {
      this.ROWID = (Integer) __fieldVal;
      return true;
    }
    else    if ("ACTIVITYID".equals(__fieldName)) {
      this.ACTIVITYID = (Integer) __fieldVal;
      return true;
    }
    else    if ("ESM".equals(__fieldName)) {
      this.ESM = (java.math.BigDecimal) __fieldVal;
      return true;
    }
    else    if ("RE".equals(__fieldName)) {
      this.RE = (java.math.BigDecimal) __fieldVal;
      return true;
    }
    else    if ("ESM_USED".equals(__fieldName)) {
      this.ESM_USED = (String) __fieldVal;
      return true;
    }
    else    if ("MEASURE_USED".equals(__fieldName)) {
      this.MEASURE_USED = (java.math.BigDecimal) __fieldVal;
      return true;
    }
    else    if ("START_DATE".equals(__fieldName)) {
      this.START_DATE = (java.sql.Timestamp) __fieldVal;
      return true;
    }
    else    if ("END_DATE".equals(__fieldName)) {
      this.END_DATE = (java.sql.Timestamp) __fieldVal;
      return true;
    }
    else    if ("SUPERVISOR_ESTIMATE".equals(__fieldName)) {
      this.SUPERVISOR_ESTIMATE = (String) __fieldVal;
      return true;
    }
    else    if ("SUPERVISOR_ESTIMATE_CHANGE_DATE".equals(__fieldName)) {
      this.SUPERVISOR_ESTIMATE_CHANGE_DATE = (java.sql.Timestamp) __fieldVal;
      return true;
    }
    else {
      return false;    }
  }
}
