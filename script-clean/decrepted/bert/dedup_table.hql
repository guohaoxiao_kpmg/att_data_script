use kpmg_ws;

drop table ert_msoc_activity_esm_re_dedup;
create table kpmg_ws.ert_msoc_activity_esm_re_dedup
as
select * from
(
select * , row_number() over( partition by ACTIVITYID order by start_date desc ) as row
from kpmg_ws.tmp_bert_msoc_activity_esm_re
)x
where row =1;


