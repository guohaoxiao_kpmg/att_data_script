// ORM class for table 'MSOC..MSOC_ACTIVITY'
// WARNING: This class is AUTO-GENERATED. Modify at your own risk.
//
// Debug information:
// Generated date: Wed Mar 09 21:06:32 EST 2016
// For connector: org.apache.sqoop.manager.GenericJdbcManager
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapred.lib.db.DBWritable;
import com.cloudera.sqoop.lib.JdbcWritableBridge;
import com.cloudera.sqoop.lib.DelimiterSet;
import com.cloudera.sqoop.lib.FieldFormatter;
import com.cloudera.sqoop.lib.RecordParser;
import com.cloudera.sqoop.lib.BooleanParser;
import com.cloudera.sqoop.lib.BlobRef;
import com.cloudera.sqoop.lib.ClobRef;
import com.cloudera.sqoop.lib.LargeObjectLoader;
import com.cloudera.sqoop.lib.SqoopRecord;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class MSOC__MSOC_ACTIVITY extends SqoopRecord  implements DBWritable, Writable {
  private final int PROTOCOL_VERSION = 3;
  public int getClassFormatVersion() { return PROTOCOL_VERSION; }
  protected ResultSet __cur_result_set;
  private Integer ACTIVITYID;
  public Integer get_ACTIVITYID() {
    return ACTIVITYID;
  }
  public void set_ACTIVITYID(Integer ACTIVITYID) {
    this.ACTIVITYID = ACTIVITYID;
  }
  public MSOC__MSOC_ACTIVITY with_ACTIVITYID(Integer ACTIVITYID) {
    this.ACTIVITYID = ACTIVITYID;
    return this;
  }
  private String DEPARTMENT;
  public String get_DEPARTMENT() {
    return DEPARTMENT;
  }
  public void set_DEPARTMENT(String DEPARTMENT) {
    this.DEPARTMENT = DEPARTMENT;
  }
  public MSOC__MSOC_ACTIVITY with_DEPARTMENT(String DEPARTMENT) {
    this.DEPARTMENT = DEPARTMENT;
    return this;
  }
  private String ACTIVITY_GROUP;
  public String get_ACTIVITY_GROUP() {
    return ACTIVITY_GROUP;
  }
  public void set_ACTIVITY_GROUP(String ACTIVITY_GROUP) {
    this.ACTIVITY_GROUP = ACTIVITY_GROUP;
  }
  public MSOC__MSOC_ACTIVITY with_ACTIVITY_GROUP(String ACTIVITY_GROUP) {
    this.ACTIVITY_GROUP = ACTIVITY_GROUP;
    return this;
  }
  private String ACTIVITY_TYPE;
  public String get_ACTIVITY_TYPE() {
    return ACTIVITY_TYPE;
  }
  public void set_ACTIVITY_TYPE(String ACTIVITY_TYPE) {
    this.ACTIVITY_TYPE = ACTIVITY_TYPE;
  }
  public MSOC__MSOC_ACTIVITY with_ACTIVITY_TYPE(String ACTIVITY_TYPE) {
    this.ACTIVITY_TYPE = ACTIVITY_TYPE;
    return this;
  }
  private String ACTIVITY;
  public String get_ACTIVITY() {
    return ACTIVITY;
  }
  public void set_ACTIVITY(String ACTIVITY) {
    this.ACTIVITY = ACTIVITY;
  }
  public MSOC__MSOC_ACTIVITY with_ACTIVITY(String ACTIVITY) {
    this.ACTIVITY = ACTIVITY;
    return this;
  }
  private String STATUS;
  public String get_STATUS() {
    return STATUS;
  }
  public void set_STATUS(String STATUS) {
    this.STATUS = STATUS;
  }
  public MSOC__MSOC_ACTIVITY with_STATUS(String STATUS) {
    this.STATUS = STATUS;
    return this;
  }
  private String DESCRIPTION;
  public String get_DESCRIPTION() {
    return DESCRIPTION;
  }
  public void set_DESCRIPTION(String DESCRIPTION) {
    this.DESCRIPTION = DESCRIPTION;
  }
  public MSOC__MSOC_ACTIVITY with_DESCRIPTION(String DESCRIPTION) {
    this.DESCRIPTION = DESCRIPTION;
    return this;
  }
  private Double MEASURE_USED;
  public Double get_MEASURE_USED() {
    return MEASURE_USED;
  }
  public void set_MEASURE_USED(Double MEASURE_USED) {
    this.MEASURE_USED = MEASURE_USED;
  }
  public MSOC__MSOC_ACTIVITY with_MEASURE_USED(Double MEASURE_USED) {
    this.MEASURE_USED = MEASURE_USED;
    return this;
  }
  private Double RE;
  public Double get_RE() {
    return RE;
  }
  public void set_RE(Double RE) {
    this.RE = RE;
  }
  public MSOC__MSOC_ACTIVITY with_RE(Double RE) {
    this.RE = RE;
    return this;
  }
  private Double ESM;
  public Double get_ESM() {
    return ESM;
  }
  public void set_ESM(Double ESM) {
    this.ESM = ESM;
  }
  public MSOC__MSOC_ACTIVITY with_ESM(Double ESM) {
    this.ESM = ESM;
    return this;
  }
  private String SOURCE;
  public String get_SOURCE() {
    return SOURCE;
  }
  public void set_SOURCE(String SOURCE) {
    this.SOURCE = SOURCE;
  }
  public MSOC__MSOC_ACTIVITY with_SOURCE(String SOURCE) {
    this.SOURCE = SOURCE;
    return this;
  }
  private String KVI;
  public String get_KVI() {
    return KVI;
  }
  public void set_KVI(String KVI) {
    this.KVI = KVI;
  }
  public MSOC__MSOC_ACTIVITY with_KVI(String KVI) {
    this.KVI = KVI;
    return this;
  }
  private Integer Sort_Order;
  public Integer get_Sort_Order() {
    return Sort_Order;
  }
  public void set_Sort_Order(Integer Sort_Order) {
    this.Sort_Order = Sort_Order;
  }
  public MSOC__MSOC_ACTIVITY with_Sort_Order(Integer Sort_Order) {
    this.Sort_Order = Sort_Order;
    return this;
  }
  private Double GOAL;
  public Double get_GOAL() {
    return GOAL;
  }
  public void set_GOAL(Double GOAL) {
    this.GOAL = GOAL;
  }
  public MSOC__MSOC_ACTIVITY with_GOAL(Double GOAL) {
    this.GOAL = GOAL;
    return this;
  }
  private String GOAL_TYPE;
  public String get_GOAL_TYPE() {
    return GOAL_TYPE;
  }
  public void set_GOAL_TYPE(String GOAL_TYPE) {
    this.GOAL_TYPE = GOAL_TYPE;
  }
  public MSOC__MSOC_ACTIVITY with_GOAL_TYPE(String GOAL_TYPE) {
    this.GOAL_TYPE = GOAL_TYPE;
    return this;
  }
  private String SOURCE_SYSTEM;
  public String get_SOURCE_SYSTEM() {
    return SOURCE_SYSTEM;
  }
  public void set_SOURCE_SYSTEM(String SOURCE_SYSTEM) {
    this.SOURCE_SYSTEM = SOURCE_SYSTEM;
  }
  public MSOC__MSOC_ACTIVITY with_SOURCE_SYSTEM(String SOURCE_SYSTEM) {
    this.SOURCE_SYSTEM = SOURCE_SYSTEM;
    return this;
  }
  private String SUPERVISOR_ESTIMATE;
  public String get_SUPERVISOR_ESTIMATE() {
    return SUPERVISOR_ESTIMATE;
  }
  public void set_SUPERVISOR_ESTIMATE(String SUPERVISOR_ESTIMATE) {
    this.SUPERVISOR_ESTIMATE = SUPERVISOR_ESTIMATE;
  }
  public MSOC__MSOC_ACTIVITY with_SUPERVISOR_ESTIMATE(String SUPERVISOR_ESTIMATE) {
    this.SUPERVISOR_ESTIMATE = SUPERVISOR_ESTIMATE;
    return this;
  }
  private java.sql.Timestamp SUPERVISOR_ESTIMATE_CHANGE_DATE;
  public java.sql.Timestamp get_SUPERVISOR_ESTIMATE_CHANGE_DATE() {
    return SUPERVISOR_ESTIMATE_CHANGE_DATE;
  }
  public void set_SUPERVISOR_ESTIMATE_CHANGE_DATE(java.sql.Timestamp SUPERVISOR_ESTIMATE_CHANGE_DATE) {
    this.SUPERVISOR_ESTIMATE_CHANGE_DATE = SUPERVISOR_ESTIMATE_CHANGE_DATE;
  }
  public MSOC__MSOC_ACTIVITY with_SUPERVISOR_ESTIMATE_CHANGE_DATE(java.sql.Timestamp SUPERVISOR_ESTIMATE_CHANGE_DATE) {
    this.SUPERVISOR_ESTIMATE_CHANGE_DATE = SUPERVISOR_ESTIMATE_CHANGE_DATE;
    return this;
  }
  private java.sql.Timestamp ACTIVITY_START_DATE;
  public java.sql.Timestamp get_ACTIVITY_START_DATE() {
    return ACTIVITY_START_DATE;
  }
  public void set_ACTIVITY_START_DATE(java.sql.Timestamp ACTIVITY_START_DATE) {
    this.ACTIVITY_START_DATE = ACTIVITY_START_DATE;
  }
  public MSOC__MSOC_ACTIVITY with_ACTIVITY_START_DATE(java.sql.Timestamp ACTIVITY_START_DATE) {
    this.ACTIVITY_START_DATE = ACTIVITY_START_DATE;
    return this;
  }
  private java.sql.Timestamp ACTIVITY_END_DATE;
  public java.sql.Timestamp get_ACTIVITY_END_DATE() {
    return ACTIVITY_END_DATE;
  }
  public void set_ACTIVITY_END_DATE(java.sql.Timestamp ACTIVITY_END_DATE) {
    this.ACTIVITY_END_DATE = ACTIVITY_END_DATE;
  }
  public MSOC__MSOC_ACTIVITY with_ACTIVITY_END_DATE(java.sql.Timestamp ACTIVITY_END_DATE) {
    this.ACTIVITY_END_DATE = ACTIVITY_END_DATE;
    return this;
  }
  private String KEY_INDICATOR;
  public String get_KEY_INDICATOR() {
    return KEY_INDICATOR;
  }
  public void set_KEY_INDICATOR(String KEY_INDICATOR) {
    this.KEY_INDICATOR = KEY_INDICATOR;
  }
  public MSOC__MSOC_ACTIVITY with_KEY_INDICATOR(String KEY_INDICATOR) {
    this.KEY_INDICATOR = KEY_INDICATOR;
    return this;
  }
  private String KVI_GROUP;
  public String get_KVI_GROUP() {
    return KVI_GROUP;
  }
  public void set_KVI_GROUP(String KVI_GROUP) {
    this.KVI_GROUP = KVI_GROUP;
  }
  public MSOC__MSOC_ACTIVITY with_KVI_GROUP(String KVI_GROUP) {
    this.KVI_GROUP = KVI_GROUP;
    return this;
  }
  private String COLOR;
  public String get_COLOR() {
    return COLOR;
  }
  public void set_COLOR(String COLOR) {
    this.COLOR = COLOR;
  }
  public MSOC__MSOC_ACTIVITY with_COLOR(String COLOR) {
    this.COLOR = COLOR;
    return this;
  }
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof MSOC__MSOC_ACTIVITY)) {
      return false;
    }
    MSOC__MSOC_ACTIVITY that = (MSOC__MSOC_ACTIVITY) o;
    boolean equal = true;
    equal = equal && (this.ACTIVITYID == null ? that.ACTIVITYID == null : this.ACTIVITYID.equals(that.ACTIVITYID));
    equal = equal && (this.DEPARTMENT == null ? that.DEPARTMENT == null : this.DEPARTMENT.equals(that.DEPARTMENT));
    equal = equal && (this.ACTIVITY_GROUP == null ? that.ACTIVITY_GROUP == null : this.ACTIVITY_GROUP.equals(that.ACTIVITY_GROUP));
    equal = equal && (this.ACTIVITY_TYPE == null ? that.ACTIVITY_TYPE == null : this.ACTIVITY_TYPE.equals(that.ACTIVITY_TYPE));
    equal = equal && (this.ACTIVITY == null ? that.ACTIVITY == null : this.ACTIVITY.equals(that.ACTIVITY));
    equal = equal && (this.STATUS == null ? that.STATUS == null : this.STATUS.equals(that.STATUS));
    equal = equal && (this.DESCRIPTION == null ? that.DESCRIPTION == null : this.DESCRIPTION.equals(that.DESCRIPTION));
    equal = equal && (this.MEASURE_USED == null ? that.MEASURE_USED == null : this.MEASURE_USED.equals(that.MEASURE_USED));
    equal = equal && (this.RE == null ? that.RE == null : this.RE.equals(that.RE));
    equal = equal && (this.ESM == null ? that.ESM == null : this.ESM.equals(that.ESM));
    equal = equal && (this.SOURCE == null ? that.SOURCE == null : this.SOURCE.equals(that.SOURCE));
    equal = equal && (this.KVI == null ? that.KVI == null : this.KVI.equals(that.KVI));
    equal = equal && (this.Sort_Order == null ? that.Sort_Order == null : this.Sort_Order.equals(that.Sort_Order));
    equal = equal && (this.GOAL == null ? that.GOAL == null : this.GOAL.equals(that.GOAL));
    equal = equal && (this.GOAL_TYPE == null ? that.GOAL_TYPE == null : this.GOAL_TYPE.equals(that.GOAL_TYPE));
    equal = equal && (this.SOURCE_SYSTEM == null ? that.SOURCE_SYSTEM == null : this.SOURCE_SYSTEM.equals(that.SOURCE_SYSTEM));
    equal = equal && (this.SUPERVISOR_ESTIMATE == null ? that.SUPERVISOR_ESTIMATE == null : this.SUPERVISOR_ESTIMATE.equals(that.SUPERVISOR_ESTIMATE));
    equal = equal && (this.SUPERVISOR_ESTIMATE_CHANGE_DATE == null ? that.SUPERVISOR_ESTIMATE_CHANGE_DATE == null : this.SUPERVISOR_ESTIMATE_CHANGE_DATE.equals(that.SUPERVISOR_ESTIMATE_CHANGE_DATE));
    equal = equal && (this.ACTIVITY_START_DATE == null ? that.ACTIVITY_START_DATE == null : this.ACTIVITY_START_DATE.equals(that.ACTIVITY_START_DATE));
    equal = equal && (this.ACTIVITY_END_DATE == null ? that.ACTIVITY_END_DATE == null : this.ACTIVITY_END_DATE.equals(that.ACTIVITY_END_DATE));
    equal = equal && (this.KEY_INDICATOR == null ? that.KEY_INDICATOR == null : this.KEY_INDICATOR.equals(that.KEY_INDICATOR));
    equal = equal && (this.KVI_GROUP == null ? that.KVI_GROUP == null : this.KVI_GROUP.equals(that.KVI_GROUP));
    equal = equal && (this.COLOR == null ? that.COLOR == null : this.COLOR.equals(that.COLOR));
    return equal;
  }
  public boolean equals0(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof MSOC__MSOC_ACTIVITY)) {
      return false;
    }
    MSOC__MSOC_ACTIVITY that = (MSOC__MSOC_ACTIVITY) o;
    boolean equal = true;
    equal = equal && (this.ACTIVITYID == null ? that.ACTIVITYID == null : this.ACTIVITYID.equals(that.ACTIVITYID));
    equal = equal && (this.DEPARTMENT == null ? that.DEPARTMENT == null : this.DEPARTMENT.equals(that.DEPARTMENT));
    equal = equal && (this.ACTIVITY_GROUP == null ? that.ACTIVITY_GROUP == null : this.ACTIVITY_GROUP.equals(that.ACTIVITY_GROUP));
    equal = equal && (this.ACTIVITY_TYPE == null ? that.ACTIVITY_TYPE == null : this.ACTIVITY_TYPE.equals(that.ACTIVITY_TYPE));
    equal = equal && (this.ACTIVITY == null ? that.ACTIVITY == null : this.ACTIVITY.equals(that.ACTIVITY));
    equal = equal && (this.STATUS == null ? that.STATUS == null : this.STATUS.equals(that.STATUS));
    equal = equal && (this.DESCRIPTION == null ? that.DESCRIPTION == null : this.DESCRIPTION.equals(that.DESCRIPTION));
    equal = equal && (this.MEASURE_USED == null ? that.MEASURE_USED == null : this.MEASURE_USED.equals(that.MEASURE_USED));
    equal = equal && (this.RE == null ? that.RE == null : this.RE.equals(that.RE));
    equal = equal && (this.ESM == null ? that.ESM == null : this.ESM.equals(that.ESM));
    equal = equal && (this.SOURCE == null ? that.SOURCE == null : this.SOURCE.equals(that.SOURCE));
    equal = equal && (this.KVI == null ? that.KVI == null : this.KVI.equals(that.KVI));
    equal = equal && (this.Sort_Order == null ? that.Sort_Order == null : this.Sort_Order.equals(that.Sort_Order));
    equal = equal && (this.GOAL == null ? that.GOAL == null : this.GOAL.equals(that.GOAL));
    equal = equal && (this.GOAL_TYPE == null ? that.GOAL_TYPE == null : this.GOAL_TYPE.equals(that.GOAL_TYPE));
    equal = equal && (this.SOURCE_SYSTEM == null ? that.SOURCE_SYSTEM == null : this.SOURCE_SYSTEM.equals(that.SOURCE_SYSTEM));
    equal = equal && (this.SUPERVISOR_ESTIMATE == null ? that.SUPERVISOR_ESTIMATE == null : this.SUPERVISOR_ESTIMATE.equals(that.SUPERVISOR_ESTIMATE));
    equal = equal && (this.SUPERVISOR_ESTIMATE_CHANGE_DATE == null ? that.SUPERVISOR_ESTIMATE_CHANGE_DATE == null : this.SUPERVISOR_ESTIMATE_CHANGE_DATE.equals(that.SUPERVISOR_ESTIMATE_CHANGE_DATE));
    equal = equal && (this.ACTIVITY_START_DATE == null ? that.ACTIVITY_START_DATE == null : this.ACTIVITY_START_DATE.equals(that.ACTIVITY_START_DATE));
    equal = equal && (this.ACTIVITY_END_DATE == null ? that.ACTIVITY_END_DATE == null : this.ACTIVITY_END_DATE.equals(that.ACTIVITY_END_DATE));
    equal = equal && (this.KEY_INDICATOR == null ? that.KEY_INDICATOR == null : this.KEY_INDICATOR.equals(that.KEY_INDICATOR));
    equal = equal && (this.KVI_GROUP == null ? that.KVI_GROUP == null : this.KVI_GROUP.equals(that.KVI_GROUP));
    equal = equal && (this.COLOR == null ? that.COLOR == null : this.COLOR.equals(that.COLOR));
    return equal;
  }
  public void readFields(ResultSet __dbResults) throws SQLException {
    this.__cur_result_set = __dbResults;
    this.ACTIVITYID = JdbcWritableBridge.readInteger(1, __dbResults);
    this.DEPARTMENT = JdbcWritableBridge.readString(2, __dbResults);
    this.ACTIVITY_GROUP = JdbcWritableBridge.readString(3, __dbResults);
    this.ACTIVITY_TYPE = JdbcWritableBridge.readString(4, __dbResults);
    this.ACTIVITY = JdbcWritableBridge.readString(5, __dbResults);
    this.STATUS = JdbcWritableBridge.readString(6, __dbResults);
    this.DESCRIPTION = JdbcWritableBridge.readString(7, __dbResults);
    this.MEASURE_USED = JdbcWritableBridge.readDouble(8, __dbResults);
    this.RE = JdbcWritableBridge.readDouble(9, __dbResults);
    this.ESM = JdbcWritableBridge.readDouble(10, __dbResults);
    this.SOURCE = JdbcWritableBridge.readString(11, __dbResults);
    this.KVI = JdbcWritableBridge.readString(12, __dbResults);
    this.Sort_Order = JdbcWritableBridge.readInteger(13, __dbResults);
    this.GOAL = JdbcWritableBridge.readDouble(14, __dbResults);
    this.GOAL_TYPE = JdbcWritableBridge.readString(15, __dbResults);
    this.SOURCE_SYSTEM = JdbcWritableBridge.readString(16, __dbResults);
    this.SUPERVISOR_ESTIMATE = JdbcWritableBridge.readString(17, __dbResults);
    this.SUPERVISOR_ESTIMATE_CHANGE_DATE = JdbcWritableBridge.readTimestamp(18, __dbResults);
    this.ACTIVITY_START_DATE = JdbcWritableBridge.readTimestamp(19, __dbResults);
    this.ACTIVITY_END_DATE = JdbcWritableBridge.readTimestamp(20, __dbResults);
    this.KEY_INDICATOR = JdbcWritableBridge.readString(21, __dbResults);
    this.KVI_GROUP = JdbcWritableBridge.readString(22, __dbResults);
    this.COLOR = JdbcWritableBridge.readString(23, __dbResults);
  }
  public void readFields0(ResultSet __dbResults) throws SQLException {
    this.ACTIVITYID = JdbcWritableBridge.readInteger(1, __dbResults);
    this.DEPARTMENT = JdbcWritableBridge.readString(2, __dbResults);
    this.ACTIVITY_GROUP = JdbcWritableBridge.readString(3, __dbResults);
    this.ACTIVITY_TYPE = JdbcWritableBridge.readString(4, __dbResults);
    this.ACTIVITY = JdbcWritableBridge.readString(5, __dbResults);
    this.STATUS = JdbcWritableBridge.readString(6, __dbResults);
    this.DESCRIPTION = JdbcWritableBridge.readString(7, __dbResults);
    this.MEASURE_USED = JdbcWritableBridge.readDouble(8, __dbResults);
    this.RE = JdbcWritableBridge.readDouble(9, __dbResults);
    this.ESM = JdbcWritableBridge.readDouble(10, __dbResults);
    this.SOURCE = JdbcWritableBridge.readString(11, __dbResults);
    this.KVI = JdbcWritableBridge.readString(12, __dbResults);
    this.Sort_Order = JdbcWritableBridge.readInteger(13, __dbResults);
    this.GOAL = JdbcWritableBridge.readDouble(14, __dbResults);
    this.GOAL_TYPE = JdbcWritableBridge.readString(15, __dbResults);
    this.SOURCE_SYSTEM = JdbcWritableBridge.readString(16, __dbResults);
    this.SUPERVISOR_ESTIMATE = JdbcWritableBridge.readString(17, __dbResults);
    this.SUPERVISOR_ESTIMATE_CHANGE_DATE = JdbcWritableBridge.readTimestamp(18, __dbResults);
    this.ACTIVITY_START_DATE = JdbcWritableBridge.readTimestamp(19, __dbResults);
    this.ACTIVITY_END_DATE = JdbcWritableBridge.readTimestamp(20, __dbResults);
    this.KEY_INDICATOR = JdbcWritableBridge.readString(21, __dbResults);
    this.KVI_GROUP = JdbcWritableBridge.readString(22, __dbResults);
    this.COLOR = JdbcWritableBridge.readString(23, __dbResults);
  }
  public void loadLargeObjects(LargeObjectLoader __loader)
      throws SQLException, IOException, InterruptedException {
  }
  public void loadLargeObjects0(LargeObjectLoader __loader)
      throws SQLException, IOException, InterruptedException {
  }
  public void write(PreparedStatement __dbStmt) throws SQLException {
    write(__dbStmt, 0);
  }

  public int write(PreparedStatement __dbStmt, int __off) throws SQLException {
    JdbcWritableBridge.writeInteger(ACTIVITYID, 1 + __off, 4, __dbStmt);
    JdbcWritableBridge.writeString(DEPARTMENT, 2 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(ACTIVITY_GROUP, 3 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(ACTIVITY_TYPE, 4 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(ACTIVITY, 5 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(STATUS, 6 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(DESCRIPTION, 7 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeDouble(MEASURE_USED, 8 + __off, 8, __dbStmt);
    JdbcWritableBridge.writeDouble(RE, 9 + __off, 8, __dbStmt);
    JdbcWritableBridge.writeDouble(ESM, 10 + __off, 8, __dbStmt);
    JdbcWritableBridge.writeString(SOURCE, 11 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(KVI, 12 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeInteger(Sort_Order, 13 + __off, 4, __dbStmt);
    JdbcWritableBridge.writeDouble(GOAL, 14 + __off, 8, __dbStmt);
    JdbcWritableBridge.writeString(GOAL_TYPE, 15 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(SOURCE_SYSTEM, 16 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(SUPERVISOR_ESTIMATE, 17 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeTimestamp(SUPERVISOR_ESTIMATE_CHANGE_DATE, 18 + __off, 93, __dbStmt);
    JdbcWritableBridge.writeTimestamp(ACTIVITY_START_DATE, 19 + __off, 93, __dbStmt);
    JdbcWritableBridge.writeTimestamp(ACTIVITY_END_DATE, 20 + __off, 93, __dbStmt);
    JdbcWritableBridge.writeString(KEY_INDICATOR, 21 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(KVI_GROUP, 22 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(COLOR, 23 + __off, 1, __dbStmt);
    return 23;
  }
  public void write0(PreparedStatement __dbStmt, int __off) throws SQLException {
    JdbcWritableBridge.writeInteger(ACTIVITYID, 1 + __off, 4, __dbStmt);
    JdbcWritableBridge.writeString(DEPARTMENT, 2 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(ACTIVITY_GROUP, 3 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(ACTIVITY_TYPE, 4 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(ACTIVITY, 5 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(STATUS, 6 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(DESCRIPTION, 7 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeDouble(MEASURE_USED, 8 + __off, 8, __dbStmt);
    JdbcWritableBridge.writeDouble(RE, 9 + __off, 8, __dbStmt);
    JdbcWritableBridge.writeDouble(ESM, 10 + __off, 8, __dbStmt);
    JdbcWritableBridge.writeString(SOURCE, 11 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(KVI, 12 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeInteger(Sort_Order, 13 + __off, 4, __dbStmt);
    JdbcWritableBridge.writeDouble(GOAL, 14 + __off, 8, __dbStmt);
    JdbcWritableBridge.writeString(GOAL_TYPE, 15 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(SOURCE_SYSTEM, 16 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(SUPERVISOR_ESTIMATE, 17 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeTimestamp(SUPERVISOR_ESTIMATE_CHANGE_DATE, 18 + __off, 93, __dbStmt);
    JdbcWritableBridge.writeTimestamp(ACTIVITY_START_DATE, 19 + __off, 93, __dbStmt);
    JdbcWritableBridge.writeTimestamp(ACTIVITY_END_DATE, 20 + __off, 93, __dbStmt);
    JdbcWritableBridge.writeString(KEY_INDICATOR, 21 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(KVI_GROUP, 22 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(COLOR, 23 + __off, 1, __dbStmt);
  }
  public void readFields(DataInput __dataIn) throws IOException {
this.readFields0(__dataIn);  }
  public void readFields0(DataInput __dataIn) throws IOException {
    if (__dataIn.readBoolean()) { 
        this.ACTIVITYID = null;
    } else {
    this.ACTIVITYID = Integer.valueOf(__dataIn.readInt());
    }
    if (__dataIn.readBoolean()) { 
        this.DEPARTMENT = null;
    } else {
    this.DEPARTMENT = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.ACTIVITY_GROUP = null;
    } else {
    this.ACTIVITY_GROUP = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.ACTIVITY_TYPE = null;
    } else {
    this.ACTIVITY_TYPE = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.ACTIVITY = null;
    } else {
    this.ACTIVITY = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.STATUS = null;
    } else {
    this.STATUS = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.DESCRIPTION = null;
    } else {
    this.DESCRIPTION = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.MEASURE_USED = null;
    } else {
    this.MEASURE_USED = Double.valueOf(__dataIn.readDouble());
    }
    if (__dataIn.readBoolean()) { 
        this.RE = null;
    } else {
    this.RE = Double.valueOf(__dataIn.readDouble());
    }
    if (__dataIn.readBoolean()) { 
        this.ESM = null;
    } else {
    this.ESM = Double.valueOf(__dataIn.readDouble());
    }
    if (__dataIn.readBoolean()) { 
        this.SOURCE = null;
    } else {
    this.SOURCE = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.KVI = null;
    } else {
    this.KVI = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.Sort_Order = null;
    } else {
    this.Sort_Order = Integer.valueOf(__dataIn.readInt());
    }
    if (__dataIn.readBoolean()) { 
        this.GOAL = null;
    } else {
    this.GOAL = Double.valueOf(__dataIn.readDouble());
    }
    if (__dataIn.readBoolean()) { 
        this.GOAL_TYPE = null;
    } else {
    this.GOAL_TYPE = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.SOURCE_SYSTEM = null;
    } else {
    this.SOURCE_SYSTEM = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.SUPERVISOR_ESTIMATE = null;
    } else {
    this.SUPERVISOR_ESTIMATE = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.SUPERVISOR_ESTIMATE_CHANGE_DATE = null;
    } else {
    this.SUPERVISOR_ESTIMATE_CHANGE_DATE = new Timestamp(__dataIn.readLong());
    this.SUPERVISOR_ESTIMATE_CHANGE_DATE.setNanos(__dataIn.readInt());
    }
    if (__dataIn.readBoolean()) { 
        this.ACTIVITY_START_DATE = null;
    } else {
    this.ACTIVITY_START_DATE = new Timestamp(__dataIn.readLong());
    this.ACTIVITY_START_DATE.setNanos(__dataIn.readInt());
    }
    if (__dataIn.readBoolean()) { 
        this.ACTIVITY_END_DATE = null;
    } else {
    this.ACTIVITY_END_DATE = new Timestamp(__dataIn.readLong());
    this.ACTIVITY_END_DATE.setNanos(__dataIn.readInt());
    }
    if (__dataIn.readBoolean()) { 
        this.KEY_INDICATOR = null;
    } else {
    this.KEY_INDICATOR = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.KVI_GROUP = null;
    } else {
    this.KVI_GROUP = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.COLOR = null;
    } else {
    this.COLOR = Text.readString(__dataIn);
    }
  }
  public void write(DataOutput __dataOut) throws IOException {
    if (null == this.ACTIVITYID) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.ACTIVITYID);
    }
    if (null == this.DEPARTMENT) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, DEPARTMENT);
    }
    if (null == this.ACTIVITY_GROUP) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, ACTIVITY_GROUP);
    }
    if (null == this.ACTIVITY_TYPE) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, ACTIVITY_TYPE);
    }
    if (null == this.ACTIVITY) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, ACTIVITY);
    }
    if (null == this.STATUS) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, STATUS);
    }
    if (null == this.DESCRIPTION) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, DESCRIPTION);
    }
    if (null == this.MEASURE_USED) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeDouble(this.MEASURE_USED);
    }
    if (null == this.RE) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeDouble(this.RE);
    }
    if (null == this.ESM) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeDouble(this.ESM);
    }
    if (null == this.SOURCE) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, SOURCE);
    }
    if (null == this.KVI) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, KVI);
    }
    if (null == this.Sort_Order) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.Sort_Order);
    }
    if (null == this.GOAL) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeDouble(this.GOAL);
    }
    if (null == this.GOAL_TYPE) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, GOAL_TYPE);
    }
    if (null == this.SOURCE_SYSTEM) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, SOURCE_SYSTEM);
    }
    if (null == this.SUPERVISOR_ESTIMATE) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, SUPERVISOR_ESTIMATE);
    }
    if (null == this.SUPERVISOR_ESTIMATE_CHANGE_DATE) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.SUPERVISOR_ESTIMATE_CHANGE_DATE.getTime());
    __dataOut.writeInt(this.SUPERVISOR_ESTIMATE_CHANGE_DATE.getNanos());
    }
    if (null == this.ACTIVITY_START_DATE) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.ACTIVITY_START_DATE.getTime());
    __dataOut.writeInt(this.ACTIVITY_START_DATE.getNanos());
    }
    if (null == this.ACTIVITY_END_DATE) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.ACTIVITY_END_DATE.getTime());
    __dataOut.writeInt(this.ACTIVITY_END_DATE.getNanos());
    }
    if (null == this.KEY_INDICATOR) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, KEY_INDICATOR);
    }
    if (null == this.KVI_GROUP) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, KVI_GROUP);
    }
    if (null == this.COLOR) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, COLOR);
    }
  }
  public void write0(DataOutput __dataOut) throws IOException {
    if (null == this.ACTIVITYID) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.ACTIVITYID);
    }
    if (null == this.DEPARTMENT) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, DEPARTMENT);
    }
    if (null == this.ACTIVITY_GROUP) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, ACTIVITY_GROUP);
    }
    if (null == this.ACTIVITY_TYPE) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, ACTIVITY_TYPE);
    }
    if (null == this.ACTIVITY) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, ACTIVITY);
    }
    if (null == this.STATUS) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, STATUS);
    }
    if (null == this.DESCRIPTION) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, DESCRIPTION);
    }
    if (null == this.MEASURE_USED) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeDouble(this.MEASURE_USED);
    }
    if (null == this.RE) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeDouble(this.RE);
    }
    if (null == this.ESM) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeDouble(this.ESM);
    }
    if (null == this.SOURCE) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, SOURCE);
    }
    if (null == this.KVI) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, KVI);
    }
    if (null == this.Sort_Order) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.Sort_Order);
    }
    if (null == this.GOAL) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeDouble(this.GOAL);
    }
    if (null == this.GOAL_TYPE) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, GOAL_TYPE);
    }
    if (null == this.SOURCE_SYSTEM) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, SOURCE_SYSTEM);
    }
    if (null == this.SUPERVISOR_ESTIMATE) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, SUPERVISOR_ESTIMATE);
    }
    if (null == this.SUPERVISOR_ESTIMATE_CHANGE_DATE) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.SUPERVISOR_ESTIMATE_CHANGE_DATE.getTime());
    __dataOut.writeInt(this.SUPERVISOR_ESTIMATE_CHANGE_DATE.getNanos());
    }
    if (null == this.ACTIVITY_START_DATE) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.ACTIVITY_START_DATE.getTime());
    __dataOut.writeInt(this.ACTIVITY_START_DATE.getNanos());
    }
    if (null == this.ACTIVITY_END_DATE) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.ACTIVITY_END_DATE.getTime());
    __dataOut.writeInt(this.ACTIVITY_END_DATE.getNanos());
    }
    if (null == this.KEY_INDICATOR) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, KEY_INDICATOR);
    }
    if (null == this.KVI_GROUP) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, KVI_GROUP);
    }
    if (null == this.COLOR) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, COLOR);
    }
  }
  private static final DelimiterSet __outputDelimiters = new DelimiterSet((char) 1, (char) 10, (char) 0, (char) 0, false);
  public String toString() {
    return toString(__outputDelimiters, true);
  }
  public String toString(DelimiterSet delimiters) {
    return toString(delimiters, true);
  }
  public String toString(boolean useRecordDelim) {
    return toString(__outputDelimiters, useRecordDelim);
  }
  public String toString(DelimiterSet delimiters, boolean useRecordDelim) {
    StringBuilder __sb = new StringBuilder();
    char fieldDelim = delimiters.getFieldsTerminatedBy();
    __sb.append(FieldFormatter.escapeAndEnclose(ACTIVITYID==null?"\\N":"" + ACTIVITYID, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(DEPARTMENT==null?"\\N":DEPARTMENT, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(ACTIVITY_GROUP==null?"\\N":ACTIVITY_GROUP, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(ACTIVITY_TYPE==null?"\\N":ACTIVITY_TYPE, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(ACTIVITY==null?"\\N":ACTIVITY, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(STATUS==null?"\\N":STATUS, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(DESCRIPTION==null?"\\N":DESCRIPTION, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(MEASURE_USED==null?"\\N":"" + MEASURE_USED, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(RE==null?"\\N":"" + RE, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(ESM==null?"\\N":"" + ESM, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(SOURCE==null?"\\N":SOURCE, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(KVI==null?"\\N":KVI, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(Sort_Order==null?"\\N":"" + Sort_Order, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(GOAL==null?"\\N":"" + GOAL, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(GOAL_TYPE==null?"\\N":GOAL_TYPE, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(SOURCE_SYSTEM==null?"\\N":SOURCE_SYSTEM, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(SUPERVISOR_ESTIMATE==null?"\\N":SUPERVISOR_ESTIMATE, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(SUPERVISOR_ESTIMATE_CHANGE_DATE==null?"\\N":"" + SUPERVISOR_ESTIMATE_CHANGE_DATE, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(ACTIVITY_START_DATE==null?"\\N":"" + ACTIVITY_START_DATE, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(ACTIVITY_END_DATE==null?"\\N":"" + ACTIVITY_END_DATE, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(KEY_INDICATOR==null?"\\N":KEY_INDICATOR, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(KVI_GROUP==null?"\\N":KVI_GROUP, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(COLOR==null?"\\N":COLOR, delimiters));
    if (useRecordDelim) {
      __sb.append(delimiters.getLinesTerminatedBy());
    }
    return __sb.toString();
  }
  public void toString0(DelimiterSet delimiters, StringBuilder __sb, char fieldDelim) {
    __sb.append(FieldFormatter.escapeAndEnclose(ACTIVITYID==null?"\\N":"" + ACTIVITYID, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(DEPARTMENT==null?"\\N":DEPARTMENT, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(ACTIVITY_GROUP==null?"\\N":ACTIVITY_GROUP, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(ACTIVITY_TYPE==null?"\\N":ACTIVITY_TYPE, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(ACTIVITY==null?"\\N":ACTIVITY, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(STATUS==null?"\\N":STATUS, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(DESCRIPTION==null?"\\N":DESCRIPTION, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(MEASURE_USED==null?"\\N":"" + MEASURE_USED, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(RE==null?"\\N":"" + RE, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(ESM==null?"\\N":"" + ESM, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(SOURCE==null?"\\N":SOURCE, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(KVI==null?"\\N":KVI, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(Sort_Order==null?"\\N":"" + Sort_Order, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(GOAL==null?"\\N":"" + GOAL, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(GOAL_TYPE==null?"\\N":GOAL_TYPE, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(SOURCE_SYSTEM==null?"\\N":SOURCE_SYSTEM, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(SUPERVISOR_ESTIMATE==null?"\\N":SUPERVISOR_ESTIMATE, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(SUPERVISOR_ESTIMATE_CHANGE_DATE==null?"\\N":"" + SUPERVISOR_ESTIMATE_CHANGE_DATE, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(ACTIVITY_START_DATE==null?"\\N":"" + ACTIVITY_START_DATE, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(ACTIVITY_END_DATE==null?"\\N":"" + ACTIVITY_END_DATE, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(KEY_INDICATOR==null?"\\N":KEY_INDICATOR, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(KVI_GROUP==null?"\\N":KVI_GROUP, delimiters));
    __sb.append(fieldDelim);
    // special case for strings hive, droppingdelimiters \n,\r,\01 from strings
    __sb.append(FieldFormatter.hiveStringDropDelims(COLOR==null?"\\N":COLOR, delimiters));
  }
  private static final DelimiterSet __inputDelimiters = new DelimiterSet((char) 1, (char) 10, (char) 0, (char) 0, false);
  private RecordParser __parser;
  public void parse(Text __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(CharSequence __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(byte [] __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(char [] __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(ByteBuffer __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(CharBuffer __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  private void __loadFromFields(List<String> fields) {
    Iterator<String> __it = fields.listIterator();
    String __cur_str = null;
    try {
    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.ACTIVITYID = null; } else {
      this.ACTIVITYID = Integer.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.DEPARTMENT = null; } else {
      this.DEPARTMENT = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.ACTIVITY_GROUP = null; } else {
      this.ACTIVITY_GROUP = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.ACTIVITY_TYPE = null; } else {
      this.ACTIVITY_TYPE = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.ACTIVITY = null; } else {
      this.ACTIVITY = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.STATUS = null; } else {
      this.STATUS = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.DESCRIPTION = null; } else {
      this.DESCRIPTION = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.MEASURE_USED = null; } else {
      this.MEASURE_USED = Double.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.RE = null; } else {
      this.RE = Double.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.ESM = null; } else {
      this.ESM = Double.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.SOURCE = null; } else {
      this.SOURCE = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.KVI = null; } else {
      this.KVI = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.Sort_Order = null; } else {
      this.Sort_Order = Integer.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.GOAL = null; } else {
      this.GOAL = Double.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.GOAL_TYPE = null; } else {
      this.GOAL_TYPE = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.SOURCE_SYSTEM = null; } else {
      this.SOURCE_SYSTEM = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.SUPERVISOR_ESTIMATE = null; } else {
      this.SUPERVISOR_ESTIMATE = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.SUPERVISOR_ESTIMATE_CHANGE_DATE = null; } else {
      this.SUPERVISOR_ESTIMATE_CHANGE_DATE = java.sql.Timestamp.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.ACTIVITY_START_DATE = null; } else {
      this.ACTIVITY_START_DATE = java.sql.Timestamp.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.ACTIVITY_END_DATE = null; } else {
      this.ACTIVITY_END_DATE = java.sql.Timestamp.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.KEY_INDICATOR = null; } else {
      this.KEY_INDICATOR = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.KVI_GROUP = null; } else {
      this.KVI_GROUP = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.COLOR = null; } else {
      this.COLOR = __cur_str;
    }

    } catch (RuntimeException e) {    throw new RuntimeException("Can't parse input data: '" + __cur_str + "'", e);    }  }

  private void __loadFromFields0(Iterator<String> __it) {
    String __cur_str = null;
    try {
    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.ACTIVITYID = null; } else {
      this.ACTIVITYID = Integer.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.DEPARTMENT = null; } else {
      this.DEPARTMENT = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.ACTIVITY_GROUP = null; } else {
      this.ACTIVITY_GROUP = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.ACTIVITY_TYPE = null; } else {
      this.ACTIVITY_TYPE = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.ACTIVITY = null; } else {
      this.ACTIVITY = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.STATUS = null; } else {
      this.STATUS = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.DESCRIPTION = null; } else {
      this.DESCRIPTION = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.MEASURE_USED = null; } else {
      this.MEASURE_USED = Double.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.RE = null; } else {
      this.RE = Double.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.ESM = null; } else {
      this.ESM = Double.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.SOURCE = null; } else {
      this.SOURCE = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.KVI = null; } else {
      this.KVI = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.Sort_Order = null; } else {
      this.Sort_Order = Integer.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.GOAL = null; } else {
      this.GOAL = Double.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.GOAL_TYPE = null; } else {
      this.GOAL_TYPE = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.SOURCE_SYSTEM = null; } else {
      this.SOURCE_SYSTEM = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.SUPERVISOR_ESTIMATE = null; } else {
      this.SUPERVISOR_ESTIMATE = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.SUPERVISOR_ESTIMATE_CHANGE_DATE = null; } else {
      this.SUPERVISOR_ESTIMATE_CHANGE_DATE = java.sql.Timestamp.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.ACTIVITY_START_DATE = null; } else {
      this.ACTIVITY_START_DATE = java.sql.Timestamp.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.ACTIVITY_END_DATE = null; } else {
      this.ACTIVITY_END_DATE = java.sql.Timestamp.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.KEY_INDICATOR = null; } else {
      this.KEY_INDICATOR = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.KVI_GROUP = null; } else {
      this.KVI_GROUP = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.COLOR = null; } else {
      this.COLOR = __cur_str;
    }

    } catch (RuntimeException e) {    throw new RuntimeException("Can't parse input data: '" + __cur_str + "'", e);    }  }

  public Object clone() throws CloneNotSupportedException {
    MSOC__MSOC_ACTIVITY o = (MSOC__MSOC_ACTIVITY) super.clone();
    o.SUPERVISOR_ESTIMATE_CHANGE_DATE = (o.SUPERVISOR_ESTIMATE_CHANGE_DATE != null) ? (java.sql.Timestamp) o.SUPERVISOR_ESTIMATE_CHANGE_DATE.clone() : null;
    o.ACTIVITY_START_DATE = (o.ACTIVITY_START_DATE != null) ? (java.sql.Timestamp) o.ACTIVITY_START_DATE.clone() : null;
    o.ACTIVITY_END_DATE = (o.ACTIVITY_END_DATE != null) ? (java.sql.Timestamp) o.ACTIVITY_END_DATE.clone() : null;
    return o;
  }

  public void clone0(MSOC__MSOC_ACTIVITY o) throws CloneNotSupportedException {
    o.SUPERVISOR_ESTIMATE_CHANGE_DATE = (o.SUPERVISOR_ESTIMATE_CHANGE_DATE != null) ? (java.sql.Timestamp) o.SUPERVISOR_ESTIMATE_CHANGE_DATE.clone() : null;
    o.ACTIVITY_START_DATE = (o.ACTIVITY_START_DATE != null) ? (java.sql.Timestamp) o.ACTIVITY_START_DATE.clone() : null;
    o.ACTIVITY_END_DATE = (o.ACTIVITY_END_DATE != null) ? (java.sql.Timestamp) o.ACTIVITY_END_DATE.clone() : null;
  }

  public Map<String, Object> getFieldMap() {
    Map<String, Object> __sqoop$field_map = new TreeMap<String, Object>();
    __sqoop$field_map.put("ACTIVITYID", this.ACTIVITYID);
    __sqoop$field_map.put("DEPARTMENT", this.DEPARTMENT);
    __sqoop$field_map.put("ACTIVITY_GROUP", this.ACTIVITY_GROUP);
    __sqoop$field_map.put("ACTIVITY_TYPE", this.ACTIVITY_TYPE);
    __sqoop$field_map.put("ACTIVITY", this.ACTIVITY);
    __sqoop$field_map.put("STATUS", this.STATUS);
    __sqoop$field_map.put("DESCRIPTION", this.DESCRIPTION);
    __sqoop$field_map.put("MEASURE_USED", this.MEASURE_USED);
    __sqoop$field_map.put("RE", this.RE);
    __sqoop$field_map.put("ESM", this.ESM);
    __sqoop$field_map.put("SOURCE", this.SOURCE);
    __sqoop$field_map.put("KVI", this.KVI);
    __sqoop$field_map.put("Sort_Order", this.Sort_Order);
    __sqoop$field_map.put("GOAL", this.GOAL);
    __sqoop$field_map.put("GOAL_TYPE", this.GOAL_TYPE);
    __sqoop$field_map.put("SOURCE_SYSTEM", this.SOURCE_SYSTEM);
    __sqoop$field_map.put("SUPERVISOR_ESTIMATE", this.SUPERVISOR_ESTIMATE);
    __sqoop$field_map.put("SUPERVISOR_ESTIMATE_CHANGE_DATE", this.SUPERVISOR_ESTIMATE_CHANGE_DATE);
    __sqoop$field_map.put("ACTIVITY_START_DATE", this.ACTIVITY_START_DATE);
    __sqoop$field_map.put("ACTIVITY_END_DATE", this.ACTIVITY_END_DATE);
    __sqoop$field_map.put("KEY_INDICATOR", this.KEY_INDICATOR);
    __sqoop$field_map.put("KVI_GROUP", this.KVI_GROUP);
    __sqoop$field_map.put("COLOR", this.COLOR);
    return __sqoop$field_map;
  }

  public void getFieldMap0(Map<String, Object> __sqoop$field_map) {
    __sqoop$field_map.put("ACTIVITYID", this.ACTIVITYID);
    __sqoop$field_map.put("DEPARTMENT", this.DEPARTMENT);
    __sqoop$field_map.put("ACTIVITY_GROUP", this.ACTIVITY_GROUP);
    __sqoop$field_map.put("ACTIVITY_TYPE", this.ACTIVITY_TYPE);
    __sqoop$field_map.put("ACTIVITY", this.ACTIVITY);
    __sqoop$field_map.put("STATUS", this.STATUS);
    __sqoop$field_map.put("DESCRIPTION", this.DESCRIPTION);
    __sqoop$field_map.put("MEASURE_USED", this.MEASURE_USED);
    __sqoop$field_map.put("RE", this.RE);
    __sqoop$field_map.put("ESM", this.ESM);
    __sqoop$field_map.put("SOURCE", this.SOURCE);
    __sqoop$field_map.put("KVI", this.KVI);
    __sqoop$field_map.put("Sort_Order", this.Sort_Order);
    __sqoop$field_map.put("GOAL", this.GOAL);
    __sqoop$field_map.put("GOAL_TYPE", this.GOAL_TYPE);
    __sqoop$field_map.put("SOURCE_SYSTEM", this.SOURCE_SYSTEM);
    __sqoop$field_map.put("SUPERVISOR_ESTIMATE", this.SUPERVISOR_ESTIMATE);
    __sqoop$field_map.put("SUPERVISOR_ESTIMATE_CHANGE_DATE", this.SUPERVISOR_ESTIMATE_CHANGE_DATE);
    __sqoop$field_map.put("ACTIVITY_START_DATE", this.ACTIVITY_START_DATE);
    __sqoop$field_map.put("ACTIVITY_END_DATE", this.ACTIVITY_END_DATE);
    __sqoop$field_map.put("KEY_INDICATOR", this.KEY_INDICATOR);
    __sqoop$field_map.put("KVI_GROUP", this.KVI_GROUP);
    __sqoop$field_map.put("COLOR", this.COLOR);
  }

  public void setField(String __fieldName, Object __fieldVal) {
    if ("ACTIVITYID".equals(__fieldName)) {
      this.ACTIVITYID = (Integer) __fieldVal;
    }
    else    if ("DEPARTMENT".equals(__fieldName)) {
      this.DEPARTMENT = (String) __fieldVal;
    }
    else    if ("ACTIVITY_GROUP".equals(__fieldName)) {
      this.ACTIVITY_GROUP = (String) __fieldVal;
    }
    else    if ("ACTIVITY_TYPE".equals(__fieldName)) {
      this.ACTIVITY_TYPE = (String) __fieldVal;
    }
    else    if ("ACTIVITY".equals(__fieldName)) {
      this.ACTIVITY = (String) __fieldVal;
    }
    else    if ("STATUS".equals(__fieldName)) {
      this.STATUS = (String) __fieldVal;
    }
    else    if ("DESCRIPTION".equals(__fieldName)) {
      this.DESCRIPTION = (String) __fieldVal;
    }
    else    if ("MEASURE_USED".equals(__fieldName)) {
      this.MEASURE_USED = (Double) __fieldVal;
    }
    else    if ("RE".equals(__fieldName)) {
      this.RE = (Double) __fieldVal;
    }
    else    if ("ESM".equals(__fieldName)) {
      this.ESM = (Double) __fieldVal;
    }
    else    if ("SOURCE".equals(__fieldName)) {
      this.SOURCE = (String) __fieldVal;
    }
    else    if ("KVI".equals(__fieldName)) {
      this.KVI = (String) __fieldVal;
    }
    else    if ("Sort_Order".equals(__fieldName)) {
      this.Sort_Order = (Integer) __fieldVal;
    }
    else    if ("GOAL".equals(__fieldName)) {
      this.GOAL = (Double) __fieldVal;
    }
    else    if ("GOAL_TYPE".equals(__fieldName)) {
      this.GOAL_TYPE = (String) __fieldVal;
    }
    else    if ("SOURCE_SYSTEM".equals(__fieldName)) {
      this.SOURCE_SYSTEM = (String) __fieldVal;
    }
    else    if ("SUPERVISOR_ESTIMATE".equals(__fieldName)) {
      this.SUPERVISOR_ESTIMATE = (String) __fieldVal;
    }
    else    if ("SUPERVISOR_ESTIMATE_CHANGE_DATE".equals(__fieldName)) {
      this.SUPERVISOR_ESTIMATE_CHANGE_DATE = (java.sql.Timestamp) __fieldVal;
    }
    else    if ("ACTIVITY_START_DATE".equals(__fieldName)) {
      this.ACTIVITY_START_DATE = (java.sql.Timestamp) __fieldVal;
    }
    else    if ("ACTIVITY_END_DATE".equals(__fieldName)) {
      this.ACTIVITY_END_DATE = (java.sql.Timestamp) __fieldVal;
    }
    else    if ("KEY_INDICATOR".equals(__fieldName)) {
      this.KEY_INDICATOR = (String) __fieldVal;
    }
    else    if ("KVI_GROUP".equals(__fieldName)) {
      this.KVI_GROUP = (String) __fieldVal;
    }
    else    if ("COLOR".equals(__fieldName)) {
      this.COLOR = (String) __fieldVal;
    }
    else {
      throw new RuntimeException("No such field: " + __fieldName);
    }
  }
  public boolean setField0(String __fieldName, Object __fieldVal) {
    if ("ACTIVITYID".equals(__fieldName)) {
      this.ACTIVITYID = (Integer) __fieldVal;
      return true;
    }
    else    if ("DEPARTMENT".equals(__fieldName)) {
      this.DEPARTMENT = (String) __fieldVal;
      return true;
    }
    else    if ("ACTIVITY_GROUP".equals(__fieldName)) {
      this.ACTIVITY_GROUP = (String) __fieldVal;
      return true;
    }
    else    if ("ACTIVITY_TYPE".equals(__fieldName)) {
      this.ACTIVITY_TYPE = (String) __fieldVal;
      return true;
    }
    else    if ("ACTIVITY".equals(__fieldName)) {
      this.ACTIVITY = (String) __fieldVal;
      return true;
    }
    else    if ("STATUS".equals(__fieldName)) {
      this.STATUS = (String) __fieldVal;
      return true;
    }
    else    if ("DESCRIPTION".equals(__fieldName)) {
      this.DESCRIPTION = (String) __fieldVal;
      return true;
    }
    else    if ("MEASURE_USED".equals(__fieldName)) {
      this.MEASURE_USED = (Double) __fieldVal;
      return true;
    }
    else    if ("RE".equals(__fieldName)) {
      this.RE = (Double) __fieldVal;
      return true;
    }
    else    if ("ESM".equals(__fieldName)) {
      this.ESM = (Double) __fieldVal;
      return true;
    }
    else    if ("SOURCE".equals(__fieldName)) {
      this.SOURCE = (String) __fieldVal;
      return true;
    }
    else    if ("KVI".equals(__fieldName)) {
      this.KVI = (String) __fieldVal;
      return true;
    }
    else    if ("Sort_Order".equals(__fieldName)) {
      this.Sort_Order = (Integer) __fieldVal;
      return true;
    }
    else    if ("GOAL".equals(__fieldName)) {
      this.GOAL = (Double) __fieldVal;
      return true;
    }
    else    if ("GOAL_TYPE".equals(__fieldName)) {
      this.GOAL_TYPE = (String) __fieldVal;
      return true;
    }
    else    if ("SOURCE_SYSTEM".equals(__fieldName)) {
      this.SOURCE_SYSTEM = (String) __fieldVal;
      return true;
    }
    else    if ("SUPERVISOR_ESTIMATE".equals(__fieldName)) {
      this.SUPERVISOR_ESTIMATE = (String) __fieldVal;
      return true;
    }
    else    if ("SUPERVISOR_ESTIMATE_CHANGE_DATE".equals(__fieldName)) {
      this.SUPERVISOR_ESTIMATE_CHANGE_DATE = (java.sql.Timestamp) __fieldVal;
      return true;
    }
    else    if ("ACTIVITY_START_DATE".equals(__fieldName)) {
      this.ACTIVITY_START_DATE = (java.sql.Timestamp) __fieldVal;
      return true;
    }
    else    if ("ACTIVITY_END_DATE".equals(__fieldName)) {
      this.ACTIVITY_END_DATE = (java.sql.Timestamp) __fieldVal;
      return true;
    }
    else    if ("KEY_INDICATOR".equals(__fieldName)) {
      this.KEY_INDICATOR = (String) __fieldVal;
      return true;
    }
    else    if ("KVI_GROUP".equals(__fieldName)) {
      this.KVI_GROUP = (String) __fieldVal;
      return true;
    }
    else    if ("COLOR".equals(__fieldName)) {
      this.COLOR = (String) __fieldVal;
      return true;
    }
    else {
      return false;    }
  }
}
