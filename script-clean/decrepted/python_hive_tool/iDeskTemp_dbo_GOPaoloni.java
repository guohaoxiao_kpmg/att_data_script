// ORM class for table 'iDeskTemp.dbo.GOPaoloni'
// WARNING: This class is AUTO-GENERATED. Modify at your own risk.
//
// Debug information:
// Generated date: Mon Jul 18 15:25:30 EDT 2016
// For connector: org.apache.sqoop.manager.GenericJdbcManager
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapred.lib.db.DBWritable;
import com.cloudera.sqoop.lib.JdbcWritableBridge;
import com.cloudera.sqoop.lib.DelimiterSet;
import com.cloudera.sqoop.lib.FieldFormatter;
import com.cloudera.sqoop.lib.RecordParser;
import com.cloudera.sqoop.lib.BooleanParser;
import com.cloudera.sqoop.lib.BlobRef;
import com.cloudera.sqoop.lib.ClobRef;
import com.cloudera.sqoop.lib.LargeObjectLoader;
import com.cloudera.sqoop.lib.SqoopRecord;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class iDeskTemp_dbo_GOPaoloni extends SqoopRecord  implements DBWritable, Writable {
  private final int PROTOCOL_VERSION = 3;
  public int getClassFormatVersion() { return PROTOCOL_VERSION; }
  protected ResultSet __cur_result_set;
  private String sr_accepted_date;
  public String get_sr_accepted_date() {
    return sr_accepted_date;
  }
  public void set_sr_accepted_date(String sr_accepted_date) {
    this.sr_accepted_date = sr_accepted_date;
  }
  public iDeskTemp_dbo_GOPaoloni with_sr_accepted_date(String sr_accepted_date) {
    this.sr_accepted_date = sr_accepted_date;
    return this;
  }
  private String number_of_circuits;
  public String get_number_of_circuits() {
    return number_of_circuits;
  }
  public void set_number_of_circuits(String number_of_circuits) {
    this.number_of_circuits = number_of_circuits;
  }
  public iDeskTemp_dbo_GOPaoloni with_number_of_circuits(String number_of_circuits) {
    this.number_of_circuits = number_of_circuits;
    return this;
  }
  private String sr_id;
  public String get_sr_id() {
    return sr_id;
  }
  public void set_sr_id(String sr_id) {
    this.sr_id = sr_id;
  }
  public iDeskTemp_dbo_GOPaoloni with_sr_id(String sr_id) {
    this.sr_id = sr_id;
    return this;
  }
  private String is_this_a_managed_service;
  public String get_is_this_a_managed_service() {
    return is_this_a_managed_service;
  }
  public void set_is_this_a_managed_service(String is_this_a_managed_service) {
    this.is_this_a_managed_service = is_this_a_managed_service;
  }
  public iDeskTemp_dbo_GOPaoloni with_is_this_a_managed_service(String is_this_a_managed_service) {
    this.is_this_a_managed_service = is_this_a_managed_service;
    return this;
  }
  private String number_of_sites;
  public String get_number_of_sites() {
    return number_of_sites;
  }
  public void set_number_of_sites(String number_of_sites) {
    this.number_of_sites = number_of_sites;
  }
  public iDeskTemp_dbo_GOPaoloni with_number_of_sites(String number_of_sites) {
    this.number_of_sites = number_of_sites;
    return this;
  }
  private String order_type;
  public String get_order_type() {
    return order_type;
  }
  public void set_order_type(String order_type) {
    this.order_type = order_type;
  }
  public iDeskTemp_dbo_GOPaoloni with_order_type(String order_type) {
    this.order_type = order_type;
    return this;
  }
  private String sr_order_type_and_numbers;
  public String get_sr_order_type_and_numbers() {
    return sr_order_type_and_numbers;
  }
  public void set_sr_order_type_and_numbers(String sr_order_type_and_numbers) {
    this.sr_order_type_and_numbers = sr_order_type_and_numbers;
  }
  public iDeskTemp_dbo_GOPaoloni with_sr_order_type_and_numbers(String sr_order_type_and_numbers) {
    this.sr_order_type_and_numbers = sr_order_type_and_numbers;
    return this;
  }
  private String task_css_template;
  public String get_task_css_template() {
    return task_css_template;
  }
  public void set_task_css_template(String task_css_template) {
    this.task_css_template = task_css_template;
  }
  public iDeskTemp_dbo_GOPaoloni with_task_css_template(String task_css_template) {
    this.task_css_template = task_css_template;
    return this;
  }
  private String oppty_id;
  public String get_oppty_id() {
    return oppty_id;
  }
  public void set_oppty_id(String oppty_id) {
    this.oppty_id = oppty_id;
  }
  public iDeskTemp_dbo_GOPaoloni with_oppty_id(String oppty_id) {
    this.oppty_id = oppty_id;
    return this;
  }
  private String sr_productlist;
  public String get_sr_productlist() {
    return sr_productlist;
  }
  public void set_sr_productlist(String sr_productlist) {
    this.sr_productlist = sr_productlist;
  }
  public iDeskTemp_dbo_GOPaoloni with_sr_productlist(String sr_productlist) {
    this.sr_productlist = sr_productlist;
    return this;
  }
  private String task_completed_actual_date;
  public String get_task_completed_actual_date() {
    return task_completed_actual_date;
  }
  public void set_task_completed_actual_date(String task_completed_actual_date) {
    this.task_completed_actual_date = task_completed_actual_date;
  }
  public iDeskTemp_dbo_GOPaoloni with_task_completed_actual_date(String task_completed_actual_date) {
    this.task_completed_actual_date = task_completed_actual_date;
    return this;
  }
  private String task_name;
  public String get_task_name() {
    return task_name;
  }
  public void set_task_name(String task_name) {
    this.task_name = task_name;
  }
  public iDeskTemp_dbo_GOPaoloni with_task_name(String task_name) {
    this.task_name = task_name;
    return this;
  }
  private String task_status;
  public String get_task_status() {
    return task_status;
  }
  public void set_task_status(String task_status) {
    this.task_status = task_status;
  }
  public iDeskTemp_dbo_GOPaoloni with_task_status(String task_status) {
    this.task_status = task_status;
    return this;
  }
  private String task_last_updated_by;
  public String get_task_last_updated_by() {
    return task_last_updated_by;
  }
  public void set_task_last_updated_by(String task_last_updated_by) {
    this.task_last_updated_by = task_last_updated_by;
  }
  public iDeskTemp_dbo_GOPaoloni with_task_last_updated_by(String task_last_updated_by) {
    this.task_last_updated_by = task_last_updated_by;
    return this;
  }
  private String task_product_group;
  public String get_task_product_group() {
    return task_product_group;
  }
  public void set_task_product_group(String task_product_group) {
    this.task_product_group = task_product_group;
  }
  public iDeskTemp_dbo_GOPaoloni with_task_product_group(String task_product_group) {
    this.task_product_group = task_product_group;
    return this;
  }
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof iDeskTemp_dbo_GOPaoloni)) {
      return false;
    }
    iDeskTemp_dbo_GOPaoloni that = (iDeskTemp_dbo_GOPaoloni) o;
    boolean equal = true;
    equal = equal && (this.sr_accepted_date == null ? that.sr_accepted_date == null : this.sr_accepted_date.equals(that.sr_accepted_date));
    equal = equal && (this.number_of_circuits == null ? that.number_of_circuits == null : this.number_of_circuits.equals(that.number_of_circuits));
    equal = equal && (this.sr_id == null ? that.sr_id == null : this.sr_id.equals(that.sr_id));
    equal = equal && (this.is_this_a_managed_service == null ? that.is_this_a_managed_service == null : this.is_this_a_managed_service.equals(that.is_this_a_managed_service));
    equal = equal && (this.number_of_sites == null ? that.number_of_sites == null : this.number_of_sites.equals(that.number_of_sites));
    equal = equal && (this.order_type == null ? that.order_type == null : this.order_type.equals(that.order_type));
    equal = equal && (this.sr_order_type_and_numbers == null ? that.sr_order_type_and_numbers == null : this.sr_order_type_and_numbers.equals(that.sr_order_type_and_numbers));
    equal = equal && (this.task_css_template == null ? that.task_css_template == null : this.task_css_template.equals(that.task_css_template));
    equal = equal && (this.oppty_id == null ? that.oppty_id == null : this.oppty_id.equals(that.oppty_id));
    equal = equal && (this.sr_productlist == null ? that.sr_productlist == null : this.sr_productlist.equals(that.sr_productlist));
    equal = equal && (this.task_completed_actual_date == null ? that.task_completed_actual_date == null : this.task_completed_actual_date.equals(that.task_completed_actual_date));
    equal = equal && (this.task_name == null ? that.task_name == null : this.task_name.equals(that.task_name));
    equal = equal && (this.task_status == null ? that.task_status == null : this.task_status.equals(that.task_status));
    equal = equal && (this.task_last_updated_by == null ? that.task_last_updated_by == null : this.task_last_updated_by.equals(that.task_last_updated_by));
    equal = equal && (this.task_product_group == null ? that.task_product_group == null : this.task_product_group.equals(that.task_product_group));
    return equal;
  }
  public boolean equals0(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof iDeskTemp_dbo_GOPaoloni)) {
      return false;
    }
    iDeskTemp_dbo_GOPaoloni that = (iDeskTemp_dbo_GOPaoloni) o;
    boolean equal = true;
    equal = equal && (this.sr_accepted_date == null ? that.sr_accepted_date == null : this.sr_accepted_date.equals(that.sr_accepted_date));
    equal = equal && (this.number_of_circuits == null ? that.number_of_circuits == null : this.number_of_circuits.equals(that.number_of_circuits));
    equal = equal && (this.sr_id == null ? that.sr_id == null : this.sr_id.equals(that.sr_id));
    equal = equal && (this.is_this_a_managed_service == null ? that.is_this_a_managed_service == null : this.is_this_a_managed_service.equals(that.is_this_a_managed_service));
    equal = equal && (this.number_of_sites == null ? that.number_of_sites == null : this.number_of_sites.equals(that.number_of_sites));
    equal = equal && (this.order_type == null ? that.order_type == null : this.order_type.equals(that.order_type));
    equal = equal && (this.sr_order_type_and_numbers == null ? that.sr_order_type_and_numbers == null : this.sr_order_type_and_numbers.equals(that.sr_order_type_and_numbers));
    equal = equal && (this.task_css_template == null ? that.task_css_template == null : this.task_css_template.equals(that.task_css_template));
    equal = equal && (this.oppty_id == null ? that.oppty_id == null : this.oppty_id.equals(that.oppty_id));
    equal = equal && (this.sr_productlist == null ? that.sr_productlist == null : this.sr_productlist.equals(that.sr_productlist));
    equal = equal && (this.task_completed_actual_date == null ? that.task_completed_actual_date == null : this.task_completed_actual_date.equals(that.task_completed_actual_date));
    equal = equal && (this.task_name == null ? that.task_name == null : this.task_name.equals(that.task_name));
    equal = equal && (this.task_status == null ? that.task_status == null : this.task_status.equals(that.task_status));
    equal = equal && (this.task_last_updated_by == null ? that.task_last_updated_by == null : this.task_last_updated_by.equals(that.task_last_updated_by));
    equal = equal && (this.task_product_group == null ? that.task_product_group == null : this.task_product_group.equals(that.task_product_group));
    return equal;
  }
  public void readFields(ResultSet __dbResults) throws SQLException {
    this.__cur_result_set = __dbResults;
    this.sr_accepted_date = JdbcWritableBridge.readString(1, __dbResults);
    this.number_of_circuits = JdbcWritableBridge.readString(2, __dbResults);
    this.sr_id = JdbcWritableBridge.readString(3, __dbResults);
    this.is_this_a_managed_service = JdbcWritableBridge.readString(4, __dbResults);
    this.number_of_sites = JdbcWritableBridge.readString(5, __dbResults);
    this.order_type = JdbcWritableBridge.readString(6, __dbResults);
    this.sr_order_type_and_numbers = JdbcWritableBridge.readString(7, __dbResults);
    this.task_css_template = JdbcWritableBridge.readString(8, __dbResults);
    this.oppty_id = JdbcWritableBridge.readString(9, __dbResults);
    this.sr_productlist = JdbcWritableBridge.readString(10, __dbResults);
    this.task_completed_actual_date = JdbcWritableBridge.readString(11, __dbResults);
    this.task_name = JdbcWritableBridge.readString(12, __dbResults);
    this.task_status = JdbcWritableBridge.readString(13, __dbResults);
    this.task_last_updated_by = JdbcWritableBridge.readString(14, __dbResults);
    this.task_product_group = JdbcWritableBridge.readString(15, __dbResults);
  }
  public void readFields0(ResultSet __dbResults) throws SQLException {
    this.sr_accepted_date = JdbcWritableBridge.readString(1, __dbResults);
    this.number_of_circuits = JdbcWritableBridge.readString(2, __dbResults);
    this.sr_id = JdbcWritableBridge.readString(3, __dbResults);
    this.is_this_a_managed_service = JdbcWritableBridge.readString(4, __dbResults);
    this.number_of_sites = JdbcWritableBridge.readString(5, __dbResults);
    this.order_type = JdbcWritableBridge.readString(6, __dbResults);
    this.sr_order_type_and_numbers = JdbcWritableBridge.readString(7, __dbResults);
    this.task_css_template = JdbcWritableBridge.readString(8, __dbResults);
    this.oppty_id = JdbcWritableBridge.readString(9, __dbResults);
    this.sr_productlist = JdbcWritableBridge.readString(10, __dbResults);
    this.task_completed_actual_date = JdbcWritableBridge.readString(11, __dbResults);
    this.task_name = JdbcWritableBridge.readString(12, __dbResults);
    this.task_status = JdbcWritableBridge.readString(13, __dbResults);
    this.task_last_updated_by = JdbcWritableBridge.readString(14, __dbResults);
    this.task_product_group = JdbcWritableBridge.readString(15, __dbResults);
  }
  public void loadLargeObjects(LargeObjectLoader __loader)
      throws SQLException, IOException, InterruptedException {
  }
  public void loadLargeObjects0(LargeObjectLoader __loader)
      throws SQLException, IOException, InterruptedException {
  }
  public void write(PreparedStatement __dbStmt) throws SQLException {
    write(__dbStmt, 0);
  }

  public int write(PreparedStatement __dbStmt, int __off) throws SQLException {
    JdbcWritableBridge.writeString(sr_accepted_date, 1 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(number_of_circuits, 2 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(sr_id, 3 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(is_this_a_managed_service, 4 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(number_of_sites, 5 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(order_type, 6 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(sr_order_type_and_numbers, 7 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(task_css_template, 8 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(oppty_id, 9 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(sr_productlist, 10 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(task_completed_actual_date, 11 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(task_name, 12 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(task_status, 13 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(task_last_updated_by, 14 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(task_product_group, 15 + __off, 12, __dbStmt);
    return 15;
  }
  public void write0(PreparedStatement __dbStmt, int __off) throws SQLException {
    JdbcWritableBridge.writeString(sr_accepted_date, 1 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(number_of_circuits, 2 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(sr_id, 3 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(is_this_a_managed_service, 4 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(number_of_sites, 5 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(order_type, 6 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(sr_order_type_and_numbers, 7 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(task_css_template, 8 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(oppty_id, 9 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(sr_productlist, 10 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(task_completed_actual_date, 11 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(task_name, 12 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(task_status, 13 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(task_last_updated_by, 14 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(task_product_group, 15 + __off, 12, __dbStmt);
  }
  public void readFields(DataInput __dataIn) throws IOException {
this.readFields0(__dataIn);  }
  public void readFields0(DataInput __dataIn) throws IOException {
    if (__dataIn.readBoolean()) { 
        this.sr_accepted_date = null;
    } else {
    this.sr_accepted_date = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.number_of_circuits = null;
    } else {
    this.number_of_circuits = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.sr_id = null;
    } else {
    this.sr_id = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.is_this_a_managed_service = null;
    } else {
    this.is_this_a_managed_service = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.number_of_sites = null;
    } else {
    this.number_of_sites = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.order_type = null;
    } else {
    this.order_type = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.sr_order_type_and_numbers = null;
    } else {
    this.sr_order_type_and_numbers = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.task_css_template = null;
    } else {
    this.task_css_template = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.oppty_id = null;
    } else {
    this.oppty_id = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.sr_productlist = null;
    } else {
    this.sr_productlist = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.task_completed_actual_date = null;
    } else {
    this.task_completed_actual_date = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.task_name = null;
    } else {
    this.task_name = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.task_status = null;
    } else {
    this.task_status = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.task_last_updated_by = null;
    } else {
    this.task_last_updated_by = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.task_product_group = null;
    } else {
    this.task_product_group = Text.readString(__dataIn);
    }
  }
  public void write(DataOutput __dataOut) throws IOException {
    if (null == this.sr_accepted_date) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, sr_accepted_date);
    }
    if (null == this.number_of_circuits) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, number_of_circuits);
    }
    if (null == this.sr_id) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, sr_id);
    }
    if (null == this.is_this_a_managed_service) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, is_this_a_managed_service);
    }
    if (null == this.number_of_sites) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, number_of_sites);
    }
    if (null == this.order_type) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, order_type);
    }
    if (null == this.sr_order_type_and_numbers) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, sr_order_type_and_numbers);
    }
    if (null == this.task_css_template) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, task_css_template);
    }
    if (null == this.oppty_id) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, oppty_id);
    }
    if (null == this.sr_productlist) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, sr_productlist);
    }
    if (null == this.task_completed_actual_date) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, task_completed_actual_date);
    }
    if (null == this.task_name) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, task_name);
    }
    if (null == this.task_status) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, task_status);
    }
    if (null == this.task_last_updated_by) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, task_last_updated_by);
    }
    if (null == this.task_product_group) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, task_product_group);
    }
  }
  public void write0(DataOutput __dataOut) throws IOException {
    if (null == this.sr_accepted_date) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, sr_accepted_date);
    }
    if (null == this.number_of_circuits) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, number_of_circuits);
    }
    if (null == this.sr_id) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, sr_id);
    }
    if (null == this.is_this_a_managed_service) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, is_this_a_managed_service);
    }
    if (null == this.number_of_sites) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, number_of_sites);
    }
    if (null == this.order_type) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, order_type);
    }
    if (null == this.sr_order_type_and_numbers) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, sr_order_type_and_numbers);
    }
    if (null == this.task_css_template) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, task_css_template);
    }
    if (null == this.oppty_id) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, oppty_id);
    }
    if (null == this.sr_productlist) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, sr_productlist);
    }
    if (null == this.task_completed_actual_date) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, task_completed_actual_date);
    }
    if (null == this.task_name) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, task_name);
    }
    if (null == this.task_status) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, task_status);
    }
    if (null == this.task_last_updated_by) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, task_last_updated_by);
    }
    if (null == this.task_product_group) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, task_product_group);
    }
  }
  private static final DelimiterSet __outputDelimiters = new DelimiterSet((char) 1, (char) 10, (char) 0, (char) 0, false);
  public String toString() {
    return toString(__outputDelimiters, true);
  }
  public String toString(DelimiterSet delimiters) {
    return toString(delimiters, true);
  }
  public String toString(boolean useRecordDelim) {
    return toString(__outputDelimiters, useRecordDelim);
  }
  public String toString(DelimiterSet delimiters, boolean useRecordDelim) {
    StringBuilder __sb = new StringBuilder();
    char fieldDelim = delimiters.getFieldsTerminatedBy();
    __sb.append(FieldFormatter.escapeAndEnclose(sr_accepted_date==null?"null":sr_accepted_date, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(number_of_circuits==null?"null":number_of_circuits, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(sr_id==null?"null":sr_id, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(is_this_a_managed_service==null?"null":is_this_a_managed_service, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(number_of_sites==null?"null":number_of_sites, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(order_type==null?"null":order_type, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(sr_order_type_and_numbers==null?"null":sr_order_type_and_numbers, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(task_css_template==null?"null":task_css_template, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(oppty_id==null?"null":oppty_id, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(sr_productlist==null?"null":sr_productlist, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(task_completed_actual_date==null?"null":task_completed_actual_date, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(task_name==null?"null":task_name, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(task_status==null?"null":task_status, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(task_last_updated_by==null?"null":task_last_updated_by, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(task_product_group==null?"null":task_product_group, delimiters));
    if (useRecordDelim) {
      __sb.append(delimiters.getLinesTerminatedBy());
    }
    return __sb.toString();
  }
  public void toString0(DelimiterSet delimiters, StringBuilder __sb, char fieldDelim) {
    __sb.append(FieldFormatter.escapeAndEnclose(sr_accepted_date==null?"null":sr_accepted_date, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(number_of_circuits==null?"null":number_of_circuits, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(sr_id==null?"null":sr_id, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(is_this_a_managed_service==null?"null":is_this_a_managed_service, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(number_of_sites==null?"null":number_of_sites, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(order_type==null?"null":order_type, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(sr_order_type_and_numbers==null?"null":sr_order_type_and_numbers, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(task_css_template==null?"null":task_css_template, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(oppty_id==null?"null":oppty_id, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(sr_productlist==null?"null":sr_productlist, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(task_completed_actual_date==null?"null":task_completed_actual_date, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(task_name==null?"null":task_name, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(task_status==null?"null":task_status, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(task_last_updated_by==null?"null":task_last_updated_by, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(task_product_group==null?"null":task_product_group, delimiters));
  }
  private static final DelimiterSet __inputDelimiters = new DelimiterSet((char) 1, (char) 10, (char) 0, (char) 0, false);
  private RecordParser __parser;
  public void parse(Text __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(CharSequence __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(byte [] __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(char [] __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(ByteBuffer __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(CharBuffer __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  private void __loadFromFields(List<String> fields) {
    Iterator<String> __it = fields.listIterator();
    String __cur_str = null;
    try {
    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.sr_accepted_date = null; } else {
      this.sr_accepted_date = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.number_of_circuits = null; } else {
      this.number_of_circuits = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.sr_id = null; } else {
      this.sr_id = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.is_this_a_managed_service = null; } else {
      this.is_this_a_managed_service = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.number_of_sites = null; } else {
      this.number_of_sites = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.order_type = null; } else {
      this.order_type = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.sr_order_type_and_numbers = null; } else {
      this.sr_order_type_and_numbers = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.task_css_template = null; } else {
      this.task_css_template = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.oppty_id = null; } else {
      this.oppty_id = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.sr_productlist = null; } else {
      this.sr_productlist = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.task_completed_actual_date = null; } else {
      this.task_completed_actual_date = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.task_name = null; } else {
      this.task_name = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.task_status = null; } else {
      this.task_status = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.task_last_updated_by = null; } else {
      this.task_last_updated_by = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.task_product_group = null; } else {
      this.task_product_group = __cur_str;
    }

    } catch (RuntimeException e) {    throw new RuntimeException("Can't parse input data: '" + __cur_str + "'", e);    }  }

  private void __loadFromFields0(Iterator<String> __it) {
    String __cur_str = null;
    try {
    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.sr_accepted_date = null; } else {
      this.sr_accepted_date = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.number_of_circuits = null; } else {
      this.number_of_circuits = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.sr_id = null; } else {
      this.sr_id = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.is_this_a_managed_service = null; } else {
      this.is_this_a_managed_service = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.number_of_sites = null; } else {
      this.number_of_sites = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.order_type = null; } else {
      this.order_type = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.sr_order_type_and_numbers = null; } else {
      this.sr_order_type_and_numbers = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.task_css_template = null; } else {
      this.task_css_template = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.oppty_id = null; } else {
      this.oppty_id = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.sr_productlist = null; } else {
      this.sr_productlist = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.task_completed_actual_date = null; } else {
      this.task_completed_actual_date = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.task_name = null; } else {
      this.task_name = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.task_status = null; } else {
      this.task_status = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.task_last_updated_by = null; } else {
      this.task_last_updated_by = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.task_product_group = null; } else {
      this.task_product_group = __cur_str;
    }

    } catch (RuntimeException e) {    throw new RuntimeException("Can't parse input data: '" + __cur_str + "'", e);    }  }

  public Object clone() throws CloneNotSupportedException {
    iDeskTemp_dbo_GOPaoloni o = (iDeskTemp_dbo_GOPaoloni) super.clone();
    return o;
  }

  public void clone0(iDeskTemp_dbo_GOPaoloni o) throws CloneNotSupportedException {
  }

  public Map<String, Object> getFieldMap() {
    Map<String, Object> __sqoop$field_map = new TreeMap<String, Object>();
    __sqoop$field_map.put("sr_accepted_date", this.sr_accepted_date);
    __sqoop$field_map.put("number_of_circuits", this.number_of_circuits);
    __sqoop$field_map.put("sr_id", this.sr_id);
    __sqoop$field_map.put("is_this_a_managed_service", this.is_this_a_managed_service);
    __sqoop$field_map.put("number_of_sites", this.number_of_sites);
    __sqoop$field_map.put("order_type", this.order_type);
    __sqoop$field_map.put("sr_order_type_and_numbers", this.sr_order_type_and_numbers);
    __sqoop$field_map.put("task_css_template", this.task_css_template);
    __sqoop$field_map.put("oppty_id", this.oppty_id);
    __sqoop$field_map.put("sr_productlist", this.sr_productlist);
    __sqoop$field_map.put("task_completed_actual_date", this.task_completed_actual_date);
    __sqoop$field_map.put("task_name", this.task_name);
    __sqoop$field_map.put("task_status", this.task_status);
    __sqoop$field_map.put("task_last_updated_by", this.task_last_updated_by);
    __sqoop$field_map.put("task_product_group", this.task_product_group);
    return __sqoop$field_map;
  }

  public void getFieldMap0(Map<String, Object> __sqoop$field_map) {
    __sqoop$field_map.put("sr_accepted_date", this.sr_accepted_date);
    __sqoop$field_map.put("number_of_circuits", this.number_of_circuits);
    __sqoop$field_map.put("sr_id", this.sr_id);
    __sqoop$field_map.put("is_this_a_managed_service", this.is_this_a_managed_service);
    __sqoop$field_map.put("number_of_sites", this.number_of_sites);
    __sqoop$field_map.put("order_type", this.order_type);
    __sqoop$field_map.put("sr_order_type_and_numbers", this.sr_order_type_and_numbers);
    __sqoop$field_map.put("task_css_template", this.task_css_template);
    __sqoop$field_map.put("oppty_id", this.oppty_id);
    __sqoop$field_map.put("sr_productlist", this.sr_productlist);
    __sqoop$field_map.put("task_completed_actual_date", this.task_completed_actual_date);
    __sqoop$field_map.put("task_name", this.task_name);
    __sqoop$field_map.put("task_status", this.task_status);
    __sqoop$field_map.put("task_last_updated_by", this.task_last_updated_by);
    __sqoop$field_map.put("task_product_group", this.task_product_group);
  }

  public void setField(String __fieldName, Object __fieldVal) {
    if ("sr_accepted_date".equals(__fieldName)) {
      this.sr_accepted_date = (String) __fieldVal;
    }
    else    if ("number_of_circuits".equals(__fieldName)) {
      this.number_of_circuits = (String) __fieldVal;
    }
    else    if ("sr_id".equals(__fieldName)) {
      this.sr_id = (String) __fieldVal;
    }
    else    if ("is_this_a_managed_service".equals(__fieldName)) {
      this.is_this_a_managed_service = (String) __fieldVal;
    }
    else    if ("number_of_sites".equals(__fieldName)) {
      this.number_of_sites = (String) __fieldVal;
    }
    else    if ("order_type".equals(__fieldName)) {
      this.order_type = (String) __fieldVal;
    }
    else    if ("sr_order_type_and_numbers".equals(__fieldName)) {
      this.sr_order_type_and_numbers = (String) __fieldVal;
    }
    else    if ("task_css_template".equals(__fieldName)) {
      this.task_css_template = (String) __fieldVal;
    }
    else    if ("oppty_id".equals(__fieldName)) {
      this.oppty_id = (String) __fieldVal;
    }
    else    if ("sr_productlist".equals(__fieldName)) {
      this.sr_productlist = (String) __fieldVal;
    }
    else    if ("task_completed_actual_date".equals(__fieldName)) {
      this.task_completed_actual_date = (String) __fieldVal;
    }
    else    if ("task_name".equals(__fieldName)) {
      this.task_name = (String) __fieldVal;
    }
    else    if ("task_status".equals(__fieldName)) {
      this.task_status = (String) __fieldVal;
    }
    else    if ("task_last_updated_by".equals(__fieldName)) {
      this.task_last_updated_by = (String) __fieldVal;
    }
    else    if ("task_product_group".equals(__fieldName)) {
      this.task_product_group = (String) __fieldVal;
    }
    else {
      throw new RuntimeException("No such field: " + __fieldName);
    }
  }
  public boolean setField0(String __fieldName, Object __fieldVal) {
    if ("sr_accepted_date".equals(__fieldName)) {
      this.sr_accepted_date = (String) __fieldVal;
      return true;
    }
    else    if ("number_of_circuits".equals(__fieldName)) {
      this.number_of_circuits = (String) __fieldVal;
      return true;
    }
    else    if ("sr_id".equals(__fieldName)) {
      this.sr_id = (String) __fieldVal;
      return true;
    }
    else    if ("is_this_a_managed_service".equals(__fieldName)) {
      this.is_this_a_managed_service = (String) __fieldVal;
      return true;
    }
    else    if ("number_of_sites".equals(__fieldName)) {
      this.number_of_sites = (String) __fieldVal;
      return true;
    }
    else    if ("order_type".equals(__fieldName)) {
      this.order_type = (String) __fieldVal;
      return true;
    }
    else    if ("sr_order_type_and_numbers".equals(__fieldName)) {
      this.sr_order_type_and_numbers = (String) __fieldVal;
      return true;
    }
    else    if ("task_css_template".equals(__fieldName)) {
      this.task_css_template = (String) __fieldVal;
      return true;
    }
    else    if ("oppty_id".equals(__fieldName)) {
      this.oppty_id = (String) __fieldVal;
      return true;
    }
    else    if ("sr_productlist".equals(__fieldName)) {
      this.sr_productlist = (String) __fieldVal;
      return true;
    }
    else    if ("task_completed_actual_date".equals(__fieldName)) {
      this.task_completed_actual_date = (String) __fieldVal;
      return true;
    }
    else    if ("task_name".equals(__fieldName)) {
      this.task_name = (String) __fieldVal;
      return true;
    }
    else    if ("task_status".equals(__fieldName)) {
      this.task_status = (String) __fieldVal;
      return true;
    }
    else    if ("task_last_updated_by".equals(__fieldName)) {
      this.task_last_updated_by = (String) __fieldVal;
      return true;
    }
    else    if ("task_product_group".equals(__fieldName)) {
      this.task_product_group = (String) __fieldVal;
      return true;
    }
    else {
      return false;    }
  }
}
