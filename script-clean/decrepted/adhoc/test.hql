

USE KPMG_WS;
set hive.vectorized.execution.enabled = true;
set hive.vectorized.execution.reduce.enabled =true;
set hive.cbo.enable=true;
set hive.compute.query.using.stats=true;
set hive.stats.fetch.column.stats=true;
set hive.stats.fetch.partition.stats=true;
set hive.exec.parallel=true;

drop table test;
create table test
as
SELECT a.attuid,
machine_id,
ACTIVE_WINDOW_START_DATE,
active_window_end_timetamp
FROM
KPMG.IDESK_LOG_DETAIL_CLEAN  A LEFT OUTER JOIN
(SELECT * FROM kpmg_ws.user_details WHERE VP_NAME = "MARK O PAGE" limit 10 )  B
ON A.ATTUID = B.ATTUID
WHERE APP_CLASS_GROUP != "INPUT_ERROR" AND
active_window_start_date >= "2016-07-01" AND active_window_start_date < "2017-06-02" AND
DURATION_IN_SEC >= 0 AND APP_CLASS_GROUP IS NOT NULL OR LENGTH(APP_CLASS_GROUP) < 1
AND DURATION_IN_SEC >= 0 AND APP_CLASS_GROUP IS NOT NULL OR LENGTH(APP_CLASS_GROUP) < 1 and B.ATTUID IS NULL
;

--set mapred.reduce.tasks=10;
set mapreduce.job.reduces=20;
select * from test A
distribute BY A.ATTUID,MACHINE_ID,ACTIVE_WINDOW_START_DATE  sort by A.ATTUID,MACHINE_ID,ACTIVE_WINDOW_START_DATE , active_window_end_timetamp
limit 100
;
