use KPMG;

drop table idesk_log_detail;
CREATE external TABLE idesk_log_detail
(
  svc_id                       int,
  user_id                      string,
  window_title                 string,
  app_name                     string,
  url                          string,
  site                         string,
  report_run_timestamp         timestamp,
  active_window_end_timetamp   timestamp,
  time_zone                    string,
  machine_id                   string,
  copy_app                     string,
  copy_title                   string,
  copy_timestamp               timestamp,
  paste_app                    string,
  paste_title                  string,
  paste_timestamp              timestamp,
  active_window_start_timetamp timestamp,
  duration_in_sec              double,
  processor                    string,
  ram                          string,
  os_version                   string,
  domain                       string,
  sys_type                     string
)
comment 'partitioned table to store iDesk raw log'
partitioned by (active_window_start_date   string)
location '/user/gc096s/idesk_log-detail'
;

insert into table idesk_log_detail
PARTITION(active_window_start_date)
select svc_id
      ,user_id
      ,window_title
      ,app_desc as app_name
      ,url_desc as url
      ,proc_desc as site
      ,report_run_timestamp
      ,active_window_end_timetamp
      ,time_zone
      ,machine_id
      ,copy_app
      ,copy_title
      ,copy_ts as copy_timestamp
      ,paste_app
      ,paste_title
      ,paste_ts as paste_timestamp
      ,active_window_start_timetamp
      ,duration_in_sec
      ,domain
      ,processor
      ,ram
      ,os_version
      ,sys_type
      ,substr(active_window_start_timetamp,1,10)   as active_window_start_date
from raw_idesk_log
where active_window_start_timetamp is not null
;

