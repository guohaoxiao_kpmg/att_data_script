// ORM class for table 'SDA_Stage.dbo.UsageAnalysis'
// WARNING: This class is AUTO-GENERATED. Modify at your own risk.
//
// Debug information:
// Generated date: Wed May 25 10:13:14 EDT 2016
// For connector: org.apache.sqoop.manager.GenericJdbcManager
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapred.lib.db.DBWritable;
import com.cloudera.sqoop.lib.JdbcWritableBridge;
import com.cloudera.sqoop.lib.DelimiterSet;
import com.cloudera.sqoop.lib.FieldFormatter;
import com.cloudera.sqoop.lib.RecordParser;
import com.cloudera.sqoop.lib.BooleanParser;
import com.cloudera.sqoop.lib.BlobRef;
import com.cloudera.sqoop.lib.ClobRef;
import com.cloudera.sqoop.lib.LargeObjectLoader;
import com.cloudera.sqoop.lib.SqoopRecord;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class SDA_Stage_dbo_UsageAnalysis extends SqoopRecord  implements DBWritable, Writable {
  private final int PROTOCOL_VERSION = 3;
  public int getClassFormatVersion() { return PROTOCOL_VERSION; }
  protected ResultSet __cur_result_set;
  private String attuid;
  public String get_attuid() {
    return attuid;
  }
  public void set_attuid(String attuid) {
    this.attuid = attuid;
  }
  public SDA_Stage_dbo_UsageAnalysis with_attuid(String attuid) {
    this.attuid = attuid;
    return this;
  }
  private String date;
  public String get_date() {
    return date;
  }
  public void set_date(String date) {
    this.date = date;
  }
  public SDA_Stage_dbo_UsageAnalysis with_date(String date) {
    this.date = date;
    return this;
  }
  private String vp_name;
  public String get_vp_name() {
    return vp_name;
  }
  public void set_vp_name(String vp_name) {
    this.vp_name = vp_name;
  }
  public SDA_Stage_dbo_UsageAnalysis with_vp_name(String vp_name) {
    this.vp_name = vp_name;
    return this;
  }
  private String week;
  public String get_week() {
    return week;
  }
  public void set_week(String week) {
    this.week = week;
  }
  public SDA_Stage_dbo_UsageAnalysis with_week(String week) {
    this.week = week;
    return this;
  }
  private String week_name;
  public String get_week_name() {
    return week_name;
  }
  public void set_week_name(String week_name) {
    this.week_name = week_name;
  }
  public SDA_Stage_dbo_UsageAnalysis with_week_name(String week_name) {
    this.week_name = week_name;
    return this;
  }
  private Integer number_of_computers;
  public Integer get_number_of_computers() {
    return number_of_computers;
  }
  public void set_number_of_computers(Integer number_of_computers) {
    this.number_of_computers = number_of_computers;
  }
  public SDA_Stage_dbo_UsageAnalysis with_number_of_computers(Integer number_of_computers) {
    this.number_of_computers = number_of_computers;
    return this;
  }
  private java.math.BigDecimal productive_time_hours;
  public java.math.BigDecimal get_productive_time_hours() {
    return productive_time_hours;
  }
  public void set_productive_time_hours(java.math.BigDecimal productive_time_hours) {
    this.productive_time_hours = productive_time_hours;
  }
  public SDA_Stage_dbo_UsageAnalysis with_productive_time_hours(java.math.BigDecimal productive_time_hours) {
    this.productive_time_hours = productive_time_hours;
    return this;
  }
  private java.math.BigDecimal personal_time_hours;
  public java.math.BigDecimal get_personal_time_hours() {
    return personal_time_hours;
  }
  public void set_personal_time_hours(java.math.BigDecimal personal_time_hours) {
    this.personal_time_hours = personal_time_hours;
  }
  public SDA_Stage_dbo_UsageAnalysis with_personal_time_hours(java.math.BigDecimal personal_time_hours) {
    this.personal_time_hours = personal_time_hours;
    return this;
  }
  private java.math.BigDecimal idle_lock_time_hours;
  public java.math.BigDecimal get_idle_lock_time_hours() {
    return idle_lock_time_hours;
  }
  public void set_idle_lock_time_hours(java.math.BigDecimal idle_lock_time_hours) {
    this.idle_lock_time_hours = idle_lock_time_hours;
  }
  public SDA_Stage_dbo_UsageAnalysis with_idle_lock_time_hours(java.math.BigDecimal idle_lock_time_hours) {
    this.idle_lock_time_hours = idle_lock_time_hours;
    return this;
  }
  private java.math.BigDecimal before_first_task_idle_lock_time_hours;
  public java.math.BigDecimal get_before_first_task_idle_lock_time_hours() {
    return before_first_task_idle_lock_time_hours;
  }
  public void set_before_first_task_idle_lock_time_hours(java.math.BigDecimal before_first_task_idle_lock_time_hours) {
    this.before_first_task_idle_lock_time_hours = before_first_task_idle_lock_time_hours;
  }
  public SDA_Stage_dbo_UsageAnalysis with_before_first_task_idle_lock_time_hours(java.math.BigDecimal before_first_task_idle_lock_time_hours) {
    this.before_first_task_idle_lock_time_hours = before_first_task_idle_lock_time_hours;
    return this;
  }
  private java.math.BigDecimal normal_idle_lock_time_hours;
  public java.math.BigDecimal get_normal_idle_lock_time_hours() {
    return normal_idle_lock_time_hours;
  }
  public void set_normal_idle_lock_time_hours(java.math.BigDecimal normal_idle_lock_time_hours) {
    this.normal_idle_lock_time_hours = normal_idle_lock_time_hours;
  }
  public SDA_Stage_dbo_UsageAnalysis with_normal_idle_lock_time_hours(java.math.BigDecimal normal_idle_lock_time_hours) {
    this.normal_idle_lock_time_hours = normal_idle_lock_time_hours;
    return this;
  }
  private java.math.BigDecimal after_last_task_idle_lock_time_hours;
  public java.math.BigDecimal get_after_last_task_idle_lock_time_hours() {
    return after_last_task_idle_lock_time_hours;
  }
  public void set_after_last_task_idle_lock_time_hours(java.math.BigDecimal after_last_task_idle_lock_time_hours) {
    this.after_last_task_idle_lock_time_hours = after_last_task_idle_lock_time_hours;
  }
  public SDA_Stage_dbo_UsageAnalysis with_after_last_task_idle_lock_time_hours(java.math.BigDecimal after_last_task_idle_lock_time_hours) {
    this.after_last_task_idle_lock_time_hours = after_last_task_idle_lock_time_hours;
    return this;
  }
  private java.math.BigDecimal unclassified_time_hours;
  public java.math.BigDecimal get_unclassified_time_hours() {
    return unclassified_time_hours;
  }
  public void set_unclassified_time_hours(java.math.BigDecimal unclassified_time_hours) {
    this.unclassified_time_hours = unclassified_time_hours;
  }
  public SDA_Stage_dbo_UsageAnalysis with_unclassified_time_hours(java.math.BigDecimal unclassified_time_hours) {
    this.unclassified_time_hours = unclassified_time_hours;
    return this;
  }
  private java.math.BigDecimal microsoft_office_time_hours;
  public java.math.BigDecimal get_microsoft_office_time_hours() {
    return microsoft_office_time_hours;
  }
  public void set_microsoft_office_time_hours(java.math.BigDecimal microsoft_office_time_hours) {
    this.microsoft_office_time_hours = microsoft_office_time_hours;
  }
  public SDA_Stage_dbo_UsageAnalysis with_microsoft_office_time_hours(java.math.BigDecimal microsoft_office_time_hours) {
    this.microsoft_office_time_hours = microsoft_office_time_hours;
    return this;
  }
  private Long microsoft_office_counts;
  public Long get_microsoft_office_counts() {
    return microsoft_office_counts;
  }
  public void set_microsoft_office_counts(Long microsoft_office_counts) {
    this.microsoft_office_counts = microsoft_office_counts;
  }
  public SDA_Stage_dbo_UsageAnalysis with_microsoft_office_counts(Long microsoft_office_counts) {
    this.microsoft_office_counts = microsoft_office_counts;
    return this;
  }
  private java.math.BigDecimal outlook_time_hours;
  public java.math.BigDecimal get_outlook_time_hours() {
    return outlook_time_hours;
  }
  public void set_outlook_time_hours(java.math.BigDecimal outlook_time_hours) {
    this.outlook_time_hours = outlook_time_hours;
  }
  public SDA_Stage_dbo_UsageAnalysis with_outlook_time_hours(java.math.BigDecimal outlook_time_hours) {
    this.outlook_time_hours = outlook_time_hours;
    return this;
  }
  private Long outlook_counts;
  public Long get_outlook_counts() {
    return outlook_counts;
  }
  public void set_outlook_counts(Long outlook_counts) {
    this.outlook_counts = outlook_counts;
  }
  public SDA_Stage_dbo_UsageAnalysis with_outlook_counts(Long outlook_counts) {
    this.outlook_counts = outlook_counts;
    return this;
  }
  private java.math.BigDecimal first_asset_time_hours;
  public java.math.BigDecimal get_first_asset_time_hours() {
    return first_asset_time_hours;
  }
  public void set_first_asset_time_hours(java.math.BigDecimal first_asset_time_hours) {
    this.first_asset_time_hours = first_asset_time_hours;
  }
  public SDA_Stage_dbo_UsageAnalysis with_first_asset_time_hours(java.math.BigDecimal first_asset_time_hours) {
    this.first_asset_time_hours = first_asset_time_hours;
    return this;
  }
  private Long first_asset_counts;
  public Long get_first_asset_counts() {
    return first_asset_counts;
  }
  public void set_first_asset_counts(Long first_asset_counts) {
    this.first_asset_counts = first_asset_counts;
  }
  public SDA_Stage_dbo_UsageAnalysis with_first_asset_counts(Long first_asset_counts) {
    this.first_asset_counts = first_asset_counts;
    return this;
  }
  private java.math.BigDecimal adopt_time_hours;
  public java.math.BigDecimal get_adopt_time_hours() {
    return adopt_time_hours;
  }
  public void set_adopt_time_hours(java.math.BigDecimal adopt_time_hours) {
    this.adopt_time_hours = adopt_time_hours;
  }
  public SDA_Stage_dbo_UsageAnalysis with_adopt_time_hours(java.math.BigDecimal adopt_time_hours) {
    this.adopt_time_hours = adopt_time_hours;
    return this;
  }
  private Long adopt_counts;
  public Long get_adopt_counts() {
    return adopt_counts;
  }
  public void set_adopt_counts(Long adopt_counts) {
    this.adopt_counts = adopt_counts;
  }
  public SDA_Stage_dbo_UsageAnalysis with_adopt_counts(Long adopt_counts) {
    this.adopt_counts = adopt_counts;
    return this;
  }
  private java.math.BigDecimal qmessenger_time_hours;
  public java.math.BigDecimal get_qmessenger_time_hours() {
    return qmessenger_time_hours;
  }
  public void set_qmessenger_time_hours(java.math.BigDecimal qmessenger_time_hours) {
    this.qmessenger_time_hours = qmessenger_time_hours;
  }
  public SDA_Stage_dbo_UsageAnalysis with_qmessenger_time_hours(java.math.BigDecimal qmessenger_time_hours) {
    this.qmessenger_time_hours = qmessenger_time_hours;
    return this;
  }
  private Long qmessenger_counts;
  public Long get_qmessenger_counts() {
    return qmessenger_counts;
  }
  public void set_qmessenger_counts(Long qmessenger_counts) {
    this.qmessenger_counts = qmessenger_counts;
  }
  public SDA_Stage_dbo_UsageAnalysis with_qmessenger_counts(Long qmessenger_counts) {
    this.qmessenger_counts = qmessenger_counts;
    return this;
  }
  private java.math.BigDecimal rome_time_hours;
  public java.math.BigDecimal get_rome_time_hours() {
    return rome_time_hours;
  }
  public void set_rome_time_hours(java.math.BigDecimal rome_time_hours) {
    this.rome_time_hours = rome_time_hours;
  }
  public SDA_Stage_dbo_UsageAnalysis with_rome_time_hours(java.math.BigDecimal rome_time_hours) {
    this.rome_time_hours = rome_time_hours;
    return this;
  }
  private Long rome_counts;
  public Long get_rome_counts() {
    return rome_counts;
  }
  public void set_rome_counts(Long rome_counts) {
    this.rome_counts = rome_counts;
  }
  public SDA_Stage_dbo_UsageAnalysis with_rome_counts(Long rome_counts) {
    this.rome_counts = rome_counts;
    return this;
  }
  private java.math.BigDecimal omx_time_hours;
  public java.math.BigDecimal get_omx_time_hours() {
    return omx_time_hours;
  }
  public void set_omx_time_hours(java.math.BigDecimal omx_time_hours) {
    this.omx_time_hours = omx_time_hours;
  }
  public SDA_Stage_dbo_UsageAnalysis with_omx_time_hours(java.math.BigDecimal omx_time_hours) {
    this.omx_time_hours = omx_time_hours;
    return this;
  }
  private Long omx_counts;
  public Long get_omx_counts() {
    return omx_counts;
  }
  public void set_omx_counts(Long omx_counts) {
    this.omx_counts = omx_counts;
  }
  public SDA_Stage_dbo_UsageAnalysis with_omx_counts(Long omx_counts) {
    this.omx_counts = omx_counts;
    return this;
  }
  private java.math.BigDecimal cth_time_hours;
  public java.math.BigDecimal get_cth_time_hours() {
    return cth_time_hours;
  }
  public void set_cth_time_hours(java.math.BigDecimal cth_time_hours) {
    this.cth_time_hours = cth_time_hours;
  }
  public SDA_Stage_dbo_UsageAnalysis with_cth_time_hours(java.math.BigDecimal cth_time_hours) {
    this.cth_time_hours = cth_time_hours;
    return this;
  }
  private Long cth_counts;
  public Long get_cth_counts() {
    return cth_counts;
  }
  public void set_cth_counts(Long cth_counts) {
    this.cth_counts = cth_counts;
  }
  public SDA_Stage_dbo_UsageAnalysis with_cth_counts(Long cth_counts) {
    this.cth_counts = cth_counts;
    return this;
  }
  private java.math.BigDecimal smartchat_time_hours;
  public java.math.BigDecimal get_smartchat_time_hours() {
    return smartchat_time_hours;
  }
  public void set_smartchat_time_hours(java.math.BigDecimal smartchat_time_hours) {
    this.smartchat_time_hours = smartchat_time_hours;
  }
  public SDA_Stage_dbo_UsageAnalysis with_smartchat_time_hours(java.math.BigDecimal smartchat_time_hours) {
    this.smartchat_time_hours = smartchat_time_hours;
    return this;
  }
  private Long smartchat_counts;
  public Long get_smartchat_counts() {
    return smartchat_counts;
  }
  public void set_smartchat_counts(Long smartchat_counts) {
    this.smartchat_counts = smartchat_counts;
  }
  public SDA_Stage_dbo_UsageAnalysis with_smartchat_counts(Long smartchat_counts) {
    this.smartchat_counts = smartchat_counts;
    return this;
  }
  private java.math.BigDecimal att_connect_time_hours;
  public java.math.BigDecimal get_att_connect_time_hours() {
    return att_connect_time_hours;
  }
  public void set_att_connect_time_hours(java.math.BigDecimal att_connect_time_hours) {
    this.att_connect_time_hours = att_connect_time_hours;
  }
  public SDA_Stage_dbo_UsageAnalysis with_att_connect_time_hours(java.math.BigDecimal att_connect_time_hours) {
    this.att_connect_time_hours = att_connect_time_hours;
    return this;
  }
  private Long att_connect_counts;
  public Long get_att_connect_counts() {
    return att_connect_counts;
  }
  public void set_att_connect_counts(Long att_connect_counts) {
    this.att_connect_counts = att_connect_counts;
  }
  public SDA_Stage_dbo_UsageAnalysis with_att_connect_counts(Long att_connect_counts) {
    this.att_connect_counts = att_connect_counts;
    return this;
  }
  private java.math.BigDecimal worklist_time_hours;
  public java.math.BigDecimal get_worklist_time_hours() {
    return worklist_time_hours;
  }
  public void set_worklist_time_hours(java.math.BigDecimal worklist_time_hours) {
    this.worklist_time_hours = worklist_time_hours;
  }
  public SDA_Stage_dbo_UsageAnalysis with_worklist_time_hours(java.math.BigDecimal worklist_time_hours) {
    this.worklist_time_hours = worklist_time_hours;
    return this;
  }
  private Long worklist_counts;
  public Long get_worklist_counts() {
    return worklist_counts;
  }
  public void set_worklist_counts(Long worklist_counts) {
    this.worklist_counts = worklist_counts;
  }
  public SDA_Stage_dbo_UsageAnalysis with_worklist_counts(Long worklist_counts) {
    this.worklist_counts = worklist_counts;
    return this;
  }
  private java.math.BigDecimal iol_time_hours;
  public java.math.BigDecimal get_iol_time_hours() {
    return iol_time_hours;
  }
  public void set_iol_time_hours(java.math.BigDecimal iol_time_hours) {
    this.iol_time_hours = iol_time_hours;
  }
  public SDA_Stage_dbo_UsageAnalysis with_iol_time_hours(java.math.BigDecimal iol_time_hours) {
    this.iol_time_hours = iol_time_hours;
    return this;
  }
  private Long iol_counts;
  public Long get_iol_counts() {
    return iol_counts;
  }
  public void set_iol_counts(Long iol_counts) {
    this.iol_counts = iol_counts;
  }
  public SDA_Stage_dbo_UsageAnalysis with_iol_counts(Long iol_counts) {
    this.iol_counts = iol_counts;
    return this;
  }
  private java.math.BigDecimal ace_db_time_hours;
  public java.math.BigDecimal get_ace_db_time_hours() {
    return ace_db_time_hours;
  }
  public void set_ace_db_time_hours(java.math.BigDecimal ace_db_time_hours) {
    this.ace_db_time_hours = ace_db_time_hours;
  }
  public SDA_Stage_dbo_UsageAnalysis with_ace_db_time_hours(java.math.BigDecimal ace_db_time_hours) {
    this.ace_db_time_hours = ace_db_time_hours;
    return this;
  }
  private Long ace_db_counts;
  public Long get_ace_db_counts() {
    return ace_db_counts;
  }
  public void set_ace_db_counts(Long ace_db_counts) {
    this.ace_db_counts = ace_db_counts;
  }
  public SDA_Stage_dbo_UsageAnalysis with_ace_db_counts(Long ace_db_counts) {
    this.ace_db_counts = ace_db_counts;
    return this;
  }
  private java.math.BigDecimal tirks_time_hours;
  public java.math.BigDecimal get_tirks_time_hours() {
    return tirks_time_hours;
  }
  public void set_tirks_time_hours(java.math.BigDecimal tirks_time_hours) {
    this.tirks_time_hours = tirks_time_hours;
  }
  public SDA_Stage_dbo_UsageAnalysis with_tirks_time_hours(java.math.BigDecimal tirks_time_hours) {
    this.tirks_time_hours = tirks_time_hours;
    return this;
  }
  private Long tirks_counts;
  public Long get_tirks_counts() {
    return tirks_counts;
  }
  public void set_tirks_counts(Long tirks_counts) {
    this.tirks_counts = tirks_counts;
  }
  public SDA_Stage_dbo_UsageAnalysis with_tirks_counts(Long tirks_counts) {
    this.tirks_counts = tirks_counts;
    return this;
  }
  private java.math.BigDecimal core_time_hours;
  public java.math.BigDecimal get_core_time_hours() {
    return core_time_hours;
  }
  public void set_core_time_hours(java.math.BigDecimal core_time_hours) {
    this.core_time_hours = core_time_hours;
  }
  public SDA_Stage_dbo_UsageAnalysis with_core_time_hours(java.math.BigDecimal core_time_hours) {
    this.core_time_hours = core_time_hours;
    return this;
  }
  private java.math.BigDecimal support_time_hours;
  public java.math.BigDecimal get_support_time_hours() {
    return support_time_hours;
  }
  public void set_support_time_hours(java.math.BigDecimal support_time_hours) {
    this.support_time_hours = support_time_hours;
  }
  public SDA_Stage_dbo_UsageAnalysis with_support_time_hours(java.math.BigDecimal support_time_hours) {
    this.support_time_hours = support_time_hours;
    return this;
  }
  private java.math.BigDecimal exception_time_hours;
  public java.math.BigDecimal get_exception_time_hours() {
    return exception_time_hours;
  }
  public void set_exception_time_hours(java.math.BigDecimal exception_time_hours) {
    this.exception_time_hours = exception_time_hours;
  }
  public SDA_Stage_dbo_UsageAnalysis with_exception_time_hours(java.math.BigDecimal exception_time_hours) {
    this.exception_time_hours = exception_time_hours;
    return this;
  }
  private Integer idle_prevention_app_flag;
  public Integer get_idle_prevention_app_flag() {
    return idle_prevention_app_flag;
  }
  public void set_idle_prevention_app_flag(Integer idle_prevention_app_flag) {
    this.idle_prevention_app_flag = idle_prevention_app_flag;
  }
  public SDA_Stage_dbo_UsageAnalysis with_idle_prevention_app_flag(Integer idle_prevention_app_flag) {
    this.idle_prevention_app_flag = idle_prevention_app_flag;
    return this;
  }
  private java.math.BigDecimal total_hours;
  public java.math.BigDecimal get_total_hours() {
    return total_hours;
  }
  public void set_total_hours(java.math.BigDecimal total_hours) {
    this.total_hours = total_hours;
  }
  public SDA_Stage_dbo_UsageAnalysis with_total_hours(java.math.BigDecimal total_hours) {
    this.total_hours = total_hours;
    return this;
  }
  private java.math.BigDecimal overtime;
  public java.math.BigDecimal get_overtime() {
    return overtime;
  }
  public void set_overtime(java.math.BigDecimal overtime) {
    this.overtime = overtime;
  }
  public SDA_Stage_dbo_UsageAnalysis with_overtime(java.math.BigDecimal overtime) {
    this.overtime = overtime;
    return this;
  }
  private java.math.BigDecimal absence_time;
  public java.math.BigDecimal get_absence_time() {
    return absence_time;
  }
  public void set_absence_time(java.math.BigDecimal absence_time) {
    this.absence_time = absence_time;
  }
  public SDA_Stage_dbo_UsageAnalysis with_absence_time(java.math.BigDecimal absence_time) {
    this.absence_time = absence_time;
    return this;
  }
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof SDA_Stage_dbo_UsageAnalysis)) {
      return false;
    }
    SDA_Stage_dbo_UsageAnalysis that = (SDA_Stage_dbo_UsageAnalysis) o;
    boolean equal = true;
    equal = equal && (this.attuid == null ? that.attuid == null : this.attuid.equals(that.attuid));
    equal = equal && (this.date == null ? that.date == null : this.date.equals(that.date));
    equal = equal && (this.vp_name == null ? that.vp_name == null : this.vp_name.equals(that.vp_name));
    equal = equal && (this.week == null ? that.week == null : this.week.equals(that.week));
    equal = equal && (this.week_name == null ? that.week_name == null : this.week_name.equals(that.week_name));
    equal = equal && (this.number_of_computers == null ? that.number_of_computers == null : this.number_of_computers.equals(that.number_of_computers));
    equal = equal && (this.productive_time_hours == null ? that.productive_time_hours == null : this.productive_time_hours.equals(that.productive_time_hours));
    equal = equal && (this.personal_time_hours == null ? that.personal_time_hours == null : this.personal_time_hours.equals(that.personal_time_hours));
    equal = equal && (this.idle_lock_time_hours == null ? that.idle_lock_time_hours == null : this.idle_lock_time_hours.equals(that.idle_lock_time_hours));
    equal = equal && (this.before_first_task_idle_lock_time_hours == null ? that.before_first_task_idle_lock_time_hours == null : this.before_first_task_idle_lock_time_hours.equals(that.before_first_task_idle_lock_time_hours));
    equal = equal && (this.normal_idle_lock_time_hours == null ? that.normal_idle_lock_time_hours == null : this.normal_idle_lock_time_hours.equals(that.normal_idle_lock_time_hours));
    equal = equal && (this.after_last_task_idle_lock_time_hours == null ? that.after_last_task_idle_lock_time_hours == null : this.after_last_task_idle_lock_time_hours.equals(that.after_last_task_idle_lock_time_hours));
    equal = equal && (this.unclassified_time_hours == null ? that.unclassified_time_hours == null : this.unclassified_time_hours.equals(that.unclassified_time_hours));
    equal = equal && (this.microsoft_office_time_hours == null ? that.microsoft_office_time_hours == null : this.microsoft_office_time_hours.equals(that.microsoft_office_time_hours));
    equal = equal && (this.microsoft_office_counts == null ? that.microsoft_office_counts == null : this.microsoft_office_counts.equals(that.microsoft_office_counts));
    equal = equal && (this.outlook_time_hours == null ? that.outlook_time_hours == null : this.outlook_time_hours.equals(that.outlook_time_hours));
    equal = equal && (this.outlook_counts == null ? that.outlook_counts == null : this.outlook_counts.equals(that.outlook_counts));
    equal = equal && (this.first_asset_time_hours == null ? that.first_asset_time_hours == null : this.first_asset_time_hours.equals(that.first_asset_time_hours));
    equal = equal && (this.first_asset_counts == null ? that.first_asset_counts == null : this.first_asset_counts.equals(that.first_asset_counts));
    equal = equal && (this.adopt_time_hours == null ? that.adopt_time_hours == null : this.adopt_time_hours.equals(that.adopt_time_hours));
    equal = equal && (this.adopt_counts == null ? that.adopt_counts == null : this.adopt_counts.equals(that.adopt_counts));
    equal = equal && (this.qmessenger_time_hours == null ? that.qmessenger_time_hours == null : this.qmessenger_time_hours.equals(that.qmessenger_time_hours));
    equal = equal && (this.qmessenger_counts == null ? that.qmessenger_counts == null : this.qmessenger_counts.equals(that.qmessenger_counts));
    equal = equal && (this.rome_time_hours == null ? that.rome_time_hours == null : this.rome_time_hours.equals(that.rome_time_hours));
    equal = equal && (this.rome_counts == null ? that.rome_counts == null : this.rome_counts.equals(that.rome_counts));
    equal = equal && (this.omx_time_hours == null ? that.omx_time_hours == null : this.omx_time_hours.equals(that.omx_time_hours));
    equal = equal && (this.omx_counts == null ? that.omx_counts == null : this.omx_counts.equals(that.omx_counts));
    equal = equal && (this.cth_time_hours == null ? that.cth_time_hours == null : this.cth_time_hours.equals(that.cth_time_hours));
    equal = equal && (this.cth_counts == null ? that.cth_counts == null : this.cth_counts.equals(that.cth_counts));
    equal = equal && (this.smartchat_time_hours == null ? that.smartchat_time_hours == null : this.smartchat_time_hours.equals(that.smartchat_time_hours));
    equal = equal && (this.smartchat_counts == null ? that.smartchat_counts == null : this.smartchat_counts.equals(that.smartchat_counts));
    equal = equal && (this.att_connect_time_hours == null ? that.att_connect_time_hours == null : this.att_connect_time_hours.equals(that.att_connect_time_hours));
    equal = equal && (this.att_connect_counts == null ? that.att_connect_counts == null : this.att_connect_counts.equals(that.att_connect_counts));
    equal = equal && (this.worklist_time_hours == null ? that.worklist_time_hours == null : this.worklist_time_hours.equals(that.worklist_time_hours));
    equal = equal && (this.worklist_counts == null ? that.worklist_counts == null : this.worklist_counts.equals(that.worklist_counts));
    equal = equal && (this.iol_time_hours == null ? that.iol_time_hours == null : this.iol_time_hours.equals(that.iol_time_hours));
    equal = equal && (this.iol_counts == null ? that.iol_counts == null : this.iol_counts.equals(that.iol_counts));
    equal = equal && (this.ace_db_time_hours == null ? that.ace_db_time_hours == null : this.ace_db_time_hours.equals(that.ace_db_time_hours));
    equal = equal && (this.ace_db_counts == null ? that.ace_db_counts == null : this.ace_db_counts.equals(that.ace_db_counts));
    equal = equal && (this.tirks_time_hours == null ? that.tirks_time_hours == null : this.tirks_time_hours.equals(that.tirks_time_hours));
    equal = equal && (this.tirks_counts == null ? that.tirks_counts == null : this.tirks_counts.equals(that.tirks_counts));
    equal = equal && (this.core_time_hours == null ? that.core_time_hours == null : this.core_time_hours.equals(that.core_time_hours));
    equal = equal && (this.support_time_hours == null ? that.support_time_hours == null : this.support_time_hours.equals(that.support_time_hours));
    equal = equal && (this.exception_time_hours == null ? that.exception_time_hours == null : this.exception_time_hours.equals(that.exception_time_hours));
    equal = equal && (this.idle_prevention_app_flag == null ? that.idle_prevention_app_flag == null : this.idle_prevention_app_flag.equals(that.idle_prevention_app_flag));
    equal = equal && (this.total_hours == null ? that.total_hours == null : this.total_hours.equals(that.total_hours));
    equal = equal && (this.overtime == null ? that.overtime == null : this.overtime.equals(that.overtime));
    equal = equal && (this.absence_time == null ? that.absence_time == null : this.absence_time.equals(that.absence_time));
    return equal;
  }
  public boolean equals0(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof SDA_Stage_dbo_UsageAnalysis)) {
      return false;
    }
    SDA_Stage_dbo_UsageAnalysis that = (SDA_Stage_dbo_UsageAnalysis) o;
    boolean equal = true;
    equal = equal && (this.attuid == null ? that.attuid == null : this.attuid.equals(that.attuid));
    equal = equal && (this.date == null ? that.date == null : this.date.equals(that.date));
    equal = equal && (this.vp_name == null ? that.vp_name == null : this.vp_name.equals(that.vp_name));
    equal = equal && (this.week == null ? that.week == null : this.week.equals(that.week));
    equal = equal && (this.week_name == null ? that.week_name == null : this.week_name.equals(that.week_name));
    equal = equal && (this.number_of_computers == null ? that.number_of_computers == null : this.number_of_computers.equals(that.number_of_computers));
    equal = equal && (this.productive_time_hours == null ? that.productive_time_hours == null : this.productive_time_hours.equals(that.productive_time_hours));
    equal = equal && (this.personal_time_hours == null ? that.personal_time_hours == null : this.personal_time_hours.equals(that.personal_time_hours));
    equal = equal && (this.idle_lock_time_hours == null ? that.idle_lock_time_hours == null : this.idle_lock_time_hours.equals(that.idle_lock_time_hours));
    equal = equal && (this.before_first_task_idle_lock_time_hours == null ? that.before_first_task_idle_lock_time_hours == null : this.before_first_task_idle_lock_time_hours.equals(that.before_first_task_idle_lock_time_hours));
    equal = equal && (this.normal_idle_lock_time_hours == null ? that.normal_idle_lock_time_hours == null : this.normal_idle_lock_time_hours.equals(that.normal_idle_lock_time_hours));
    equal = equal && (this.after_last_task_idle_lock_time_hours == null ? that.after_last_task_idle_lock_time_hours == null : this.after_last_task_idle_lock_time_hours.equals(that.after_last_task_idle_lock_time_hours));
    equal = equal && (this.unclassified_time_hours == null ? that.unclassified_time_hours == null : this.unclassified_time_hours.equals(that.unclassified_time_hours));
    equal = equal && (this.microsoft_office_time_hours == null ? that.microsoft_office_time_hours == null : this.microsoft_office_time_hours.equals(that.microsoft_office_time_hours));
    equal = equal && (this.microsoft_office_counts == null ? that.microsoft_office_counts == null : this.microsoft_office_counts.equals(that.microsoft_office_counts));
    equal = equal && (this.outlook_time_hours == null ? that.outlook_time_hours == null : this.outlook_time_hours.equals(that.outlook_time_hours));
    equal = equal && (this.outlook_counts == null ? that.outlook_counts == null : this.outlook_counts.equals(that.outlook_counts));
    equal = equal && (this.first_asset_time_hours == null ? that.first_asset_time_hours == null : this.first_asset_time_hours.equals(that.first_asset_time_hours));
    equal = equal && (this.first_asset_counts == null ? that.first_asset_counts == null : this.first_asset_counts.equals(that.first_asset_counts));
    equal = equal && (this.adopt_time_hours == null ? that.adopt_time_hours == null : this.adopt_time_hours.equals(that.adopt_time_hours));
    equal = equal && (this.adopt_counts == null ? that.adopt_counts == null : this.adopt_counts.equals(that.adopt_counts));
    equal = equal && (this.qmessenger_time_hours == null ? that.qmessenger_time_hours == null : this.qmessenger_time_hours.equals(that.qmessenger_time_hours));
    equal = equal && (this.qmessenger_counts == null ? that.qmessenger_counts == null : this.qmessenger_counts.equals(that.qmessenger_counts));
    equal = equal && (this.rome_time_hours == null ? that.rome_time_hours == null : this.rome_time_hours.equals(that.rome_time_hours));
    equal = equal && (this.rome_counts == null ? that.rome_counts == null : this.rome_counts.equals(that.rome_counts));
    equal = equal && (this.omx_time_hours == null ? that.omx_time_hours == null : this.omx_time_hours.equals(that.omx_time_hours));
    equal = equal && (this.omx_counts == null ? that.omx_counts == null : this.omx_counts.equals(that.omx_counts));
    equal = equal && (this.cth_time_hours == null ? that.cth_time_hours == null : this.cth_time_hours.equals(that.cth_time_hours));
    equal = equal && (this.cth_counts == null ? that.cth_counts == null : this.cth_counts.equals(that.cth_counts));
    equal = equal && (this.smartchat_time_hours == null ? that.smartchat_time_hours == null : this.smartchat_time_hours.equals(that.smartchat_time_hours));
    equal = equal && (this.smartchat_counts == null ? that.smartchat_counts == null : this.smartchat_counts.equals(that.smartchat_counts));
    equal = equal && (this.att_connect_time_hours == null ? that.att_connect_time_hours == null : this.att_connect_time_hours.equals(that.att_connect_time_hours));
    equal = equal && (this.att_connect_counts == null ? that.att_connect_counts == null : this.att_connect_counts.equals(that.att_connect_counts));
    equal = equal && (this.worklist_time_hours == null ? that.worklist_time_hours == null : this.worklist_time_hours.equals(that.worklist_time_hours));
    equal = equal && (this.worklist_counts == null ? that.worklist_counts == null : this.worklist_counts.equals(that.worklist_counts));
    equal = equal && (this.iol_time_hours == null ? that.iol_time_hours == null : this.iol_time_hours.equals(that.iol_time_hours));
    equal = equal && (this.iol_counts == null ? that.iol_counts == null : this.iol_counts.equals(that.iol_counts));
    equal = equal && (this.ace_db_time_hours == null ? that.ace_db_time_hours == null : this.ace_db_time_hours.equals(that.ace_db_time_hours));
    equal = equal && (this.ace_db_counts == null ? that.ace_db_counts == null : this.ace_db_counts.equals(that.ace_db_counts));
    equal = equal && (this.tirks_time_hours == null ? that.tirks_time_hours == null : this.tirks_time_hours.equals(that.tirks_time_hours));
    equal = equal && (this.tirks_counts == null ? that.tirks_counts == null : this.tirks_counts.equals(that.tirks_counts));
    equal = equal && (this.core_time_hours == null ? that.core_time_hours == null : this.core_time_hours.equals(that.core_time_hours));
    equal = equal && (this.support_time_hours == null ? that.support_time_hours == null : this.support_time_hours.equals(that.support_time_hours));
    equal = equal && (this.exception_time_hours == null ? that.exception_time_hours == null : this.exception_time_hours.equals(that.exception_time_hours));
    equal = equal && (this.idle_prevention_app_flag == null ? that.idle_prevention_app_flag == null : this.idle_prevention_app_flag.equals(that.idle_prevention_app_flag));
    equal = equal && (this.total_hours == null ? that.total_hours == null : this.total_hours.equals(that.total_hours));
    equal = equal && (this.overtime == null ? that.overtime == null : this.overtime.equals(that.overtime));
    equal = equal && (this.absence_time == null ? that.absence_time == null : this.absence_time.equals(that.absence_time));
    return equal;
  }
  public void readFields(ResultSet __dbResults) throws SQLException {
    this.__cur_result_set = __dbResults;
    this.attuid = JdbcWritableBridge.readString(1, __dbResults);
    this.date = JdbcWritableBridge.readString(2, __dbResults);
    this.vp_name = JdbcWritableBridge.readString(3, __dbResults);
    this.week = JdbcWritableBridge.readString(4, __dbResults);
    this.week_name = JdbcWritableBridge.readString(5, __dbResults);
    this.number_of_computers = JdbcWritableBridge.readInteger(6, __dbResults);
    this.productive_time_hours = JdbcWritableBridge.readBigDecimal(7, __dbResults);
    this.personal_time_hours = JdbcWritableBridge.readBigDecimal(8, __dbResults);
    this.idle_lock_time_hours = JdbcWritableBridge.readBigDecimal(9, __dbResults);
    this.before_first_task_idle_lock_time_hours = JdbcWritableBridge.readBigDecimal(10, __dbResults);
    this.normal_idle_lock_time_hours = JdbcWritableBridge.readBigDecimal(11, __dbResults);
    this.after_last_task_idle_lock_time_hours = JdbcWritableBridge.readBigDecimal(12, __dbResults);
    this.unclassified_time_hours = JdbcWritableBridge.readBigDecimal(13, __dbResults);
    this.microsoft_office_time_hours = JdbcWritableBridge.readBigDecimal(14, __dbResults);
    this.microsoft_office_counts = JdbcWritableBridge.readLong(15, __dbResults);
    this.outlook_time_hours = JdbcWritableBridge.readBigDecimal(16, __dbResults);
    this.outlook_counts = JdbcWritableBridge.readLong(17, __dbResults);
    this.first_asset_time_hours = JdbcWritableBridge.readBigDecimal(18, __dbResults);
    this.first_asset_counts = JdbcWritableBridge.readLong(19, __dbResults);
    this.adopt_time_hours = JdbcWritableBridge.readBigDecimal(20, __dbResults);
    this.adopt_counts = JdbcWritableBridge.readLong(21, __dbResults);
    this.qmessenger_time_hours = JdbcWritableBridge.readBigDecimal(22, __dbResults);
    this.qmessenger_counts = JdbcWritableBridge.readLong(23, __dbResults);
    this.rome_time_hours = JdbcWritableBridge.readBigDecimal(24, __dbResults);
    this.rome_counts = JdbcWritableBridge.readLong(25, __dbResults);
    this.omx_time_hours = JdbcWritableBridge.readBigDecimal(26, __dbResults);
    this.omx_counts = JdbcWritableBridge.readLong(27, __dbResults);
    this.cth_time_hours = JdbcWritableBridge.readBigDecimal(28, __dbResults);
    this.cth_counts = JdbcWritableBridge.readLong(29, __dbResults);
    this.smartchat_time_hours = JdbcWritableBridge.readBigDecimal(30, __dbResults);
    this.smartchat_counts = JdbcWritableBridge.readLong(31, __dbResults);
    this.att_connect_time_hours = JdbcWritableBridge.readBigDecimal(32, __dbResults);
    this.att_connect_counts = JdbcWritableBridge.readLong(33, __dbResults);
    this.worklist_time_hours = JdbcWritableBridge.readBigDecimal(34, __dbResults);
    this.worklist_counts = JdbcWritableBridge.readLong(35, __dbResults);
    this.iol_time_hours = JdbcWritableBridge.readBigDecimal(36, __dbResults);
    this.iol_counts = JdbcWritableBridge.readLong(37, __dbResults);
    this.ace_db_time_hours = JdbcWritableBridge.readBigDecimal(38, __dbResults);
    this.ace_db_counts = JdbcWritableBridge.readLong(39, __dbResults);
    this.tirks_time_hours = JdbcWritableBridge.readBigDecimal(40, __dbResults);
    this.tirks_counts = JdbcWritableBridge.readLong(41, __dbResults);
    this.core_time_hours = JdbcWritableBridge.readBigDecimal(42, __dbResults);
    this.support_time_hours = JdbcWritableBridge.readBigDecimal(43, __dbResults);
    this.exception_time_hours = JdbcWritableBridge.readBigDecimal(44, __dbResults);
    this.idle_prevention_app_flag = JdbcWritableBridge.readInteger(45, __dbResults);
    this.total_hours = JdbcWritableBridge.readBigDecimal(46, __dbResults);
    this.overtime = JdbcWritableBridge.readBigDecimal(47, __dbResults);
    this.absence_time = JdbcWritableBridge.readBigDecimal(48, __dbResults);
  }
  public void readFields0(ResultSet __dbResults) throws SQLException {
    this.attuid = JdbcWritableBridge.readString(1, __dbResults);
    this.date = JdbcWritableBridge.readString(2, __dbResults);
    this.vp_name = JdbcWritableBridge.readString(3, __dbResults);
    this.week = JdbcWritableBridge.readString(4, __dbResults);
    this.week_name = JdbcWritableBridge.readString(5, __dbResults);
    this.number_of_computers = JdbcWritableBridge.readInteger(6, __dbResults);
    this.productive_time_hours = JdbcWritableBridge.readBigDecimal(7, __dbResults);
    this.personal_time_hours = JdbcWritableBridge.readBigDecimal(8, __dbResults);
    this.idle_lock_time_hours = JdbcWritableBridge.readBigDecimal(9, __dbResults);
    this.before_first_task_idle_lock_time_hours = JdbcWritableBridge.readBigDecimal(10, __dbResults);
    this.normal_idle_lock_time_hours = JdbcWritableBridge.readBigDecimal(11, __dbResults);
    this.after_last_task_idle_lock_time_hours = JdbcWritableBridge.readBigDecimal(12, __dbResults);
    this.unclassified_time_hours = JdbcWritableBridge.readBigDecimal(13, __dbResults);
    this.microsoft_office_time_hours = JdbcWritableBridge.readBigDecimal(14, __dbResults);
    this.microsoft_office_counts = JdbcWritableBridge.readLong(15, __dbResults);
    this.outlook_time_hours = JdbcWritableBridge.readBigDecimal(16, __dbResults);
    this.outlook_counts = JdbcWritableBridge.readLong(17, __dbResults);
    this.first_asset_time_hours = JdbcWritableBridge.readBigDecimal(18, __dbResults);
    this.first_asset_counts = JdbcWritableBridge.readLong(19, __dbResults);
    this.adopt_time_hours = JdbcWritableBridge.readBigDecimal(20, __dbResults);
    this.adopt_counts = JdbcWritableBridge.readLong(21, __dbResults);
    this.qmessenger_time_hours = JdbcWritableBridge.readBigDecimal(22, __dbResults);
    this.qmessenger_counts = JdbcWritableBridge.readLong(23, __dbResults);
    this.rome_time_hours = JdbcWritableBridge.readBigDecimal(24, __dbResults);
    this.rome_counts = JdbcWritableBridge.readLong(25, __dbResults);
    this.omx_time_hours = JdbcWritableBridge.readBigDecimal(26, __dbResults);
    this.omx_counts = JdbcWritableBridge.readLong(27, __dbResults);
    this.cth_time_hours = JdbcWritableBridge.readBigDecimal(28, __dbResults);
    this.cth_counts = JdbcWritableBridge.readLong(29, __dbResults);
    this.smartchat_time_hours = JdbcWritableBridge.readBigDecimal(30, __dbResults);
    this.smartchat_counts = JdbcWritableBridge.readLong(31, __dbResults);
    this.att_connect_time_hours = JdbcWritableBridge.readBigDecimal(32, __dbResults);
    this.att_connect_counts = JdbcWritableBridge.readLong(33, __dbResults);
    this.worklist_time_hours = JdbcWritableBridge.readBigDecimal(34, __dbResults);
    this.worklist_counts = JdbcWritableBridge.readLong(35, __dbResults);
    this.iol_time_hours = JdbcWritableBridge.readBigDecimal(36, __dbResults);
    this.iol_counts = JdbcWritableBridge.readLong(37, __dbResults);
    this.ace_db_time_hours = JdbcWritableBridge.readBigDecimal(38, __dbResults);
    this.ace_db_counts = JdbcWritableBridge.readLong(39, __dbResults);
    this.tirks_time_hours = JdbcWritableBridge.readBigDecimal(40, __dbResults);
    this.tirks_counts = JdbcWritableBridge.readLong(41, __dbResults);
    this.core_time_hours = JdbcWritableBridge.readBigDecimal(42, __dbResults);
    this.support_time_hours = JdbcWritableBridge.readBigDecimal(43, __dbResults);
    this.exception_time_hours = JdbcWritableBridge.readBigDecimal(44, __dbResults);
    this.idle_prevention_app_flag = JdbcWritableBridge.readInteger(45, __dbResults);
    this.total_hours = JdbcWritableBridge.readBigDecimal(46, __dbResults);
    this.overtime = JdbcWritableBridge.readBigDecimal(47, __dbResults);
    this.absence_time = JdbcWritableBridge.readBigDecimal(48, __dbResults);
  }
  public void loadLargeObjects(LargeObjectLoader __loader)
      throws SQLException, IOException, InterruptedException {
  }
  public void loadLargeObjects0(LargeObjectLoader __loader)
      throws SQLException, IOException, InterruptedException {
  }
  public void write(PreparedStatement __dbStmt) throws SQLException {
    write(__dbStmt, 0);
  }

  public int write(PreparedStatement __dbStmt, int __off) throws SQLException {
    JdbcWritableBridge.writeString(attuid, 1 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(date, 2 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(vp_name, 3 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(week, 4 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(week_name, 5 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeInteger(number_of_computers, 6 + __off, 4, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(productive_time_hours, 7 + __off, 2, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(personal_time_hours, 8 + __off, 2, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(idle_lock_time_hours, 9 + __off, 2, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(before_first_task_idle_lock_time_hours, 10 + __off, 2, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(normal_idle_lock_time_hours, 11 + __off, 2, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(after_last_task_idle_lock_time_hours, 12 + __off, 2, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(unclassified_time_hours, 13 + __off, 2, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(microsoft_office_time_hours, 14 + __off, 2, __dbStmt);
    JdbcWritableBridge.writeLong(microsoft_office_counts, 15 + __off, -5, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(outlook_time_hours, 16 + __off, 2, __dbStmt);
    JdbcWritableBridge.writeLong(outlook_counts, 17 + __off, -5, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(first_asset_time_hours, 18 + __off, 2, __dbStmt);
    JdbcWritableBridge.writeLong(first_asset_counts, 19 + __off, -5, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(adopt_time_hours, 20 + __off, 2, __dbStmt);
    JdbcWritableBridge.writeLong(adopt_counts, 21 + __off, -5, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(qmessenger_time_hours, 22 + __off, 2, __dbStmt);
    JdbcWritableBridge.writeLong(qmessenger_counts, 23 + __off, -5, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(rome_time_hours, 24 + __off, 2, __dbStmt);
    JdbcWritableBridge.writeLong(rome_counts, 25 + __off, -5, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(omx_time_hours, 26 + __off, 2, __dbStmt);
    JdbcWritableBridge.writeLong(omx_counts, 27 + __off, -5, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(cth_time_hours, 28 + __off, 2, __dbStmt);
    JdbcWritableBridge.writeLong(cth_counts, 29 + __off, -5, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(smartchat_time_hours, 30 + __off, 2, __dbStmt);
    JdbcWritableBridge.writeLong(smartchat_counts, 31 + __off, -5, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(att_connect_time_hours, 32 + __off, 2, __dbStmt);
    JdbcWritableBridge.writeLong(att_connect_counts, 33 + __off, -5, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(worklist_time_hours, 34 + __off, 2, __dbStmt);
    JdbcWritableBridge.writeLong(worklist_counts, 35 + __off, -5, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(iol_time_hours, 36 + __off, 2, __dbStmt);
    JdbcWritableBridge.writeLong(iol_counts, 37 + __off, -5, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(ace_db_time_hours, 38 + __off, 2, __dbStmt);
    JdbcWritableBridge.writeLong(ace_db_counts, 39 + __off, -5, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(tirks_time_hours, 40 + __off, 2, __dbStmt);
    JdbcWritableBridge.writeLong(tirks_counts, 41 + __off, -5, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(core_time_hours, 42 + __off, 2, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(support_time_hours, 43 + __off, 2, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(exception_time_hours, 44 + __off, 2, __dbStmt);
    JdbcWritableBridge.writeInteger(idle_prevention_app_flag, 45 + __off, 4, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(total_hours, 46 + __off, 2, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(overtime, 47 + __off, 2, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(absence_time, 48 + __off, 2, __dbStmt);
    return 48;
  }
  public void write0(PreparedStatement __dbStmt, int __off) throws SQLException {
    JdbcWritableBridge.writeString(attuid, 1 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(date, 2 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(vp_name, 3 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(week, 4 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(week_name, 5 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeInteger(number_of_computers, 6 + __off, 4, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(productive_time_hours, 7 + __off, 2, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(personal_time_hours, 8 + __off, 2, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(idle_lock_time_hours, 9 + __off, 2, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(before_first_task_idle_lock_time_hours, 10 + __off, 2, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(normal_idle_lock_time_hours, 11 + __off, 2, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(after_last_task_idle_lock_time_hours, 12 + __off, 2, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(unclassified_time_hours, 13 + __off, 2, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(microsoft_office_time_hours, 14 + __off, 2, __dbStmt);
    JdbcWritableBridge.writeLong(microsoft_office_counts, 15 + __off, -5, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(outlook_time_hours, 16 + __off, 2, __dbStmt);
    JdbcWritableBridge.writeLong(outlook_counts, 17 + __off, -5, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(first_asset_time_hours, 18 + __off, 2, __dbStmt);
    JdbcWritableBridge.writeLong(first_asset_counts, 19 + __off, -5, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(adopt_time_hours, 20 + __off, 2, __dbStmt);
    JdbcWritableBridge.writeLong(adopt_counts, 21 + __off, -5, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(qmessenger_time_hours, 22 + __off, 2, __dbStmt);
    JdbcWritableBridge.writeLong(qmessenger_counts, 23 + __off, -5, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(rome_time_hours, 24 + __off, 2, __dbStmt);
    JdbcWritableBridge.writeLong(rome_counts, 25 + __off, -5, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(omx_time_hours, 26 + __off, 2, __dbStmt);
    JdbcWritableBridge.writeLong(omx_counts, 27 + __off, -5, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(cth_time_hours, 28 + __off, 2, __dbStmt);
    JdbcWritableBridge.writeLong(cth_counts, 29 + __off, -5, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(smartchat_time_hours, 30 + __off, 2, __dbStmt);
    JdbcWritableBridge.writeLong(smartchat_counts, 31 + __off, -5, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(att_connect_time_hours, 32 + __off, 2, __dbStmt);
    JdbcWritableBridge.writeLong(att_connect_counts, 33 + __off, -5, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(worklist_time_hours, 34 + __off, 2, __dbStmt);
    JdbcWritableBridge.writeLong(worklist_counts, 35 + __off, -5, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(iol_time_hours, 36 + __off, 2, __dbStmt);
    JdbcWritableBridge.writeLong(iol_counts, 37 + __off, -5, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(ace_db_time_hours, 38 + __off, 2, __dbStmt);
    JdbcWritableBridge.writeLong(ace_db_counts, 39 + __off, -5, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(tirks_time_hours, 40 + __off, 2, __dbStmt);
    JdbcWritableBridge.writeLong(tirks_counts, 41 + __off, -5, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(core_time_hours, 42 + __off, 2, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(support_time_hours, 43 + __off, 2, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(exception_time_hours, 44 + __off, 2, __dbStmt);
    JdbcWritableBridge.writeInteger(idle_prevention_app_flag, 45 + __off, 4, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(total_hours, 46 + __off, 2, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(overtime, 47 + __off, 2, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(absence_time, 48 + __off, 2, __dbStmt);
  }
  public void readFields(DataInput __dataIn) throws IOException {
this.readFields0(__dataIn);  }
  public void readFields0(DataInput __dataIn) throws IOException {
    if (__dataIn.readBoolean()) { 
        this.attuid = null;
    } else {
    this.attuid = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.date = null;
    } else {
    this.date = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.vp_name = null;
    } else {
    this.vp_name = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.week = null;
    } else {
    this.week = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.week_name = null;
    } else {
    this.week_name = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.number_of_computers = null;
    } else {
    this.number_of_computers = Integer.valueOf(__dataIn.readInt());
    }
    if (__dataIn.readBoolean()) { 
        this.productive_time_hours = null;
    } else {
    this.productive_time_hours = com.cloudera.sqoop.lib.BigDecimalSerializer.readFields(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.personal_time_hours = null;
    } else {
    this.personal_time_hours = com.cloudera.sqoop.lib.BigDecimalSerializer.readFields(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.idle_lock_time_hours = null;
    } else {
    this.idle_lock_time_hours = com.cloudera.sqoop.lib.BigDecimalSerializer.readFields(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.before_first_task_idle_lock_time_hours = null;
    } else {
    this.before_first_task_idle_lock_time_hours = com.cloudera.sqoop.lib.BigDecimalSerializer.readFields(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.normal_idle_lock_time_hours = null;
    } else {
    this.normal_idle_lock_time_hours = com.cloudera.sqoop.lib.BigDecimalSerializer.readFields(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.after_last_task_idle_lock_time_hours = null;
    } else {
    this.after_last_task_idle_lock_time_hours = com.cloudera.sqoop.lib.BigDecimalSerializer.readFields(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.unclassified_time_hours = null;
    } else {
    this.unclassified_time_hours = com.cloudera.sqoop.lib.BigDecimalSerializer.readFields(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.microsoft_office_time_hours = null;
    } else {
    this.microsoft_office_time_hours = com.cloudera.sqoop.lib.BigDecimalSerializer.readFields(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.microsoft_office_counts = null;
    } else {
    this.microsoft_office_counts = Long.valueOf(__dataIn.readLong());
    }
    if (__dataIn.readBoolean()) { 
        this.outlook_time_hours = null;
    } else {
    this.outlook_time_hours = com.cloudera.sqoop.lib.BigDecimalSerializer.readFields(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.outlook_counts = null;
    } else {
    this.outlook_counts = Long.valueOf(__dataIn.readLong());
    }
    if (__dataIn.readBoolean()) { 
        this.first_asset_time_hours = null;
    } else {
    this.first_asset_time_hours = com.cloudera.sqoop.lib.BigDecimalSerializer.readFields(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.first_asset_counts = null;
    } else {
    this.first_asset_counts = Long.valueOf(__dataIn.readLong());
    }
    if (__dataIn.readBoolean()) { 
        this.adopt_time_hours = null;
    } else {
    this.adopt_time_hours = com.cloudera.sqoop.lib.BigDecimalSerializer.readFields(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.adopt_counts = null;
    } else {
    this.adopt_counts = Long.valueOf(__dataIn.readLong());
    }
    if (__dataIn.readBoolean()) { 
        this.qmessenger_time_hours = null;
    } else {
    this.qmessenger_time_hours = com.cloudera.sqoop.lib.BigDecimalSerializer.readFields(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.qmessenger_counts = null;
    } else {
    this.qmessenger_counts = Long.valueOf(__dataIn.readLong());
    }
    if (__dataIn.readBoolean()) { 
        this.rome_time_hours = null;
    } else {
    this.rome_time_hours = com.cloudera.sqoop.lib.BigDecimalSerializer.readFields(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.rome_counts = null;
    } else {
    this.rome_counts = Long.valueOf(__dataIn.readLong());
    }
    if (__dataIn.readBoolean()) { 
        this.omx_time_hours = null;
    } else {
    this.omx_time_hours = com.cloudera.sqoop.lib.BigDecimalSerializer.readFields(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.omx_counts = null;
    } else {
    this.omx_counts = Long.valueOf(__dataIn.readLong());
    }
    if (__dataIn.readBoolean()) { 
        this.cth_time_hours = null;
    } else {
    this.cth_time_hours = com.cloudera.sqoop.lib.BigDecimalSerializer.readFields(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.cth_counts = null;
    } else {
    this.cth_counts = Long.valueOf(__dataIn.readLong());
    }
    if (__dataIn.readBoolean()) { 
        this.smartchat_time_hours = null;
    } else {
    this.smartchat_time_hours = com.cloudera.sqoop.lib.BigDecimalSerializer.readFields(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.smartchat_counts = null;
    } else {
    this.smartchat_counts = Long.valueOf(__dataIn.readLong());
    }
    if (__dataIn.readBoolean()) { 
        this.att_connect_time_hours = null;
    } else {
    this.att_connect_time_hours = com.cloudera.sqoop.lib.BigDecimalSerializer.readFields(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.att_connect_counts = null;
    } else {
    this.att_connect_counts = Long.valueOf(__dataIn.readLong());
    }
    if (__dataIn.readBoolean()) { 
        this.worklist_time_hours = null;
    } else {
    this.worklist_time_hours = com.cloudera.sqoop.lib.BigDecimalSerializer.readFields(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.worklist_counts = null;
    } else {
    this.worklist_counts = Long.valueOf(__dataIn.readLong());
    }
    if (__dataIn.readBoolean()) { 
        this.iol_time_hours = null;
    } else {
    this.iol_time_hours = com.cloudera.sqoop.lib.BigDecimalSerializer.readFields(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.iol_counts = null;
    } else {
    this.iol_counts = Long.valueOf(__dataIn.readLong());
    }
    if (__dataIn.readBoolean()) { 
        this.ace_db_time_hours = null;
    } else {
    this.ace_db_time_hours = com.cloudera.sqoop.lib.BigDecimalSerializer.readFields(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.ace_db_counts = null;
    } else {
    this.ace_db_counts = Long.valueOf(__dataIn.readLong());
    }
    if (__dataIn.readBoolean()) { 
        this.tirks_time_hours = null;
    } else {
    this.tirks_time_hours = com.cloudera.sqoop.lib.BigDecimalSerializer.readFields(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.tirks_counts = null;
    } else {
    this.tirks_counts = Long.valueOf(__dataIn.readLong());
    }
    if (__dataIn.readBoolean()) { 
        this.core_time_hours = null;
    } else {
    this.core_time_hours = com.cloudera.sqoop.lib.BigDecimalSerializer.readFields(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.support_time_hours = null;
    } else {
    this.support_time_hours = com.cloudera.sqoop.lib.BigDecimalSerializer.readFields(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.exception_time_hours = null;
    } else {
    this.exception_time_hours = com.cloudera.sqoop.lib.BigDecimalSerializer.readFields(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.idle_prevention_app_flag = null;
    } else {
    this.idle_prevention_app_flag = Integer.valueOf(__dataIn.readInt());
    }
    if (__dataIn.readBoolean()) { 
        this.total_hours = null;
    } else {
    this.total_hours = com.cloudera.sqoop.lib.BigDecimalSerializer.readFields(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.overtime = null;
    } else {
    this.overtime = com.cloudera.sqoop.lib.BigDecimalSerializer.readFields(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.absence_time = null;
    } else {
    this.absence_time = com.cloudera.sqoop.lib.BigDecimalSerializer.readFields(__dataIn);
    }
  }
  public void write(DataOutput __dataOut) throws IOException {
    if (null == this.attuid) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, attuid);
    }
    if (null == this.date) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, date);
    }
    if (null == this.vp_name) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, vp_name);
    }
    if (null == this.week) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, week);
    }
    if (null == this.week_name) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, week_name);
    }
    if (null == this.number_of_computers) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.number_of_computers);
    }
    if (null == this.productive_time_hours) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.productive_time_hours, __dataOut);
    }
    if (null == this.personal_time_hours) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.personal_time_hours, __dataOut);
    }
    if (null == this.idle_lock_time_hours) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.idle_lock_time_hours, __dataOut);
    }
    if (null == this.before_first_task_idle_lock_time_hours) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.before_first_task_idle_lock_time_hours, __dataOut);
    }
    if (null == this.normal_idle_lock_time_hours) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.normal_idle_lock_time_hours, __dataOut);
    }
    if (null == this.after_last_task_idle_lock_time_hours) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.after_last_task_idle_lock_time_hours, __dataOut);
    }
    if (null == this.unclassified_time_hours) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.unclassified_time_hours, __dataOut);
    }
    if (null == this.microsoft_office_time_hours) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.microsoft_office_time_hours, __dataOut);
    }
    if (null == this.microsoft_office_counts) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.microsoft_office_counts);
    }
    if (null == this.outlook_time_hours) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.outlook_time_hours, __dataOut);
    }
    if (null == this.outlook_counts) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.outlook_counts);
    }
    if (null == this.first_asset_time_hours) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.first_asset_time_hours, __dataOut);
    }
    if (null == this.first_asset_counts) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.first_asset_counts);
    }
    if (null == this.adopt_time_hours) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.adopt_time_hours, __dataOut);
    }
    if (null == this.adopt_counts) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.adopt_counts);
    }
    if (null == this.qmessenger_time_hours) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.qmessenger_time_hours, __dataOut);
    }
    if (null == this.qmessenger_counts) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.qmessenger_counts);
    }
    if (null == this.rome_time_hours) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.rome_time_hours, __dataOut);
    }
    if (null == this.rome_counts) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.rome_counts);
    }
    if (null == this.omx_time_hours) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.omx_time_hours, __dataOut);
    }
    if (null == this.omx_counts) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.omx_counts);
    }
    if (null == this.cth_time_hours) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.cth_time_hours, __dataOut);
    }
    if (null == this.cth_counts) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.cth_counts);
    }
    if (null == this.smartchat_time_hours) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.smartchat_time_hours, __dataOut);
    }
    if (null == this.smartchat_counts) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.smartchat_counts);
    }
    if (null == this.att_connect_time_hours) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.att_connect_time_hours, __dataOut);
    }
    if (null == this.att_connect_counts) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.att_connect_counts);
    }
    if (null == this.worklist_time_hours) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.worklist_time_hours, __dataOut);
    }
    if (null == this.worklist_counts) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.worklist_counts);
    }
    if (null == this.iol_time_hours) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.iol_time_hours, __dataOut);
    }
    if (null == this.iol_counts) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.iol_counts);
    }
    if (null == this.ace_db_time_hours) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.ace_db_time_hours, __dataOut);
    }
    if (null == this.ace_db_counts) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.ace_db_counts);
    }
    if (null == this.tirks_time_hours) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.tirks_time_hours, __dataOut);
    }
    if (null == this.tirks_counts) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.tirks_counts);
    }
    if (null == this.core_time_hours) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.core_time_hours, __dataOut);
    }
    if (null == this.support_time_hours) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.support_time_hours, __dataOut);
    }
    if (null == this.exception_time_hours) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.exception_time_hours, __dataOut);
    }
    if (null == this.idle_prevention_app_flag) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.idle_prevention_app_flag);
    }
    if (null == this.total_hours) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.total_hours, __dataOut);
    }
    if (null == this.overtime) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.overtime, __dataOut);
    }
    if (null == this.absence_time) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.absence_time, __dataOut);
    }
  }
  public void write0(DataOutput __dataOut) throws IOException {
    if (null == this.attuid) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, attuid);
    }
    if (null == this.date) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, date);
    }
    if (null == this.vp_name) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, vp_name);
    }
    if (null == this.week) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, week);
    }
    if (null == this.week_name) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, week_name);
    }
    if (null == this.number_of_computers) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.number_of_computers);
    }
    if (null == this.productive_time_hours) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.productive_time_hours, __dataOut);
    }
    if (null == this.personal_time_hours) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.personal_time_hours, __dataOut);
    }
    if (null == this.idle_lock_time_hours) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.idle_lock_time_hours, __dataOut);
    }
    if (null == this.before_first_task_idle_lock_time_hours) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.before_first_task_idle_lock_time_hours, __dataOut);
    }
    if (null == this.normal_idle_lock_time_hours) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.normal_idle_lock_time_hours, __dataOut);
    }
    if (null == this.after_last_task_idle_lock_time_hours) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.after_last_task_idle_lock_time_hours, __dataOut);
    }
    if (null == this.unclassified_time_hours) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.unclassified_time_hours, __dataOut);
    }
    if (null == this.microsoft_office_time_hours) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.microsoft_office_time_hours, __dataOut);
    }
    if (null == this.microsoft_office_counts) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.microsoft_office_counts);
    }
    if (null == this.outlook_time_hours) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.outlook_time_hours, __dataOut);
    }
    if (null == this.outlook_counts) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.outlook_counts);
    }
    if (null == this.first_asset_time_hours) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.first_asset_time_hours, __dataOut);
    }
    if (null == this.first_asset_counts) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.first_asset_counts);
    }
    if (null == this.adopt_time_hours) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.adopt_time_hours, __dataOut);
    }
    if (null == this.adopt_counts) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.adopt_counts);
    }
    if (null == this.qmessenger_time_hours) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.qmessenger_time_hours, __dataOut);
    }
    if (null == this.qmessenger_counts) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.qmessenger_counts);
    }
    if (null == this.rome_time_hours) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.rome_time_hours, __dataOut);
    }
    if (null == this.rome_counts) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.rome_counts);
    }
    if (null == this.omx_time_hours) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.omx_time_hours, __dataOut);
    }
    if (null == this.omx_counts) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.omx_counts);
    }
    if (null == this.cth_time_hours) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.cth_time_hours, __dataOut);
    }
    if (null == this.cth_counts) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.cth_counts);
    }
    if (null == this.smartchat_time_hours) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.smartchat_time_hours, __dataOut);
    }
    if (null == this.smartchat_counts) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.smartchat_counts);
    }
    if (null == this.att_connect_time_hours) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.att_connect_time_hours, __dataOut);
    }
    if (null == this.att_connect_counts) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.att_connect_counts);
    }
    if (null == this.worklist_time_hours) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.worklist_time_hours, __dataOut);
    }
    if (null == this.worklist_counts) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.worklist_counts);
    }
    if (null == this.iol_time_hours) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.iol_time_hours, __dataOut);
    }
    if (null == this.iol_counts) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.iol_counts);
    }
    if (null == this.ace_db_time_hours) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.ace_db_time_hours, __dataOut);
    }
    if (null == this.ace_db_counts) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.ace_db_counts);
    }
    if (null == this.tirks_time_hours) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.tirks_time_hours, __dataOut);
    }
    if (null == this.tirks_counts) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.tirks_counts);
    }
    if (null == this.core_time_hours) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.core_time_hours, __dataOut);
    }
    if (null == this.support_time_hours) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.support_time_hours, __dataOut);
    }
    if (null == this.exception_time_hours) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.exception_time_hours, __dataOut);
    }
    if (null == this.idle_prevention_app_flag) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.idle_prevention_app_flag);
    }
    if (null == this.total_hours) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.total_hours, __dataOut);
    }
    if (null == this.overtime) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.overtime, __dataOut);
    }
    if (null == this.absence_time) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.absence_time, __dataOut);
    }
  }
  private static final DelimiterSet __outputDelimiters = new DelimiterSet((char) 1, (char) 10, (char) 0, (char) 0, false);
  public String toString() {
    return toString(__outputDelimiters, true);
  }
  public String toString(DelimiterSet delimiters) {
    return toString(delimiters, true);
  }
  public String toString(boolean useRecordDelim) {
    return toString(__outputDelimiters, useRecordDelim);
  }
  public String toString(DelimiterSet delimiters, boolean useRecordDelim) {
    StringBuilder __sb = new StringBuilder();
    char fieldDelim = delimiters.getFieldsTerminatedBy();
    __sb.append(FieldFormatter.escapeAndEnclose(attuid==null?"null":attuid, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(date==null?"null":date, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(vp_name==null?"null":vp_name, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(week==null?"null":week, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(week_name==null?"null":week_name, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(number_of_computers==null?"null":"" + number_of_computers, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(productive_time_hours==null?"null":productive_time_hours.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(personal_time_hours==null?"null":personal_time_hours.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(idle_lock_time_hours==null?"null":idle_lock_time_hours.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(before_first_task_idle_lock_time_hours==null?"null":before_first_task_idle_lock_time_hours.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(normal_idle_lock_time_hours==null?"null":normal_idle_lock_time_hours.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(after_last_task_idle_lock_time_hours==null?"null":after_last_task_idle_lock_time_hours.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(unclassified_time_hours==null?"null":unclassified_time_hours.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(microsoft_office_time_hours==null?"null":microsoft_office_time_hours.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(microsoft_office_counts==null?"null":"" + microsoft_office_counts, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(outlook_time_hours==null?"null":outlook_time_hours.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(outlook_counts==null?"null":"" + outlook_counts, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(first_asset_time_hours==null?"null":first_asset_time_hours.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(first_asset_counts==null?"null":"" + first_asset_counts, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(adopt_time_hours==null?"null":adopt_time_hours.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(adopt_counts==null?"null":"" + adopt_counts, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(qmessenger_time_hours==null?"null":qmessenger_time_hours.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(qmessenger_counts==null?"null":"" + qmessenger_counts, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(rome_time_hours==null?"null":rome_time_hours.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(rome_counts==null?"null":"" + rome_counts, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(omx_time_hours==null?"null":omx_time_hours.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(omx_counts==null?"null":"" + omx_counts, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(cth_time_hours==null?"null":cth_time_hours.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(cth_counts==null?"null":"" + cth_counts, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(smartchat_time_hours==null?"null":smartchat_time_hours.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(smartchat_counts==null?"null":"" + smartchat_counts, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(att_connect_time_hours==null?"null":att_connect_time_hours.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(att_connect_counts==null?"null":"" + att_connect_counts, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(worklist_time_hours==null?"null":worklist_time_hours.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(worklist_counts==null?"null":"" + worklist_counts, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(iol_time_hours==null?"null":iol_time_hours.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(iol_counts==null?"null":"" + iol_counts, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(ace_db_time_hours==null?"null":ace_db_time_hours.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(ace_db_counts==null?"null":"" + ace_db_counts, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(tirks_time_hours==null?"null":tirks_time_hours.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(tirks_counts==null?"null":"" + tirks_counts, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(core_time_hours==null?"null":core_time_hours.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(support_time_hours==null?"null":support_time_hours.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(exception_time_hours==null?"null":exception_time_hours.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(idle_prevention_app_flag==null?"null":"" + idle_prevention_app_flag, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(total_hours==null?"null":total_hours.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(overtime==null?"null":overtime.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(absence_time==null?"null":absence_time.toPlainString(), delimiters));
    if (useRecordDelim) {
      __sb.append(delimiters.getLinesTerminatedBy());
    }
    return __sb.toString();
  }
  public void toString0(DelimiterSet delimiters, StringBuilder __sb, char fieldDelim) {
    __sb.append(FieldFormatter.escapeAndEnclose(attuid==null?"null":attuid, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(date==null?"null":date, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(vp_name==null?"null":vp_name, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(week==null?"null":week, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(week_name==null?"null":week_name, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(number_of_computers==null?"null":"" + number_of_computers, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(productive_time_hours==null?"null":productive_time_hours.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(personal_time_hours==null?"null":personal_time_hours.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(idle_lock_time_hours==null?"null":idle_lock_time_hours.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(before_first_task_idle_lock_time_hours==null?"null":before_first_task_idle_lock_time_hours.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(normal_idle_lock_time_hours==null?"null":normal_idle_lock_time_hours.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(after_last_task_idle_lock_time_hours==null?"null":after_last_task_idle_lock_time_hours.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(unclassified_time_hours==null?"null":unclassified_time_hours.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(microsoft_office_time_hours==null?"null":microsoft_office_time_hours.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(microsoft_office_counts==null?"null":"" + microsoft_office_counts, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(outlook_time_hours==null?"null":outlook_time_hours.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(outlook_counts==null?"null":"" + outlook_counts, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(first_asset_time_hours==null?"null":first_asset_time_hours.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(first_asset_counts==null?"null":"" + first_asset_counts, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(adopt_time_hours==null?"null":adopt_time_hours.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(adopt_counts==null?"null":"" + adopt_counts, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(qmessenger_time_hours==null?"null":qmessenger_time_hours.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(qmessenger_counts==null?"null":"" + qmessenger_counts, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(rome_time_hours==null?"null":rome_time_hours.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(rome_counts==null?"null":"" + rome_counts, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(omx_time_hours==null?"null":omx_time_hours.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(omx_counts==null?"null":"" + omx_counts, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(cth_time_hours==null?"null":cth_time_hours.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(cth_counts==null?"null":"" + cth_counts, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(smartchat_time_hours==null?"null":smartchat_time_hours.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(smartchat_counts==null?"null":"" + smartchat_counts, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(att_connect_time_hours==null?"null":att_connect_time_hours.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(att_connect_counts==null?"null":"" + att_connect_counts, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(worklist_time_hours==null?"null":worklist_time_hours.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(worklist_counts==null?"null":"" + worklist_counts, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(iol_time_hours==null?"null":iol_time_hours.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(iol_counts==null?"null":"" + iol_counts, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(ace_db_time_hours==null?"null":ace_db_time_hours.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(ace_db_counts==null?"null":"" + ace_db_counts, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(tirks_time_hours==null?"null":tirks_time_hours.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(tirks_counts==null?"null":"" + tirks_counts, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(core_time_hours==null?"null":core_time_hours.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(support_time_hours==null?"null":support_time_hours.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(exception_time_hours==null?"null":exception_time_hours.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(idle_prevention_app_flag==null?"null":"" + idle_prevention_app_flag, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(total_hours==null?"null":total_hours.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(overtime==null?"null":overtime.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(absence_time==null?"null":absence_time.toPlainString(), delimiters));
  }
  private static final DelimiterSet __inputDelimiters = new DelimiterSet((char) 1, (char) 10, (char) 0, (char) 0, false);
  private RecordParser __parser;
  public void parse(Text __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(CharSequence __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(byte [] __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(char [] __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(ByteBuffer __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(CharBuffer __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  private void __loadFromFields(List<String> fields) {
    Iterator<String> __it = fields.listIterator();
    String __cur_str = null;
    try {
    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.attuid = null; } else {
      this.attuid = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.date = null; } else {
      this.date = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.vp_name = null; } else {
      this.vp_name = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.week = null; } else {
      this.week = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.week_name = null; } else {
      this.week_name = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.number_of_computers = null; } else {
      this.number_of_computers = Integer.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.productive_time_hours = null; } else {
      this.productive_time_hours = new java.math.BigDecimal(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.personal_time_hours = null; } else {
      this.personal_time_hours = new java.math.BigDecimal(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.idle_lock_time_hours = null; } else {
      this.idle_lock_time_hours = new java.math.BigDecimal(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.before_first_task_idle_lock_time_hours = null; } else {
      this.before_first_task_idle_lock_time_hours = new java.math.BigDecimal(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.normal_idle_lock_time_hours = null; } else {
      this.normal_idle_lock_time_hours = new java.math.BigDecimal(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.after_last_task_idle_lock_time_hours = null; } else {
      this.after_last_task_idle_lock_time_hours = new java.math.BigDecimal(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.unclassified_time_hours = null; } else {
      this.unclassified_time_hours = new java.math.BigDecimal(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.microsoft_office_time_hours = null; } else {
      this.microsoft_office_time_hours = new java.math.BigDecimal(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.microsoft_office_counts = null; } else {
      this.microsoft_office_counts = Long.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.outlook_time_hours = null; } else {
      this.outlook_time_hours = new java.math.BigDecimal(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.outlook_counts = null; } else {
      this.outlook_counts = Long.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.first_asset_time_hours = null; } else {
      this.first_asset_time_hours = new java.math.BigDecimal(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.first_asset_counts = null; } else {
      this.first_asset_counts = Long.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.adopt_time_hours = null; } else {
      this.adopt_time_hours = new java.math.BigDecimal(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.adopt_counts = null; } else {
      this.adopt_counts = Long.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.qmessenger_time_hours = null; } else {
      this.qmessenger_time_hours = new java.math.BigDecimal(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.qmessenger_counts = null; } else {
      this.qmessenger_counts = Long.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.rome_time_hours = null; } else {
      this.rome_time_hours = new java.math.BigDecimal(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.rome_counts = null; } else {
      this.rome_counts = Long.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.omx_time_hours = null; } else {
      this.omx_time_hours = new java.math.BigDecimal(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.omx_counts = null; } else {
      this.omx_counts = Long.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.cth_time_hours = null; } else {
      this.cth_time_hours = new java.math.BigDecimal(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.cth_counts = null; } else {
      this.cth_counts = Long.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.smartchat_time_hours = null; } else {
      this.smartchat_time_hours = new java.math.BigDecimal(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.smartchat_counts = null; } else {
      this.smartchat_counts = Long.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.att_connect_time_hours = null; } else {
      this.att_connect_time_hours = new java.math.BigDecimal(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.att_connect_counts = null; } else {
      this.att_connect_counts = Long.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.worklist_time_hours = null; } else {
      this.worklist_time_hours = new java.math.BigDecimal(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.worklist_counts = null; } else {
      this.worklist_counts = Long.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.iol_time_hours = null; } else {
      this.iol_time_hours = new java.math.BigDecimal(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.iol_counts = null; } else {
      this.iol_counts = Long.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.ace_db_time_hours = null; } else {
      this.ace_db_time_hours = new java.math.BigDecimal(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.ace_db_counts = null; } else {
      this.ace_db_counts = Long.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.tirks_time_hours = null; } else {
      this.tirks_time_hours = new java.math.BigDecimal(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.tirks_counts = null; } else {
      this.tirks_counts = Long.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.core_time_hours = null; } else {
      this.core_time_hours = new java.math.BigDecimal(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.support_time_hours = null; } else {
      this.support_time_hours = new java.math.BigDecimal(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.exception_time_hours = null; } else {
      this.exception_time_hours = new java.math.BigDecimal(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.idle_prevention_app_flag = null; } else {
      this.idle_prevention_app_flag = Integer.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.total_hours = null; } else {
      this.total_hours = new java.math.BigDecimal(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.overtime = null; } else {
      this.overtime = new java.math.BigDecimal(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.absence_time = null; } else {
      this.absence_time = new java.math.BigDecimal(__cur_str);
    }

    } catch (RuntimeException e) {    throw new RuntimeException("Can't parse input data: '" + __cur_str + "'", e);    }  }

  private void __loadFromFields0(Iterator<String> __it) {
    String __cur_str = null;
    try {
    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.attuid = null; } else {
      this.attuid = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.date = null; } else {
      this.date = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.vp_name = null; } else {
      this.vp_name = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.week = null; } else {
      this.week = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.week_name = null; } else {
      this.week_name = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.number_of_computers = null; } else {
      this.number_of_computers = Integer.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.productive_time_hours = null; } else {
      this.productive_time_hours = new java.math.BigDecimal(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.personal_time_hours = null; } else {
      this.personal_time_hours = new java.math.BigDecimal(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.idle_lock_time_hours = null; } else {
      this.idle_lock_time_hours = new java.math.BigDecimal(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.before_first_task_idle_lock_time_hours = null; } else {
      this.before_first_task_idle_lock_time_hours = new java.math.BigDecimal(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.normal_idle_lock_time_hours = null; } else {
      this.normal_idle_lock_time_hours = new java.math.BigDecimal(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.after_last_task_idle_lock_time_hours = null; } else {
      this.after_last_task_idle_lock_time_hours = new java.math.BigDecimal(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.unclassified_time_hours = null; } else {
      this.unclassified_time_hours = new java.math.BigDecimal(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.microsoft_office_time_hours = null; } else {
      this.microsoft_office_time_hours = new java.math.BigDecimal(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.microsoft_office_counts = null; } else {
      this.microsoft_office_counts = Long.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.outlook_time_hours = null; } else {
      this.outlook_time_hours = new java.math.BigDecimal(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.outlook_counts = null; } else {
      this.outlook_counts = Long.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.first_asset_time_hours = null; } else {
      this.first_asset_time_hours = new java.math.BigDecimal(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.first_asset_counts = null; } else {
      this.first_asset_counts = Long.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.adopt_time_hours = null; } else {
      this.adopt_time_hours = new java.math.BigDecimal(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.adopt_counts = null; } else {
      this.adopt_counts = Long.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.qmessenger_time_hours = null; } else {
      this.qmessenger_time_hours = new java.math.BigDecimal(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.qmessenger_counts = null; } else {
      this.qmessenger_counts = Long.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.rome_time_hours = null; } else {
      this.rome_time_hours = new java.math.BigDecimal(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.rome_counts = null; } else {
      this.rome_counts = Long.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.omx_time_hours = null; } else {
      this.omx_time_hours = new java.math.BigDecimal(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.omx_counts = null; } else {
      this.omx_counts = Long.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.cth_time_hours = null; } else {
      this.cth_time_hours = new java.math.BigDecimal(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.cth_counts = null; } else {
      this.cth_counts = Long.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.smartchat_time_hours = null; } else {
      this.smartchat_time_hours = new java.math.BigDecimal(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.smartchat_counts = null; } else {
      this.smartchat_counts = Long.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.att_connect_time_hours = null; } else {
      this.att_connect_time_hours = new java.math.BigDecimal(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.att_connect_counts = null; } else {
      this.att_connect_counts = Long.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.worklist_time_hours = null; } else {
      this.worklist_time_hours = new java.math.BigDecimal(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.worklist_counts = null; } else {
      this.worklist_counts = Long.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.iol_time_hours = null; } else {
      this.iol_time_hours = new java.math.BigDecimal(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.iol_counts = null; } else {
      this.iol_counts = Long.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.ace_db_time_hours = null; } else {
      this.ace_db_time_hours = new java.math.BigDecimal(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.ace_db_counts = null; } else {
      this.ace_db_counts = Long.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.tirks_time_hours = null; } else {
      this.tirks_time_hours = new java.math.BigDecimal(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.tirks_counts = null; } else {
      this.tirks_counts = Long.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.core_time_hours = null; } else {
      this.core_time_hours = new java.math.BigDecimal(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.support_time_hours = null; } else {
      this.support_time_hours = new java.math.BigDecimal(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.exception_time_hours = null; } else {
      this.exception_time_hours = new java.math.BigDecimal(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.idle_prevention_app_flag = null; } else {
      this.idle_prevention_app_flag = Integer.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.total_hours = null; } else {
      this.total_hours = new java.math.BigDecimal(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.overtime = null; } else {
      this.overtime = new java.math.BigDecimal(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.absence_time = null; } else {
      this.absence_time = new java.math.BigDecimal(__cur_str);
    }

    } catch (RuntimeException e) {    throw new RuntimeException("Can't parse input data: '" + __cur_str + "'", e);    }  }

  public Object clone() throws CloneNotSupportedException {
    SDA_Stage_dbo_UsageAnalysis o = (SDA_Stage_dbo_UsageAnalysis) super.clone();
    return o;
  }

  public void clone0(SDA_Stage_dbo_UsageAnalysis o) throws CloneNotSupportedException {
  }

  public Map<String, Object> getFieldMap() {
    Map<String, Object> __sqoop$field_map = new TreeMap<String, Object>();
    __sqoop$field_map.put("attuid", this.attuid);
    __sqoop$field_map.put("date", this.date);
    __sqoop$field_map.put("vp_name", this.vp_name);
    __sqoop$field_map.put("week", this.week);
    __sqoop$field_map.put("week_name", this.week_name);
    __sqoop$field_map.put("number_of_computers", this.number_of_computers);
    __sqoop$field_map.put("productive_time_hours", this.productive_time_hours);
    __sqoop$field_map.put("personal_time_hours", this.personal_time_hours);
    __sqoop$field_map.put("idle_lock_time_hours", this.idle_lock_time_hours);
    __sqoop$field_map.put("before_first_task_idle_lock_time_hours", this.before_first_task_idle_lock_time_hours);
    __sqoop$field_map.put("normal_idle_lock_time_hours", this.normal_idle_lock_time_hours);
    __sqoop$field_map.put("after_last_task_idle_lock_time_hours", this.after_last_task_idle_lock_time_hours);
    __sqoop$field_map.put("unclassified_time_hours", this.unclassified_time_hours);
    __sqoop$field_map.put("microsoft_office_time_hours", this.microsoft_office_time_hours);
    __sqoop$field_map.put("microsoft_office_counts", this.microsoft_office_counts);
    __sqoop$field_map.put("outlook_time_hours", this.outlook_time_hours);
    __sqoop$field_map.put("outlook_counts", this.outlook_counts);
    __sqoop$field_map.put("first_asset_time_hours", this.first_asset_time_hours);
    __sqoop$field_map.put("first_asset_counts", this.first_asset_counts);
    __sqoop$field_map.put("adopt_time_hours", this.adopt_time_hours);
    __sqoop$field_map.put("adopt_counts", this.adopt_counts);
    __sqoop$field_map.put("qmessenger_time_hours", this.qmessenger_time_hours);
    __sqoop$field_map.put("qmessenger_counts", this.qmessenger_counts);
    __sqoop$field_map.put("rome_time_hours", this.rome_time_hours);
    __sqoop$field_map.put("rome_counts", this.rome_counts);
    __sqoop$field_map.put("omx_time_hours", this.omx_time_hours);
    __sqoop$field_map.put("omx_counts", this.omx_counts);
    __sqoop$field_map.put("cth_time_hours", this.cth_time_hours);
    __sqoop$field_map.put("cth_counts", this.cth_counts);
    __sqoop$field_map.put("smartchat_time_hours", this.smartchat_time_hours);
    __sqoop$field_map.put("smartchat_counts", this.smartchat_counts);
    __sqoop$field_map.put("att_connect_time_hours", this.att_connect_time_hours);
    __sqoop$field_map.put("att_connect_counts", this.att_connect_counts);
    __sqoop$field_map.put("worklist_time_hours", this.worklist_time_hours);
    __sqoop$field_map.put("worklist_counts", this.worklist_counts);
    __sqoop$field_map.put("iol_time_hours", this.iol_time_hours);
    __sqoop$field_map.put("iol_counts", this.iol_counts);
    __sqoop$field_map.put("ace_db_time_hours", this.ace_db_time_hours);
    __sqoop$field_map.put("ace_db_counts", this.ace_db_counts);
    __sqoop$field_map.put("tirks_time_hours", this.tirks_time_hours);
    __sqoop$field_map.put("tirks_counts", this.tirks_counts);
    __sqoop$field_map.put("core_time_hours", this.core_time_hours);
    __sqoop$field_map.put("support_time_hours", this.support_time_hours);
    __sqoop$field_map.put("exception_time_hours", this.exception_time_hours);
    __sqoop$field_map.put("idle_prevention_app_flag", this.idle_prevention_app_flag);
    __sqoop$field_map.put("total_hours", this.total_hours);
    __sqoop$field_map.put("overtime", this.overtime);
    __sqoop$field_map.put("absence_time", this.absence_time);
    return __sqoop$field_map;
  }

  public void getFieldMap0(Map<String, Object> __sqoop$field_map) {
    __sqoop$field_map.put("attuid", this.attuid);
    __sqoop$field_map.put("date", this.date);
    __sqoop$field_map.put("vp_name", this.vp_name);
    __sqoop$field_map.put("week", this.week);
    __sqoop$field_map.put("week_name", this.week_name);
    __sqoop$field_map.put("number_of_computers", this.number_of_computers);
    __sqoop$field_map.put("productive_time_hours", this.productive_time_hours);
    __sqoop$field_map.put("personal_time_hours", this.personal_time_hours);
    __sqoop$field_map.put("idle_lock_time_hours", this.idle_lock_time_hours);
    __sqoop$field_map.put("before_first_task_idle_lock_time_hours", this.before_first_task_idle_lock_time_hours);
    __sqoop$field_map.put("normal_idle_lock_time_hours", this.normal_idle_lock_time_hours);
    __sqoop$field_map.put("after_last_task_idle_lock_time_hours", this.after_last_task_idle_lock_time_hours);
    __sqoop$field_map.put("unclassified_time_hours", this.unclassified_time_hours);
    __sqoop$field_map.put("microsoft_office_time_hours", this.microsoft_office_time_hours);
    __sqoop$field_map.put("microsoft_office_counts", this.microsoft_office_counts);
    __sqoop$field_map.put("outlook_time_hours", this.outlook_time_hours);
    __sqoop$field_map.put("outlook_counts", this.outlook_counts);
    __sqoop$field_map.put("first_asset_time_hours", this.first_asset_time_hours);
    __sqoop$field_map.put("first_asset_counts", this.first_asset_counts);
    __sqoop$field_map.put("adopt_time_hours", this.adopt_time_hours);
    __sqoop$field_map.put("adopt_counts", this.adopt_counts);
    __sqoop$field_map.put("qmessenger_time_hours", this.qmessenger_time_hours);
    __sqoop$field_map.put("qmessenger_counts", this.qmessenger_counts);
    __sqoop$field_map.put("rome_time_hours", this.rome_time_hours);
    __sqoop$field_map.put("rome_counts", this.rome_counts);
    __sqoop$field_map.put("omx_time_hours", this.omx_time_hours);
    __sqoop$field_map.put("omx_counts", this.omx_counts);
    __sqoop$field_map.put("cth_time_hours", this.cth_time_hours);
    __sqoop$field_map.put("cth_counts", this.cth_counts);
    __sqoop$field_map.put("smartchat_time_hours", this.smartchat_time_hours);
    __sqoop$field_map.put("smartchat_counts", this.smartchat_counts);
    __sqoop$field_map.put("att_connect_time_hours", this.att_connect_time_hours);
    __sqoop$field_map.put("att_connect_counts", this.att_connect_counts);
    __sqoop$field_map.put("worklist_time_hours", this.worklist_time_hours);
    __sqoop$field_map.put("worklist_counts", this.worklist_counts);
    __sqoop$field_map.put("iol_time_hours", this.iol_time_hours);
    __sqoop$field_map.put("iol_counts", this.iol_counts);
    __sqoop$field_map.put("ace_db_time_hours", this.ace_db_time_hours);
    __sqoop$field_map.put("ace_db_counts", this.ace_db_counts);
    __sqoop$field_map.put("tirks_time_hours", this.tirks_time_hours);
    __sqoop$field_map.put("tirks_counts", this.tirks_counts);
    __sqoop$field_map.put("core_time_hours", this.core_time_hours);
    __sqoop$field_map.put("support_time_hours", this.support_time_hours);
    __sqoop$field_map.put("exception_time_hours", this.exception_time_hours);
    __sqoop$field_map.put("idle_prevention_app_flag", this.idle_prevention_app_flag);
    __sqoop$field_map.put("total_hours", this.total_hours);
    __sqoop$field_map.put("overtime", this.overtime);
    __sqoop$field_map.put("absence_time", this.absence_time);
  }

  public void setField(String __fieldName, Object __fieldVal) {
    if ("attuid".equals(__fieldName)) {
      this.attuid = (String) __fieldVal;
    }
    else    if ("date".equals(__fieldName)) {
      this.date = (String) __fieldVal;
    }
    else    if ("vp_name".equals(__fieldName)) {
      this.vp_name = (String) __fieldVal;
    }
    else    if ("week".equals(__fieldName)) {
      this.week = (String) __fieldVal;
    }
    else    if ("week_name".equals(__fieldName)) {
      this.week_name = (String) __fieldVal;
    }
    else    if ("number_of_computers".equals(__fieldName)) {
      this.number_of_computers = (Integer) __fieldVal;
    }
    else    if ("productive_time_hours".equals(__fieldName)) {
      this.productive_time_hours = (java.math.BigDecimal) __fieldVal;
    }
    else    if ("personal_time_hours".equals(__fieldName)) {
      this.personal_time_hours = (java.math.BigDecimal) __fieldVal;
    }
    else    if ("idle_lock_time_hours".equals(__fieldName)) {
      this.idle_lock_time_hours = (java.math.BigDecimal) __fieldVal;
    }
    else    if ("before_first_task_idle_lock_time_hours".equals(__fieldName)) {
      this.before_first_task_idle_lock_time_hours = (java.math.BigDecimal) __fieldVal;
    }
    else    if ("normal_idle_lock_time_hours".equals(__fieldName)) {
      this.normal_idle_lock_time_hours = (java.math.BigDecimal) __fieldVal;
    }
    else    if ("after_last_task_idle_lock_time_hours".equals(__fieldName)) {
      this.after_last_task_idle_lock_time_hours = (java.math.BigDecimal) __fieldVal;
    }
    else    if ("unclassified_time_hours".equals(__fieldName)) {
      this.unclassified_time_hours = (java.math.BigDecimal) __fieldVal;
    }
    else    if ("microsoft_office_time_hours".equals(__fieldName)) {
      this.microsoft_office_time_hours = (java.math.BigDecimal) __fieldVal;
    }
    else    if ("microsoft_office_counts".equals(__fieldName)) {
      this.microsoft_office_counts = (Long) __fieldVal;
    }
    else    if ("outlook_time_hours".equals(__fieldName)) {
      this.outlook_time_hours = (java.math.BigDecimal) __fieldVal;
    }
    else    if ("outlook_counts".equals(__fieldName)) {
      this.outlook_counts = (Long) __fieldVal;
    }
    else    if ("first_asset_time_hours".equals(__fieldName)) {
      this.first_asset_time_hours = (java.math.BigDecimal) __fieldVal;
    }
    else    if ("first_asset_counts".equals(__fieldName)) {
      this.first_asset_counts = (Long) __fieldVal;
    }
    else    if ("adopt_time_hours".equals(__fieldName)) {
      this.adopt_time_hours = (java.math.BigDecimal) __fieldVal;
    }
    else    if ("adopt_counts".equals(__fieldName)) {
      this.adopt_counts = (Long) __fieldVal;
    }
    else    if ("qmessenger_time_hours".equals(__fieldName)) {
      this.qmessenger_time_hours = (java.math.BigDecimal) __fieldVal;
    }
    else    if ("qmessenger_counts".equals(__fieldName)) {
      this.qmessenger_counts = (Long) __fieldVal;
    }
    else    if ("rome_time_hours".equals(__fieldName)) {
      this.rome_time_hours = (java.math.BigDecimal) __fieldVal;
    }
    else    if ("rome_counts".equals(__fieldName)) {
      this.rome_counts = (Long) __fieldVal;
    }
    else    if ("omx_time_hours".equals(__fieldName)) {
      this.omx_time_hours = (java.math.BigDecimal) __fieldVal;
    }
    else    if ("omx_counts".equals(__fieldName)) {
      this.omx_counts = (Long) __fieldVal;
    }
    else    if ("cth_time_hours".equals(__fieldName)) {
      this.cth_time_hours = (java.math.BigDecimal) __fieldVal;
    }
    else    if ("cth_counts".equals(__fieldName)) {
      this.cth_counts = (Long) __fieldVal;
    }
    else    if ("smartchat_time_hours".equals(__fieldName)) {
      this.smartchat_time_hours = (java.math.BigDecimal) __fieldVal;
    }
    else    if ("smartchat_counts".equals(__fieldName)) {
      this.smartchat_counts = (Long) __fieldVal;
    }
    else    if ("att_connect_time_hours".equals(__fieldName)) {
      this.att_connect_time_hours = (java.math.BigDecimal) __fieldVal;
    }
    else    if ("att_connect_counts".equals(__fieldName)) {
      this.att_connect_counts = (Long) __fieldVal;
    }
    else    if ("worklist_time_hours".equals(__fieldName)) {
      this.worklist_time_hours = (java.math.BigDecimal) __fieldVal;
    }
    else    if ("worklist_counts".equals(__fieldName)) {
      this.worklist_counts = (Long) __fieldVal;
    }
    else    if ("iol_time_hours".equals(__fieldName)) {
      this.iol_time_hours = (java.math.BigDecimal) __fieldVal;
    }
    else    if ("iol_counts".equals(__fieldName)) {
      this.iol_counts = (Long) __fieldVal;
    }
    else    if ("ace_db_time_hours".equals(__fieldName)) {
      this.ace_db_time_hours = (java.math.BigDecimal) __fieldVal;
    }
    else    if ("ace_db_counts".equals(__fieldName)) {
      this.ace_db_counts = (Long) __fieldVal;
    }
    else    if ("tirks_time_hours".equals(__fieldName)) {
      this.tirks_time_hours = (java.math.BigDecimal) __fieldVal;
    }
    else    if ("tirks_counts".equals(__fieldName)) {
      this.tirks_counts = (Long) __fieldVal;
    }
    else    if ("core_time_hours".equals(__fieldName)) {
      this.core_time_hours = (java.math.BigDecimal) __fieldVal;
    }
    else    if ("support_time_hours".equals(__fieldName)) {
      this.support_time_hours = (java.math.BigDecimal) __fieldVal;
    }
    else    if ("exception_time_hours".equals(__fieldName)) {
      this.exception_time_hours = (java.math.BigDecimal) __fieldVal;
    }
    else    if ("idle_prevention_app_flag".equals(__fieldName)) {
      this.idle_prevention_app_flag = (Integer) __fieldVal;
    }
    else    if ("total_hours".equals(__fieldName)) {
      this.total_hours = (java.math.BigDecimal) __fieldVal;
    }
    else    if ("overtime".equals(__fieldName)) {
      this.overtime = (java.math.BigDecimal) __fieldVal;
    }
    else    if ("absence_time".equals(__fieldName)) {
      this.absence_time = (java.math.BigDecimal) __fieldVal;
    }
    else {
      throw new RuntimeException("No such field: " + __fieldName);
    }
  }
  public boolean setField0(String __fieldName, Object __fieldVal) {
    if ("attuid".equals(__fieldName)) {
      this.attuid = (String) __fieldVal;
      return true;
    }
    else    if ("date".equals(__fieldName)) {
      this.date = (String) __fieldVal;
      return true;
    }
    else    if ("vp_name".equals(__fieldName)) {
      this.vp_name = (String) __fieldVal;
      return true;
    }
    else    if ("week".equals(__fieldName)) {
      this.week = (String) __fieldVal;
      return true;
    }
    else    if ("week_name".equals(__fieldName)) {
      this.week_name = (String) __fieldVal;
      return true;
    }
    else    if ("number_of_computers".equals(__fieldName)) {
      this.number_of_computers = (Integer) __fieldVal;
      return true;
    }
    else    if ("productive_time_hours".equals(__fieldName)) {
      this.productive_time_hours = (java.math.BigDecimal) __fieldVal;
      return true;
    }
    else    if ("personal_time_hours".equals(__fieldName)) {
      this.personal_time_hours = (java.math.BigDecimal) __fieldVal;
      return true;
    }
    else    if ("idle_lock_time_hours".equals(__fieldName)) {
      this.idle_lock_time_hours = (java.math.BigDecimal) __fieldVal;
      return true;
    }
    else    if ("before_first_task_idle_lock_time_hours".equals(__fieldName)) {
      this.before_first_task_idle_lock_time_hours = (java.math.BigDecimal) __fieldVal;
      return true;
    }
    else    if ("normal_idle_lock_time_hours".equals(__fieldName)) {
      this.normal_idle_lock_time_hours = (java.math.BigDecimal) __fieldVal;
      return true;
    }
    else    if ("after_last_task_idle_lock_time_hours".equals(__fieldName)) {
      this.after_last_task_idle_lock_time_hours = (java.math.BigDecimal) __fieldVal;
      return true;
    }
    else    if ("unclassified_time_hours".equals(__fieldName)) {
      this.unclassified_time_hours = (java.math.BigDecimal) __fieldVal;
      return true;
    }
    else    if ("microsoft_office_time_hours".equals(__fieldName)) {
      this.microsoft_office_time_hours = (java.math.BigDecimal) __fieldVal;
      return true;
    }
    else    if ("microsoft_office_counts".equals(__fieldName)) {
      this.microsoft_office_counts = (Long) __fieldVal;
      return true;
    }
    else    if ("outlook_time_hours".equals(__fieldName)) {
      this.outlook_time_hours = (java.math.BigDecimal) __fieldVal;
      return true;
    }
    else    if ("outlook_counts".equals(__fieldName)) {
      this.outlook_counts = (Long) __fieldVal;
      return true;
    }
    else    if ("first_asset_time_hours".equals(__fieldName)) {
      this.first_asset_time_hours = (java.math.BigDecimal) __fieldVal;
      return true;
    }
    else    if ("first_asset_counts".equals(__fieldName)) {
      this.first_asset_counts = (Long) __fieldVal;
      return true;
    }
    else    if ("adopt_time_hours".equals(__fieldName)) {
      this.adopt_time_hours = (java.math.BigDecimal) __fieldVal;
      return true;
    }
    else    if ("adopt_counts".equals(__fieldName)) {
      this.adopt_counts = (Long) __fieldVal;
      return true;
    }
    else    if ("qmessenger_time_hours".equals(__fieldName)) {
      this.qmessenger_time_hours = (java.math.BigDecimal) __fieldVal;
      return true;
    }
    else    if ("qmessenger_counts".equals(__fieldName)) {
      this.qmessenger_counts = (Long) __fieldVal;
      return true;
    }
    else    if ("rome_time_hours".equals(__fieldName)) {
      this.rome_time_hours = (java.math.BigDecimal) __fieldVal;
      return true;
    }
    else    if ("rome_counts".equals(__fieldName)) {
      this.rome_counts = (Long) __fieldVal;
      return true;
    }
    else    if ("omx_time_hours".equals(__fieldName)) {
      this.omx_time_hours = (java.math.BigDecimal) __fieldVal;
      return true;
    }
    else    if ("omx_counts".equals(__fieldName)) {
      this.omx_counts = (Long) __fieldVal;
      return true;
    }
    else    if ("cth_time_hours".equals(__fieldName)) {
      this.cth_time_hours = (java.math.BigDecimal) __fieldVal;
      return true;
    }
    else    if ("cth_counts".equals(__fieldName)) {
      this.cth_counts = (Long) __fieldVal;
      return true;
    }
    else    if ("smartchat_time_hours".equals(__fieldName)) {
      this.smartchat_time_hours = (java.math.BigDecimal) __fieldVal;
      return true;
    }
    else    if ("smartchat_counts".equals(__fieldName)) {
      this.smartchat_counts = (Long) __fieldVal;
      return true;
    }
    else    if ("att_connect_time_hours".equals(__fieldName)) {
      this.att_connect_time_hours = (java.math.BigDecimal) __fieldVal;
      return true;
    }
    else    if ("att_connect_counts".equals(__fieldName)) {
      this.att_connect_counts = (Long) __fieldVal;
      return true;
    }
    else    if ("worklist_time_hours".equals(__fieldName)) {
      this.worklist_time_hours = (java.math.BigDecimal) __fieldVal;
      return true;
    }
    else    if ("worklist_counts".equals(__fieldName)) {
      this.worklist_counts = (Long) __fieldVal;
      return true;
    }
    else    if ("iol_time_hours".equals(__fieldName)) {
      this.iol_time_hours = (java.math.BigDecimal) __fieldVal;
      return true;
    }
    else    if ("iol_counts".equals(__fieldName)) {
      this.iol_counts = (Long) __fieldVal;
      return true;
    }
    else    if ("ace_db_time_hours".equals(__fieldName)) {
      this.ace_db_time_hours = (java.math.BigDecimal) __fieldVal;
      return true;
    }
    else    if ("ace_db_counts".equals(__fieldName)) {
      this.ace_db_counts = (Long) __fieldVal;
      return true;
    }
    else    if ("tirks_time_hours".equals(__fieldName)) {
      this.tirks_time_hours = (java.math.BigDecimal) __fieldVal;
      return true;
    }
    else    if ("tirks_counts".equals(__fieldName)) {
      this.tirks_counts = (Long) __fieldVal;
      return true;
    }
    else    if ("core_time_hours".equals(__fieldName)) {
      this.core_time_hours = (java.math.BigDecimal) __fieldVal;
      return true;
    }
    else    if ("support_time_hours".equals(__fieldName)) {
      this.support_time_hours = (java.math.BigDecimal) __fieldVal;
      return true;
    }
    else    if ("exception_time_hours".equals(__fieldName)) {
      this.exception_time_hours = (java.math.BigDecimal) __fieldVal;
      return true;
    }
    else    if ("idle_prevention_app_flag".equals(__fieldName)) {
      this.idle_prevention_app_flag = (Integer) __fieldVal;
      return true;
    }
    else    if ("total_hours".equals(__fieldName)) {
      this.total_hours = (java.math.BigDecimal) __fieldVal;
      return true;
    }
    else    if ("overtime".equals(__fieldName)) {
      this.overtime = (java.math.BigDecimal) __fieldVal;
      return true;
    }
    else    if ("absence_time".equals(__fieldName)) {
      this.absence_time = (java.math.BigDecimal) __fieldVal;
      return true;
    }
    else {
      return false;    }
  }
}
