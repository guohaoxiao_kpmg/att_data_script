// ORM class for table 'SDA_Stage.dbo.TOPAPPS'
// WARNING: This class is AUTO-GENERATED. Modify at your own risk.
//
// Debug information:
// Generated date: Wed May 25 13:35:42 EDT 2016
// For connector: org.apache.sqoop.manager.GenericJdbcManager
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapred.lib.db.DBWritable;
import com.cloudera.sqoop.lib.JdbcWritableBridge;
import com.cloudera.sqoop.lib.DelimiterSet;
import com.cloudera.sqoop.lib.FieldFormatter;
import com.cloudera.sqoop.lib.RecordParser;
import com.cloudera.sqoop.lib.BooleanParser;
import com.cloudera.sqoop.lib.BlobRef;
import com.cloudera.sqoop.lib.ClobRef;
import com.cloudera.sqoop.lib.LargeObjectLoader;
import com.cloudera.sqoop.lib.SqoopRecord;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class SDA_Stage_dbo_TOPAPPS extends SqoopRecord  implements DBWritable, Writable {
  private final int PROTOCOL_VERSION = 3;
  public int getClassFormatVersion() { return PROTOCOL_VERSION; }
  protected ResultSet __cur_result_set;
  private String attuid;
  public String get_attuid() {
    return attuid;
  }
  public void set_attuid(String attuid) {
    this.attuid = attuid;
  }
  public SDA_Stage_dbo_TOPAPPS with_attuid(String attuid) {
    this.attuid = attuid;
    return this;
  }
  private String active_window_start_date;
  public String get_active_window_start_date() {
    return active_window_start_date;
  }
  public void set_active_window_start_date(String active_window_start_date) {
    this.active_window_start_date = active_window_start_date;
  }
  public SDA_Stage_dbo_TOPAPPS with_active_window_start_date(String active_window_start_date) {
    this.active_window_start_date = active_window_start_date;
    return this;
  }
  private String classification;
  public String get_classification() {
    return classification;
  }
  public void set_classification(String classification) {
    this.classification = classification;
  }
  public SDA_Stage_dbo_TOPAPPS with_classification(String classification) {
    this.classification = classification;
    return this;
  }
  private String sub_classification;
  public String get_sub_classification() {
    return sub_classification;
  }
  public void set_sub_classification(String sub_classification) {
    this.sub_classification = sub_classification;
  }
  public SDA_Stage_dbo_TOPAPPS with_sub_classification(String sub_classification) {
    this.sub_classification = sub_classification;
    return this;
  }
  private String sub_classification_name;
  public String get_sub_classification_name() {
    return sub_classification_name;
  }
  public void set_sub_classification_name(String sub_classification_name) {
    this.sub_classification_name = sub_classification_name;
  }
  public SDA_Stage_dbo_TOPAPPS with_sub_classification_name(String sub_classification_name) {
    this.sub_classification_name = sub_classification_name;
    return this;
  }
  private String app_name;
  public String get_app_name() {
    return app_name;
  }
  public void set_app_name(String app_name) {
    this.app_name = app_name;
  }
  public SDA_Stage_dbo_TOPAPPS with_app_name(String app_name) {
    this.app_name = app_name;
    return this;
  }
  private Integer rank;
  public Integer get_rank() {
    return rank;
  }
  public void set_rank(Integer rank) {
    this.rank = rank;
  }
  public SDA_Stage_dbo_TOPAPPS with_rank(Integer rank) {
    this.rank = rank;
    return this;
  }
  private Long sub_classification_name_count;
  public Long get_sub_classification_name_count() {
    return sub_classification_name_count;
  }
  public void set_sub_classification_name_count(Long sub_classification_name_count) {
    this.sub_classification_name_count = sub_classification_name_count;
  }
  public SDA_Stage_dbo_TOPAPPS with_sub_classification_name_count(Long sub_classification_name_count) {
    this.sub_classification_name_count = sub_classification_name_count;
    return this;
  }
  private java.math.BigDecimal sub_classification_name_total_duration;
  public java.math.BigDecimal get_sub_classification_name_total_duration() {
    return sub_classification_name_total_duration;
  }
  public void set_sub_classification_name_total_duration(java.math.BigDecimal sub_classification_name_total_duration) {
    this.sub_classification_name_total_duration = sub_classification_name_total_duration;
  }
  public SDA_Stage_dbo_TOPAPPS with_sub_classification_name_total_duration(java.math.BigDecimal sub_classification_name_total_duration) {
    this.sub_classification_name_total_duration = sub_classification_name_total_duration;
    return this;
  }
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof SDA_Stage_dbo_TOPAPPS)) {
      return false;
    }
    SDA_Stage_dbo_TOPAPPS that = (SDA_Stage_dbo_TOPAPPS) o;
    boolean equal = true;
    equal = equal && (this.attuid == null ? that.attuid == null : this.attuid.equals(that.attuid));
    equal = equal && (this.active_window_start_date == null ? that.active_window_start_date == null : this.active_window_start_date.equals(that.active_window_start_date));
    equal = equal && (this.classification == null ? that.classification == null : this.classification.equals(that.classification));
    equal = equal && (this.sub_classification == null ? that.sub_classification == null : this.sub_classification.equals(that.sub_classification));
    equal = equal && (this.sub_classification_name == null ? that.sub_classification_name == null : this.sub_classification_name.equals(that.sub_classification_name));
    equal = equal && (this.app_name == null ? that.app_name == null : this.app_name.equals(that.app_name));
    equal = equal && (this.rank == null ? that.rank == null : this.rank.equals(that.rank));
    equal = equal && (this.sub_classification_name_count == null ? that.sub_classification_name_count == null : this.sub_classification_name_count.equals(that.sub_classification_name_count));
    equal = equal && (this.sub_classification_name_total_duration == null ? that.sub_classification_name_total_duration == null : this.sub_classification_name_total_duration.equals(that.sub_classification_name_total_duration));
    return equal;
  }
  public boolean equals0(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof SDA_Stage_dbo_TOPAPPS)) {
      return false;
    }
    SDA_Stage_dbo_TOPAPPS that = (SDA_Stage_dbo_TOPAPPS) o;
    boolean equal = true;
    equal = equal && (this.attuid == null ? that.attuid == null : this.attuid.equals(that.attuid));
    equal = equal && (this.active_window_start_date == null ? that.active_window_start_date == null : this.active_window_start_date.equals(that.active_window_start_date));
    equal = equal && (this.classification == null ? that.classification == null : this.classification.equals(that.classification));
    equal = equal && (this.sub_classification == null ? that.sub_classification == null : this.sub_classification.equals(that.sub_classification));
    equal = equal && (this.sub_classification_name == null ? that.sub_classification_name == null : this.sub_classification_name.equals(that.sub_classification_name));
    equal = equal && (this.app_name == null ? that.app_name == null : this.app_name.equals(that.app_name));
    equal = equal && (this.rank == null ? that.rank == null : this.rank.equals(that.rank));
    equal = equal && (this.sub_classification_name_count == null ? that.sub_classification_name_count == null : this.sub_classification_name_count.equals(that.sub_classification_name_count));
    equal = equal && (this.sub_classification_name_total_duration == null ? that.sub_classification_name_total_duration == null : this.sub_classification_name_total_duration.equals(that.sub_classification_name_total_duration));
    return equal;
  }
  public void readFields(ResultSet __dbResults) throws SQLException {
    this.__cur_result_set = __dbResults;
    this.attuid = JdbcWritableBridge.readString(1, __dbResults);
    this.active_window_start_date = JdbcWritableBridge.readString(2, __dbResults);
    this.classification = JdbcWritableBridge.readString(3, __dbResults);
    this.sub_classification = JdbcWritableBridge.readString(4, __dbResults);
    this.sub_classification_name = JdbcWritableBridge.readString(5, __dbResults);
    this.app_name = JdbcWritableBridge.readString(6, __dbResults);
    this.rank = JdbcWritableBridge.readInteger(7, __dbResults);
    this.sub_classification_name_count = JdbcWritableBridge.readLong(8, __dbResults);
    this.sub_classification_name_total_duration = JdbcWritableBridge.readBigDecimal(9, __dbResults);
  }
  public void readFields0(ResultSet __dbResults) throws SQLException {
    this.attuid = JdbcWritableBridge.readString(1, __dbResults);
    this.active_window_start_date = JdbcWritableBridge.readString(2, __dbResults);
    this.classification = JdbcWritableBridge.readString(3, __dbResults);
    this.sub_classification = JdbcWritableBridge.readString(4, __dbResults);
    this.sub_classification_name = JdbcWritableBridge.readString(5, __dbResults);
    this.app_name = JdbcWritableBridge.readString(6, __dbResults);
    this.rank = JdbcWritableBridge.readInteger(7, __dbResults);
    this.sub_classification_name_count = JdbcWritableBridge.readLong(8, __dbResults);
    this.sub_classification_name_total_duration = JdbcWritableBridge.readBigDecimal(9, __dbResults);
  }
  public void loadLargeObjects(LargeObjectLoader __loader)
      throws SQLException, IOException, InterruptedException {
  }
  public void loadLargeObjects0(LargeObjectLoader __loader)
      throws SQLException, IOException, InterruptedException {
  }
  public void write(PreparedStatement __dbStmt) throws SQLException {
    write(__dbStmt, 0);
  }

  public int write(PreparedStatement __dbStmt, int __off) throws SQLException {
    JdbcWritableBridge.writeString(attuid, 1 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(active_window_start_date, 2 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(classification, 3 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(sub_classification, 4 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(sub_classification_name, 5 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(app_name, 6 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeInteger(rank, 7 + __off, 4, __dbStmt);
    JdbcWritableBridge.writeLong(sub_classification_name_count, 8 + __off, -5, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(sub_classification_name_total_duration, 9 + __off, 2, __dbStmt);
    return 9;
  }
  public void write0(PreparedStatement __dbStmt, int __off) throws SQLException {
    JdbcWritableBridge.writeString(attuid, 1 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(active_window_start_date, 2 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(classification, 3 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(sub_classification, 4 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(sub_classification_name, 5 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(app_name, 6 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeInteger(rank, 7 + __off, 4, __dbStmt);
    JdbcWritableBridge.writeLong(sub_classification_name_count, 8 + __off, -5, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(sub_classification_name_total_duration, 9 + __off, 2, __dbStmt);
  }
  public void readFields(DataInput __dataIn) throws IOException {
this.readFields0(__dataIn);  }
  public void readFields0(DataInput __dataIn) throws IOException {
    if (__dataIn.readBoolean()) { 
        this.attuid = null;
    } else {
    this.attuid = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.active_window_start_date = null;
    } else {
    this.active_window_start_date = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.classification = null;
    } else {
    this.classification = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.sub_classification = null;
    } else {
    this.sub_classification = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.sub_classification_name = null;
    } else {
    this.sub_classification_name = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.app_name = null;
    } else {
    this.app_name = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.rank = null;
    } else {
    this.rank = Integer.valueOf(__dataIn.readInt());
    }
    if (__dataIn.readBoolean()) { 
        this.sub_classification_name_count = null;
    } else {
    this.sub_classification_name_count = Long.valueOf(__dataIn.readLong());
    }
    if (__dataIn.readBoolean()) { 
        this.sub_classification_name_total_duration = null;
    } else {
    this.sub_classification_name_total_duration = com.cloudera.sqoop.lib.BigDecimalSerializer.readFields(__dataIn);
    }
  }
  public void write(DataOutput __dataOut) throws IOException {
    if (null == this.attuid) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, attuid);
    }
    if (null == this.active_window_start_date) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, active_window_start_date);
    }
    if (null == this.classification) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, classification);
    }
    if (null == this.sub_classification) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, sub_classification);
    }
    if (null == this.sub_classification_name) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, sub_classification_name);
    }
    if (null == this.app_name) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, app_name);
    }
    if (null == this.rank) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.rank);
    }
    if (null == this.sub_classification_name_count) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.sub_classification_name_count);
    }
    if (null == this.sub_classification_name_total_duration) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.sub_classification_name_total_duration, __dataOut);
    }
  }
  public void write0(DataOutput __dataOut) throws IOException {
    if (null == this.attuid) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, attuid);
    }
    if (null == this.active_window_start_date) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, active_window_start_date);
    }
    if (null == this.classification) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, classification);
    }
    if (null == this.sub_classification) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, sub_classification);
    }
    if (null == this.sub_classification_name) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, sub_classification_name);
    }
    if (null == this.app_name) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, app_name);
    }
    if (null == this.rank) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.rank);
    }
    if (null == this.sub_classification_name_count) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.sub_classification_name_count);
    }
    if (null == this.sub_classification_name_total_duration) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    com.cloudera.sqoop.lib.BigDecimalSerializer.write(this.sub_classification_name_total_duration, __dataOut);
    }
  }
  private static final DelimiterSet __outputDelimiters = new DelimiterSet((char) 1, (char) 10, (char) 0, (char) 0, false);
  public String toString() {
    return toString(__outputDelimiters, true);
  }
  public String toString(DelimiterSet delimiters) {
    return toString(delimiters, true);
  }
  public String toString(boolean useRecordDelim) {
    return toString(__outputDelimiters, useRecordDelim);
  }
  public String toString(DelimiterSet delimiters, boolean useRecordDelim) {
    StringBuilder __sb = new StringBuilder();
    char fieldDelim = delimiters.getFieldsTerminatedBy();
    __sb.append(FieldFormatter.escapeAndEnclose(attuid==null?"null":attuid, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(active_window_start_date==null?"null":active_window_start_date, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(classification==null?"null":classification, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(sub_classification==null?"null":sub_classification, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(sub_classification_name==null?"null":sub_classification_name, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(app_name==null?"null":app_name, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(rank==null?"null":"" + rank, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(sub_classification_name_count==null?"null":"" + sub_classification_name_count, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(sub_classification_name_total_duration==null?"null":sub_classification_name_total_duration.toPlainString(), delimiters));
    if (useRecordDelim) {
      __sb.append(delimiters.getLinesTerminatedBy());
    }
    return __sb.toString();
  }
  public void toString0(DelimiterSet delimiters, StringBuilder __sb, char fieldDelim) {
    __sb.append(FieldFormatter.escapeAndEnclose(attuid==null?"null":attuid, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(active_window_start_date==null?"null":active_window_start_date, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(classification==null?"null":classification, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(sub_classification==null?"null":sub_classification, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(sub_classification_name==null?"null":sub_classification_name, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(app_name==null?"null":app_name, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(rank==null?"null":"" + rank, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(sub_classification_name_count==null?"null":"" + sub_classification_name_count, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(sub_classification_name_total_duration==null?"null":sub_classification_name_total_duration.toPlainString(), delimiters));
  }
  private static final DelimiterSet __inputDelimiters = new DelimiterSet((char) 1, (char) 10, (char) 0, (char) 0, false);
  private RecordParser __parser;
  public void parse(Text __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(CharSequence __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(byte [] __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(char [] __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(ByteBuffer __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(CharBuffer __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  private void __loadFromFields(List<String> fields) {
    Iterator<String> __it = fields.listIterator();
    String __cur_str = null;
    try {
    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.attuid = null; } else {
      this.attuid = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.active_window_start_date = null; } else {
      this.active_window_start_date = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.classification = null; } else {
      this.classification = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.sub_classification = null; } else {
      this.sub_classification = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.sub_classification_name = null; } else {
      this.sub_classification_name = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.app_name = null; } else {
      this.app_name = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.rank = null; } else {
      this.rank = Integer.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.sub_classification_name_count = null; } else {
      this.sub_classification_name_count = Long.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.sub_classification_name_total_duration = null; } else {
      this.sub_classification_name_total_duration = new java.math.BigDecimal(__cur_str);
    }

    } catch (RuntimeException e) {    throw new RuntimeException("Can't parse input data: '" + __cur_str + "'", e);    }  }

  private void __loadFromFields0(Iterator<String> __it) {
    String __cur_str = null;
    try {
    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.attuid = null; } else {
      this.attuid = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.active_window_start_date = null; } else {
      this.active_window_start_date = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.classification = null; } else {
      this.classification = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.sub_classification = null; } else {
      this.sub_classification = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.sub_classification_name = null; } else {
      this.sub_classification_name = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N")) { this.app_name = null; } else {
      this.app_name = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.rank = null; } else {
      this.rank = Integer.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.sub_classification_name_count = null; } else {
      this.sub_classification_name_count = Long.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("\\N") || __cur_str.length() == 0) { this.sub_classification_name_total_duration = null; } else {
      this.sub_classification_name_total_duration = new java.math.BigDecimal(__cur_str);
    }

    } catch (RuntimeException e) {    throw new RuntimeException("Can't parse input data: '" + __cur_str + "'", e);    }  }

  public Object clone() throws CloneNotSupportedException {
    SDA_Stage_dbo_TOPAPPS o = (SDA_Stage_dbo_TOPAPPS) super.clone();
    return o;
  }

  public void clone0(SDA_Stage_dbo_TOPAPPS o) throws CloneNotSupportedException {
  }

  public Map<String, Object> getFieldMap() {
    Map<String, Object> __sqoop$field_map = new TreeMap<String, Object>();
    __sqoop$field_map.put("attuid", this.attuid);
    __sqoop$field_map.put("active_window_start_date", this.active_window_start_date);
    __sqoop$field_map.put("classification", this.classification);
    __sqoop$field_map.put("sub_classification", this.sub_classification);
    __sqoop$field_map.put("sub_classification_name", this.sub_classification_name);
    __sqoop$field_map.put("app_name", this.app_name);
    __sqoop$field_map.put("rank", this.rank);
    __sqoop$field_map.put("sub_classification_name_count", this.sub_classification_name_count);
    __sqoop$field_map.put("sub_classification_name_total_duration", this.sub_classification_name_total_duration);
    return __sqoop$field_map;
  }

  public void getFieldMap0(Map<String, Object> __sqoop$field_map) {
    __sqoop$field_map.put("attuid", this.attuid);
    __sqoop$field_map.put("active_window_start_date", this.active_window_start_date);
    __sqoop$field_map.put("classification", this.classification);
    __sqoop$field_map.put("sub_classification", this.sub_classification);
    __sqoop$field_map.put("sub_classification_name", this.sub_classification_name);
    __sqoop$field_map.put("app_name", this.app_name);
    __sqoop$field_map.put("rank", this.rank);
    __sqoop$field_map.put("sub_classification_name_count", this.sub_classification_name_count);
    __sqoop$field_map.put("sub_classification_name_total_duration", this.sub_classification_name_total_duration);
  }

  public void setField(String __fieldName, Object __fieldVal) {
    if ("attuid".equals(__fieldName)) {
      this.attuid = (String) __fieldVal;
    }
    else    if ("active_window_start_date".equals(__fieldName)) {
      this.active_window_start_date = (String) __fieldVal;
    }
    else    if ("classification".equals(__fieldName)) {
      this.classification = (String) __fieldVal;
    }
    else    if ("sub_classification".equals(__fieldName)) {
      this.sub_classification = (String) __fieldVal;
    }
    else    if ("sub_classification_name".equals(__fieldName)) {
      this.sub_classification_name = (String) __fieldVal;
    }
    else    if ("app_name".equals(__fieldName)) {
      this.app_name = (String) __fieldVal;
    }
    else    if ("rank".equals(__fieldName)) {
      this.rank = (Integer) __fieldVal;
    }
    else    if ("sub_classification_name_count".equals(__fieldName)) {
      this.sub_classification_name_count = (Long) __fieldVal;
    }
    else    if ("sub_classification_name_total_duration".equals(__fieldName)) {
      this.sub_classification_name_total_duration = (java.math.BigDecimal) __fieldVal;
    }
    else {
      throw new RuntimeException("No such field: " + __fieldName);
    }
  }
  public boolean setField0(String __fieldName, Object __fieldVal) {
    if ("attuid".equals(__fieldName)) {
      this.attuid = (String) __fieldVal;
      return true;
    }
    else    if ("active_window_start_date".equals(__fieldName)) {
      this.active_window_start_date = (String) __fieldVal;
      return true;
    }
    else    if ("classification".equals(__fieldName)) {
      this.classification = (String) __fieldVal;
      return true;
    }
    else    if ("sub_classification".equals(__fieldName)) {
      this.sub_classification = (String) __fieldVal;
      return true;
    }
    else    if ("sub_classification_name".equals(__fieldName)) {
      this.sub_classification_name = (String) __fieldVal;
      return true;
    }
    else    if ("app_name".equals(__fieldName)) {
      this.app_name = (String) __fieldVal;
      return true;
    }
    else    if ("rank".equals(__fieldName)) {
      this.rank = (Integer) __fieldVal;
      return true;
    }
    else    if ("sub_classification_name_count".equals(__fieldName)) {
      this.sub_classification_name_count = (Long) __fieldVal;
      return true;
    }
    else    if ("sub_classification_name_total_duration".equals(__fieldName)) {
      this.sub_classification_name_total_duration = (java.math.BigDecimal) __fieldVal;
      return true;
    }
    else {
      return false;    }
  }
}
