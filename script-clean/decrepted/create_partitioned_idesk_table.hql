set hive.exec.dynamic.partition.mode=nonstrict;
set hive.exec.dynamic.partition=true;
set hive.exec.max.dynamic.partitions.pernode=300;
set hive.exec.max.dynamic.partitions =10000000;
set hive.optimize.sort.dynamic.partition=true;

set fs.file.impl.disable.cache=false;
set fs.hdfs.impl.disable.cache=false;


insert into table kpmg.idesk_log_detail
PARTITION(active_window_start_date)
select *,to_date(active_window_start_timetamp) as active_window_start_date
from kpmg.raw_idesk_log_new
where active_window_start_timetamp is not null;
